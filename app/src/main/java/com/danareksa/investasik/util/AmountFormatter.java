package com.danareksa.investasik.util;

import android.net.ParseException;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Fajar on 5/11/2015.
 */
public class AmountFormatter {

    public static String formatWithoutIdr(double amount){
        String amountOnString = String.format("%1$,.2f", amount);
        amountOnString = amountOnString.replaceAll(",",".");
        StringBuilder formatedDecimalPlaces = new StringBuilder(amountOnString);
        formatedDecimalPlaces.setCharAt(amountOnString.length() - 3, ',');
        return formatedDecimalPlaces.toString();
    }

    public static String format(double amount){
        String amountOnString = "IDR" + String.format("%1$,.2f", amount); //Rp
        amountOnString = amountOnString.replaceAll(",",".");
        StringBuilder formatedDecimalPlaces = new StringBuilder(amountOnString);
        formatedDecimalPlaces.setCharAt(amountOnString.length() - 3, ',');
        return formatedDecimalPlaces.toString();
    }

    public static String format(String amount){
        try {
            double amountOnDouble = Double.parseDouble(amount);
            String amountOnString = "IDR" + String.format("%1$,.2f", amountOnDouble); //Rp
            amountOnString = amountOnString.replaceAll(",",".");
            StringBuilder formatedDecimalPlaces = new StringBuilder(amountOnString);
            formatedDecimalPlaces.setCharAt(amountOnString.length() - 3, ',');
            return formatedDecimalPlaces.toString();
        } catch (ParseException e) {
            return amount;
        }
    }

    public static String formatNonCurrency(double amount){
        return new DecimalFormat("#,###,###", new DecimalFormatSymbols(Locale.GERMAN)).format(amount);
    }


    public static String formatUsd(double value){
        String amountOnString = "USD " + String.format("%1$,.2f", value); //Rp
        amountOnString = amountOnString.replaceAll(",",".");
        StringBuilder formatedDecimalPlaces = new StringBuilder(amountOnString);
        formatedDecimalPlaces.setCharAt(amountOnString.length() - 3, ',');
        return formatedDecimalPlaces.toString();
    }

    public static String formatUsd(String amount){

        try {
            double amountOnDouble = Double.parseDouble(amount);
            String amountOnString = "USD " + String.format("%1$,.2f", amountOnDouble); //Rp
            amountOnString = amountOnString.replaceAll(",",".");
            StringBuilder formatedDecimalPlaces = new StringBuilder(amountOnString);
            formatedDecimalPlaces.setCharAt(amountOnString.length() - 3, ',');
            return formatedDecimalPlaces.toString();
        } catch (ParseException e) {
            return amount;
        }
    }



    public static String formatFeePercent(double amount){
        String result = ""; double nom = 0;
        nom = amount * 100;
        System.out.println("nom : " + nom + " amount : " + amount);
        NumberFormat defaultFormat = NumberFormat.getPercentInstance();
        if(nom %2==0 || (nom+1)%2==0){
            defaultFormat.setMinimumFractionDigits(0); //bilangan bulat
        }else{
            defaultFormat.setMinimumFractionDigits(2); //bilangan decimal
        }
        result = defaultFormat.format(amount);
        result = result.replace(".",",");
        return result;
    }


    public static String formatCurrencyWithoutComma(double amount){
        String result = "";
        DecimalFormat decimalFormat = new DecimalFormat("#,###,###", new DecimalFormatSymbols(Locale.GERMAN));
        result = "IDR" + decimalFormat.format(amount);
        return  result;
    }


    public static String formatIdrWithoutComma(double amount){
        String result = "";
        DecimalFormat decimalFormat = new DecimalFormat("#,###,###", new DecimalFormatSymbols(Locale.GERMAN));
        result = "IDR "  + decimalFormat.format(amount);
        return  result;
    }



    public static String formatUnitWithComma(double amount){
        String result = "";
        DecimalFormat decimalFormat = new DecimalFormat("#,###,###", new DecimalFormatSymbols(Locale.GERMAN));
        result = decimalFormat.format(amount);
        return  result;
    }

    public static String formatPercent(double amount){
        String result = "";
        DecimalFormat decimalFormat = new DecimalFormat("#,###,###", new DecimalFormatSymbols(Locale.GERMAN));

        if(amount %2==0 || (amount+1)%2==0){
            decimalFormat.setMinimumFractionDigits(0); //bilangan bulat
        }else{
            decimalFormat.setMinimumFractionDigits(2); //bilangan decimal
        }
        result = decimalFormat.format(amount);
        return result + "%";
    }


    public static String formatUnitWithComma4(double amount){
        String result = "";
        DecimalFormat decimalFormat = new DecimalFormat("#,###,###", new DecimalFormatSymbols(Locale.GERMAN));

        if(amount %2==0 || (amount+1)%2==0){
            decimalFormat.setMinimumFractionDigits(0); //bilangan bulat
        }else{
            decimalFormat.setMinimumFractionDigits(4); //bilangan decimal
        }
        result = decimalFormat.format(amount);
        return result;
    }


    public static String formatUnitComma4(double amount){
        String result = "";
        DecimalFormat decimalFormat = new DecimalFormat("#,###,###", new DecimalFormatSymbols(Locale.GERMAN));
        decimalFormat.setMinimumFractionDigits(4); //bilangan decimal
        result = decimalFormat.format(amount);
        return result;
    }


}
