package com.danareksa.investasik.util;

/**
 * Created by fajarfatur on 2/17/16.
 */

public class Constant {

    /*RESPONSE CODE*/

    public static final int INVALID_TOKEN = 100;

    /**
     * USER STATUS
     */

    public static final String USER_STATUS_REGISTER = "REG";
    public static final String USER_STATUS_ACTIVE = "ACT";
    public static final String USER_STATUS_PENDING = "PEN";
    public static final String USER_STATUS_VERIFIED = "VER";


    public static final String CODE_PAYMENT_TYPE_MANDIRI_CLICKPAY = "MNDC";
    public static final String CODE_PAYMENT_TYPE_BCA_KLIKPAY = "BCAK";
    public static final String CODE_PAYMENT_TYPE_VA_PERMATA = "PRMT";
    public static final String CODE_PAYMENT_TYPE_TRANS_MANDIRI = "MNDU";
    public static final String CODE_PAYMENT_TYPE_TRANS_BCA = "BCAU";



    /**
     * KYC LOOKUP CATEGORY
     */


    //dana reksa
    public static final String KYC_CAT_ANNUAL_INCOME = "ANNUAL_INCOME";
    public static final String KYC_CAT_BENEFICIARY_TYPE = "BENEFICIARY_TYPE";
    public static final String KYC_CAT_CITIZENSHIP = "CITIZENSHIP";
    public static final String KYC_CAT_EDUCATION_BACKGROUND = "EDUCATION_BACKGROUND";
    public static final String KYC_CAT_GENDER = "GENDER";
    public static final String KYC_CAT_ID_TYPE = "ID_TYPE";
    public static final String KYC_CAT_INVESTMENT_PURPOSE = "INVESTMENT_PURPOSE";
    public static final String KYC_CAT_MARITAL_STATUS = "MARITAL_STATUS";
    public static final String KYC_CAT_SOURCE_OF_INCOME = "SOURCE_OF_INCOME";
    public static final String KYC_CAT_RELIGION = "RELIGION";
    public static final String KYC_CAT_OCCUPATION = "OCCUPATION";
    public static final String KYC_CAT_STATEMENT_TYPE = "STATEMENT_TYPE";
    public static final String KYC_CAT_RELATIONSHIP = "RELATIONSHIP";
    public static final String KYC_CAT_RELATIONSHIP_HEIR = "RELATIONSHIP_HEIR";
    public static final String KYC_CAT_INVESTMENT_GOAL = "INVESTMENT_GOAL";
    public static final String KYC_CAT_REGULER_PERIOD = "REGULER_PERIOD";


    public static final String BENEFICIARY_TYPE_OTHER = "OTH";
    public static final String BENEFICIARY_TYPE_SELF = "SLF";
    public static final String ID_TYPE_KTP = "IDC";
    public static final String ID_TYPE_PAS = "PSP";
    public static final String CITIZENSHIP_INDO = "DOM";
    public static final String CITIZENSHIP_LUAR = "FRG";

    public static final String mailingAddressTypeKtp = "1";
    public static final String mailingAddressTypeOffice = "2";
    public static final String mailingAddressTypeLain = "3";

    public static final String SOURCE_ROUTE = "";
    public static final String SOURCE_ROUTE_REG = "REG";
    public static final String INVEST_TYPE_LUMPSUM = "LUMPSUM";
    public static final String INVEST_TYPE_REGULER = "REGULER";
    public static final String ID_COUNTRY_INDO = "52";

    public static final String PAYMENT_TYPE_VA_PERMATA = "VA_PERMATA";
    public static final String PAYMENT_TYPE_TRANS_BCA = "TRANS_BCA";
    public static final String PAYMENT_TYPE_TRANS_MANDIRI = "TRANS_MANDIRI";


    public static final String GENDER_MALE = "ML";
    public static final String GENDER_FEMALE = "FML";

    //invisee
    public static final String KYC_CAT_SALUTATION = "SALUTATION";
    public static final String KYC_CAT_REFERRAL = "REFERRAL";
    public static final String KYC_CAT_NATURE_OF_BUSINESS = "NATURE_OF_BUSINESS";
    public static final String KYC_CAT_TOTAL_ASSET = "TOTAL_ASSET";
    public static final String KYC_CAT_INVESTMENT_EXPERIENCE = "INVESTMENT_EXPERIENCE";
    public static final String KYC_CAT_PEP_RELATIONSHIP = "PEP_RELATIONSHIP";
    public static final String KYC_CAT_FUND_SOURCE_RELATIONSHIP = "FUND_SOURCE_RELATIONSHIP";
    public static final String KYC_CAT_FUND_SOURCE_OCCUPATION = "FUND_SOURCE_OCCUPATION";
    public static final String KYC_CAT_FUND_SOURCE_OF_FUND_SOURCE = "FUND_SOURCE_OF_FUND_SOURCE";
    public static final String KYC_CAT_BANK_NAME = "BANK_NAME";







}

