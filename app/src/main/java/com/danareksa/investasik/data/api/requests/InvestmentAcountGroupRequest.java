package com.danareksa.investasik.data.api.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Rizal Gunawan on 7/25/18.
 */

public class InvestmentAcountGroupRequest {

    @SerializedName("packageId")
    @Expose
    private Integer packageId;

    @SerializedName("invAccGroupId")
    @Expose
    private Integer invAccGroupId;

    @SerializedName("token")
    @Expose
    private String token;

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public Integer getInvAccGroupId() {
        return invAccGroupId;
    }

    public void setInvAccGroupId(Integer invAccGroupId) {
        this.invAccGroupId = invAccGroupId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
