package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by asep.surahman on 14/08/2018.
 */

public class ProductRegulerIfua implements Serializable {

    @SerializedName("bankAccountName")
    @Expose
    private String bankAccountName;

    @SerializedName("cif")
    @Expose
    private String cif;

    @SerializedName("fullName")
    @Expose
    private String fullName;

    @SerializedName("bankName")
    @Expose
    private String bankName;

    @SerializedName("bankAccountNumber")
    @Expose
    private String bankAccountNumber;

    @SerializedName("idNumber")
    @Expose
    private String idNumber;

    @SerializedName("packageList")
    @Expose
    private List<PackageReguler> packageList;

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public List<PackageReguler> getPackageList() {
        return packageList;
    }

    public void setPackageList(List<PackageReguler> packageList) {
        this.packageList = packageList;
    }
}
