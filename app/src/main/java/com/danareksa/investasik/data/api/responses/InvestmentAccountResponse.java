package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.InvestmentAccount;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by glenrynaldi on 4/14/16.
 */
public class InvestmentAccountResponse extends com.danareksa.investasik.data.api.responses.GenericResponse {

    @SerializedName("data")
    @Expose
    private List<InvestmentAccount> data = new ArrayList<>();

    /**
     * @return The data
     */
    public List<InvestmentAccount> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<InvestmentAccount> data) {
        this.data = data;
    }

}
