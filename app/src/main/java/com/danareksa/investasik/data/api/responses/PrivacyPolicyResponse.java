package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.PrivacyPolicy;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Rizal Gunawan on 7/17/18.
 */

public class PrivacyPolicyResponse implements Serializable {
    @SerializedName("code")
    private Integer code;

    @SerializedName("data")
    private PrivacyPolicy data;

    /**
     * @return The code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * @param code The code
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * @return The data
     */
    public PrivacyPolicy getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(PrivacyPolicy data) {
        this.data = data;
    }
}
