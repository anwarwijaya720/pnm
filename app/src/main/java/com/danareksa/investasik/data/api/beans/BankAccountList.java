package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 10/07/2018.
 */

public class BankAccountList {

    @SerializedName("bankAccountImage")
    @Expose
    private BankAccountImage bankAccountImage;

    @SerializedName("bankAccountNumber")
    @Expose
    private String bankAccountNumber;

    @SerializedName("bankBranchOther")
    @Expose
    private String bankBranchOther;

    @SerializedName("bankAccountId")
    @Expose
    private String bankAccountId;

    @SerializedName("bankAccountName")
    @Expose
    private String bankAccountName;

    @SerializedName("bankBranch")
    @Expose
    private String bankBranch;

    @SerializedName("bankId")
    @Expose
    private String bankId;

    @SerializedName("bankAccountImageKey")
    @Expose
    private String bankAccountImageKey;

    public BankAccountImage getBankAccountImage() {
        return bankAccountImage;
    }

    public void setBankAccountImage(BankAccountImage bankAccountImage) {
        this.bankAccountImage = bankAccountImage;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(String bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankAccountImageKey() {
        return bankAccountImageKey;
    }

    public void setBankAccountImageKey(String bankAccountImageKey) {
        this.bankAccountImageKey = bankAccountImageKey;
    }

    public String getBankBranchOther() {
        return bankBranchOther;
    }

    public void setBankBranchOther(String bankBranchOther) {
        this.bankBranchOther = bankBranchOther;
    }
}
