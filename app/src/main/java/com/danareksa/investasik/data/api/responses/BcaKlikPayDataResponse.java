package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.BcaKlikPayData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


/**
 * Created by asep.surahman on 02/10/2018.
 */

public class BcaKlikPayDataResponse extends GenericResponse implements Serializable{

    @SerializedName("data")
    @Expose
    private BcaKlikPayData data;

    public BcaKlikPayData getData() {
        return data;
    }

    public void setData(BcaKlikPayData data) {
        this.data = data;
    }
}
