package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.DocumentTransaction;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pandu.abbiyuarsyah on 31/03/2017.
 */

public class DocTransactionResponse extends com.danareksa.investasik.data.api.responses.GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose

    private List<DocumentTransaction> data = new ArrayList<>();

    /**
     * @return The data
     */
    public List<DocumentTransaction> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<DocumentTransaction> data) {
        this.data = data;
    }
}
