package com.danareksa.investasik.data.api;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.data.api.beans.Bank;
import com.danareksa.investasik.data.api.beans.Branch;
import com.danareksa.investasik.data.api.beans.City;
import com.danareksa.investasik.data.api.beans.Country;
import com.danareksa.investasik.data.api.beans.FatcaQuestion;
import com.danareksa.investasik.data.api.beans.Fee;
import com.danareksa.investasik.data.api.beans.KycLookup;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.PortfolioChartData;
import com.danareksa.investasik.data.api.beans.RedemptionFee;
import com.danareksa.investasik.data.api.beans.Reminder;
import com.danareksa.investasik.data.api.beans.RiskProfileQuestion;
import com.danareksa.investasik.data.api.beans.SecurityQuestion;
import com.danareksa.investasik.data.api.beans.State;
import com.danareksa.investasik.data.api.beans.TopUpTransaction;
import com.danareksa.investasik.data.api.beans.TransactionHistory;
import com.danareksa.investasik.data.api.requests.ActivationCodeRequest;
import com.danareksa.investasik.data.api.requests.AddAccountLumpsumRequest;
import com.danareksa.investasik.data.api.requests.AddAccountRegulerRequest;
import com.danareksa.investasik.data.api.requests.AddProductRegulerRequest;
import com.danareksa.investasik.data.api.requests.AmountSummaryRequest;
import com.danareksa.investasik.data.api.requests.AndroidVersionRequest;
import com.danareksa.investasik.data.api.requests.BankListRequest;
import com.danareksa.investasik.data.api.requests.BcaKlikpayConfirmationRequest;
import com.danareksa.investasik.data.api.requests.BcaKlikpayTransRequest;
import com.danareksa.investasik.data.api.requests.ChangePasswordRequest;
import com.danareksa.investasik.data.api.requests.CheckPinRequest;
import com.danareksa.investasik.data.api.requests.ConfirmCheckPinRequest;
import com.danareksa.investasik.data.api.requests.CountryRequest;
import com.danareksa.investasik.data.api.requests.CustomerDataByIfuaRequest;
import com.danareksa.investasik.data.api.requests.CustomerDataRequest;
import com.danareksa.investasik.data.api.requests.DeleteReminderRequest;
import com.danareksa.investasik.data.api.requests.DetailNewsRequest;
import com.danareksa.investasik.data.api.requests.DetailWaitingTopUpRequest;
import com.danareksa.investasik.data.api.requests.ForgotPasswordUserRequest;
import com.danareksa.investasik.data.api.requests.GetOrderNumberMandiriClickpayRequest;
import com.danareksa.investasik.data.api.requests.InvestmentAcountGroupRequest;
import com.danareksa.investasik.data.api.requests.InvestmentByProductTypeRequest;
import com.danareksa.investasik.data.api.requests.InvestmentPerformanceRequest;
import com.danareksa.investasik.data.api.requests.JoinPromoRequest;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.api.requests.KycLookupRequest;
import com.danareksa.investasik.data.api.requests.ListRequest;
import com.danareksa.investasik.data.api.requests.LoginRequest;
import com.danareksa.investasik.data.api.requests.MandiriClickpayRequest;
import com.danareksa.investasik.data.api.requests.OtpRequest;
import com.danareksa.investasik.data.api.requests.PackageListRequest;
import com.danareksa.investasik.data.api.requests.PackagePerformanceRequest;
import com.danareksa.investasik.data.api.requests.PayWalletRequest;
import com.danareksa.investasik.data.api.requests.PaymentPayProRequest;
import com.danareksa.investasik.data.api.requests.PaymentTransferRequest;
import com.danareksa.investasik.data.api.requests.PieChartRequest;
import com.danareksa.investasik.data.api.requests.PromoDetailRequest;
import com.danareksa.investasik.data.api.requests.PurchasePackageListRequest;
import com.danareksa.investasik.data.api.requests.RegisterNasabahRequest;
import com.danareksa.investasik.data.api.requests.RegisterNewCustomerRequest;
import com.danareksa.investasik.data.api.requests.RegisterOnlineAccessRequest;
import com.danareksa.investasik.data.api.requests.ResetPasswordRequest;
import com.danareksa.investasik.data.api.requests.RetryPayProRequest;
import com.danareksa.investasik.data.api.requests.RiskProfileRequest;
import com.danareksa.investasik.data.api.requests.SaveReminderRequest;
import com.danareksa.investasik.data.api.requests.SendOtpRequest;
import com.danareksa.investasik.data.api.requests.TopUpViseePayRequest;
import com.danareksa.investasik.data.api.requests.TransactionTransferRequest;
import com.danareksa.investasik.data.api.requests.ValidateSubscRegulerRequest;
import com.danareksa.investasik.data.api.requests.WalletRequest;
import com.danareksa.investasik.data.api.responses.ActivationCodeResponse;
import com.danareksa.investasik.data.api.responses.AmountSummaryResponse;
import com.danareksa.investasik.data.api.responses.AndroidVersionResponse;
import com.danareksa.investasik.data.api.responses.BankListResponse;
import com.danareksa.investasik.data.api.responses.BcaKlikPayDataResponse;
import com.danareksa.investasik.data.api.responses.CartListResponse;
import com.danareksa.investasik.data.api.responses.CheckRedemptionResponse;
import com.danareksa.investasik.data.api.responses.CompanyContactsResponse;
import com.danareksa.investasik.data.api.responses.CompletenessPercentageResponse;
import com.danareksa.investasik.data.api.responses.ConfirmCheckPinResponse;
import com.danareksa.investasik.data.api.responses.ConfirmationRedemptionResponse;
import com.danareksa.investasik.data.api.responses.CreateRedemptionResponse;
import com.danareksa.investasik.data.api.responses.CustomerDataByIfuaResponse;
import com.danareksa.investasik.data.api.responses.CustomerDataRekeningResponse;
import com.danareksa.investasik.data.api.responses.CustomerDocumentResponse;
import com.danareksa.investasik.data.api.responses.CustomerDocumentSettlementResponse;
import com.danareksa.investasik.data.api.responses.CustomerDocumentSignatureResponse;
import com.danareksa.investasik.data.api.responses.DocTransactionResponse;
import com.danareksa.investasik.data.api.responses.FaqResponse;
import com.danareksa.investasik.data.api.responses.FundAllocationResponse;
import com.danareksa.investasik.data.api.responses.GeneratePINResponse;
import com.danareksa.investasik.data.api.responses.GenericResponse;
import com.danareksa.investasik.data.api.responses.InvestAccountResponse;
import com.danareksa.investasik.data.api.responses.InvestmentAccountInfoResponse;
import com.danareksa.investasik.data.api.responses.InvestmentAccountResponse;
import com.danareksa.investasik.data.api.responses.InvestmentListByProductTypeResponse;
import com.danareksa.investasik.data.api.responses.InvestmentPerformanceResponse;
import com.danareksa.investasik.data.api.responses.KycLookupResponse;
import com.danareksa.investasik.data.api.responses.LoadKycDataResponse;
import com.danareksa.investasik.data.api.responses.LoginResponse;
import com.danareksa.investasik.data.api.responses.MandiriClickpayOrderNumberResponse;
import com.danareksa.investasik.data.api.responses.MaxScoreResponse;
import com.danareksa.investasik.data.api.responses.NewRegistrationResponse;
import com.danareksa.investasik.data.api.responses.NewsFeedResponse;
import com.danareksa.investasik.data.api.responses.OtpRequestResponse;
import com.danareksa.investasik.data.api.responses.PackageByTokenResponse;
import com.danareksa.investasik.data.api.responses.PackageListRegulerResponse;
import com.danareksa.investasik.data.api.responses.PackageResponse;
import com.danareksa.investasik.data.api.responses.PackagesPerformanceResponse;
import com.danareksa.investasik.data.api.responses.PartialRedemtionResponse;
import com.danareksa.investasik.data.api.responses.PaymentMethodResponse;
import com.danareksa.investasik.data.api.responses.PendingOrderDetailResponse;
import com.danareksa.investasik.data.api.responses.PeriodeResp;
import com.danareksa.investasik.data.api.responses.PieChartResponse;
import com.danareksa.investasik.data.api.responses.PortfolioInvestmentListResponse;
import com.danareksa.investasik.data.api.responses.PortfolioInvestmentSummaryResponse;
import com.danareksa.investasik.data.api.responses.PrivacyPolicyResponse;
import com.danareksa.investasik.data.api.responses.ProductRegulerResponse;
import com.danareksa.investasik.data.api.responses.ProductResponse;
import com.danareksa.investasik.data.api.responses.PromoDetailResponse;
import com.danareksa.investasik.data.api.responses.PromoListResponse;
import com.danareksa.investasik.data.api.responses.RedeemFeeResponse;
import com.danareksa.investasik.data.api.responses.RedemptionOrderResponse;
import com.danareksa.investasik.data.api.responses.ReferralResponse;
import com.danareksa.investasik.data.api.responses.ReminderDetailResponse;
import com.danareksa.investasik.data.api.responses.ReminderListResponse;
import com.danareksa.investasik.data.api.responses.RiskProfileNewResponse;
import com.danareksa.investasik.data.api.responses.RiskProfileResponse;
import com.danareksa.investasik.data.api.responses.SaveDataKycResponse;
import com.danareksa.investasik.data.api.responses.SendOtpResponse;
import com.danareksa.investasik.data.api.responses.SettlementInfoResponse;
import com.danareksa.investasik.data.api.responses.SliderResponse;
import com.danareksa.investasik.data.api.responses.SlideshowResponse;
import com.danareksa.investasik.data.api.responses.StatusCustomerResponse;
import com.danareksa.investasik.data.api.responses.TermAndConditionResponse;
import com.danareksa.investasik.data.api.responses.TopUpViseePayResponse;
import com.danareksa.investasik.data.api.responses.TransactionHistoryResponse;
import com.danareksa.investasik.data.api.responses.TransactionTransferResponse;
import com.danareksa.investasik.data.api.responses.TrxTransCodeResponse;
import com.danareksa.investasik.data.api.responses.TrxTransMandiriClickPayResponse;
import com.danareksa.investasik.data.api.responses.UploadResponse;
import com.danareksa.investasik.data.api.responses.UserInfoResponse;
import com.danareksa.investasik.data.api.responses.ValidateIfuaRegulerResponse;
import com.danareksa.investasik.data.api.responses.WalletBalanceResponse;
import com.danareksa.investasik.data.api.responses.WalletHistoryResponse;
import com.danareksa.investasik.ui.receiver.Tls12SocketFactory;
import com.danareksa.investasik.util.Constant;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.readystatesoftware.chuck.ChuckInterceptor;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;

import okhttp3.CertificatePinner;
import okhttp3.ConnectionSpec;
import okhttp3.Interceptor;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.ByteString;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by fajarfatur on 2/1/16.
 */
public class InviseeService {

    private Api api;

    /*Danareksa Production*/
    public final static String BASE_URL = BuildConfig.SERVER_URL;


    public final static String PORTAL_URL = "attachFile/download?id=";
    public final static String IMAGE_DOWNLOAD_URL = BASE_URL + "attachFile/download?id=";
    public final static String ASSETS_DOWNLOAD_URL = BASE_URL +  "attachFile/download?";
    public final static String IMAGE_CUSTOMER_DOWNLOAD_URL = BASE_URL + "customerDocument/download?id=";
    private final static String MOBILE_URL = "mobile-invisee/service/";
    public final static String UPLOAD = BASE_URL + "customerDocument/uploadByCustomer";
    public final static String Download = BASE_URL + "customerDocument/download";

    public static class ByteArrayAdapter extends TypeAdapter<byte[]> {
        public ByteArrayAdapter() {
        }

        public void write(JsonWriter out, byte[] value) throws IOException {
            if (value == null) {
                out.nullValue();
            } else {
                out.setHtmlSafe(false);
                out.jsonValue(ByteString.of(value).base64().replace("\\u003d","="));
            }

        }

        public byte[] read(JsonReader in) throws IOException {
            switch(in.peek()) {
                case NULL:
                    in.nextNull();
                    return null;
                default:
                    String bytesAsBase64 = in.nextString();
                    ByteString byteString = ByteString.decodeBase64(bytesAsBase64);
                    return byteString.toByteArray();
            }
        }
    }

    public InviseeService(Context context) {
        Retrofit retrofit = null;
        try {
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .disableHtmlEscaping()
                    .registerTypeAdapter(byte[].class, new ByteArrayAdapter())
                    .create();

            retrofit = new Retrofit.Builder()
                    .client(provideOkHttpClient(context))
                    .baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        this.api = retrofit.create(Api.class);
    }

    public static OkHttpClient.Builder enableTls12OnPreLollipop(OkHttpClient.Builder httpClient) {
        if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT < 22) {
            try {
                SSLContext sc = SSLContext.getInstance("TLSv1.2");
                sc.init(null, null, null);
                httpClient.sslSocketFactory(new Tls12SocketFactory(sc.getSocketFactory()));

                ConnectionSpec cs = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                        .tlsVersions(TlsVersion.TLS_1_2)
                        .build();

                List<ConnectionSpec> specs = new ArrayList<>();
                specs.add(cs);
                specs.add(ConnectionSpec.COMPATIBLE_TLS);
                specs.add(ConnectionSpec.CLEARTEXT);
                httpClient.connectionSpecs(specs);
            } catch (Exception exc) {
                Log.e("OkHttpTLSCompat", "Error while setting TLS 1.2", exc);
            }
        }

        return httpClient;
    }

    private OkHttpClient provideOkHttpClient(final Context context) throws MalformedURLException {

        OkHttpClient.Builder httpClient = null;

        URL url = new URL(BuildConfig.SERVER_URL);
        if (BuildConfig.IS_PRODUCTION){
            String serverHostname = url.getHost();
            CertificatePinner.Builder certificatePinner = new  CertificatePinner.Builder();
            certificatePinner
                    .add(serverHostname,"sha256/qCUnx5/U5qKEHAVrNzMPnzi/EkCjsP73xZiwF/HERYk=")
                    .add(serverHostname,"sha256/5kJvNEMw0KjrCAu7eXY5HZdvyCS13BbA0VJG1RSP91w=")
                    .add(serverHostname,"sha256/r/mIkG3eEpVdm+u/ko/cwxzOMo1bk4TyHIlByibiA5E=")
                    .build();

            httpClient = new OkHttpClient().newBuilder()
                    .certificatePinner(certificatePinner.build());
        } else {
            httpClient = new OkHttpClient.Builder();
        }

        httpClient.readTimeout(500, TimeUnit.SECONDS);
        httpClient.connectTimeout(500, TimeUnit.SECONDS);
        if (BuildConfig.IS_DEV_MODE){
            HttpLoggingInterceptor httpLoggingInterceptorinterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptorinterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(httpLoggingInterceptorinterceptor); //disable ketika production
            httpClient.addInterceptor(new ChuckInterceptor(context)); //disable ketika production
        }
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Response response = chain.proceed(request);
                invalidTokenHandler(response, context);
                return response;
            }
        });
        return enableTls12OnPreLollipop(httpClient).build();
    }

    private void invalidTokenHandler(Response response, Context context) throws IOException {
        String responseBody = response.peekBody(100000L).string();
        try {
            Object json = new JSONTokener(responseBody).nextValue();
            if (json instanceof JSONObject) {
                JSONObject jsonObject = new JSONObject(responseBody);
                if (jsonObject.has("code") && jsonObject.getInt("code") == Constant.INVALID_TOKEN) {
                    Intent intent = new Intent("Invalid");
                    context.sendBroadcast(intent);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Api getApi() {
        return api;
    }

    public interface Api {

        @POST("question/loadSecurityQuestion")
        Observable<List<SecurityQuestion>> loadSecurityQuestion();

        @POST("customersApi/registerCustomer")
        Observable<GenericResponse> regsiterCustomer(@Query("security_answer") String answerNote,
                                                     @Query("email") String email,
                                                     @Query("first_name") String firstName,
                                                     @Query("phone_number") String homePhoneNumber,
                                                     @Query("last_name") String lastName,
                                                     @Query("middle_name") String middleName,
                                                     @Query("mobile_number") String mobileNumber,
                                                     @Query("password") String password,
                                                     @Query("confirm_password") String confirmPassword,
                                                     @Query("security_question") String question,
                                                     @Query("agent") String agent);

        @POST("customersApi/loginEmailPassword")
        Observable<LoginResponse> login(@Query("email") String email,
                                        @Query("password") String password);


        @POST("customersApi/forgotPassword")
        Observable<GenericResponse> forgotPassword(@Query("email") String email,
                                                   @Query("answer") String answer,
                                                   @Query("question") String question);

        @POST("customersApi/doResetPassword")
        Observable<GenericResponse> resetPassword(@Body ResetPasswordRequest request);

        @POST("customersApi/generateResetCode")
        Observable<GenericResponse> resendCode(@Query("email") String email,
                                               @Query("securityAnswer") String securityAnswer,
                                               @Query("securityQuestion") String securityQuestion);

        @POST("customersApi/changeNewPassword")
        Observable<GenericResponse> changePassword(@Body ChangePasswordRequest request);

        @POST("customersApi/inputSecurityActivationCode")
        Observable<LoginResponse> activateUserWithActivationCode(@Query("email") String email,
                                                                 @Query("reset_code") String activationCode,
                                                                 @Query("token") String token);

        @POST("customersApi/resendCode")
        Observable<GenericResponse> resendActivationCode(@Query("email") String email);

        @POST("kycApi/load")
        Observable<LoadKycDataResponse> loadKycData(@Query("email") String email,
                                                    @Query("token") String token);

        @POST("KycApi/saveKyc")
        Observable<GenericResponse> saveKycData(@Query("salutation") String salutation,
                                                @Query("firstName") String firstName,
                                                @Query("middleName") String middleName,
                                                @Query("lastName") String lastName,
                                                @Query("email") String email,
                                                @Query("birthDate") String birthDate,
                                                @Query("birthPlace") String birthPlace,
                                                @Query("occupation") String occupation,
                                                @Query("natureOfBusiness") String natureOfBusiness,
                                                @Query("employerName") String employerName,
                                                @Query("homeCountry") String homeCountry,
                                                @Query("homeProvince") String homeProvince,
                                                @Query("homeCity") String homeCity,
                                                @Query("homeAddress") String homeAddress,
                                                @Query("homePostalCode") String homePostalCode,
                                                @Query("homePhoneNumber") String homePhoneNumber,
                                                @Query("legalCountry") String legalCountry,
                                                @Query("legalProvince") String legalProvince,
                                                @Query("legalCity") String legalCity,
                                                @Query("legalAddress") String legalAddress,
                                                @Query("legalPostalCode") String legalPostalCode,
                                                @Query("legalPhoneNumber") String legalPhoneNumber,
                                                @Query("idType") String idType,
                                                @Query("idNumber") String idNumber,
                                                @Query("idExpirationDate") String idExpirationDate,
                                                @Query("nationality") String nationality,
                                                @Query("citizenship") String citizenship,
                                                @Query("gender") String gender,
                                                @Query("religion") String religion,
                                                @Query("maritalStatus") String maritalStatus,
                                                @Query("educationBackground") String educationBackground,
                                                @Query("sourceOfIncome") String sourceOfIncome,
                                                @Query("totalIncomePa") String totalIncomePa,
                                                @Query("totalAsset") String totalAsset,
                                                @Query("investmentExperience") String investmentExperience,
                                                @Query("investmentPurpose") String investmentPurpose,
                                                @Query("otherInvestmentExperience") String otherInvestmentExperience,
                                                @Query("pepName") String pepName,
                                               /* @Query("pepCountry") String pepCountry,*/
                                                @Query("pepPosition") String pepPosition,
                                                @Query("pepPublicFunction") String pepPublicFunction,
                                                @Query("pepYearOfService") String pepYearOfService,
                                                @Query("pepRelationship") String pepRelationship,
                                                @Query("motherMaidenName") String motherMaidenName,
                                                @Query("beneficiaryName") String beneficiaryName,
                                                @Query("beneficiaryRelationship") String beneficiaryRelationship,
                                                @Query("preferredMailingAddress") String preferredMailingAddress,
                                                @Query("taxId") String taxId,
//                                                @Query("taxIdRegisDate") String taxIdRegisDate,
                                                @Query("token") String token,
                                                @Query("referral") String referral,
                                                @Query("referralName") String referralName,
                                                @Query("spouseName") String spouseName,
                                                @Query("bankId") String bankId,
                                                @Query("branchId") String branchId,
                                                @Query("settlementAccountName") String settlementAccountName,
                                                @Query("settlementAccountNo") String settlementAccountNo);

        @POST("KycApi/saveOrUpdateSettlementInformation")
        Observable<GenericResponse> saveOrUpdateSettlementInformation(@Query("bankId.id") String bankId,
                                                                      @Query("branchId.id") String branchId,
                                                                      @Query("email") String email,
                                                                      @Query("settlementAccountName") String settlementAccountName,
                                                                      @Query("settlementAccountNo") String settlementAccountNo,
                                                                      @Query("token") String token);

        @POST("lookup/getAllLookup")
        Observable<List<KycLookup>> getKycLookup(@Query("token") String token);

        @POST("country/getAllCountry")
        Observable<List<Country>> getAllCountry(@Query("token") String token);

        @POST("state/getStateByCountry")
        Observable<List<State>> getStateByCountry(@Query("countryId") String countryId,
                                                  @Query("token") String token);

        @POST("city/getCityByState")
        Observable<List<City>> getCityByState(@Query("state") String state,
                                              @Query("token") String token);

        @POST("kycApi/loadFatchaQuestionAndAnswer")
        Observable<ArrayList<FatcaQuestion>> loadFatcaQuestionAndAnswer(@Query("token") String token);

        @POST("kycApi/loadRiskProfileQuestionAndAnswer")
        Observable<ArrayList<RiskProfileQuestion>> loadRiskProfileQuestionAndAnswer(@Query("token") String token);

        @POST("kycApi/saveOrUpdateCustomerAnswer")
        Observable<GenericResponse> saveCunstomerAnswer(@Query("batch") String batch,
                                                        @Query("questionaireId") String questionaireId,
                                                        @Query("token") String token);


        @POST("kycApi/loadSettlementInformation")
        Observable<SettlementInfoResponse> loadSettlementInformation(@Query("email") String email,
                                                                     @Query("token") String token);

        @POST("kycApi/getRiskProfileResult")
        Observable<RiskProfileResponse> getRiskProfileResult(@Query("token") String token);

        @POST("bank/getAll")
        Observable<List<Bank>> getAllBank(@Query("max") String max,
                                          @Query("offset") String offset,
                                          @Query("token") String token);

        @POST("branch/getListBranchByBankCode")
        Observable<List<Branch>> getListBranchByBankCode(@Query("bankId") String bankId,
                                                         @Query("token") String token);

        @POST("customersApi/completenessPercentage")
        Observable<CompletenessPercentageResponse> getCompletenessPercentage(@Query("token") String token);


        @POST("userApi/pendingCustomer")
        Observable<GenericResponse> postPendingCustomer(@Query("token") String token);

        @POST("fundPackagesApi/fundPackagesMobile")
        Observable<List<Packages>> packageList(@Query("token") String token);

        @GET("fundPackages/getProductLists")
        Observable<ProductResponse> getProductList();

        @GET("fundPackages/getPackageById")
        Observable<PackageResponse> packageDetail(@Query("id") Long id,
                                                  @Query("token") String token);

        @GET("fundPackages/getMaxScore")
        Observable<MaxScoreResponse> getMaxScore(@Query("id") Long id,
                                                 @Query("token") String token);

        @POST("fundPackagesApi/fundAllocationInfo")
        Observable<FundAllocationResponse> fundAllocationInfo(@Query("packageId") Long id,
                                                              @Query("token") String token);


        @POST("performance/getPackagePerformance")
        Observable<PackagesPerformanceResponse> packagePerformanceInfo(@Body PackagePerformanceRequest request);

        @GET("fundPackageFeeSetup/getSubscriptionFee")
        Observable<List<Fee>> subscriptionFee(@Query("fp") Long packageId,
                                              @Query("type") Long type,
                                              @Query("token") String token);

        @GET("fundPackageFeeSetup/getRedemptionFee")
        Observable<List<RedemptionFee>> redemptionFee(@Query("fp") Long packageId,
                                                      @Query("type") Long type,
                                                      @Query("token") String token);

        @GET("redemption/redemptionOrderDetails")
        Observable<RedemptionOrderResponse> redemptionOrderDetails(@Query("investmentAccountNo") String investmentAccountNo,
                                                                   @Query("token") String token);

        @GET("redemption/checkRedemptionTransaction")
        Observable<CheckRedemptionResponse> checkRedemptionTransaction(@Query("investmentAccountNo") String investmentAccountNo,
                                                                       @Query("token") String token);

        @GET("trxApi/createRedemption")
        Observable<CreateRedemptionResponse> createRedemption(@Query("index") String index,
                                                              @Query("investId") String investId,
                                                              @Query("pin") String pin,
                                                              @Query("token") String token);



        @GET("trxApi/confirmTransRedemption")
        Observable<ConfirmationRedemptionResponse> confirmTransRedemption(@Query("confirm") String confirm,
                                                                          @Query("pin") String pin,
                                                                          @Query("index") String index,
                                                                          @Query("investId") String investId,
                                                                          @Query("token") String token);

        @GET("trxApi/getDetailPendingOrderByOrderNumber")
        Observable<PendingOrderDetailResponse> getDetailPendingOrder(@Query("investment_number") String invesmentNumber,
                                                                     @Query("orderNumber") String orderNumber,
                                                                     @Query("token") String token);


        //INVESTMENT LIST PORTOFOLIO
        @POST("customersApi/getInvestmentList")
        Observable<PortfolioInvestmentListResponse> getInvestmentList(@Body InvestmentAcountGroupRequest acountGroupRequest);

        @POST("performance/getInvestmentPerformance")
        Observable<InvestmentPerformanceResponse> getInvestmentPerformance(@Query("token") String token,
                                                                           @Body InvestmentPerformanceRequest request);

        @POST("customersApi/getInvestmentSummary")
        Observable<PortfolioInvestmentSummaryResponse> getInvestmentSummary(@Query("token") String token);


        @POST("trxApi/createTrxCart")
        Observable<GenericResponse> createTrxCart(@Query("amount") String amount,
                                                  @Query("investAccountNo") String investAccountNo,
                                                  @Query("packageCode") String packageCode,
                                                  @Query("trxType") String trxType,
                                                  @Query("token") String token);

        @POST("trxApi/createTrxCart")
        Observable<GenericResponse> createTrxCart(@Query("ifuaId") String amount,
                                                  @Query("packageCode") String packageCode,
                                                  @Query("token") String token);

        @POST("trxApi/createTrxCart")
        Observable<GenericResponse> topUpToCart(@Query("amount") String amount,
                                                  @Query("investAccountNo") String investAccountNo,
                                                  @Query("packageCode") String packageCode,
                                                  @Query("trxType") String trxType,
                                                  @Query("token") String token,
                                                  @Query("ifuaId") String ifuaId
                                                );


        @GET("redemption/topup_validation")
        Observable<GenericResponse> topUpValidation(@Query("investmentAccountNo") String investAccountNo,
                                                    @Query("token") String token);


        @GET("trxApi/checkTrxCartByInvestmentNumber")
        Observable<GenericResponse> checkTrxCartByInvestmentNumber(@Query("investmentAccountNo") String investAccountNo,
                                                                   @Query("token") String token);



        @POST("trxApi/cartList")
        Observable<List<CartListResponse>> getCartList(@Query("token") String token);

        @POST("trxApi/deleteCart")
        Observable<GenericResponse> deleteCart(@Query("id") String id,
                                               @Query("token") String token);

        @GET("customerDocument/getCustomerDocument")
        Observable<CustomerDocumentResponse> getDocumentDoc01(@Query("code") String id,
                                                              @Query("token") String token);


        @GET("customerDocument/getCustomerDocument")
        Observable<CustomerDocumentSignatureResponse> getDocumentDoc03(@Query("code") String id,
                                                                       @Query("token") String token);

        @GET("customerDocument/getCustomerDocument")
        Observable<CustomerDocumentSettlementResponse> getDocumentDoc04(@Query("code") String id,
                                                                        @Query("token") String token);

        @GET("customerDocument/getDocTransactionByOrderNo")
        Observable<DocTransactionResponse> getDocTransaction(@Query("orderno") String orderno,
                                                             @Query("token") String token);

        @POST("paymentTransaction/checkPin")
        Observable<AmountSummaryResponse> checkPin(@Query("token") String token,
                                                   @Body CheckPinRequest request);

        @POST("paymentTransaction/confirmCheckPin")
        Observable<ConfirmCheckPinResponse> confirmCheckPin(@Query("token") String token,
                                                            @Body ConfirmCheckPinRequest request);

        @POST("paymentTransaction/confirmCheckPin")
        Observable<AmountSummaryResponse> confirmCheckPinPaypro(@Query("token") String token,
                                                                @Body ConfirmCheckPinRequest request);


        @POST("paymentTransaction/transactionTransfer")
        Observable<TransactionTransferResponse> transactionTransfer(@Query("token") String token,
                                                                    @Body TransactionTransferRequest request);

        @POST("paymentTransaction/payWallet")
        Observable<GenericResponse> payWallet(@Query("token") String token,
                                              @Body PayWalletRequest request);

        @POST("paymentTransaction/amountSummaryWallet")
        Observable<AmountSummaryResponse> amountSummaryWallet(@Query("token") String token,
                                                              @Body AmountSummaryRequest request);


        @POST("trxApi/pieChart")
        Observable<List<PortfolioChartData>> getPieChart(@Query("token") String token);

        @POST("customersApi/getPieChartDashboard")
        Observable<PieChartResponse> getPieChartDashboard(@Query("token") String token,
                                                          @Body PieChartRequest request);

        @POST("trxApi/transactionHistory")
        Observable<List<TransactionHistory>> getPendingTransaction(@Query("token") String token,
                                                                   @Query("trxStatus") String trxStatus,
                                                                   @Query("max") String max);

        @POST("trxApi/transactionHistory")
        Observable<List<TransactionHistory>> getTransactionHistory(@Query("token") String token,
                                                                   @Query("max") String max,
                                                                   @Query("trxType") String trxStatus);

        @POST("trxApi/transactionHistory")
        Observable<TransactionHistoryResponse> getTransactionHistory(@QueryMap Map<String, String> options);

        @POST(MOBILE_URL + "investment/list")
        Observable<GenericResponse> investmentList(@Header("token") String token,
                                                   @Body ListRequest request);


        //NEWS LIST
        @POST("News/getAllNews")
        Observable<NewsFeedResponse> getAllNews(@Query("token") String token);

        @POST("News/getDetailById")
        Observable<NewsFeedResponse> getDetailNews(@Query("token") String token,
                                                   @Body DetailNewsRequest request);

        @POST(MOBILE_URL + "news/load")
        Observable<GenericResponse> newsList(@Body PackagePerformanceRequest request);


        @FormUrlEncoded
        @POST("paymentTransaction/transactionWalletBalance")
        Observable<WalletBalanceResponse> requestWalletBalance(@Field("token") String token);

        @POST("paymentTransaction/transactionWalletHistory")
        Observable<WalletHistoryResponse> requestWalletHistory(@Query("token") String token,
                                                               @Body WalletRequest request);

        @FormUrlEncoded
        @POST("reminder/list")
        Observable<List<Reminder>> reminderList(@Field("token") String token);

        @FormUrlEncoded
        @POST("reminder/reminderList")
        Observable<ReminderListResponse> reminders(@Field("token") String token);

        @POST("reminder/delete")
        Observable<GenericResponse> deleteReminder(@Query("token") String token,
                                                   @Body DeleteReminderRequest request);

        @FormUrlEncoded
        @POST("reminder/viewPackageName")
        Observable<PackageByTokenResponse> viewPackageName(@Field("token") String token);


        @POST("reminder/viewInvestementAccount")
        Observable<InvestmentAccountResponse> viewInvestementAccount(@Query("fundPackage") String fundPackage,
                                                                     @Query("token") String token);

        @FormUrlEncoded
        @POST("reminder/viewInvestementAccount")
        Observable<InvestmentAccountResponse> viewInvestementAccountrEeminder(@Field("token") String fundPackage,
                                                                              @Field("fundPackage") String token);

        @POST("reminder/saveOrUpdate")
        Observable<GenericResponse> saveOrUpdateReminder(@Query("token") String token,
                                                         @Body SaveReminderRequest request);

        @POST("reminder/getById")
        Observable<ReminderDetailResponse> getReminderDetail(@Query("id") String id,
                                                             @Query("token") String token);

        @FormUrlEncoded
        @POST("reminder/getById")
        Observable<GenericResponse> getReminderById(@Field("token") String token,
                                                    @Field("endDate") String endDate,
                                                    @Field("id") String id,
                                                    @Field("packagename") String packageName,
                                                    @Field("investNumber") String investNumber,
                                                    @Field("reminderAmount") String reminderAmount,
                                                    @Field("reminderIType") String reminderIType,
                                                    @Field("reminderTime") String reminderTime,
                                                    @Field("startdate") String startdate);

        @GET(ASSETS_DOWNLOAD_URL)
        Observable<ResponseBody> downloadAsset(@Query("id") String id);


        @FormUrlEncoded
        @POST("FAQ/getAllFAQ")
        Observable<FaqResponse> getFaq(@Field("token") String token);

        @POST("FAQ/getAllFAQ")
        Observable<FaqResponse> getFaqA();


        @FormUrlEncoded
        @POST("customersApi/generalInfo")
        Observable<UserInfoResponse> userInfo(@Field("token") String token);

        //@FormUrlEncoded
        @POST("customersApi/saveGeneralInfo")
        Observable<GenericResponse> saveUserInfo(@Query("answerNote") String answerNote,
                                                 @Query("email") String email,
                                                 @Query("firstName") String firstName,
                                                 @Query("homePhoneNumber") String homePhoneNumber,
                                                 @Query("imageKey") String imageKey,
                                                 @Query("lastName") String lastName,
                                                 @Query("middleName") String middleName,
                                                 @Query("mobileNumber") String mobileNumber,
                                                 @Query("question") String question,
                                                 @Query("token") String token);

        @Multipart
        @POST("attachFile/upload")
        Observable<UploadResponse> uploadPhoto(@Query("token") String token,
                                               @Part MultipartBody.Part file);
        @Multipart
        @POST("customerDocument/uploadByCustomer")
        Observable<GenericResponse> uploadByCustomerDoc1(@Query("token") String token,
                                                         @Query("type") String type,
                                                         @Part MultipartBody.Part body);

        @Multipart
        @POST("customerDocument/uploadByCustomerMobile")
        Observable<GenericResponse> uploadByCustomerVerDoc1(@Query("token") String token,
                                                            @Query("type") String type,
                                                            @Part MultipartBody.Part body);

        @Multipart
        @POST("customerDocument/uploadTransactionByCustomer")
        Observable<GenericResponse> uploadTransaction(@Query("orderno") String orderno,
                                                      @Query("token") String token,
                                                      @Query("type") String type,
                                                      @Part MultipartBody.Part body);



        @GET("customersApi/generatePin")
        Observable<GeneratePINResponse> generatePin(@Query("token") String token);




        @GET("userApi/signout")
        Observable<GenericResponse> logout(@Query("token") String token);

        @GET("redemption/rangeOfPartial")
        Observable<PartialRedemtionResponse> rangeOfPartialRedemtion(@Query("id") String idProduct,
                                                                     @Query("token") String token);



        @GET("redemption/getRedemptionFee")
        Observable<RedeemFeeResponse> redemptionFee(@Query("invNo") String invNo,
                                                    @Query("token") String token);

        @GET("redemption/partialRedemptionOrderDetails")
        Observable<RedemptionOrderResponse> partialRedemptionOrderDetails(@Query("investmentAccountNo") String investmentAccountNo,
                                                                          @Query("percentage") String percentage,
                                                                          @Query("token") String token);

        @GET("trxApi/createPartialRedemption")
        Observable<CreateRedemptionResponse> createPartialRedemption(@Query("index") String index,
                                                                     @Query("investId") String investId,
                                                                     @Query("percentage") String percentage,
                                                                     @Query("pin") String pin,
                                                                     @Query("token") String token);

        @GET("trxApi/confirmTransPartialRedemption")
        Observable<ConfirmationRedemptionResponse> confirmTransPartialRedemption(@Query("confirm") String confirm,
                                                                                 @Query("pin") String pin,
                                                                                 @Query("index") String index,
                                                                                 @Query("investId") String investId,
                                                                                 @Query("percentage") String percentage,
                                                                                 @Query("token") String token);

        @POST("version/android")
        Observable<AndroidVersionResponse> checkVersion(@Body AndroidVersionRequest request);

        @POST("promo/list")
        Observable<PromoListResponse> getPromoList(@Query("token") String token);

        @POST("promo/detail")
        Observable<PromoDetailResponse> getPromoDetail(@Body PromoDetailRequest request);

        @POST("promo/promo_detail?token=userNotLogin")
        Call<PromoDetailResponse> getPromoDesc(@Body PromoDetailRequest request);

        @POST("promo/join_promo")
        Observable<GenericResponse> joinPromo(@Body JoinPromoRequest request);

        @GET("paymentTransaction/paymentmethod?")
        Observable<PaymentMethodResponse> getPaymentMethod(@Query("token") String token);

        @POST("paymentTransaction/retry_dompetku")
        Observable<AmountSummaryResponse> retryPaypro(@Body RetryPayProRequest request);

        @POST("customersApi/payment_method_update")
        Observable<GenericResponse> getPaymentPaypro(@Body PaymentPayProRequest request);

        @GET("customersApi/checkStatusCustomer")
        Observable<StatusCustomerResponse> getStatusCustomer(@Query("token") String token);

        @POST("topUpTransaction/topUpViseePay")
        Observable<TopUpViseePayResponse> topUpViseePay(@Query("token") String token,
                                                        @Body TopUpViseePayRequest request);

        @POST("trxApi/listTopUp")
        Observable<List<TopUpTransaction>> getListTopUp(@Query("token") String token,
                                                        @Query("bank") String bank,
                                                        @Query("statusBayar") String statusBayar);

        @POST("topUpTransaction/detailWaitingTopUp")
        Observable<TopUpViseePayResponse> detailWaitingTopUp(@Query("token") String token,
                                                             @Body DetailWaitingTopUpRequest request);

        @POST("slideshow/customerlist")
        Observable<SliderResponse> getSlider();

        @GET("customersApi/getCustomerReferral")
        Observable<ReferralResponse> getCustomerReferral(@Query("token") String token);



        //============API new Dana Reksa========================================

        //landing page
        @POST("slideshow/getList")
        Observable<SlideshowResponse> getSlideshow();


        @POST("customersApi/registerNewCustomer")
        Observable<NewRegistrationResponse> regsiterNewCustomer(@Body RegisterNewCustomerRequest request);

        //new api register nasabah existing btn - andrew
        @POST("customersApi/registerNasabah")
        Observable<GenericResponse> regsiterBtnCustomer(@Body RegisterNasabahRequest request);

        @POST("customersApi/registerOnlineAccess")
        Observable<GenericResponse> regsiterOnlineAccess(@Body RegisterOnlineAccessRequest registerOnlineAccessRequest);


        @POST("customersApi/activateAccount")
        Observable<ActivationCodeResponse> activateAccountWithActivationCode(@Body ActivationCodeRequest activationCodeRequest);


        @POST("customersApi/loginUsernamePassword")
        Observable<LoginResponse> loginUser(@Body LoginRequest loginRequest);


        @POST("customersApi/urgentForgotPassword")
        Observable<GenericResponse> urgentForgotPassword(@Body ForgotPasswordUserRequest forgotPasswordUserRequest);

        @POST("lookup/getAllLookup")
        Observable<KycLookupResponse> getKycLookupNew(@Body KycLookupRequest kycLookupRequest);

        @POST("country/getAllCountry")
        Observable<List<Country>> getAllCountryNew(@Body CountryRequest countryRequest);


        @POST("customersApi/getKycData")
        Observable<LoadKycDataResponse> loadKycDataNew(@Body KycDataRequest kycDataRequest);


        @POST("customersApi/saveKycData")
        Observable<SaveDataKycResponse> saveKycDataNew(@Body KycDataRequest kycDataRequest);


        @POST("question/getRiskProfileLevel")
        Observable<RiskProfileNewResponse> riskProfileSimulation(@Body RiskProfileRequest riskProfileRequest);

        @GET("customersApi/getPrivacyPolicy")
        Observable<PrivacyPolicyResponse> getPrivacyPolicy();

        @GET("customersApi/getTermAndCondition")
        Observable<TermAndConditionResponse> getTermAndCondition();

        @GET("customersApi/getCompanyContacts")
        Observable<CompanyContactsResponse> getContactUs();

        @GET("customersApi/getNewCustomerTerm")
        Observable<TermAndConditionResponse> getTermAndConditionNewCustomer();

        @GET("customersApi/getExistingCustomerTerm")
        Observable<TermAndConditionResponse> getTermAndConditionExistingCustomer();

        @POST("customersApi/getInvestmentAccountGroup")
        Observable<InvestmentAccountInfoResponse> getInvestmentAccountGroup(@Body InvestmentAcountGroupRequest acountGroupRequest);

        @POST("branch/getListBranchByBankCode")
        Observable<List<Branch>> getListBranchByBankCodeNew(@Query("bankId") String bankId,
                                                            @Query("token") String token);


        @POST("requestInvAccGroup/getCustomerData")
        Observable<CustomerDataRekeningResponse> getCustomerData(@Body CustomerDataRequest customerDataRequest);

        @POST("requestInvAccGroup/getCustomerDataByIfua")
        Observable<CustomerDataByIfuaResponse> getCustomerDataByIfua(@Body CustomerDataByIfuaRequest customerDataRequest);

        @POST("requestInvAccGroup/getPackageListReguler")
        Observable<PackageListRegulerResponse> getPackageListReguler(@Body PackageListRequest packageListRequest);

        @POST("requestInvAccGroup/getPackageListReguler")
        Observable<PackageListRegulerResponse> getPackageListRegulerWithIfua(@Body PurchasePackageListRequest packageListRequest);

        @POST("requestInvAccGroup/getBankListReguler")
        Observable<BankListResponse> getBankListReguler(@Body BankListRequest bankListRequest);

        @POST("requestInvAccGroup/submitAccountRequest")
        Observable<InvestAccountResponse> submitAccountReguler(@Body AddAccountRegulerRequest accountRegulerRequest);

        @POST("requestInvAccGroup/submitAccountRequest")
        Observable<InvestAccountResponse> submitAccountLumpsum(@Body AddAccountLumpsumRequest accountLumpsumRequest);

        @POST("paymentTransaction/confirmTrxTransWithCode")
        Observable<TrxTransCodeResponse>  transferWithUniqeCode(@Body PaymentTransferRequest paymentTransferRequest);


        @POST("requestInvAccGroup/addProductToRegulerIfua")
        Observable<ProductRegulerResponse>  addProductToRegulerIfua(@Body AddProductRegulerRequest addProductRegulerRequest);

        @POST("paymentTransaction/confirmTrxTransVA")
        Observable<TrxTransCodeResponse>  paymentVaPermata(@Body PaymentTransferRequest paymentTransferRequest);

        @POST("otp/request_otp")
        Observable<OtpRequestResponse> requestOtp(@Body OtpRequest otpRequest);


        @POST("otp/send_otp")
        Observable<SendOtpResponse> sendOtp(@Body SendOtpRequest sendOtpRequest);

        @GET("trxApi/createRedemption")
        Observable<CreateRedemptionResponse> createRedemptionNew(
                                                              @Query("investId") String investId,
                                                              @Query("token_otp") String token_otp,
                                                              @Query("token") String token);

        @GET("trxApi/createPartialRedemption")
        Observable<CreateRedemptionResponse> createPartialRedemptionNew(
                                                                     @Query("investId") String investId,
                                                                     @Query("percentage") Double percentage,
                                                                     @Query("token_otp") String token_otp,
                                                                     @Query("token") String token);


        @POST("payment")
        Observable<TrxTransMandiriClickPayResponse>  mandiriClickpay(@Body MandiriClickpayRequest mandiriClickpayRequest);


        @POST("payment/getTrxNoMNDC")
        Observable<MandiriClickpayOrderNumberResponse>  getOrderNumberMandiriClickpay(@Body GetOrderNumberMandiriClickpayRequest request);

        @POST("payment")
        Observable<BcaKlikPayDataResponse>  getTransactionBcaKlikpay(@Body BcaKlikpayTransRequest request);


        @POST("customersApi/getInvestmentListByProductType")
        Observable<InvestmentListByProductTypeResponse>  getInvestmentListByProductType(@Body InvestmentByProductTypeRequest request);
        //InvestmentListByProductTypeResponse

        @POST("payment/paymentConfirmation")
        Observable<GenericResponse>  paymentConfirmationBcaKlikpay(@Body BcaKlikpayConfirmationRequest request);

        @POST("requestInvAccGroup/validateSubsIfuaReguler")
        Observable<ValidateIfuaRegulerResponse>  getValidateIfuaReguler(@Body ValidateSubscRegulerRequest request);

        //Additional Ersa
        @GET("https://private-16555-erd.apiary-mock.com/getPeriode")
        Observable<PeriodeResp> getPeriode();

    }

}
