package com.danareksa.investasik.data.api.responses;
import com.danareksa.investasik.data.api.beans.ActivationCode;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 03/07/2018.
 */

public class ActivationCodeResponse extends GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private ActivationCode data;

    public ActivationCode getData() {
        return data;
    }

    public void setData(ActivationCode data) {
        this.data = data;
    }
}
