package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.Article;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by asep.surahman on 30/04/2018.
 */

public class ArticleResponse extends com.danareksa.investasik.data.api.responses.GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private List<Article> data = new ArrayList<>();


    public List<Article> getData() {
        return data;
    }

    public void setData(List<Article> data) {
        this.data = data;
    }
}
