package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TransactionHistoryData implements Serializable{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("ifua")
    @Expose
    private String ifua;
    @SerializedName("transactions")
    @Expose
    private List<TransactionHistory> transactions = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIfua() {
        return ifua;
    }

    public void setIfua(String ifua) {
        this.ifua = ifua;
    }

    public List<TransactionHistory> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<TransactionHistory> transactions) {
        this.transactions = transactions;
    }
}
