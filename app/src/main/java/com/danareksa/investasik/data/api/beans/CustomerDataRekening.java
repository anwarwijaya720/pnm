package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by asep.surahman on 25/07/2018.
 */

public class CustomerDataRekening implements Serializable{

    @SerializedName("nextAutoDebit")
    @Expose
    private String nextAutoDebit;

    @SerializedName("cif")
    @Expose
    private String cif;

    @SerializedName("fullName")
    @Expose
    private String fullName;

    @SerializedName("idNumber")
    @Expose
    private String idNumber;

    @SerializedName("bankAccount")
    @Expose
    private List<BankAccount> bankAccount;

    public String getNextAutoDebit() {
        return nextAutoDebit;
    }

    public void setNextAutoDebit(String nextAutoDebit) {
        this.nextAutoDebit = nextAutoDebit;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public List<BankAccount> getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(List<BankAccount> bankAccount) {
        this.bankAccount = bankAccount;
    }
}
