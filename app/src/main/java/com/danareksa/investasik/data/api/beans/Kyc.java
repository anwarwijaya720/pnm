package com.danareksa.investasik.data.api.beans;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by fajarfatur on 2/2/16.
 */
public class Kyc{

    /*
    @SerializedName("idImage")
    @Expose
    private String idImage;

    @SerializedName("selfieImage")
    @Expose
    private String selfieImage;

    @SerializedName("taxIdImage")
    @Expose
    private String taxIdImage;*/

    @SerializedName("appsType")
    @Expose
    private String appsType;

    @SerializedName("beneficiaryRelationshipOther")
    @Expose
    private String beneficiaryRelationshipOther;

    @SerializedName("idImage")
    @Expose
    private ImageIdUpload idImage;

    @SerializedName("selfieImage")
    @Expose
    private ImageSelfieUpload selfieImage;

    @SerializedName("taxIdImage")
    @Expose
    private ImageTaxIdUpload taxIdImage;

    //new
    @SerializedName("heirInvestmentGoal")
    @Expose
    private String heirInvestmentGoal;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("citizenship")
    @Expose
    private String citizenship;

    @SerializedName("idType")
    @Expose
    private String idType;

    @SerializedName("idNumber")
    @Expose
    private String idNumber;

    @SerializedName("taxId")
    @Expose
    private String taxId;

    @SerializedName("nationality")
    @Expose
    private String nationality;

    @SerializedName("idImageKey")
    @Expose
    private String idImageKey;

    @SerializedName("sefieImageKey")
    @Expose
    private String sefieImageKey;

    @SerializedName("taxIdImageKey")
    @Expose
    private String taxIdImageKey;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("birthPlace")
    @Expose
    private String birthPlace;

    @SerializedName("birthDate")
    @Expose
    private String birthDate;

    @SerializedName("religion")
    @Expose
    private String religion;

    @SerializedName("maritalStatus")
    @Expose
    private String maritalStatus;

    @SerializedName("motherName")
    @Expose
    private String motherName;

    @SerializedName("education")
    @Expose
    private String education;

    @SerializedName("annualIncome")
    @Expose
    private String annualIncome;

    @SerializedName("fundSource")
    @Expose
    private String fundSource;

    @SerializedName("investmentPurpose")
    @Expose
    private String investmentPurpose;

    @SerializedName("idAddress")
    @Expose
    private String idAddress;

    @SerializedName("idCountry")
    @Expose
    private String idCountry;

    @SerializedName("idProvince")
    @Expose
    private String idProvince;

    @SerializedName("idCity")
    @Expose
    private String idCity;

    @SerializedName("idPostalCode")
    @Expose
    private String idPostalCode;

    @SerializedName("occupation")
    @Expose
    private String occupation;

    @SerializedName("employer")
    @Expose
    private String employer;

    @SerializedName("jobPosition")
    @Expose
    private String jobPosition;

    @SerializedName("officeAddress")
    @Expose
    private String officeAddress;

    @SerializedName("officeCountry")
    @Expose
    private String officeCountry;

    @SerializedName("officeProvince")
    @Expose
    private String officeProvince;

    @SerializedName("officeCity")
    @Expose
    private String officeCity;

    @SerializedName("officePostalCode")
    @Expose
    private String officePostalCode;


    @SerializedName("additionalIncome")
    @Expose
    private String additionalIncome;

    @SerializedName("additionalFundSource")
    @Expose
    private String additionalFundSource;

    @SerializedName("bankAccountList")
    @Expose
    private List<BankAccountList> bankAccountLists;

    @SerializedName("mailingCountry")
    @Expose
    private String mailingCountry;

    @SerializedName("mailingProvince")
    @Expose
    private String mailingProvince;

    @SerializedName("mailingCity")
    @Expose
    private String mailingCity;

    @SerializedName("mailingAddress")
    @Expose
    private String mailingAddress;

    @SerializedName("mailingPostalCode")
    @Expose
    private String mailingPostalCode;

    @SerializedName("mailingAddressType")
    @Expose
    private String mailingAddressType;

    @SerializedName("fundSourceName")
    @Expose
    private String fundSourceName;

    @SerializedName("fundSourceRelationship")
    @Expose
    private String fundSourceRelationship;

    @SerializedName("fundSourceIdType")
    @Expose
    private String fundSourceIdType;

    @SerializedName("fundSourceIdNumber")
    @Expose
    private String fundSourceIdNumber;

    @SerializedName("fundSourceOccupation")
    @Expose
    private String fundSourceOccupation;

    @SerializedName("fundSourceEmployer")
    @Expose
    private String fundSourceEmployer;

    @SerializedName("fundSourceOfFundSource")
    @Expose
    private String fundSourceOfFundSource;

    @SerializedName("beneficiaryType")
    @Expose
    private String beneficiaryType;

    @SerializedName("beneficiaryName")
    @Expose
    private String beneficiaryName;

    @SerializedName("beneficiaryRelationship")
    @Expose
    private String beneficiaryRelationship;

    @SerializedName("beneficiaryIdType")
    @Expose
    private String beneficiaryIdType;

    @SerializedName("beneficiaryIdNumber")
    @Expose
    private String beneficiaryIdNumber;

    @SerializedName("beneficiaryOccupation")
    @Expose
    private String beneficiaryOccupation;

    @SerializedName("beneficiaryEmployer")
    @Expose
    private String beneficiaryEmployer;

    @SerializedName("beneficiaryFundSource")
    @Expose
    private String beneficiaryFundSource;

    @SerializedName("beneficiaryAnnualIncome")
    @Expose
    private String beneficiaryAnnualIncome;

    @SerializedName("heirName")
    @Expose
    private String heirName;

    @SerializedName("heirMobileNumber")
    @Expose
    private String heirMobileNumber;

    @SerializedName("heirRelationship")
    @Expose
    private String heirRelationship;

    @SerializedName("heirRelationshipOther")
    @Expose
    private String heirRelationshipOther;

    @SerializedName("riskProfile")
    @Expose
    private String riskProfile;

    @SerializedName("riskProfileName")
    @Expose
    private String riskProfileName;

    @SerializedName("riskProfileDescription")
    @Expose
    private String riskProfileDescription;

    @SerializedName("riskProfileScore")
    @Expose
    private String riskProfileScore;

    @SerializedName("riskQuestionnaire")
    @Expose
    private List<RiskQuestionnaire> riskQuestionnaire;

    @SerializedName("riskQuestionnaireList")
    @Expose
    private List<RiskQuestionnaireInt> riskQuestionnaireList;


    //bank
    @SerializedName("bankAccountImageKey")
    @Expose
    private String bankAccountImageKey;

    @SerializedName("bankAccountNumber")
    @Expose
    private String bankAccountNumber;

    @SerializedName("bankBranchOther")
    @Expose
    private String bankBranchOther;

    @SerializedName("bankAccountId")
    @Expose
    private String bankAccountId;

    @SerializedName("bankAccountName")
    @Expose
    private String bankAccountName;

    @SerializedName("bankBranch")
    @Expose
    private String bankBranch;

    @SerializedName("bankId")
    @Expose
    private String bankId;



    //========================================
    //old KYC

    @SerializedName("id")
    @Expose
    private int id = 0;
    @SerializedName("salutation")
    @Expose
    private String salutation;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("middleName")
    @Expose
    private String middleName;
    @SerializedName("lastName")
    @Expose
    private String lastName;

    @SerializedName("natureOfBusiness")
    @Expose
    private String natureOfBusiness;
    @SerializedName("employerName")
    @Expose
    private String employerName;
    @SerializedName("homeCountry")
    @Expose
    private String homeCountry;
    @SerializedName("homeProvince")
    @Expose
    private String homeProvince;
    @SerializedName("homeCity")
    @Expose
    private String homeCity;
    @SerializedName("homeAddress")
    @Expose
    private String homeAddress;
    @SerializedName("homePostalCode")
    @Expose
    private String homePostalCode;
    @SerializedName("homePhoneNumber")
    @Expose
    private String homePhoneNumber;

    @SerializedName("legalCountry")
    @Expose
    private String legalCountry;
    @SerializedName("legalProvince")
    @Expose
    private String legalProvince;
    @SerializedName("legalCity")
    @Expose
    private String legalCity;
    @SerializedName("legalAddress")
    @Expose
    private String legalAddress;
    @SerializedName("legalPostalCode")
    @Expose
    private String legalPostalCode;
    @SerializedName("legalPhoneNumber")
    @Expose
    private String legalPhoneNumber;

    @SerializedName("idExpirationDate")
    @Expose
    private String idExpirationDate;


    @SerializedName("sourceOfIncome")
    @Expose
    private String sourceOfIncome;

    @SerializedName("totalIncomePa")
    @Expose
    private String totalIncomePa;

    @SerializedName("totalAsset")
    @Expose
    private String totalAsset;
    @SerializedName("investmentExperience")
    @Expose
    private String investmentExperience;
    @SerializedName("otherInvestmentExperience")
    @Expose
    private String otherInvesmentExperience;
    @SerializedName("pepName")
    @Expose
    private String pepName;
    @SerializedName("pepPosition")
    @Expose
    private String pepPosition;
    @SerializedName("pepPublicFunction")
    @Expose
    private String pepPublicFunction;
    @SerializedName("pepCountry")
    @Expose
    private PepCountry pepCountry;
    @SerializedName("pepYearOfService")
    @Expose
    private String pepYearOfService;
    @SerializedName("pepRelationship")
    @Expose
    private String pepRelationship;

    @SerializedName("preferredMailingAddress")
    @Expose
    private String preferredMailingAddress;

    @SerializedName("referral")
    @Expose
    private String referral;
    @SerializedName("referralName")
    @Expose
    private String referralName;

    @SerializedName("spouseName")
    @Expose
    private String spouseName;

    @SerializedName("branchId")
    @Expose
    private String branchId;

    @SerializedName("settlementAccountName")
    @Expose
    private String settlementAccountName;

    @SerializedName("settlementAccountNo")
    @Expose
    private String settlementAccountNo;

    public String getAnnualIncome() {
        return annualIncome;
    }

    public void setAnnualIncome(String annualIncome) {
        this.annualIncome = annualIncome;
    }

    //new
    private String idPhoto;
    private String signaturePhoto;
    private int totalCompleteness;

    public String getIdPhoto() {
        return idPhoto;
    }

    public void setIdPhoto(String idPhoto) {
        this.idPhoto = idPhoto;
    }

    public String getSignaturePhoto() {
        return signaturePhoto;
    }

    public void setSignaturePhoto(String signaturePhoto) {
        this.signaturePhoto = signaturePhoto;
    }

    public int getTotalCompleteness() {
        return totalCompleteness;
    }

    public void setTotalCompleteness(int totalCompleteness) {
        this.totalCompleteness = totalCompleteness;
    }

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The salutation
     */
    public String getSalutation() {
        return salutation;
    }

    /**
     * @param salutation The salutation
     */
    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    /**
     * @return The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName The firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return The middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * @param middleName The middleName
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * @return The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName The lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The birthDate
     */
    public String getBirthDate() {
        return birthDate;
    }

    /**
     * @param birthDate The birthDate
     */
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * @return The birthPlace
     */
    public String getBirthPlace() {
        return birthPlace;
    }

    /**
     * @param birthPlace The birthPlace
     */
    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    /**
     * @return The occupation
     */
    public String getOccupation() {
        return occupation;
    }

    /**
     * @param occupation The occupation
     */
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    /**
     * @return The natureOfBusiness
     */
    public String getNatureOfBusiness() {
        return natureOfBusiness;
    }

    /**
     * @param natureOfBusiness The natureOfBusiness
     */
    public void setNatureOfBusiness(String natureOfBusiness) {
        this.natureOfBusiness = natureOfBusiness;
    }

    /**
     * @return The employerName
     */
    public String getEmployerName() {
        return employerName;
    }

    /**s
     */
    public void setEmployerName(String employerName) {
        this.employerName = employerName;
    }

    /**
     * @return The homeCountry
     */
    public String getHomeCountry() {
        return homeCountry;
    }

    /**
     * @param homeCountry The homeCountry
     */
    public void setHomeCountry(String homeCountry) {
        this.homeCountry = homeCountry;
    }

    /**
     * @return The homeProvince
     */
    public String getHomeProvince() {
        return homeProvince;
    }

    /**
     * @param homeProvince The homeProvince
     */
    public void setHomeProvince(String homeProvince) {
        this.homeProvince = homeProvince;
    }

    /**
     * @return The homeCity
     */
    public String getHomeCity() {
        return homeCity;
    }

    /**
     * @param homeCity The homeCity
     */
    public void setHomeCity(String homeCity) {
        this.homeCity = homeCity;
    }

    /**
     * @return The homeAddress
     */
    public String getHomeAddress() {
        return homeAddress;
    }

    /**
     * @param homeAddress The homeAddress
     */
    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    /**
     * @return The homePostalCode
     */
    public String getHomePostalCode() {
        return homePostalCode;
    }

    /**
     * @param homePostalCode The homePostalCode
     */
    public void setHomePostalCode(String homePostalCode) {
        this.homePostalCode = homePostalCode;
    }

    /**
     * @return The homePhoneNumber
     */
    public String getHomePhoneNumber() {
        return homePhoneNumber;
    }

    /**
     * @param homePhoneNumber The homePhoneNumber
     */
    public void setHomePhoneNumber(String homePhoneNumber) {
        this.homePhoneNumber = homePhoneNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return The legalCountry
     */
    public String getLegalCountry() {
        return legalCountry;
    }

    /**
     * @param legalCountry The legalCountry
     */
    public void setLegalCountry(String legalCountry) {
        this.legalCountry = legalCountry;
    }

    public PepCountry getPepCountry() {
        return pepCountry;
    }

    public void setPepCountry(PepCountry pepCountry) {
        //Handling if pepCountry object is null
        if(pepCountry==null){
         this.pepCountry =  new PepCountry(0);
        }else {
            this.pepCountry = pepCountry;
        }
    }

    /**
     * @return The legalProvince
     */
    public String getLegalProvince() {
        return legalProvince;
    }

    /**
     * @param legalProvince The legalProvince
     */
    public void setLegalProvince(String legalProvince) {
        this.legalProvince = legalProvince;
    }

    /**
     * @return The legalCity
     */
    public String getLegalCity() {
        return legalCity;
    }

    /**
     * @param legalCity The legalCity
     */
    public void setLegalCity(String legalCity) {
        this.legalCity = legalCity;
    }

    /**
     * @return The legalAddress
     */
    public String getLegalAddress() {
        return legalAddress;
    }

    /**
     * @param legalAddress The legalAddress
     */
    public void setLegalAddress(String legalAddress) {
        this.legalAddress = legalAddress;
    }

    /**
     * @return The legalPostalCode
     */
    public String getLegalPostalCode() {
        return legalPostalCode;
    }

    /**
     * @param legalPostalCode The legalPostalCode
     */
    public void setLegalPostalCode(String legalPostalCode) {
        this.legalPostalCode = legalPostalCode;
    }

    /**
     * @return The legalPhoneNumber
     */
    public String getLegalPhoneNumber() {
        return legalPhoneNumber;
    }

    /**
     * @param legalPhoneNumber The legalPhoneNumber
     */
    public void setLegalPhoneNumber(String legalPhoneNumber) {
        this.legalPhoneNumber = legalPhoneNumber;
    }

    /**
     * @return The idType
     */
    public String getIdType() {
        return idType;
    }

    /**
     * @param idType The idType
     */
    public void setIdType(String idType) {
        this.idType = idType;
    }

    /**
     * @return The idNumber
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * @param idNumber The idNumber
     */
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    /**
     * @return The idExpirationDate
     */
    public String getIdExpirationDate() {
        return idExpirationDate;
    }

    /**
     * @param idExpirationDate The idExpirationDate
     */
    public void setIdExpirationDate(String idExpirationDate) {
        this.idExpirationDate = idExpirationDate;
    }

    /**
     * @return The nationality
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * @param nationality The nationality
     */
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    /**
     * @return The citizenship
     */
    public String getCitizenship() {
        return citizenship;
    }

    /**
     * @param citizenship The citizenship
     */
    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    /**
     * @return The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return The religion
     */
    public String getReligion() {
        return religion;
    }

    /**
     * @param religion The religion
     */
    public void setReligion(String religion) {
        this.religion = religion;
    }

    /**
     * @return The maritalStatus
     */
    public String getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * @param maritalStatus The maritalStatus
     */
    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }



    /**
     * @return The sourceOfIncome
     */
    public String getSourceOfIncome() {
        return sourceOfIncome;
    }

    /**
     * @param sourceOfIncome The sourceOfIncome
     */
    public void setSourceOfIncome(String sourceOfIncome) {
        this.sourceOfIncome = sourceOfIncome;
    }

    /**
     * @return The totalIncomePa
     */
    public String getTotalIncomePa() {
        return totalIncomePa;
    }

    /**
     * @param totalIncomePa The totalIncomePa
     */
    public void setTotalIncomePa(String totalIncomePa) {
        this.totalIncomePa = totalIncomePa;
    }

    /**
     * @return The investmentPurpose
     */
    public String getInvestmentPurpose() {
        return investmentPurpose;
    }

    /**
     * @param investmentPurpose The investmentPurpose
     */
    public void setInvestmentPurpose(String investmentPurpose) {
        this.investmentPurpose = investmentPurpose;
    }


    public String getOtherInvesmentExperience() {
        return otherInvesmentExperience;
    }

    public void setOtherInvesmentExperience(String otherInvesmentExperience) {
        this.otherInvesmentExperience = otherInvesmentExperience;
    }




    /**
     * @return The beneficiaryName
     */
    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    /**
     * @param beneficiaryName The beneficiaryName
     */
    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    /**
     * @return The beneficiaryRelationship
     */
    public String getBeneficiaryRelationship() {
        return beneficiaryRelationship;
    }

    /**
     * @param beneficiaryRelationship The beneficiaryRelationship
     */
    public void setBeneficiaryRelationship(String beneficiaryRelationship) {
        this.beneficiaryRelationship = beneficiaryRelationship;
    }

    /**
     * @return The preferredMailingAddress
     */
    public String getPreferredMailingAddress() {
        return preferredMailingAddress;
    }

    /**
     * @param preferredMailingAddress The preferredMailingAddress
     */
    public void setPreferredMailingAddress(String preferredMailingAddress) {
        this.preferredMailingAddress = preferredMailingAddress;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public String getTotalAsset() {
        return totalAsset;
    }

    public void setTotalAsset(String totalAsset) {
        this.totalAsset = totalAsset;
    }

    public String getInvestmentExperience() {
        return investmentExperience;
    }

    public void setInvestmentExperience(String investmentExperience) {
        this.investmentExperience = investmentExperience;
    }

    public String getPepName() {
        return pepName;
    }

    public void setPepName(String pepName) {
        this.pepName = pepName;
    }

    public String getPepPosition() {
        return pepPosition;
    }

    public void setPepPosition(String pepPosition) {
        this.pepPosition = pepPosition;
    }

    public String getPepPublicFunction() {
        return pepPublicFunction;
    }

    public void setPepPublicFunction(String pepPublicFunction) {
        this.pepPublicFunction = pepPublicFunction;
    }

    public String getPepYearOfService() {
        return pepYearOfService;
    }

    public void setPepYearOfService(String pepYearOfService) {
        this.pepYearOfService = pepYearOfService;
    }

    public String getPepRelationship() {
        return pepRelationship;
    }

    public void setPepRelationship(String pepRelationship) {
        this.pepRelationship = pepRelationship;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

//    public String getTaxIdRegisDate() {
//        return taxIdRegisDate;
//    }
//
//    public void setTaxIdRegisDate(String taxIdRegisDate) {
//        this.taxIdRegisDate = taxIdRegisDate;
//    }

    public String getReferral() {
        return referral;
    }

    public void setReferral(String referral) {
        this.referral = referral;
    }

    public String getReferralName() {
        return referralName;
    }

    public void setReferralName(String referralName) {
        this.referralName = referralName;
    }

    public String getSpouseName() {
        return spouseName;
    }

    public void setSpouseName(String spouseName) {
        this.spouseName = spouseName;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getSettlementAccountName() {
        return settlementAccountName;
    }

    public void setSettlementAccountName(String settlementAccountName) {
        this.settlementAccountName = settlementAccountName;
    }

    public String getSettlementAccountNo() {
        return settlementAccountNo;
    }

    public void setSettlementAccountNo(String settlementAccountNo) {
        this.settlementAccountNo = settlementAccountNo;
    }

    public String getAdditionalIncome() {
        return additionalIncome;
    }

    public void setAdditionalIncome(String additionalIncome) {
        this.additionalIncome = additionalIncome;
    }


    //getter setter new


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdImageKey() {
        return idImageKey;
    }

    public void setIdImageKey(String idImageKey) {
        this.idImageKey = idImageKey;
    }

    public String getSefieImageKey() {
        return sefieImageKey;
    }

    public void setSefieImageKey(String selfieImageKey) {
        this.sefieImageKey = selfieImageKey;
    }

    public String getTaxIdImageKey() {
        return taxIdImageKey;
    }

    public void setTaxIdImageKey(String taxIdImageKey) {
        this.taxIdImageKey = taxIdImageKey;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getFundSource() {
        return fundSource;
    }

    public void setFundSource(String fundSource) {
        this.fundSource = fundSource;
    }

    public String getIdAddress() {
        return idAddress;
    }

    public void setIdAddress(String idAddress) {
        this.idAddress = idAddress;
    }

    public String getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(String idCountry) {
        this.idCountry = idCountry;
    }

    public String getIdProvince() {
        return idProvince;
    }

    public void setIdProvince(String idProvince) {
        this.idProvince = idProvince;
    }

    public String getIdCity() {
        return idCity;
    }

    public void setIdCity(String idCity) {
        this.idCity = idCity;
    }

    public String getIdPostalCode() {
        return idPostalCode;
    }

    public void setIdPostalCode(String idPostalCode) {
        this.idPostalCode = idPostalCode;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public String getOfficeAddress() {
        return officeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }

    public String getOfficeCountry() {
        return officeCountry;
    }

    public void setOfficeCountry(String officeCountry) {
        this.officeCountry = officeCountry;
    }

    public String getOfficeProvince() {
        return officeProvince;
    }

    public void setOfficeProvince(String officeProvince) {
        this.officeProvince = officeProvince;
    }

    public String getOfficeCity() {
        return officeCity;
    }

    public void setOfficeCity(String officeCity) {
        this.officeCity = officeCity;
    }

    public String getOfficePostalCode() {
        return officePostalCode;
    }

    public void setOfficePostalCode(String officePostalCode) {
        this.officePostalCode = officePostalCode;
    }

    public String getAdditionalFundSource() {
        return additionalFundSource;
    }

    public void setAdditionalFundSource(String additionalFundSource) {
        this.additionalFundSource = additionalFundSource;
    }

    public List<BankAccountList> getBankAccountLists() {
        return bankAccountLists;
    }

    public void setBankAccountLists(List<BankAccountList> bankAccountLists) {
        this.bankAccountLists = bankAccountLists;
    }

    public String getMailingCountry() {
        return mailingCountry;
    }

    public void setMailingCountry(String mailingCountry) {
        this.mailingCountry = mailingCountry;
    }

    public String getMailingProvince() {
        return mailingProvince;
    }

    public void setMailingProvince(String mailingProvince) {
        this.mailingProvince = mailingProvince;
    }

    public String getMailingCity() {
        return mailingCity;
    }

    public void setMailingCity(String mailingCity) {
        this.mailingCity = mailingCity;
    }

    public String getMailingAddress() {
        return mailingAddress;
    }

    public void setMailingAddress(String mailingAddress) {
        this.mailingAddress = mailingAddress;
    }

    public String getMailingPostalCode() {
        return mailingPostalCode;
    }

    public void setMailingPostalCode(String mailingPostalCode) {
        this.mailingPostalCode = mailingPostalCode;
    }

    public String getMailingAddressType() {
        return mailingAddressType;
    }

    public void setMailingAddressType(String mailingAddressType) {
        this.mailingAddressType = mailingAddressType;
    }

    public String getFundSourceName() {
        return fundSourceName;
    }

    public void setFundSourceName(String fundSourceName) {
        this.fundSourceName = fundSourceName;
    }

    public String getFundSourceRelationship() {
        return fundSourceRelationship;
    }

    public void setFundSourceRelationship(String fundSourceRelationship) {
        this.fundSourceRelationship = fundSourceRelationship;
    }

    public String getFundSourceIdType() {
        return fundSourceIdType;
    }

    public void setFundSourceIdType(String fundSourceIdType) {
        this.fundSourceIdType = fundSourceIdType;
    }

    public String getFundSourceIdNumber() {
        return fundSourceIdNumber;
    }

    public void setFundSourceIdNumber(String fundSourceIdNumber) {
        this.fundSourceIdNumber = fundSourceIdNumber;
    }

    public String getFundSourceOccupation() {
        return fundSourceOccupation;
    }

    public void setFundSourceOccupation(String fundSourceOccupation) {
        this.fundSourceOccupation = fundSourceOccupation;
    }

    public String getFundSourceEmployer() {
        return fundSourceEmployer;
    }

    public void setFundSourceEmployer(String fundSourceEmployer) {
        this.fundSourceEmployer = fundSourceEmployer;
    }

    public String getFundSourceOfFundSource() {
        return fundSourceOfFundSource;
    }

    public void setFundSourceOfFundSource(String fundSourceOfFundSource) {
        this.fundSourceOfFundSource = fundSourceOfFundSource;
    }

    public String getBeneficiaryType() {
        return beneficiaryType;
    }

    public void setBeneficiaryType(String beneficiaryType) {
        this.beneficiaryType = beneficiaryType;
    }

    public String getBeneficiaryIdType() {
        return beneficiaryIdType;
    }

    public void setBeneficiaryIdType(String beneficiaryIdType) {
        this.beneficiaryIdType = beneficiaryIdType;
    }

    public String getBeneficiaryIdNumber() {
        return beneficiaryIdNumber;
    }

    public void setBeneficiaryIdNumber(String beneficiaryIdNumber) {
        this.beneficiaryIdNumber = beneficiaryIdNumber;
    }

    public String getBeneficiaryOccupation() {
        return beneficiaryOccupation;
    }

    public void setBeneficiaryOccupation(String beneficiaryOccupation) {
        this.beneficiaryOccupation = beneficiaryOccupation;
    }

    public String getBeneficiaryEmployer() {
        return beneficiaryEmployer;
    }

    public void setBeneficiaryEmployer(String beneficiaryEmployer) {
        this.beneficiaryEmployer = beneficiaryEmployer;
    }

    public String getBeneficiaryFundSource() {
        return beneficiaryFundSource;
    }

    public void setBeneficiaryFundSource(String beneficiaryFundSource) {
        this.beneficiaryFundSource = beneficiaryFundSource;
    }

    public String getBeneficiaryAnnualIncome() {
        return beneficiaryAnnualIncome;
    }

    public void setBeneficiaryAnnualIncome(String beneficiaryAnnualIncome) {
        this.beneficiaryAnnualIncome = beneficiaryAnnualIncome;
    }

    public String getHeirName() {
        return heirName;
    }

    public void setHeirName(String heirName) {
        this.heirName = heirName;
    }

    public String getHeirMobileNumber() {
        return heirMobileNumber;
    }

    public void setHeirMobileNumber(String heirMobileNumber) {
        this.heirMobileNumber = heirMobileNumber;
    }

    public String getHeirRelationship() {
        return heirRelationship;
    }

    public void setHeirRelationship(String heirRelationship) {
        this.heirRelationship = heirRelationship;
    }

    public String getHeirRelationshipOther() {
        return heirRelationshipOther;
    }

    public void setHeirRelationshipOther(String heirRelationshipOther) {
        this.heirRelationshipOther = heirRelationshipOther;
    }

    public String getRiskProfile() {
        return riskProfile;
    }

    public void setRiskProfile(String riskProfile) {
        this.riskProfile = riskProfile;
    }

    public String getRiskProfileName() {
        return riskProfileName;
    }

    public void setRiskProfileName(String riskProfileName) {
        this.riskProfileName = riskProfileName;
    }

    public String getRiskProfileDescription() {
        return riskProfileDescription;
    }

    public void setRiskProfileDescription(String riskProfileDescription) {
        this.riskProfileDescription = riskProfileDescription;
    }

    public String getRiskProfileScore() {
        return riskProfileScore;
    }

    public void setRiskProfileScore(String riskProfileScore) {
        this.riskProfileScore = riskProfileScore;
    }

   /* public List<RiskQuestionnaireList> getRiskQuestionnaireLists() {
        return riskQuestionnaireLists;
    }

    public void setRiskQuestionnaireLists(List<RiskQuestionnaireList> riskQuestionnaireLists) {
        this.riskQuestionnaireLists = riskQuestionnaireLists;
    }*/

    public String getBankAccountImageKey() {
        return bankAccountImageKey;
    }

    public void setBankAccountImageKey(String bankAccountImageKey) {
        this.bankAccountImageKey = bankAccountImageKey;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }



    public String getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(String bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

  /*  public String getIdImage() {
        return idImage;
    }

    public void setIdImage(String idImage) {
        this.idImage = idImage;
    }

    public String getSelfieImage() {
        return selfieImage;
    }

    public void setSelfieImage(String selfieImage) {
        this.selfieImage = selfieImage;
    }

    public String getTaxIdImage() {
        return taxIdImage;
    }

    public void setTaxIdImage(String taxIdImage) {
        this.taxIdImage = taxIdImage;
    }*/

    public ImageIdUpload getIdImage() {
        return idImage;
    }

    public void setIdImage(ImageIdUpload idImage) {
        this.idImage = idImage;
    }

    public ImageSelfieUpload getSelfieImage() {
        return selfieImage;
    }

    public void setSelfieImage(ImageSelfieUpload selfieImage) {
        this.selfieImage = selfieImage;
    }

    public ImageTaxIdUpload getTaxIdImage() {
        return taxIdImage;
    }

    public void setTaxIdImage(ImageTaxIdUpload taxIdImage) {
        this.taxIdImage = taxIdImage;
    }

    public String getHeirInvestmentGoal() {
        return heirInvestmentGoal;
    }

    public void setHeirInvestmentGoal(String heirInvestmentGoal) {
        this.heirInvestmentGoal = heirInvestmentGoal;
    }

    public List<RiskQuestionnaire> getRiskQuestionnaire() {
        return riskQuestionnaire;
    }

    public void setRiskQuestionnaire(List<RiskQuestionnaire> riskQuestionnaire) {
        this.riskQuestionnaire = riskQuestionnaire;
    }

    public List<RiskQuestionnaireInt> getRiskQuestionnaireList() {
        return riskQuestionnaireList;
    }

    public void setRiskQuestionnaireList(List<RiskQuestionnaireInt> riskQuestionnaireList) {
        this.riskQuestionnaireList = riskQuestionnaireList;
    }

    public String getAppsType() {
        return appsType;
    }

    public void setAppsType(String appsType) {
        this.appsType = appsType;
    }


    public String getBankBranchOther() {
        return bankBranchOther;
    }

    public void setBankBranchOther(String bankBranchOther) {
        this.bankBranchOther = bankBranchOther;
    }

    public String getBeneficiaryRelationshipOther() {
        return beneficiaryRelationshipOther;
    }

    public void setBeneficiaryRelationshipOther(String beneficiaryRelationshipOther) {
        this.beneficiaryRelationshipOther = beneficiaryRelationshipOther;
    }
}
