package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.Slideshow;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by asep.surahman on 28/06/2018.
 */

public class SlideshowResponse extends GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private List<Slideshow> data = new ArrayList<>();

    public List<Slideshow> getData() {
        return data;
    }

    public void setData(List<Slideshow> data) {
        this.data = data;
    }

}
