package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.Products;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pandu.abbiyuarsyah on 17/03/2017.
 */

public class ProductResponse extends com.danareksa.investasik.data.api.responses.GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private List<Products> data = new ArrayList<>();

    /**
     * @return The data
     */
    public List<Products> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<Products> data) {
        this.data = data;
    }

}
