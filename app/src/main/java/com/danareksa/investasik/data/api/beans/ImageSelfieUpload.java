package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 18/07/2018.
 */

public class ImageSelfieUpload {

    @SerializedName("file_name")
    @Expose
    private String file_name;
    @SerializedName("content")
    @Expose
    private String content;

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
