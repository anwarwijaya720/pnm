package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.DataRedeemFee;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ulfah.ulmi on 12/04/2017.
 */

public class RedeemFeeResponse extends com.danareksa.investasik.data.api.responses.GenericResponse implements Serializable{
    @SerializedName("data")
    @Expose
    private DataRedeemFee data;

    public DataRedeemFee getData() {
        return data;
    }

    public void setData(DataRedeemFee data) {
        this.data = data;
    }
}
