package com.danareksa.investasik.data.api.beans;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 03/08/2018.
 */

public class HeirList implements Serializable{

    @SerializedName("heirName")
    @Expose
    private String heirName;

    @SerializedName("heirRelationship")
    @Expose
    private String heirRelationship;

    @SerializedName("heirRelationshipDetail")
    @Expose
    private String heirRelationshipDetail;

    @SerializedName("heirMobileNumber")
    @Expose
    private String heirMobileNumber;

    public String getHeirName() {
        return heirName;
    }

    public void setHeirName(String heirName) {
        this.heirName = heirName;
    }

    public String getHeirRelationship() {
        return heirRelationship;
    }

    public void setHeirRelationship(String heirRelationship) {
        this.heirRelationship = heirRelationship;
    }

    public String getHeirMobileNumber() {
        return heirMobileNumber;
    }

    public void setHeirMobileNumber(String heirMobileNumber) {
        this.heirMobileNumber = heirMobileNumber;
    }

    public String getHeirRelationshipDetail() {
        return heirRelationshipDetail;
    }

    public void setHeirRelationshipDetail(String heirRelationshipDetail) {
        this.heirRelationshipDetail = heirRelationshipDetail;
    }
}
