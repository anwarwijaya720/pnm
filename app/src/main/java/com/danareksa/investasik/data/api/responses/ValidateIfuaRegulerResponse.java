package com.danareksa.investasik.data.api.responses;
import com.danareksa.investasik.data.api.beans.ValidateIfuaReguler;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 31/10/2018.
 */

public class ValidateIfuaRegulerResponse extends GenericResponse {

    @SerializedName("data")
    @Expose
    private ValidateIfuaReguler data;

    public ValidateIfuaReguler getData() {
        return data;
    }

    public void setData(ValidateIfuaReguler data) {
        this.data = data;
    }
}
