package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Rizal Gunawan on 7/25/18.
 */

public class InvestmentList {

    @SerializedName("unitHolderNumber")
    @Expose
    private String unitHolderNumber;
    @SerializedName("investmentList")
    @Expose
    private List<PortfolioInvestment> investmentList = null;
    @SerializedName("investmentGoal")
    @Expose
    private String investmentGoal;
    @SerializedName("accountType")
    @Expose
    private String accountType;
    @SerializedName("ifua")
    @Expose
    private String ifua;
    @SerializedName("totalInvestment")
    @Expose
    private Double totalInvestment;
    @SerializedName("invAccGroupId")
    @Expose
    private Integer invAccGroupId;

    public String getUnitHolderNumber() {
        return unitHolderNumber;
    }

    public void setUnitHolderNumber(String unitHolderNumber) {
        this.unitHolderNumber = unitHolderNumber;
    }

    public List<PortfolioInvestment> getInvestmentList() {
        return investmentList;
    }

    public void setInvestmentList(List<PortfolioInvestment> investmentList) {
        this.investmentList = investmentList;
    }

    public String getInvestmentGoal() {
        return investmentGoal;
    }

    public void setInvestmentGoal(String investmentGoal) {
        this.investmentGoal = investmentGoal;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getIfua() {
        return ifua;
    }

    public void setIfua(String ifua) {
        this.ifua = ifua;
    }

    public Double getTotalInvestment() {
        return totalInvestment;
    }

    public void setTotalInvestment(Double totalInvestment) {
        this.totalInvestment = totalInvestment;
    }

    public Integer getInvAccGroupId() {
        return invAccGroupId;
    }

    public void setInvAccGroupId(Integer invAccGroupId) {
        this.invAccGroupId = invAccGroupId;
    }
}
