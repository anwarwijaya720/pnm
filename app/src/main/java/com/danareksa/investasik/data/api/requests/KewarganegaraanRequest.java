package com.danareksa.investasik.data.api.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 14/07/2018.
 */

public class KewarganegaraanRequest implements Serializable{

    @SerializedName("citizenship")  //wni = DOM  wna = FRG
    @Expose
    private String citizenship;

    @SerializedName("idType")  //ktp = IDC   paspor = PSP
    @Expose
    private String idType;

    @SerializedName("idNumber") //No KTP/No paspor
    @Expose
    private String idNumber;

    @SerializedName("taxId")  //NPWP
    @Expose
    private String taxId;

    @SerializedName("nationality")  //Kode negara kebangsaan /country
    @Expose                         //indonesia = 52
    private String nationality;


    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

}
