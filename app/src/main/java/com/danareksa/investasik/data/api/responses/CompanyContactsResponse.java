package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.CompanyContacts;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Rizal Gunawan on 7/24/18.
 */

public class CompanyContactsResponse implements Serializable {

    @SerializedName("code")
    private Integer code;

    @SerializedName("info")
    private String info;

    @SerializedName("data")
    private CompanyContacts data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public CompanyContacts getData() {
        return data;
    }

    public void setData(CompanyContacts data) {
        this.data = data;
    }
}
