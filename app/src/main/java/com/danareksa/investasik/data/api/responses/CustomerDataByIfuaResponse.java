package com.danareksa.investasik.data.api.responses;
import com.danareksa.investasik.data.api.beans.CustomerDataByIfua;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 19/09/2018.
 */

public class CustomerDataByIfuaResponse extends GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private CustomerDataByIfua data;

    public CustomerDataByIfua getData() {
        return data;
    }

    public void setData(CustomerDataByIfua data) {
        this.data = data;
    }
}
