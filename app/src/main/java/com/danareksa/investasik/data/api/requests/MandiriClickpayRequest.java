package com.danareksa.investasik.data.api.requests;

import com.danareksa.investasik.data.api.beans.MandiriClickpayDetail;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 27/08/2018.
 */

public class MandiriClickpayRequest {

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("paymentMethod")
    @Expose
    private String paymentMethod;

    @SerializedName("appsType")
    @Expose
    private String appsType;

    @SerializedName("detail")
    @Expose
    private MandiriClickpayDetail detail;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getAppsType() {
        return appsType;
    }

    public void setAppsType(String appsType) {
        this.appsType = appsType;
    }

    public MandiriClickpayDetail getDetail() {
        return detail;
    }

    public void setDetail(MandiriClickpayDetail detail) {
        this.detail = detail;
    }
}
