package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.ListDetail;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pandu.abbiyuarsyah on 07/04/2017.
 */

public class PieChartResponse extends com.danareksa.investasik.data.api.responses.GenericResponse {

    @SerializedName("data")
    @Expose
    private ListDetail data;

    /**
     * @return The data
     */
    public ListDetail getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(ListDetail data) {
        this.data = data;
    }

}
