package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by asep.surahman on 03/08/2018.
 */

public class GroupPackageListReguler implements Serializable{

    @SerializedName("packageListReguler")
    @Expose
    private List<PackageListReguler> packageListReguler;

    public List<PackageListReguler> getPackageListReguler() {
        return packageListReguler;
    }

    public void setPackageListReguler(List<PackageListReguler> packageListReguler) {
        this.packageListReguler = packageListReguler;
    }
}
