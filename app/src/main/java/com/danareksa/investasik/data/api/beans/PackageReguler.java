package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 14/08/2018.
 */

public class PackageReguler implements Serializable{




    @SerializedName("packageImageKey")
    @Expose
    private String packageImageKey;

    @SerializedName("reguler_period")
    @Expose
    private String reguler_period;

    @SerializedName("created_date_only")
    @Expose
    private String created_date_only;

    @SerializedName("monthly_investment_fee_percentage")
    @Expose
    private double monthly_investment_fee_percentage;

    @SerializedName("totalMonthlyAutodebet")
    @Expose
    private double totalMonthlyAutodebet;

    @SerializedName("monthlyInvestmentFee")
    @Expose
    private double monthlyInvestmentFee;

    @SerializedName("packageName")
    @Expose
    private String packageName;

    @SerializedName("monthlyInvestmentAmount")
    @Expose
    private double monthlyInvestmentAmount;

    @SerializedName("order_no")
    @Expose
    private String orderNo;

    public String getReguler_period() {
        return reguler_period;
    }

    public void setReguler_period(String reguler_period) {
        this.reguler_period = reguler_period;
    }

    public String getCreated_date_only() {
        return created_date_only;
    }

    public void setCreated_date_only(String created_date_only) {
        this.created_date_only = created_date_only;
    }

    public double getMonthly_investment_fee_percentage() {
        return monthly_investment_fee_percentage;
    }

    public void setMonthly_investment_fee_percentage(double monthly_investment_fee_percentage) {
        this.monthly_investment_fee_percentage = monthly_investment_fee_percentage;
    }

    public double getTotalMonthlyAutodebet() {
        return totalMonthlyAutodebet;
    }

    public void setTotalMonthlyAutodebet(double totalMonthlyAutodebet) {
        this.totalMonthlyAutodebet = totalMonthlyAutodebet;
    }

    public double getMonthlyInvestmentFee() {
        return monthlyInvestmentFee;
    }

    public void setMonthlyInvestmentFee(double monthlyInvestmentFee) {
        this.monthlyInvestmentFee = monthlyInvestmentFee;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public double getMonthlyInvestmentAmount() {
        return monthlyInvestmentAmount;
    }

    public void setMonthlyInvestmentAmount(double monthlyInvestmentAmount) {
        this.monthlyInvestmentAmount = monthlyInvestmentAmount;
    }

    public String getPackageImageKey() {
        return packageImageKey;
    }

    public void setPackageImageKey(String packageImageKey) {
        this.packageImageKey = packageImageKey;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
