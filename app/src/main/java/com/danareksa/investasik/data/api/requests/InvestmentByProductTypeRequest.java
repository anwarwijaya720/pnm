package com.danareksa.investasik.data.api.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 08/10/2018.
 */

public class InvestmentByProductTypeRequest {

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("productType")
    @Expose
    private Integer productType;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }
}
