package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.InvestmentAccountGroup;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Rizal Gunawan on 7/25/18.
 */

public class InvestmentAccountInfoResponse extends GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private InvestmentAccountGroup data = new InvestmentAccountGroup();

    public InvestmentAccountGroup getData() {
        return data;
    }

    public void setData(InvestmentAccountGroup data) {
        this.data = data;
    }
}