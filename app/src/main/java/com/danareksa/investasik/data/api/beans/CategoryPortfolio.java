package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 22/05/2018.
 */

public class CategoryPortfolio implements Serializable {

    private static final long serialVersionUID = 3992437807543448027L;

    @SerializedName("categoryId")
    @Expose
    private String categoryId;

    @SerializedName("categoryName")
    @Expose
    private String categoryName;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
