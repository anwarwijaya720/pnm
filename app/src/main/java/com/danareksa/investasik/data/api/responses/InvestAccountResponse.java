package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.InvestAccount;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 07/08/2018.
 */

public class InvestAccountResponse extends GenericResponse {

    @SerializedName("data")
    @Expose
    private InvestAccount investAccount;

    public InvestAccount getInvestAccount() {
        return investAccount;
    }

    public void setInvestAccount(InvestAccount investAccount) {
        this.investAccount = investAccount;
    }
}
