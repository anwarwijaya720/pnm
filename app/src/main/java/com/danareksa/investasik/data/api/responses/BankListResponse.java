package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.Bank;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by asep.surahman on 06/08/2018.
 */

public class BankListResponse extends GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private List<Bank> data;

    public List<Bank> getData() {
        return data;
    }

    public void setData(List<Bank> data) {
        this.data = data;
    }
}
