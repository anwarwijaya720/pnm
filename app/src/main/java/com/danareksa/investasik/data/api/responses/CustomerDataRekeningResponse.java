package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.CustomerDataRekening;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 25/07/2018.
 */

public class CustomerDataRekeningResponse extends GenericResponse {


    @SerializedName("data")
    @Expose
    private CustomerDataRekening data;

    public CustomerDataRekening getData(){
        return data;
    }

    public void setData(CustomerDataRekening data){
        this.data = data;
    }

}
