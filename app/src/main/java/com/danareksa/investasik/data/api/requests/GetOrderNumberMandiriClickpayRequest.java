package com.danareksa.investasik.data.api.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 07/09/2018.
 */

public class GetOrderNumberMandiriClickpayRequest {


    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("appsType")
    @Expose
    private String appsType;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAppsType() {
        return appsType;
    }

    public void setAppsType(String appsType) {
        this.appsType = appsType;
    }
}
