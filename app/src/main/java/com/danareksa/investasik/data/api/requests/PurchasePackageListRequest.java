package com.danareksa.investasik.data.api.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 31/10/2018.
 */

public class PurchasePackageListRequest {

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("ifua")
    @Expose
    private String ifua;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIfua() {
        return ifua;
    }

    public void setIfua(String ifua) {
        this.ifua = ifua;
    }
}
