package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.PackagesPerformance;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by fajarfatur on 2/23/16.
 */
public class PackagesPerformanceResponse extends com.danareksa.investasik.data.api.responses.GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private PackagesPerformance data;

    /**
     * @return The data
     */
    public PackagesPerformance getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(PackagesPerformance data) {
        this.data = data;
    }

}
