package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 12/08/2018.
 */

public class TrxTransCode implements Serializable{

    //new
    @SerializedName("adminFee")
    @Expose
    private Double adminFee;
    //end

    @SerializedName("paymentMethodCode")
    @Expose
    private String paymentMethodCode;
    @SerializedName("settlementCutOff")
    @Expose
    private String settlementCutOff;
    @SerializedName("orderNumber")
    @Expose
    private String orderNumber;
    @SerializedName("netAmount")
    @Expose
    private Double netAmount;
    @SerializedName("feePercentage")
    @Expose
    private Double feePercentage;
    @SerializedName("destinationAccountBankName")
    @Expose
    private String destinationAccountBankName;
    @SerializedName("destinationAccountNumber")
    @Expose
    private String destinationAccountNumber;
    @SerializedName("paymentMethodName")
    @Expose
    private String paymentMethodName;
    @SerializedName("transactionTypeCode")
    @Expose
    private String transactionTypeCode;
    @SerializedName("transactionTypeName")
    @Expose
    private String transactionTypeName;
    @SerializedName("packageImageKey")
    @Expose
    private String packageImageKey;
    @SerializedName("destinationAccountName")
    @Expose
    private String destinationAccountName;
    @SerializedName("feeAmount")
    @Expose
    private Double feeAmount;
    @SerializedName("totalAmount")
    @Expose
    private Double totalAmount;
    @SerializedName("ifuaInvestmentGoalName")
    @Expose
    private String ifuaInvestmentGoalName;
    @SerializedName("priceDate")
    @Expose
    private String priceDate;
    @SerializedName("uniqueCode")
    @Expose
    private Integer uniqueCode;
    @SerializedName("ifuaNumber")
    @Expose
    private String ifuaNumber;
    @SerializedName("packageCode")
    @Expose
    private String packageCode;
    @SerializedName("packageName")
    @Expose
    private String packageName;
    @SerializedName("ifuaType")
    @Expose
    private String ifuaType;



    public String getPaymentMethodCode() {
        return paymentMethodCode;
    }

    public void setPaymentMethodCode(String paymentMethodCode) {
        this.paymentMethodCode = paymentMethodCode;
    }

    public String getSettlementCutOff() {
        return settlementCutOff;
    }

    public void setSettlementCutOff(String settlementCutOff) {
        this.settlementCutOff = settlementCutOff;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Double netAmount) {
        this.netAmount = netAmount;
    }

    public Double getFeePercentage() {
        return feePercentage;
    }

    public void setFeePercentage(Double feePercentage) {
        this.feePercentage = feePercentage;
    }

    public String getDestinationAccountBankName() {
        return destinationAccountBankName;
    }

    public void setDestinationAccountBankName(String destinationAccountBankName) {
        this.destinationAccountBankName = destinationAccountBankName;
    }

    public String getDestinationAccountNumber() {
        return destinationAccountNumber;
    }

    public void setDestinationAccountNumber(String destinationAccountNumber) {
        this.destinationAccountNumber = destinationAccountNumber;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    public String getTransactionTypeCode() {
        return transactionTypeCode;
    }

    public void setTransactionTypeCode(String transactionTypeCode) {
        this.transactionTypeCode = transactionTypeCode;
    }

    public String getTransactionTypeName() {
        return transactionTypeName;
    }

    public void setTransactionTypeName(String transactionTypeName) {
        this.transactionTypeName = transactionTypeName;
    }

    public String getPackageImageKey() {
        return packageImageKey;
    }

    public void setPackageImageKey(String packageImageKey) {
        this.packageImageKey = packageImageKey;
    }

    public String getDestinationAccountName() {
        return destinationAccountName;
    }

    public void setDestinationAccountName(String destinationAccountName) {
        this.destinationAccountName = destinationAccountName;
    }

    public Double getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(Double feeAmount) {
        this.feeAmount = feeAmount;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getIfuaInvestmentGoalName() {
        return ifuaInvestmentGoalName;
    }

    public void setIfuaInvestmentGoalName(String ifuaInvestmentGoalName) {
        this.ifuaInvestmentGoalName = ifuaInvestmentGoalName;
    }

    public String getPriceDate() {
        return priceDate;
    }

    public void setPriceDate(String priceDate) {
        this.priceDate = priceDate;
    }

    public Integer getUniqueCode() {
        return uniqueCode;
    }

    public void setUniqueCode(Integer uniqueCode) {
        this.uniqueCode = uniqueCode;
    }

    public String getIfuaNumber() {
        return ifuaNumber;
    }

    public void setIfuaNumber(String ifuaNumber) {
        this.ifuaNumber = ifuaNumber;
    }

    public String getPackageCode() {
        return packageCode;
    }

    public void setPackageCode(String packageCode) {
        this.packageCode = packageCode;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getIfuaType() {
        return ifuaType;
    }

    public void setIfuaType(String ifuaType) {
        this.ifuaType = ifuaType;
    }


    public Double getAdminFee() {
        return adminFee;
    }

    public void setAdminFee(Double adminFee) {
        this.adminFee = adminFee;
    }
}
