package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pandu.abbiyuarsyah on 03/07/2017.
 */

public class MaxScore {

    @SerializedName("maxScoreFundPackage")
    @Expose
    private Integer maxScoreFundPackage;
    @SerializedName("maxScoreKyc")
    @Expose
    private Integer maxScoreKyc;

    public Integer getMaxScoreFundPackage() {
        return maxScoreFundPackage;
    }

    public void setMaxScoreFundPackage(Integer maxScoreFundPackage) {
        this.maxScoreFundPackage = maxScoreFundPackage;
    }

    public Integer getMaxScoreKyc() {
        return maxScoreKyc;
    }

    public void setMaxScoreKyc(Integer maxScoreKyc) {
        this.maxScoreKyc = maxScoreKyc;
    }

}
