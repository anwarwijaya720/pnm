package com.danareksa.investasik.data.api.requests;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 23/08/2018.
 */

public class SendOtpRequest {

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("trx_id")
    @Expose
    private String trx_id;

    @SerializedName("token_otp")
    @Expose
    private String token_otp;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTrx_id() {
        return trx_id;
    }

    public void setTrx_id(String trx_id) {
        this.trx_id = trx_id;
    }

    public String getToken_otp() {
        return token_otp;
    }

    public void setToken_otp(String token_otp) {
        this.token_otp = token_otp;
    }
}
