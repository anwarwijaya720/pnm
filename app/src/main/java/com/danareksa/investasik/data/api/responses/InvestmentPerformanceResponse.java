package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.InvestmentPerformance;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fajarfatur on 3/4/16.
 */
public class InvestmentPerformanceResponse extends com.danareksa.investasik.data.api.responses.GenericResponse {

    @SerializedName("data")
    @Expose
    private List<InvestmentPerformance> data = new ArrayList<>();

    /**
     *
     * @return
     * The data
     */
    public List<InvestmentPerformance> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(List<InvestmentPerformance> data) {
        this.data = data;
    }

}
