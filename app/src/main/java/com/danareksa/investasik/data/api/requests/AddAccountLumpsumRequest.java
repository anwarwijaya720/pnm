package com.danareksa.investasik.data.api.requests;

import com.danareksa.investasik.data.api.beans.AddAccountLumpsum;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 03/08/2018.
 */

public class AddAccountLumpsumRequest extends AddAccountLumpsum implements Serializable {

    @SerializedName("token")
    @Expose
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
