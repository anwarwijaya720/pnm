package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.Packages;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fajarfatur on 2/2/16.
 */
public class PackageResponse extends com.danareksa.investasik.data.api.responses.GenericResponse {

    @SerializedName("data")
    @Expose
    private Packages data;

    /**
     * @return The data
     */
    public Packages getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(Packages data) {
        this.data = data;
    }

}
