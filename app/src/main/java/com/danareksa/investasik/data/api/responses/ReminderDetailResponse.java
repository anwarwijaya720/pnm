package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.ReminderDetail;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pandu.abbiyuarsyah on 08/06/2017.
 */

public class ReminderDetailResponse extends com.danareksa.investasik.data.api.responses.GenericResponse {

    @SerializedName("reminder")
    @Expose
    private ReminderDetail reminder;

    public ReminderDetail getReminder() {
        return reminder;
    }

    public void setReminder (ReminderDetail reminder) {
        this.reminder = reminder;
    }
}
