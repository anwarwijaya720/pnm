package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by glenrynaldi on 2/17/16.
 */
public class Fee implements Serializable {

    @SerializedName("feeAmount")
    @Expose
    private Double feeAmount;
    @SerializedName("amountMin")
    @Expose
    private Double amountMin;
    @SerializedName("amountMax")
    @Expose
    private Double amountMax;

    @SerializedName("dayMin")
    @Expose
    private Integer dayMin;

    @SerializedName("dayMax")
    @Expose
    private Integer dayMax;
    /**
     * @return The feeAmount
     */
    public Double getFeeAmount() {
        return feeAmount;
    }

    /**
     * @param feeAmount The feeAmount
     */
    public void setFeeAmount(Double feeAmount) {
        this.feeAmount = feeAmount;
    }

    public Double getAmountMin() {
        return amountMin;
    }

    public void setAmountMin(Double amountMin) {
        this.amountMin = amountMin;
    }

    public Double getAmountMax() {
        return amountMax;
    }

    public void setAmountMax(Double amountMax) {
        this.amountMax = amountMax;
    }

    public Integer getDayMin() {
        return dayMin;
    }

    public void setDayMin(Integer dayMin) {
        this.dayMin = dayMin;
    }

    public Integer getDayMax() {
        return dayMax;
    }

    public void setDayMax(Integer dayMax) {
        this.dayMax = dayMax;
    }
}
