package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.TopUpViseePay;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pandu.abbiyuarsyah on 18/10/2017.
 */

public class TopUpViseePayResponse extends com.danareksa.investasik.data.api.responses.GenericResponse {

    @SerializedName("data")
    @Expose
    private TopUpViseePay data;

    public TopUpViseePay getData() {
        return data;
    }

    public void setData(TopUpViseePay data) {
        this.data = data;
    }


}
