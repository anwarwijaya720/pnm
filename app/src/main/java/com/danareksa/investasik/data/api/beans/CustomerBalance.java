package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerBalance {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("accruedAmount")
    @Expose
    private Double accruedAmount;
    @SerializedName("accruedUnit")
    @Expose
    private Double accruedUnit;
    @SerializedName("atCustBalId")
    @Expose
    private String atCustBalId;
    @SerializedName("atInvestmentAccountId")
    @Expose
    private String atInvestmentAccountId;
    @SerializedName("atProductId")
    @Expose
    private String atProductId;
    @SerializedName("averageCost")
    @Expose
    private Double averageCost;
    @SerializedName("balanceDate")
    @Expose
    private String balanceDate;
    @SerializedName("priceDate")
    @Expose
    private String priceDate;
    @SerializedName("broughtForwardAmount")
    @Expose
    private Double broughtForwardAmount;
    @SerializedName("broughtForwardUnit")
    @Expose
    private Double broughtForwardUnit;
    @SerializedName("createdBy")
    @Expose
    private String createdBy;
    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("currentAmount")
    @Expose
    private Double currentAmount;
    @SerializedName("currentUnit")
    @Expose
    private Double currentUnit;
    @SerializedName("dividendAmount")
    @Expose
    private Double dividendAmount;
    @SerializedName("dividendUnit")
    @Expose
    private Double dividendUnit;
    @SerializedName("idrAverageCost")
    @Expose
    private Double idrAverageCost;
    @SerializedName("idrCurrentAmount")
    @Expose
    private Double idrCurrentAmount;
    @SerializedName("idrTotalCost")
    @Expose
    private Double idrTotalCost;
    @SerializedName("idrUnrealizedGainLoss")
    @Expose
    private Double idrUnrealizedGainLoss;
    @SerializedName("realizedGainLoss")
    @Expose
    private Double realizedGainLoss;
    @SerializedName("redemptionAmount")
    @Expose
    private Double redemptionAmount;
    @SerializedName("redemptionUnit")
    @Expose
    private Double redemptionUnit;
    @SerializedName("subscriptionAmount")
    @Expose
    private Double subscriptionAmount;
    @SerializedName("subscriptionUnit")
    @Expose
    private Double subscriptionUnit;
    @SerializedName("switchInAmount")
    @Expose
    private Double switchInAmount;
    @SerializedName("switchInUnit")
    @Expose
    private Double switchInUnit;
    @SerializedName("switchOutAmount")
    @Expose
    private Double switchOutAmount;
    @SerializedName("switchOutUnit")
    @Expose
    private Double switchOutUnit;
    @SerializedName("totalCost")
    @Expose
    private Double totalCost;
    @SerializedName("unrealizedGainLoss")
    @Expose
    private Double unrealizedGainLoss;
    @SerializedName("updatedBy")
    @Expose
    private String updatedBy;
    @SerializedName("updatedDate")
    @Expose
    private String updatedDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getAccruedAmount() {
        return accruedAmount;
    }

    public void setAccruedAmount(Double accruedAmount) {
        this.accruedAmount = accruedAmount;
    }

    public Double getAccruedUnit() {
        return accruedUnit;
    }

    public void setAccruedUnit(Double accruedUnit) {
        this.accruedUnit = accruedUnit;
    }

    public String getAtCustBalId() {
        return atCustBalId;
    }

    public void setAtCustBalId(String atCustBalId) {
        this.atCustBalId = atCustBalId;
    }

    public String getAtInvestmentAccountId() {
        return atInvestmentAccountId;
    }

    public void setAtInvestmentAccountId(String atInvestmentAccountId) {
        this.atInvestmentAccountId = atInvestmentAccountId;
    }

    public String getAtProductId() {
        return atProductId;
    }

    public void setAtProductId(String atProductId) {
        this.atProductId = atProductId;
    }

    public Double getAverageCost() {
        return averageCost;
    }

    public void setAverageCost(Double averageCost) {
        this.averageCost = averageCost;
    }

    public String getBalanceDate() {
        return balanceDate;
    }

    public void setBalanceDate(String balanceDate) {
        this.balanceDate = balanceDate;
    }

    public Double getBroughtForwardAmount() {
        return broughtForwardAmount;
    }

    public void setBroughtForwardAmount(Double broughtForwardAmount) {
        this.broughtForwardAmount = broughtForwardAmount;
    }

    public Double getBroughtForwardUnit() {
        return broughtForwardUnit;
    }

    public void setBroughtForwardUnit(Double broughtForwardUnit) {
        this.broughtForwardUnit = broughtForwardUnit;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getPriceDate() {
        return priceDate;
    }

    public void setPriceDate(String priceDate) {
        this.priceDate = priceDate;
    }

    public Double getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(Double currentAmount) {
        this.currentAmount = currentAmount;
    }

    public Double getCurrentUnit() {
        return currentUnit;
    }

    public void setCurrentUnit(Double currentUnit) {
        this.currentUnit = currentUnit;
    }

    public Double getDividendAmount() {
        return dividendAmount;
    }

    public void setDividendAmount(Double dividendAmount) {
        this.dividendAmount = dividendAmount;
    }

    public Double getDividendUnit() {
        return dividendUnit;
    }

    public void setDividendUnit(Double dividendUnit) {
        this.dividendUnit = dividendUnit;
    }

    public Double getIdrAverageCost() {
        return idrAverageCost;
    }

    public void setIdrAverageCost(Double idrAverageCost) {
        this.idrAverageCost = idrAverageCost;
    }

    public Double getIdrCurrentAmount() {
        return idrCurrentAmount;
    }

    public void setIdrCurrentAmount(Double idrCurrentAmount) {
        this.idrCurrentAmount = idrCurrentAmount;
    }

    public Double getIdrTotalCost() {
        return idrTotalCost;
    }

    public void setIdrTotalCost(Double idrTotalCost) {
        this.idrTotalCost = idrTotalCost;
    }

    public Double getIdrUnrealizedGainLoss() {
        return idrUnrealizedGainLoss;
    }

    public void setIdrUnrealizedGainLoss(Double idrUnrealizedGainLoss) {
        this.idrUnrealizedGainLoss = idrUnrealizedGainLoss;
    }

    public Double getRealizedGainLoss() {
        return realizedGainLoss;
    }

    public void setRealizedGainLoss(Double realizedGainLoss) {
        this.realizedGainLoss = realizedGainLoss;
    }

    public Double getRedemptionAmount() {
        return redemptionAmount;
    }

    public void setRedemptionAmount(Double redemptionAmount) {
        this.redemptionAmount = redemptionAmount;
    }

    public Double getRedemptionUnit() {
        return redemptionUnit;
    }

    public void setRedemptionUnit(Double redemptionUnit) {
        this.redemptionUnit = redemptionUnit;
    }

    public Double getSubscriptionAmount() {
        return subscriptionAmount;
    }

    public void setSubscriptionAmount(Double subscriptionAmount) {
        this.subscriptionAmount = subscriptionAmount;
    }

    public Double getSubscriptionUnit() {
        return subscriptionUnit;
    }

    public void setSubscriptionUnit(Double subscriptionUnit) {
        this.subscriptionUnit = subscriptionUnit;
    }

    public Double getSwitchInAmount() {
        return switchInAmount;
    }

    public void setSwitchInAmount(Double switchInAmount) {
        this.switchInAmount = switchInAmount;
    }

    public Double getSwitchInUnit() {
        return switchInUnit;
    }

    public void setSwitchInUnit(Double switchInUnit) {
        this.switchInUnit = switchInUnit;
    }

    public Double getSwitchOutAmount() {
        return switchOutAmount;
    }

    public void setSwitchOutAmount(Double switchOutAmount) {
        this.switchOutAmount = switchOutAmount;
    }

    public Double getSwitchOutUnit() {
        return switchOutUnit;
    }

    public void setSwitchOutUnit(Double switchOutUnit) {
        this.switchOutUnit = switchOutUnit;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public Double getUnrealizedGainLoss() {
        return unrealizedGainLoss;
    }

    public void setUnrealizedGainLoss(Double unrealizedGainLoss) {
        this.unrealizedGainLoss = unrealizedGainLoss;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
}
