package com.danareksa.investasik.data.api.beans;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by asep.surahman on 06/08/2018.
 */

public class HeirListReguler {


    @SerializedName("heirName")
    @Expose
    private String heirName;

    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;

    @SerializedName("relationship")
    @Expose
    private String relationship;

    @SerializedName("relationshipDetail")
    @Expose
    private String relationshipDetail;

    @SerializedName("relationshipCode")
    @Expose
    private String relationshipCode;

    public String getHeirName() {
        return heirName;
    }

    public void setHeirName(String heirName) {
        this.heirName = heirName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getRelationshipCode() {
        return relationshipCode;
    }

    public void setRelationshipCode(String relationshipCode) {
        this.relationshipCode = relationshipCode;
    }

    public String getRelationshipDetail() {
        return relationshipDetail;
    }

    public void setRelationshipDetail(String relationshipDetail) {
        this.relationshipDetail = relationshipDetail;
    }
}
