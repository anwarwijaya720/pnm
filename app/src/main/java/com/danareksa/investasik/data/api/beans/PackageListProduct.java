package com.danareksa.investasik.data.api.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Created by asep.surahman on 06/08/2018.
 */

public class PackageListProduct implements Serializable {

    private List<PackageList> packageLists;

    public List<PackageList> getPackageLists() {
        return packageLists;
    }

    public void setPackageLists(List<PackageList> packageLists) {
        this.packageLists = packageLists;
    }
}
