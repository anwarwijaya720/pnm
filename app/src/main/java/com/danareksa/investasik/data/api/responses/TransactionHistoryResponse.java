package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.TransactionHistoryData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TransactionHistoryResponse extends GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private TransactionHistoryData data;

    public TransactionHistoryData getData() {
        return data;
    }

    public void setData(TransactionHistoryData data) {
        this.data = data;
    }
}
