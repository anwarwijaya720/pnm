package com.danareksa.investasik.data.api.requests;

import com.danareksa.investasik.data.api.beans.PaymentDetail;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by asep.surahman on 12/08/2018.
 */

public class PaymentTransferRequest {

    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("paymentMethod")
    @Expose
    private String paymentMethod;
    @SerializedName("appsType")
    @Expose
    private String appsType;
    @SerializedName("detail")
    @Expose
    private List<PaymentDetail> detail;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getAppsType() {
        return appsType;
    }

    public void setAppsType(String appsType) {
        this.appsType = appsType;
    }

    public List<PaymentDetail> getDetail() {
        return detail;
    }

    public void setDetail(List<PaymentDetail> detail) {
        this.detail = detail;
    }
}
