package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.Packages;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fajarfatur on 2/2/16.
 */
public class InvestmentListResponse extends com.danareksa.investasik.data.api.responses.GenericResponse {

    @SerializedName("data")
    @Expose
    private List<Packages> packages = new ArrayList<>();
    
    public List<Packages> getPackages() {
        return packages;
    }

    public void setPackages(List<Packages> packages) {
        this.packages = packages;
    }
}
