package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Rizal Gunawan on 7/24/18.
 */

public class CompanyContacts {

    @SerializedName("companyContacts")
    @Expose
    private String companyContacts;

    public String getCompanyContacts() {
        return companyContacts;
    }

    public void setCompanyContacts(String companyContacts) {
        this.companyContacts = companyContacts;
    }
}
