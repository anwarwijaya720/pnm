package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 28/09/2018.
 */

public class BcaKlikPayData implements Serializable {

    @SerializedName("referrer")
    @Expose
    private String referrer;

    @SerializedName("redirectURL")
    @Expose
    private String redirectURL;

    @SerializedName("priceDate")
    @Expose
    private String priceDate;

    @SerializedName("redirectData")
    @Expose
    private RedirectDataBcaKlikPay data;


    public RedirectDataBcaKlikPay getData(){
        return data;
    }

    public void setData(RedirectDataBcaKlikPay data){
        this.data = data;
    }


    public String getRedirectURL() {
        return redirectURL;
    }

    public void setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    public String getPriceDate() {
        return priceDate;
    }

    public void setPriceDate(String priceDate) {
        this.priceDate = priceDate;
    }
}
