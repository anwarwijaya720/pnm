package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 03/07/2018.
 */

public class RegisterOnlineAccess implements Serializable {


    @SerializedName("ifua")
    private String ifua;

    @SerializedName("email")
    private String email;


    public String getIfua() {
        return ifua;
    }

    public void setIfua(String ifua) {
        this.ifua = ifua;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
