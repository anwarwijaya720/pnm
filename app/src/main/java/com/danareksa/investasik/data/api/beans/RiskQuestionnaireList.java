package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 10/07/2018.
 */

public class RiskQuestionnaireList {

    @SerializedName("riskAnswerId")
    @Expose
    private int riskAnswerId;

    @SerializedName("riskQuestionId")
    @Expose
    private int riskQuestionId;

    public int getRiskAnswerId() {
        return riskAnswerId;
    }

    public void setRiskAnswerId(int riskAnswerId) {
        this.riskAnswerId = riskAnswerId;
    }

    public int getRiskQuestionId() {
        return riskQuestionId;
    }

    public void setRiskQuestionId(int riskQuestionId) {
        this.riskQuestionId = riskQuestionId;
    }
}
