package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.PackageListReguler;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by asep.surahman on 25/07/2018.
 */

public class PackageListRegulerResponse extends GenericResponse{

    @SerializedName("data")
    @Expose
    private List<PackageListReguler> packageListRegulers;

    public List<PackageListReguler> getPackageListRegulers() {
        return packageListRegulers;
    }

    public void setPackageListRegulers(List<PackageListReguler> packageListRegulers) {
        this.packageListRegulers = packageListRegulers;
    }
}
