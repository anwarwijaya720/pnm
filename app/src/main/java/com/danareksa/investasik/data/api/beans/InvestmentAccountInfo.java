package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Rizal Gunawan on 7/25/18.
 */

public class InvestmentAccountInfo implements Serializable{

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("unitHolderNumber")
    @Expose
    private String unitHolderNumber;

    @SerializedName("investmentGoal")
    @Expose
    private String investmentGoal;

    @SerializedName("ifua")
    @Expose
    private String ifua;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUnitHolderNumber() {
        return unitHolderNumber;
    }

    public void setUnitHolderNumber(String unitHolderNumber) {
        this.unitHolderNumber = unitHolderNumber;
    }

    public String getInvestmentGoal() {
        return investmentGoal;
    }

    public void setInvestmentGoal(String investmentGoal) {
        this.investmentGoal = investmentGoal;
    }

    public String getIfua() {
        return ifua;
    }

    public void setIfua(String ifua) {
        this.ifua = ifua;
    }

    @Override
    public String toString() {
        if (this.getInvestmentGoal().equals(""))
            return this.getIfua();

        return this.getIfua() + " - " + this.getInvestmentGoal();
    }
}
