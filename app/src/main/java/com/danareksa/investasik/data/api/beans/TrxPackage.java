package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 10/08/2018.
 */

public class TrxPackage implements Serializable {

    @SerializedName("accountType")
    @Expose
    private String accountType;

    @SerializedName("ifua")
    @Expose
    private String ifua;

    @SerializedName("prospectusKey")
    @Expose
    private String prospectusKey;

    @SerializedName("ffsKey")
    @Expose
    private String ffsKey;

    @SerializedName("investmentGoal")
    @Expose
    private String investmentGoal;

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getIfua() {
        return ifua;
    }

    public void setIfua(String ifua) {
        this.ifua = ifua;
    }

    public String getProspectusKey() {
        return prospectusKey;
    }

    public void setProspectusKey(String prospectusKey) {
        this.prospectusKey = prospectusKey;
    }

    public String getFfsKey() {
        return ffsKey;
    }

    public void setFfsKey(String ffsKey) {
        this.ffsKey = ffsKey;
    }

    public String getInvestmentGoal() {
        return investmentGoal;
    }

    public void setInvestmentGoal(String investmentGoal) {
        this.investmentGoal = investmentGoal;
    }
}
