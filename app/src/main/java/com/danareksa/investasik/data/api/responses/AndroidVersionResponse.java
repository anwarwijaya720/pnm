package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.AndroidVersion;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by pandu.abbiyuarsyah on 04/05/2017.
 */

public class AndroidVersionResponse extends com.danareksa.investasik.data.api.responses.GenericResponse {

    @SerializedName("data")
    @Expose
    private AndroidVersion data;

    /**
     * @return The data
     */
    public AndroidVersion getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(AndroidVersion data) {
        this.data = data;
    }

}
