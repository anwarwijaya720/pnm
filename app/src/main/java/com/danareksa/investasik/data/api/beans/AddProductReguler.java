package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by asep.surahman on 13/08/2018.
 */

public class AddProductReguler {


    @SerializedName("ifuaNumber")
    @Expose
    private String ifuaNumber;

    @SerializedName("apssType")
    @Expose
    private String apssType;

    @SerializedName("packages")
    @Expose
    private List<PackageList> packages;

    public String getIfuaNumber() {
        return ifuaNumber;
    }

    public void setIfuaNumber(String ifuaNumber) {
        this.ifuaNumber = ifuaNumber;
    }

    public String getApssType() {
        return apssType;
    }

    public void setApssType(String apssType) {
        this.apssType = apssType;
    }

    public List<PackageList> getPackages() {
        return packages;
    }

    public void setPackages(List<PackageList> packages) {
        this.packages = packages;
    }
}
