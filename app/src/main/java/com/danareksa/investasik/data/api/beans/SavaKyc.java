package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 11/07/2018.
 */

public class SavaKyc {

    @SerializedName("riskProfileName")
    @Expose
    private String riskProfileName;

    @SerializedName("riskProfile")
    @Expose
    private String riskProfile;

    @SerializedName("riskProfileDescription")
    @Expose
    private String riskProfileDescription;

    @SerializedName("completeness")
    @Expose
    private double completeness;

    @SerializedName("riskProfileScore")
    @Expose
    private int riskProfileScore;


    public String getRiskProfileName() {
        return riskProfileName;
    }

    public void setRiskProfileName(String riskProfileName) {
        this.riskProfileName = riskProfileName;
    }

    public String getRiskProfile() {
        return riskProfile;
    }

    public void setRiskProfile(String riskProfile) {
        this.riskProfile = riskProfile;
    }

    public String getRiskProfileDescription() {
        return riskProfileDescription;
    }

    public void setRiskProfileDescription(String riskProfileDescription) {
        this.riskProfileDescription = riskProfileDescription;
    }

    public double getCompleteness() {
        return completeness;
    }

    public void setCompleteness(double completeness) {
        this.completeness = completeness;
    }

    public int getRiskProfileScore() {
        return riskProfileScore;
    }

    public void setRiskProfileScore(int riskProfileScore) {
        this.riskProfileScore = riskProfileScore;
    }
}
