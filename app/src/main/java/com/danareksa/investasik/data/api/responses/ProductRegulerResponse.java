package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.ProductRegulerIfua;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


/**
 * Created by asep.surahman on 14/08/2018.
 */

public class ProductRegulerResponse extends GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private ProductRegulerIfua data;


    public ProductRegulerIfua getData() {
        return data;
    }

    public void setData(ProductRegulerIfua data) {
        this.data = data;
    }
}
