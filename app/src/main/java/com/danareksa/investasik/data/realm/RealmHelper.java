package com.danareksa.investasik.data.realm;

import android.content.Context;

import com.danareksa.investasik.data.api.beans.Kyc;
import com.danareksa.investasik.data.api.beans.KycLookup;
import com.danareksa.investasik.data.api.beans.SecurityQuestion;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.api.requests.SettlementInfoRequest;
import com.danareksa.investasik.data.realm.model.KycLookupModel;
import com.danareksa.investasik.data.realm.model.KycModel;
import com.danareksa.investasik.data.realm.model.SecurityQuestionModel;
import com.danareksa.investasik.data.realm.model.SettlementInfoModel;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import timber.log.Timber;

/**
 * Created by fajarfatur on 2/2/16.
 */
public class RealmHelper {

    /*Security Question*/

    public static void createSecurityQuestionList(Realm realm, final List<SecurityQuestion> securityQuestionList) {
        realm.beginTransaction();
        for (SecurityQuestion securityQuestion : securityQuestionList) {
            SecurityQuestionModel securityQuestionModel = realm.createObject(SecurityQuestionModel.class);
            securityQuestionModel.setId(securityQuestion.getId());
            securityQuestionModel.setQuestionName(securityQuestion.getQuestionName());
            securityQuestionModel.setQuestionText(securityQuestion.getQuestionText());
        }
        Timber.i("%d Security Question Created", securityQuestionList.size());
        realm.commitTransaction();
    }

    public static Observable<RealmResults<SecurityQuestionModel>> readSecurityQuestions(Realm realm) {
        return realm.where(SecurityQuestionModel.class).findAllAsync().asObservable()
                .filter(new Func1<RealmResults<SecurityQuestionModel>, Boolean>() {
                    @Override
                    public Boolean call(RealmResults<SecurityQuestionModel> securityQuestionModels) {
                        return securityQuestionModels.isLoaded();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static RealmResults<SecurityQuestionModel> readSecurityQuestionList(Realm realm) {
        RealmQuery<SecurityQuestionModel> query = realm.where(SecurityQuestionModel.class);
        return query.findAll();
    }

    /*KYC*/

    public static void createKycLookup(Realm realm, final List<KycLookup> kycLookupList){
        realm.beginTransaction();
        for (KycLookup kycLookup : kycLookupList){
            KycLookupModel kycLookupModel = realm.createObject(KycLookupModel.class);
            kycLookupModel.setCategory(kycLookup.getCategory());
            kycLookupModel.setCode(kycLookup.getCode());
            kycLookupModel.setValue(kycLookup.getValue());
            kycLookupModel.setSeq(kycLookup.getSeq());
            System.out.println("category : " + kycLookup.getCategory() + " code : " + kycLookup.getCode() + " value : "+ kycLookup.getValue());
        }
        Timber.i("%d KYC Lookup Created", kycLookupList.size());
        realm.commitTransaction();
    }

 /*   public static void createRiskProfileQuestion(Realm realm, final List<RiskProfileNew> riskProfileNews) {
        realm.beginTransaction();
        for (RiskProfileNew riskProfileNew : riskProfileNews) {
            RiskProfileModel riskProfileModel = realm.createObject(RiskProfileModel.class);
            riskProfileModel.setQuestion(riskProfileNew.getQuestion());
            riskProfileModel.setAnswers(riskProfileNew.getAnswers());
            System.out.println("question : " + riskProfileNew.getQuestion() + " answer : " + riskProfileNew.getAnswers() );
        }
        Timber.i("%d Risk Profile List Created", riskProfileNews.size());
        realm.commitTransaction();
    }
*/

    /**
     * @param category the category data that need to be retrieved. Set to null to retrieve all data
     */
    public static RealmResults<KycLookupModel> readKycLookupListByCategory(Realm realm, String category){
        RealmQuery<KycLookupModel> query = realm.where(KycLookupModel.class);
        if (category != null) query.equalTo("category", category);
        return query.findAll();
    }

    public static int getKycLookupSize(Realm realm) {
        int size = readKycLookupListByCategory(realm, null).size();
        return size;
    }

    /**
     * @param kyc the KYC data object that need to be saved to Realm
     */
    public static void createOrUpdateKyc(Realm realm, Kyc kyc) {
        Timber.i("Create Or Update Kyc : %s", kyc.toString());
        realm.beginTransaction();
        KycModel kycModel = readKyc(realm);
        if (kycModel == null) {
            kycModel = realm.createObject(KycModel.class);
            kycModel.setId(KycModel.DEFAULT_ID);
        }
        kycModel.setSalutation(kyc.getSalutation());
        kycModel.setFirstName(kyc.getFirstName());
        kycModel.setLastName(kyc.getLastName());
        kycModel.setEmail(kyc.getEmail());
        kycModel.setBirthDate(kyc.getBirthDate());
        kycModel.setBirthPlace(kyc.getBirthPlace());
        kycModel.setOccupation(kyc.getOccupation());
        kycModel.setNatureOfBusiness(kyc.getNatureOfBusiness());
        kycModel.setEmployerName(kyc.getEmployerName());
        kycModel.setHomeCountry(kyc.getHomeCountry());
        kycModel.setHomeProvince(kyc.getHomeProvince());
        kycModel.setHomeCity(kyc.getHomeCity());
        kycModel.setHomeAddress(kyc.getHomeAddress());
        kycModel.setHomePostalCode(kyc.getHomePostalCode());
        kycModel.setHomePhoneNumber(kyc.getHomePhoneNumber());
        kycModel.setLegalCountry(kyc.getLegalCountry());
        kycModel.setLegalProvince(kyc.getLegalProvince());
        kycModel.setLegalCity(kyc.getLegalCity());
        kycModel.setLegalAddress(kyc.getLegalAddress());
        kycModel.setLegalPostalCode(kyc.getLegalPostalCode());
        kycModel.setLegalPhoneNumber(kyc.getLegalPhoneNumber());
//        kycModel.setOfficeCountry(kyc.getOfficeCountry());
//        kycModel.setOfficeProvince(kyc.getOfficeProvince());
//        kycModel.setOfficeCity(kyc.getOfficeCity());
//        //kycModel.setOfficeAddress(kyc.getOfficeAddress());
//        kycModel.setOfficePostalCode(kyc.getOfficePostalCode());
//        kycModel.setOfficePhoneNumber(kyc.getOfficePhoneNumber());
        kycModel.setIdType(kyc.getIdType());
        kycModel.setIdNumber(kyc.getIdNumber());
        kycModel.setIdExpirationDate(kyc.getIdExpirationDate());
        kycModel.setNationality(kyc.getNationality());
        kycModel.setCitizenship(kyc.getCitizenship());
        kycModel.setGender(kyc.getGender());
        kycModel.setReligion(kyc.getReligion());
        kycModel.setMaritalStatus(kyc.getMaritalStatus());
        kycModel.setEducationBackground(kyc.getEducation());
        kycModel.setSourceOfIncome(kyc.getSourceOfIncome());
        kycModel.setTotalIncomePa(kyc.getTotalIncomePa());
        kycModel.setInvestmentPurpose(kyc.getInvestmentPurpose());
        kycModel.setMotherMaidenName(kyc.getMotherName());
        kycModel.setBeneficiaryName(kyc.getBeneficiaryName());
        kycModel.setBeneficiaryRelationship(kyc.getBeneficiaryRelationship());
        kycModel.setPreferredMailingAddress(kyc.getPreferredMailingAddress());
        kycModel.setPepCountry(kyc.getPepCountry().toString());
        kycModel.setSettlementAccountName(kyc.getSettlementAccountName());
        kycModel.setSettlementAccountNo(kyc.getSettlementAccountNo());
        realm.commitTransaction();
    }

    public static void createOrUpdateKycModel(Realm realm, KycModel kycModel, Kyc kyc) {
        Timber.i("Create Or Update Kyc Model : %s", kyc.toString());
        if (kycModel == null) {
            kycModel = realm.createObject(KycModel.class);
            kycModel.setId(KycModel.DEFAULT_ID);
        }
        realm.beginTransaction();
        kycModel.setSalutation(kyc.getSalutation());
        kycModel.setFirstName(kyc.getFirstName());
        kycModel.setLastName(kyc.getLastName());
        kycModel.setEmail(kyc.getEmail());
        kycModel.setBirthDate(kyc.getBirthDate());
        kycModel.setBirthPlace(kyc.getBirthPlace());
        kycModel.setOccupation(kyc.getOccupation());
        kycModel.setNatureOfBusiness(kyc.getNatureOfBusiness());
        kycModel.setEmployerName(kyc.getEmployerName());
        kycModel.setHomeCountry(kyc.getHomeCountry());
        kycModel.setHomeProvince(kyc.getHomeProvince());
        kycModel.setHomeCity(kyc.getHomeCity());
        kycModel.setHomeAddress(kyc.getHomeAddress());
        kycModel.setHomePostalCode(kyc.getHomePostalCode());
        kycModel.setHomePhoneNumber(kyc.getHomePhoneNumber());
        kycModel.setLegalCountry(kyc.getLegalCountry());
        kycModel.setLegalProvince(kyc.getLegalProvince());
        kycModel.setLegalCity(kyc.getLegalCity());
        kycModel.setLegalAddress(kyc.getLegalAddress());
        kycModel.setLegalPostalCode(kyc.getLegalPostalCode());
        kycModel.setLegalPhoneNumber(kyc.getLegalPhoneNumber());
//        kycModel.setOfficeCountry(kyc.getOfficeCountry());
//        kycModel.setOfficeProvince(kyc.getOfficeProvince());
//        kycModel.setOfficeCity(kyc.getOfficeCity());
//        //kycModel.setOfficeAddress(kyc.getOfficeAddress());
//        kycModel.setOfficePostalCode(kyc.getOfficePostalCode());
//        kycModel.setOfficePhoneNumber(kyc.getOfficePhoneNumber());
        kycModel.setIdType(kyc.getIdType());
        kycModel.setIdNumber(kyc.getIdNumber());
        kycModel.setIdExpirationDate(kyc.getIdExpirationDate());
        kycModel.setNationality(kyc.getNationality());
        kycModel.setCitizenship(kyc.getCitizenship());
        kycModel.setGender(kyc.getGender());
        kycModel.setReligion(kyc.getReligion());
        kycModel.setMaritalStatus(kyc.getMaritalStatus());
        kycModel.setEducationBackground(kyc.getEducation());
        kycModel.setSourceOfIncome(kyc.getSourceOfIncome());
        kycModel.setTotalIncomePa(kyc.getTotalIncomePa());
        kycModel.setInvestmentPurpose(kyc.getInvestmentPurpose());
        kycModel.setMotherMaidenName(kyc.getMotherName());
        kycModel.setBeneficiaryName(kyc.getBeneficiaryName());
        kycModel.setBeneficiaryRelationship(kyc.getBeneficiaryRelationship());
        kycModel.setPreferredMailingAddress(kyc.getPreferredMailingAddress());
        kycModel.setSettlementAccountName(kyc.getSettlementAccountName());
        kycModel.setSettlementAccountNo(kyc.getSettlementAccountNo());
        realm.commitTransaction();
    }

    public static KycModel readKyc(Realm realm) {
        RealmQuery<KycModel> query = realm.where(KycModel.class);
        query.equalTo("id", KycModel.DEFAULT_ID);
        RealmResults<KycModel> results = query.findAll();
        if (results != null && results.size() > 0) {
            KycModel kycModel = results.first(); // read from realm
            return kycModel;
        } else {
            return null;
        }
    }


   /* public static RiskProfileModel readRiskProfile(Realm realm) {
        RealmQuery<RiskProfileModel> query = realm.where(RiskProfileModel.class);
        query.equalTo("id", KycModel.DEFAULT_ID);
        RealmResults<KycModel> results = query.findAll();
        if (results != null && results.size() > 0) {
            KycModel kycModel = results.first(); // read from realm
            return kycModel;
        } else {
            return null;
        }
    }
*/

    /*Settlement Info*/

    public static void createOrUpdateSettlementInfo(Realm realm, SettlementInfoRequest request) {
        realm.beginTransaction();
        SettlementInfoModel settlementInfoModel = readSettlementInfo(realm);
        if (settlementInfoModel == null) {
            settlementInfoModel = realm.createObject(SettlementInfoModel.class);
            settlementInfoModel.setId(KycModel.DEFAULT_ID);
        }
        settlementInfoModel.setBankId(request.getBankId());
        settlementInfoModel.setBranchId(request.getBranchId());
        settlementInfoModel.setSettlementAccountNo(request.getSettlementAccountNo());
        settlementInfoModel.setSettlementAccountName(request.getSettlementAccountName());
        settlementInfoModel.setEmail(request.getEmail());
        realm.commitTransaction();
    }

    public static SettlementInfoModel readSettlementInfo(Realm realm) {
        RealmQuery<SettlementInfoModel> query = realm.where(SettlementInfoModel.class);
        query.equalTo("id", SettlementInfoModel.DEFAULT_ID);
        RealmResults<SettlementInfoModel> results = query.findAll();
        if (results != null && results.size() > 0) {
            SettlementInfoModel settlementInfoModel = results.first(); // read from realm
            return settlementInfoModel;
        } else {
            return null;
        }
    }


    //new
    public static void createOrUpdateKyc(Context context, KycDataRequest request) {
        Realm realm = Realm.getInstance(context);
        realm.beginTransaction();
        KycModel kycModel = readKyc(context);
        if (kycModel == null) {
            kycModel = realm.createObject(KycModel.class);
            kycModel.setId(KycModel.DEFAULT_ID);
        }
        kycModel.setSalutation(request.getSalutation());
        kycModel.setFirstName(request.getFirstName());
        kycModel.setLastName(request.getLastName());
        kycModel.setEmail(request.getEmail());
        kycModel.setBirthDate(request.getBirthDate());
        kycModel.setBirthPlace(request.getBirthPlace());
        kycModel.setOccupation(request.getOccupation());
        kycModel.setNatureOfBusiness(request.getNatureOfBusiness());
        kycModel.setEmployerName(request.getEmployerName());
        kycModel.setHomeCountry(request.getHomeCountry());
        kycModel.setHomeProvince(request.getHomeProvince());
        kycModel.setHomeCity(request.getHomeCity());
        kycModel.setHomeAddress(request.getHomeAddress());
        kycModel.setHomePostalCode(request.getHomePostalCode());
        kycModel.setHomePhoneNumber(request.getHomePhoneNumber());
        kycModel.setLegalCountry(request.getLegalCountry());
        kycModel.setLegalProvince(request.getLegalProvince());
        kycModel.setLegalCity(request.getLegalCity());
        kycModel.setLegalAddress(request.getLegalAddress());
        kycModel.setLegalPostalCode(request.getLegalPostalCode());
        kycModel.setLegalPhoneNumber(request.getLegalPhoneNumber());
        kycModel.setIdType(request.getIdType());
        kycModel.setIdNumber(request.getIdNumber());
        kycModel.setIdExpirationDate(request.getIdExpirationDate());
        kycModel.setNationality(request.getNationality());
        kycModel.setCitizenship(request.getCitizenship());
        kycModel.setGender(request.getGender());
        kycModel.setReligion(request.getReligion());
        kycModel.setMaritalStatus(request.getMaritalStatus());
        kycModel.setEducationBackground(request.getEducation());
        kycModel.setSourceOfIncome(request.getSourceOfIncome());
        realm.commitTransaction();
        realm.close();
    }

    public static KycModel readKyc(Context context) {
        final Realm realm = Realm.getInstance(context);
        RealmQuery<KycModel> query = realm.where(KycModel.class);
        query.equalTo("id", KycModel.DEFAULT_ID);
        RealmResults<KycModel> results = query.findAll();
        if (results != null && results.size() > 0) {
            KycModel kycModel = results.first(); // read from realm
            return kycModel;
        } else {
            return null;
        }
    }
    //end


}
