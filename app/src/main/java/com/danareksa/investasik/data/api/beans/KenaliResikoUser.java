package com.danareksa.investasik.data.api.beans;

public class KenaliResikoUser {

    private String desResiko;

    public String getDesResiko() {
        return desResiko;
    }

    public void setDesResiko(String desResiko) {
        this.desResiko = desResiko;
    }
}
