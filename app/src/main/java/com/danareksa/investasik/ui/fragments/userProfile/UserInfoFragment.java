package com.danareksa.investasik.ui.fragments.userProfile;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.SecurityQuestion;
import com.danareksa.investasik.data.api.requests.SaveUserInfoRequest;
import com.danareksa.investasik.data.api.responses.UserInfoResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.ChangePasswordActivity;
import com.danareksa.investasik.ui.activities.RegisterDataPribadiActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

import static android.app.Activity.RESULT_OK;

@RuntimePermissions
public class UserInfoFragment extends BaseFragment {

    private static final String TAG = "UserInfoFragment";
    private static final int REQUEST_CAMERA = 1;
    private static final int SELECT_FILE = 4;
    final int CROP_PIC = 2;

    @Bind(R.id.txvFirstName)
    TextView txvFirstName;
    @Bind(R.id.txvLastName)
    TextView txvLastName;
    @Bind(R.id.txvMobileNumber)
    TextView txvMobileNumber;
    @Bind(R.id.edtMobileNumberArea)
    EditText edtMobileNumberArea;
    @Bind(R.id.edtMobileNumberProfile)
    EditText edtMobileNumberProfile;
    @Bind(R.id.txvEmail)
    TextView txvEmail;
    @Bind(R.id.txvMiddleName)
    TextView txvMiddleName;
    @Bind(R.id.txvEmailID)
    TextView txvEmailID;
    @Bind(R.id.txvFullName)
    TextView txvFullName;
    @Bind(R.id.txvNameProfiel)
    TextView txvNameProfiel;
    @Bind(R.id.txvDateOfBirth)
    TextView txvDateOfBirth;
    @Bind(R.id.txvSid)
    TextView txvSid;
    @Bind(R.id.txvCif)
    TextView txvCif;
    @Bind(R.id.txt_no_telp)
    TextView txt_no_telp;

    @Bind(R.id.ivUserPhoto)
    CircleImageView imvUserPhoto;
/*    @Bind(R.id.ivChangeProfileWarning)
    ImageView imvChangeProfileWarning;*/
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;

    @Bind(R.id.ll_no_hp)LinearLayout llNoHp;

    private UserInfoPresenter presenter;
    private int choosenTask = 0;

    UserInfoResponse userInfo;
    List<SecurityQuestion> securityQuestionList;
    private Uri picUri;
    public File tempFile;

    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new UserInfoFragment(), TAG);
            fragmentTransaction.commit();
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.f_user_info;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new UserInfoPresenter(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.userInfo();
        //loadPicture();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadPicture();

      /*  if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")) {
            imvChangeProfileWarning.setVisibility(View.GONE);
        }else{
            imvChangeProfileWarning.setVisibility(View.VISIBLE);
        }*/
    }

    public void loadPicture() {
        try {
            if(!PrefHelper.getString(PrefKey.IMAGE).equals("")){

                Picasso.with(getActivity()).load(InviseeService.IMAGE_DOWNLOAD_URL + PrefHelper.getString(PrefKey.IMAGE) + "&token=" + PrefHelper.getString(PrefKey.TOKEN))
                        .error(R.drawable.ic_profile_avatar)
                        .into(imvUserPhoto);

            }else{
                imvUserPhoto.setImageResource(R.drawable.ic_profile_avatar);
            }
        }catch (Exception e){
            imvUserPhoto.setImageResource(R.drawable.ic_profile_avatar);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        UserInfoFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }


    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void startGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_FILE);
    }


    public static File createTemporaryFile(Context context, String folder_name, String ext) {
        try {
            File folder = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), folder_name);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            return File.createTempFile(""+System.currentTimeMillis(), ext, folder);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void startCamera() {
        tempFile = createTemporaryFile(getContext(), "pic_prof", ".jpg");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        picUri = FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".provider", tempFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    void onCaptureImageResult() {
        // Setting option to resize image
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(tempFile.getPath(),options);
        final int REQUIRED_SIZE = 310;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;

        // Load file with option parameter, then compress it
        Bitmap b = BitmapFactory.decodeFile(tempFile.getPath(),options);

        Matrix matrix = new Matrix();

        try {
            ExifInterface exif = null;
            exif = new ExifInterface(tempFile.getPath());
            String orientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            if (orientation.equals(ExifInterface.ORIENTATION_NORMAL)) {
                // Do nothing. The original image is fine.
            } else if (orientation.equals(ExifInterface.ORIENTATION_ROTATE_90+"")) {
                matrix.postRotate(90);
            } else if (orientation.equals(ExifInterface.ORIENTATION_ROTATE_180+"")) {
                matrix.postRotate(180);
            } else if (orientation.equals(ExifInterface.ORIENTATION_ROTATE_270+"")) {
                matrix.postRotate(270);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        Bitmap resizedBitmap = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
        resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, bytes);

        FileOutputStream fo;
        try {
            fo = new FileOutputStream(tempFile);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doUpload(tempFile);
            }
        }, 2000);
    }

    void doUpload(File targetFile){
        presenter.uploadPhoto(targetFile);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case SELECT_FILE:
                if (resultCode == RESULT_OK) {

                    Uri selectedImage = data.getData();

                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    File file = new File(picturePath);
                    //long length = file.length() / 1024; // Size in KB

                    presenter.uploadPhoto(file);

                }
                break;

            case REQUEST_CAMERA:
                if (resultCode == Activity.RESULT_OK) {
                    onCaptureImageResult();
                }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public void setView(boolean completeness) {

        txvFirstName.setText(userInfo.getData().getFirstName());
        txvLastName.setText(userInfo.getData().getLastName());
        txvMobileNumber.setText(userInfo.getData().getMobileNumber());
        txvNameProfiel.setText(userInfo.getData().getFirstName() + " " + userInfo.getData().getLastName());
        txvFullName.setText(userInfo.getData().getFirstName() + " " + userInfo.getData().getLastName());
        txt_no_telp.setText(userInfo.getData().getHomePhoneNumber());

        try {
            String[] splitedPhoneNumber = userInfo.getData().getMobileNumber().split("-");
            System.out.println("splited phone length : " + splitedPhoneNumber.length);
            if(splitedPhoneNumber.length > 1){

                if (splitedPhoneNumber[0] != null) {
                    edtMobileNumberArea.setText(splitedPhoneNumber[0]);
                } else {
                    edtMobileNumberArea.setText("");
                }
                if (splitedPhoneNumber[1] != null) {
                    edtMobileNumberProfile.setText(splitedPhoneNumber[1]);
                } else {
                    edtMobileNumberProfile.setText("");
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        txvEmail.setText(userInfo.getData().getEmail());
        txvMiddleName.setText(userInfo.getData().getMiddleName());
        txvEmailID.setText(userInfo.getData().getEmail());

        //txvDateOfBirth.setText(userInfo.getData().getBirthDate());

        txvCif.setText(userInfo.getData().getPortalcif());
        txvSid.setText(userInfo.getData().getSid());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date3 = null;
        try {
            date3 = sdf.parse(userInfo.getData().getBirthDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        String convertedDate= formatter.format(date3);

        txvDateOfBirth.setText(convertedDate);


       /* if (completeness) {
            imvChangeProfileWarning.setVisibility(View.GONE);
        } else {
            imvChangeProfileWarning.setVisibility(View.VISIBLE);
        }*/

    }


    public void saveUser() {
        SaveUserInfoRequest request = new SaveUserInfoRequest();
        request.setEmail(txvEmail.getText().toString());
        request.setFirstName(txvFirstName.getText().toString());
        request.setImageKey(PrefHelper.getString(PrefKey.IMAGE));
        request.setLastName(txvLastName.getText().toString());
        request.setMobileNumber(txvMobileNumber.getText().toString());
        System.out.println("email : " + txvEmail.getText().toString());
        presenter.saveUserInfo(request);
    }

    public void showDialogEditNoHP() {
        MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.no_hp)
                .customView(R.layout.dialog_edit_no_hp, true)
//                .positiveText(R.string.save)
//                .negativeText(R.string.cancel)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                    }
                })
                .build();

        View view = dialog.getCustomView();
        ((EditText) ButterKnife.findById(view, R.id.edtMobileNumberArea)).setText("62");
        ((EditText) ButterKnife.findById(view, R.id.edtMobileNumberProfile)).setText("");

        (ButterKnife.findById(view, R.id.btn_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        (ButterKnife.findById(view, R.id.btn_save)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @OnClick(R.id.ll_no_hp)
    public void onNoHpClicked() {
        if (BuildConfig.FLAVOR.contentEquals("btn")){
            showDialogEditNoHP();
        }
    }

    @OnClick(R.id.ll_email)
    public void onEmailClicked() {
        if (BuildConfig.FLAVOR.contentEquals("btn")){
            showDialogEditNoHP();
        }
    }

    @OnClick(R.id.btnChangeProfile)
    public void onKycClicked() {
        PrefHelper.setString(PrefKey.NO_HANDPHONE, txvMobileNumber.getText().toString());
        RegisterDataPribadiActivity.startActivity((BaseActivity) getActivity(), "");
    }

    @OnClick(R.id.btnChangePassword)
    public void onChangePasswordClicked() {
        ChangePasswordActivity.startActivity((BaseActivity) getActivity());
    }

    @OnClick(R.id.btnChangePicture)
    void onChangePictureClicked() {
        new MaterialDialog.Builder(getActivity())
                .title("Change Picture")
                .items(R.array.change_picture)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if (which == 0) {
                            choosenTask = REQUEST_CAMERA;
                            UserInfoFragmentPermissionsDispatcher.startCameraWithPermissionCheck(UserInfoFragment.this);
                        } else {
                            choosenTask = SELECT_FILE;
                            UserInfoFragmentPermissionsDispatcher.startGalleryWithPermissionCheck(UserInfoFragment.this);
                        }
                    }
                })
                .show();
    }

    @OnClick(R.id.ivUserPhoto)
    void onChangePictureClickedPhoto() {
        new MaterialDialog.Builder(getActivity())
                .title("Change Picture")
                .items(R.array.change_picture)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if (which == 0) {
                            choosenTask = REQUEST_CAMERA;
                            UserInfoFragmentPermissionsDispatcher.startCameraWithPermissionCheck(UserInfoFragment.this);
                        } else {
                            choosenTask = SELECT_FILE;
                            UserInfoFragmentPermissionsDispatcher.startGalleryWithPermissionCheck(UserInfoFragment.this);
                        }
                    }
                })
                .show();
    }

    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.tvTryAgain)
    void retryConnection() {
        presenter.userInfo();
    }



}
