package com.danareksa.investasik.ui.fragments.purchase;

import android.graphics.Color;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.PackageListReguler;
import com.danareksa.investasik.data.api.requests.AddProductRegulerRequest;
import com.danareksa.investasik.data.api.requests.CustomerDataByIfuaRequest;
import com.danareksa.investasik.data.api.requests.KycLookupRequest;
import com.danareksa.investasik.data.api.requests.PurchasePackageListRequest;
import com.danareksa.investasik.data.api.responses.CartListResponse;
import com.danareksa.investasik.data.api.responses.CustomerDataByIfuaResponse;
import com.danareksa.investasik.data.api.responses.KycLookupResponse;
import com.danareksa.investasik.data.api.responses.PackageListRegulerResponse;
import com.danareksa.investasik.data.api.responses.ProductRegulerResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.data.realm.RealmHelper;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by asep.surahman on 25/06/2018.
 */

public class RegulerPurchasePresenter {

    private RegulerPurchaseFragment fragment;

    public RegulerPurchasePresenter(RegulerPurchaseFragment fragment) {
        this.fragment = fragment;
    }

    public void cartList() {
        fragment.showProgressBar();
        fragment.getApi().getCartList(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<CartListResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.connectionError();
                        fragment.getActivity().finish();
                    }

                    @Override
                    public void onNext(List<CartListResponse> response) {
                        if (response != null && response.size() > 0) {
                            fragment.cartList = response;
                            fragment.dismissProgressBar();
                        }
                    }
                });
    }


    public void getPackageList(String ifuaId){
        fragment.showProgressBar();
        fragment.getApi().getPackageListRegulerWithIfua(getPackageListRequest(ifuaId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PackageListRegulerResponse>(){
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.dismissProgressBar();
                        fragment.connectionError();
                        System.out.println("conn error get package >>");
                    }

                    @Override
                    public void onNext(PackageListRegulerResponse packageListRegulerResponse){
                        fragment.dismissProgressBar();
                        if(packageListRegulerResponse.getCode() == 0 && packageListRegulerResponse.getPackageListRegulers().size() > 0){
                            fragment.isLoadingGetPackage = true;
                            fragment.packageListRegulersAll = packageListRegulerResponse.getPackageListRegulers();
                            fragment.loadRole();
                            System.out.println("data package success >>");
                        }else if(packageListRegulerResponse.getCode() == 0 && packageListRegulerResponse.getPackageListRegulers() == null){
                            fragment.isLoadingGetPackage = false;
                            fragment.showDialogAfterSubmit("Data tidak tersedia");
                            System.out.println("data package not success >>");
                        }else{
                            System.out.println("data package not success >>");
                            fragment.isLoadingGetPackage = false;
                            fragment.showDialogAfterSubmit(packageListRegulerResponse.getInfo());
                        }
                    }
                });
    }



    public void addProductToRegulerIfua(AddProductRegulerRequest addProductRegulerRequests){
        fragment.showProgressBar();
        fragment.getApi().addProductToRegulerIfua(getAddProductReguler(addProductRegulerRequests))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ProductRegulerResponse>(){
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.dismissProgressBar();
                        fragment.isLoadingAddReguler = true;
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(ProductRegulerResponse productRegulerResponse){
                        fragment.dismissProgressBar();
                        fragment.isLoadingAddReguler = false;
                        if(productRegulerResponse.getCode() == 0){
                            fragment.productRegulerIfua = productRegulerResponse.getData();
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(R.string.redeemtion_failed_title)
                                    .titleColor(Color.BLACK)
                                    .content("Transaksi berhasil")
                                    .contentGravity(GravityEnum.CENTER)
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.GRAY)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            fragment.showSummaryPurchase();
                                            dialog.dismiss();
                                        }
                                    })
                                    .cancelable(false)
                                    .show();

                        }else{
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(R.string.redeemtion_failed_title)
                                    .titleColor(Color.BLACK)
                                    .content(productRegulerResponse.getInfo())
                                    .contentGravity(GravityEnum.CENTER)
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.redeemtion_failed_tutup)
                                    .positiveColor(Color.GRAY)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .cancelable(true)
                                    .show();
                        }
                    }
                });
    }



    private AddProductRegulerRequest getAddProductReguler(AddProductRegulerRequest addProductRegulerRequests){
        AddProductRegulerRequest addProductRegulerRequest = new AddProductRegulerRequest();
        addProductRegulerRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        addProductRegulerRequest.setIfuaNumber(addProductRegulerRequests.getIfuaNumber());
        addProductRegulerRequest.setPackages(addProductRegulerRequests.getPackages());
        addProductRegulerRequest.setApssType(addProductRegulerRequests.getApssType());
        return addProductRegulerRequest;
    }



    public PurchasePackageListRequest getPackageListRequest(String ifua){
        PurchasePackageListRequest packageListRequest = new PurchasePackageListRequest();
        packageListRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        packageListRequest.setIfua(ifua);
        return packageListRequest;
    }


    public List<PackageListReguler> gePackageListRegulers(List<PackageListReguler> listRegulers){
        List<PackageListReguler> packageListRegulers = new ArrayList<>();
        for(int i = 0; i < listRegulers.size(); i++){
            PackageListReguler packageListReguler = new PackageListReguler();
            packageListReguler.setId(listRegulers.get(i).getId());
            packageListReguler.setProspectusKey(listRegulers.get(i).getProspectusKey());
            packageListReguler.setFeePercentage(listRegulers.get(i).getFeePercentage());
            packageListReguler.setFeePrice(listRegulers.get(i).getFeePrice());
            packageListReguler.setTransactionAmount(listRegulers.get(i).getTransactionAmount());
            packageListReguler.setTotal(listRegulers.get(i).getTotal());
            packageListReguler.setCode(listRegulers.get(i).getCode());
            packageListReguler.setName(listRegulers.get(i).getName());
            packageListReguler.setTimePeriod(listRegulers.get(i).getTimePeriod());
            packageListReguler.setMinSubscription(listRegulers.get(i).getMinSubscription());
            packageListReguler.setAccept(listRegulers.get(i).isAccept());
            packageListReguler.setFirst(listRegulers.get(i).isFirst());
            packageListRegulers.add(packageListReguler);
        }
        return packageListRegulers;
    }



    public CustomerDataByIfuaRequest getCustomerDataByIfuaRequest(String ifuaNumber){
        CustomerDataByIfuaRequest customerDataByIfuaRequest = new CustomerDataByIfuaRequest();
        customerDataByIfuaRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        customerDataByIfuaRequest.setIfuaNumber(ifuaNumber);
        return customerDataByIfuaRequest;
    }


    public void getCustomerDataByIfua(final String ifuaNumber){
        fragment.showProgressBar();
        fragment.getApi().getCustomerDataByIfua(getCustomerDataByIfuaRequest(ifuaNumber))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<CustomerDataByIfuaResponse>(){
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.dismissProgressBar();
                        fragment.connectionError();
                        System.out.println("conn errr get customer >>");
                    }

                    @Override
                    public void onNext(CustomerDataByIfuaResponse customerDataRekeningResponse){
                        fragment.dismissProgressBar();
                        if(customerDataRekeningResponse.getCode() == 0){
                            fragment.isLoadingGetCustByIfua = true;
                            fragment.fetchDataResultToLayout(customerDataRekeningResponse);
                            getPackageList(ifuaNumber);
                            System.out.println("data customer success >>");
                        }else{
                            fragment.isLoadingGetCustByIfua = false;
                            fragment.showDialogAfterSubmit(customerDataRekeningResponse.getInfo());
                            System.out.println("data customer not success >>");
                        }

                    }
                });
    }



    private KycLookupRequest getKycLookupRequest(){
        KycLookupRequest kycLookupRequest = new KycLookupRequest();
        kycLookupRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        return kycLookupRequest;
    }


    void getKycLookupData(){
        fragment.showProgressBar();
        if (!isKycLookupAlreadyOnRealm()){
            fragment.getApi().getKycLookupNew(getKycLookupRequest())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Observer<KycLookupResponse>(){
                        @Override
                        public void onCompleted(){
                        }

                        @Override
                        public void onError(Throwable e){
                            fragment.dismissProgressBar();
                            fragment.connectionError();
                            System.out.println("conn error lookup >>");
                        }

                        @Override
                        public void onNext(KycLookupResponse kycLookupResponse){
                            fragment.dismissProgressBar();
                            if(kycLookupResponse.getCode() == 0){
                                RealmHelper.createKycLookup(fragment.getRealm(), kycLookupResponse.getKycLookupList());
                                getCustomerDataByIfua(fragment.ifuaId);
                                System.out.println("data lookup not exis>>");
                                fragment.isLoadingGetKyc = true;
                            }else{
                                fragment.isLoadingGetKyc = false;
                                fragment.showDialogAfterSubmit(kycLookupResponse.getInfo());
                            }
                        }
                    });
        }else{
            System.out.println("data lookup already exis>>");
            fragment.isLoadingGetKyc = true;
            getCustomerDataByIfua(fragment.ifuaId);
        }
    }



    private boolean isKycLookupAlreadyOnRealm(){
        return RealmHelper.getKycLookupSize(fragment.getRealmReguler()) > 0;
    }







}
