package com.danareksa.investasik.ui.fragments.portfolio;

import com.danareksa.investasik.data.api.requests.InvestmentAcountGroupRequest;
import com.danareksa.investasik.data.api.responses.CartListResponse;
import com.danareksa.investasik.data.api.responses.PortfolioInvestmentListResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by fajarfatur on 3/1/16.
 */
public class ListPortfolioPresenter {

    private ListPortfolioFragment fragment;

    public ListPortfolioPresenter(ListPortfolioFragment fragment) {
        this.fragment = fragment;
    }

    void getInvestmentList(Integer catId){

        InvestmentAcountGroupRequest request = new InvestmentAcountGroupRequest();
        request.setToken(PrefHelper.getString(PrefKey.TOKEN));
        request.setInvAccGroupId(catId);

        fragment.showProgressBar();
        fragment.getApi().getInvestmentList(request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PortfolioInvestmentListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(PortfolioInvestmentListResponse portfolioInvestmentListResponse) {

                        fragment.dismissProgressBar();
                        if(portfolioInvestmentListResponse.getCode() == 1) {
                            if (portfolioInvestmentListResponse.getData().getInvestmentList().size() > 0) {
                                fragment.noPortfolio(false);
                                fragment.investmentList = portfolioInvestmentListResponse;
                                fragment.loadInvestmentList();
                            } else {
                                fragment.noPortfolio(true);
                            }
                        }else{
                            fragment.showDialogAfterSubmit(portfolioInvestmentListResponse.getInfo());
                        }

                    }
                });
    }

    public void cartList() {
        fragment.getApi().getCartList(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<CartListResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(List<CartListResponse> response) {
                        ((BaseActivity) fragment.getActivity()).setNotifCount(0);

                        if (response != null && response.size() > 0) {
                            ((BaseActivity) fragment.getActivity()).setNotifCount(response.size());
                            fragment.cartList = response;
                            for (int i = 0; i < response.size(); i++) {

                            }
                        } else {

                        }
                    }
                });
    }




}
