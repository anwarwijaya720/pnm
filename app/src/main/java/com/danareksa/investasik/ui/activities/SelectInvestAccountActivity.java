package com.danareksa.investasik.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.fragments.tambahrekening.InvestAccountMainFragment;
import com.danareksa.investasik.util.Constant;

import butterknife.Bind;
import butterknife.BindString;


/**
 * Created by asep.surahman on 06/08/2018.
 */

public class SelectInvestAccountActivity extends BaseActivity{


    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;
    @BindString(R.string.produkTitle)
    String produkTitle;
    String typeAccount;
    LayoutInflater inflater;

    public static final String TAG = InvestAccountMainFragment.class.getSimpleName();

    public static void startActivity(BaseActivity sourceActivity, String type){
        Intent intent = new Intent(sourceActivity, SelectInvestAccountActivity.class);
        intent.putExtra("type", type);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout(){
        return R.layout.a_tambah_rekening;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setupToolbar();
        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        typeAccount = getIntent().getStringExtra("type");
        if(typeAccount.equals(Constant.INVEST_TYPE_LUMPSUM)){
            title.setText("Lumpsum");
        }else if(typeAccount.equals(Constant.INVEST_TYPE_REGULER)){
            title.setText("Reguler");
        }
        InvestAccountMainFragment.showFragment(this, typeAccount);
    }

    public void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed()
    {
        //super.onBackPressed();
        //startActivity(new Intent(SelectInvestAccountActivity.this, TambahRekeningActivity.class));

        callConfirmation();
        //finish();
    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(AccountDataFragment.TAG_ACCOUNT_DATA);
        fragment.onActivityResult(requestCode, resultCode, data);
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        return true;
    }

    private void callConfirmation(){
        InvestAccountMainFragment fragment = (InvestAccountMainFragment) getSupportFragmentManager().findFragmentByTag(TAG);
        fragment.backToBefore();
    }

}
