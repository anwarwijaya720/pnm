package com.danareksa.investasik.ui.fragments.redemption;

/**
 * Created by asep.surahman on 20/08/2018.
 */

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.PortfolioInvestment;
import com.danareksa.investasik.data.api.beans.RedemptionFee;
import com.danareksa.investasik.data.api.responses.PartialRedemtionResponse;
import com.danareksa.investasik.data.api.responses.RedeemFeeResponse;
import com.danareksa.investasik.data.api.responses.RedemptionOrderDetailResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.RedemptionActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.AmountFormatter;
import com.danareksa.investasik.util.DateUtil;
import com.danareksa.investasik.util.ui.RangeSeekBar;
import com.mobsandgeeks.saripaar.annotation.Checked;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import icepick.State;

/**
 * Created by fajarfatur on 3/14/16.
 */
public class ConfirmationNewFragment extends BaseFragment {

    public static final String TAG = ConfirmationNewFragment.class.getSimpleName();
    private static final String INVESTMENT = "investment";
    private static final String PACKAGES = "packages";
    private static final String IFUA = "ifua";
    private static final String STATUS_SPECIAL_FEE = "statusSpecialFee";
    private static final String SPECIAL_FEE = "specialFee";
    private static final String TOTAL_DAYS = "totalDays";

    @Bind(R.id.bConfirm)
    Button bConfirm;
    @Bind(R.id.tvPackageName)
    TextView tvPackageName;
    @Bind(R.id.tvMarketValue)
    TextView tvMarketValue;
    @Bind(R.id.tvInvestment)
    TextView tvInvestment;
    @Bind(R.id.etAmount)
    EditText etAmount;
    @Bind(R.id.llvalidasi)
    LinearLayout llvalidasi;
    @Bind(R.id.tvValidasi)
    TextView tvValidasi;
    @Bind(R.id.tvFee)
    TextView tvFee;
    @Checked
    @Bind(R.id.cbAgree)
    CheckBox cbAgree;
    @Bind(R.id.seekbar_placeholder)
    LinearLayout llSeekBar;
    @Bind(R.id.togglebutton_redeemtion)
    Switch toggleButtonRedeemtion;
    @State
    PortfolioInvestment investment;
    @State
    Packages packages;
    @State
    RedemptionOrderDetailResponse redemptionOrderDetailResponse;
    @State
    RedeemFeeResponse redemptionFees;
    List<RedemptionFee> redemptionFeeSetups;
    @State
    PartialRedemtionResponse partialRedemtionResponse;
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.etAmountUnit)
    EditText etAmountUnit;
    @Bind(R.id.etAmountInvest)
    EditText etAmountInvest;
    @Bind(R.id.rlCumstomAmount)
    LinearLayout rlCumstomAmount;
    @Bind(R.id.llValueInvest)
    LinearLayout llValueInvest;
    @Bind(R.id.rgReedem)
    RadioGroup rgReedem;
    @Bind(R.id.tvLabelReedem)
    TextView tvLabelReedem;
    @Bind(R.id.rbUnit)
    RadioButton rbUnit;
    @Bind(R.id.rbInvest)
    RadioButton rbInvest;

    @Bind(R.id.rangeSeekBar)
    RangeSeekBar rsbUnit;
    @Bind(R.id.rangeSeekBar2)
    RangeSeekBar rsbInvest;

    @Bind(R.id.ivProduct)
    CircleImageView ivProduct;

    @Bind(R.id.tvNABDesc)
    TextView tvNABDesc;

    @Bind(R.id.wvNabDesc)
    WebView wvNabDesc;

    public String answerPartial = "";
    public String answer = "full";
    public String ifua = "";
    private double partialRedeem;
    private double feeAdmin = 0.0;


    private ConfirmationNewPresenter presenter;
    private double amount, amountMin;
    public float max, min;
    private double amountRedeem = 0;
    private double amountUnit = 0.0;
    public double redemFee = 0.0;
    private double currentUnit = 0.0;
    private double saldoUnit = 0.0;
    private double saldoInvest = 0.0;
    private boolean manualInput = false;
    private boolean doEventChange = true;

    boolean statusSpecialFee;
    double specialFee;
    Integer totalDays;
    String lastNavDate = "";


    public static void showFragment(BaseActivity sourceActivity, PortfolioInvestment investment, Packages packages, String ifua,  boolean statusSpecialFee, double specialFee, Integer totalDays){

        if (!sourceActivity.isFragmentNotNull(TAG)) {
            ConfirmationNewFragment fragment = new ConfirmationNewFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(INVESTMENT, investment);
            bundle.putSerializable(PACKAGES, packages);
            bundle.putSerializable(IFUA, ifua);
            bundle.putSerializable(STATUS_SPECIAL_FEE, statusSpecialFee);
            bundle.putSerializable(SPECIAL_FEE, specialFee);
            bundle.putSerializable(TOTAL_DAYS, totalDays);
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }


    @Override
    protected int getLayout() {
        return R.layout.f_redemption_confirmation_new;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        presenter = new ConfirmationNewPresenter(this);
        ifua = getArguments().getString(IFUA);
        if (investment == null)
            investment = (PortfolioInvestment) getArguments().getSerializable(INVESTMENT);

        saldoInvest = investment.getTotalInvestmentMarketValue();
        //if current unit & NAB exis
        if(investment.getInvestmentComposition().size() > 0 && investment.getInvestmentComposition() != null){

            for (int i = 0; i < investment.getInvestmentComposition().size(); i++) {
                saldoUnit += investment.getInvestmentComposition().get(i).getCurrentUnit();
            }

            amountUnit  = investment.getTotalInvestmentMarketValue() / investment.getInvestmentComposition().get(0).getCurrentUnit();
            currentUnit = investment.getInvestmentComposition().get(0).getCurrentUnit();
            System.out.println("current unit : " + currentUnit);

            if(!investment.getInvestmentComposition().get(0).getLastNavDate().equals("")){
                try {
                    String priceDate = investment.getInvestmentComposition().get(0).getLastNavDate();
                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
                    Date pDate = formatter.parse(priceDate);
                    lastNavDate = DateUtil.format(pDate,"dd-MM-yyyy");
                } catch (Exception e) {
                    lastNavDate = investment.getInvestmentComposition().get(0).getLastNavDate();
                }
            }
        }
        statusSpecialFee = (Boolean) getArguments().getSerializable(STATUS_SPECIAL_FEE);
        specialFee       = (Double) getArguments().getSerializable(SPECIAL_FEE);
        totalDays        = (Integer) getArguments().getSerializable(TOTAL_DAYS);

        if (packages == null)
            packages = (Packages) getArguments().getSerializable(PACKAGES);
        answer = "full";
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        ((RedemptionActivity) getActivity()).setStep(1);
        toggleButtonRedeemtion.setChecked(true);
        rlCumstomAmount.setVisibility(View.GONE);
        tvLabelReedem.setVisibility(View.GONE);
        etAmount.setEnabled(false);
        etAmountInvest.setEnabled(false);
        etAmountUnit.setEnabled(false);
        rsbInvest.setEnabled(false);
        rsbUnit.setEnabled(false);
        bConfirm.setEnabled(false);
        presenter.redemptionFee(packages);
        presenter.getRangeOfPartialRedemtion(investment.getInvestmentAccountNumber());

        //if current unit & NAB not exis
        if(investment.getInvestmentComposition().size() > 0 && investment.getInvestmentComposition() != null){
        }else{
            llvalidasi.setVisibility(View.VISIBLE);
            toggleButtonRedeemtion.setEnabled(false);
            answer = "full";
        }
        if (!lastNavDate.equals("")){
            String desc = "<html><head></head>\n" +
                    " <body style='text-align:justify;color:#717073;font-size:14.3px'>Nilai investasi yang tercantum merupakan nilai indikasi berdasarkan NAB tanggal " + lastNavDate + ". Nilai penjualan kembali Anda bisa berubah sesuai dengan NAB pada saat pemrosesan transaksi Anda.</body>\n" +
                    "</html>";
            wvNabDesc.loadData(desc, "text/html; charset=utf-8", "utf-8");
            tvNABDesc.setText(Html.fromHtml(desc));
        }else{
            tvNABDesc.setVisibility(View.GONE);
        }
    }



    void bindInvestmentData(){
        tvPackageName.setText(investment.getPackageName());
        tvInvestment.setText(ifua);
        tvMarketValue.setText(AmountFormatter.format(investment.getTotalInvestmentMarketValue()));

        if(statusSpecialFee == true){
            amount   = (specialFee) * investment.getTotalInvestmentMarketValue();
            feeAdmin = specialFee * investment.getTotalInvestmentMarketValue();
            redemFee = specialFee;
            System.out.println("amount true.. : " + amount + " fee : " + feeAdmin + " special fee : " + specialFee);
        }else{

            Boolean isInRange = false;

            if (redemptionFeeSetups != null && redemptionFeeSetups.size() > 0) {
                for (int i = 0; i < redemptionFeeSetups.size(); i++) {
                    RedemptionFee fee = redemptionFeeSetups.get(i);

                    Double dayMax =  fee.getDayMax();

                    if (fee.getDayMax().intValue() == 0) {
                            dayMax = Double.MAX_VALUE;
                    }

                    if (totalDays >= fee.getDayMin() && totalDays <= dayMax) {
                        amount   = (fee.getFeeAmount()) * investment.getTotalInvestmentMarketValue();
                        feeAdmin = fee.getFeeAmount() * investment.getTotalInvestmentMarketValue();
                        redemFee = fee.getFeeAmount();
                        isInRange = true;
                        break;
                    }
                }
            }
            if (!isInRange) {
                amount   = (redemFee) * investment.getTotalInvestmentMarketValue();
                feeAdmin = redemFee * investment.getTotalInvestmentMarketValue();
                System.out.println("amount false.. : " + amount + " fee : " + feeAdmin);
            }
        }

        amountMin = amount*min/100;
        etAmount.setText(AmountFormatter.formatFeePercent(redemFee) + " | " + AmountFormatter.format(amount));
        tvFee.setText(AmountFormatter.formatUnitComma4(saldoUnit));

        Picasso.with(getActivity()).load(InviseeService.IMAGE_DOWNLOAD_URL + investment.getPackageImageKey() + "&token=" + PrefHelper.getString(PrefKey.TOKEN))
                .placeholder(R.drawable.ic_logo_launcher)
                .error(R.drawable.ic_logo_launcher)
                .into(ivProduct);
    }


    @OnClick(R.id.bConfirm)
    void confirm() {
        validator.validate();
    }


    @Override
    public void onValidationSucceeded(){
        super.onValidationSucceeded();
        ((RedemptionActivity) getActivity()).setStep(2);
        Log.d(TAG, "onValidationSucceeded: " + answer);
        if(toggleButtonRedeemtion.isChecked() == true){
            answer = "full";
            AccountFragment.showFragment((BaseActivity) getActivity(), investment, packages, answer);
        }else{

            if(rbUnit.isChecked() == true){
                if(!answerPartial.equals("")){
                    if(!etAmountUnit.getText().toString().equals("0")){
                        AccountFragment.showFragment((BaseActivity) getActivity(), investment, packages, answerPartial);
                    }else{
                        showDialog("Silahkan masukkan unit");
                    }
                }else{
                    showDialog("Silahkan masukkan unit");
                }
            }else if(rbInvest.isChecked() == true){
                if(!answerPartial.equals("")){
                    if(!etAmountInvest.getText().toString().equals("0")){
                        AccountFragment.showFragment((BaseActivity) getActivity(), investment, packages, answerPartial);
                    }else{
                        showDialog("Silahkan masukkan nilai investasi");
                    }
                }else{
                    showDialog("Silahkan masukkan nilai investasi");
                }
            }else{
                showDialog("Silahkan pilih jenis penjualan kembali");
            }
        }


    }

    @OnCheckedChanged(R.id.togglebutton_redeemtion)
    public void onToggleChanged(){
        if (toggleButtonRedeemtion.isChecked() == false) {

            //DIMRDO-1448
            rbUnit.setChecked(true);
            rsbUnit.setEnabled(true);
            rsbInvest.setEnabled(false);
            rbInvest.setChecked(false);
            rbUnit.setChecked(true);
            etAmountInvest.setEnabled(false);
            etAmountUnit.setEnabled(true);
            viewSeekBarUnit();

            if(rbUnit.isChecked() == true){
                partialRedeem = rsbUnit.getSelectedMaxValue().floatValue() / 100;
                setvalueAmount(partialRedeem, false);
            }else if(rbInvest.isChecked() == true){
                partialRedeem = rsbInvest.getSelectedMaxValue().floatValue() / 100;
                setvalueAmount(partialRedeem, false);
            }else{
                etAmount.setText("0% | IDR0,00");
            }

            etAmount.setEnabled(false);
            rlCumstomAmount.setVisibility(View.VISIBLE);
            //tvLabelReedem.setVisibility(View.VISIBLE);
        } else {
            etAmount.setText(AmountFormatter.formatFeePercent(redemFee) + " | " + AmountFormatter.format(amount));
            etAmount.setEnabled(false);
            answer = "full";
            rlCumstomAmount.setVisibility(View.GONE);
            //tvLabelReedem.setVisibility(View.GONE);
        }
    }


    @OnClick(R.id.tvTryAgain)
    void retryConnection() {
        presenter.getRedemptionFee(investment.getInvestmentAccountNumber());
    }


    @OnClick({ R.id.rbUnit, R.id.rbInvest })
    public void onRadioButtonClicked(RadioButton radioButton){
        boolean checked = radioButton.isChecked();
        rgReedem.clearCheck();
        manualInput = false;
        switch (radioButton.getId()){
            case R.id.rbUnit:
                if(checked){
                    rsbUnit.setEnabled(true);
                    rsbInvest.setEnabled(false);
                    rbInvest.setChecked(false);
                    rbUnit.setChecked(true);
                    etAmountInvest.setEnabled(false);
                    etAmountUnit.setEnabled(true);
                    viewSeekBarUnit();
                    calculateUnit(etAmountUnit.getText());
                    partialRedeem = rsbUnit.getSelectedMaxValue().floatValue() / 100;
                    setvalueAmount(partialRedeem, false);
                }
                break;
            case R.id.rbInvest:
                if(checked){
                    rsbUnit.setEnabled(false);
                    rsbInvest.setEnabled(true);
                    rbInvest.setChecked(true);
                    rbUnit.setChecked(false);
                    etAmountUnit.setEnabled(false);
                    etAmountInvest.setEnabled(true);
                    viewSeekBarInvest();
                    calculateInvest(etAmountInvest.getText());
                    partialRedeem = rsbInvest.getSelectedMaxValue().floatValue() / 100;
                    setvalueAmount(partialRedeem, false);
                }
                break;
        }
    }

    void viewSeekBarUnit(){

        if(answerPartial.equals("")){
            setFisrtValueUnit();
            setFisrtValueInvest();
            partialRedeem = rsbUnit.getSelectedMaxValue().floatValue() / 100;
            setvalueAmount(partialRedeem, false);
        }

        rsbUnit.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Float>(){
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Float minValue, Float maxValue){
                partialRedeem = rsbUnit.getSelectedMaxValue().floatValue() / 100;
                etAmountInvest.removeTextChangedListener(amountInvestWatcher);
                etAmountUnit.removeTextChangedListener(amountUnitWatcher);
                setvalueAmount(partialRedeem, false);
                etAmountUnit.addTextChangedListener(amountUnitWatcher);
                etAmountInvest.addTextChangedListener(amountInvestWatcher);
                rsbInvest.setSelectedMaxValue(maxValue);
            }

            @Override
            public void onTrackingTouch(Boolean isTouch) {
                manualInput = false;
                ((BaseActivity)getActivity()).hideKeyboard();
            }
        });

    }

    void viewSeekBarInvest(){

        if(answerPartial.equals("")){
            setFisrtValueUnit();
            setFisrtValueInvest();
            partialRedeem = rsbInvest.getSelectedMaxValue().floatValue() / 100;
            setvalueAmount(partialRedeem, false);
        }

        rsbInvest.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Float>(){
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Float minValue, Float maxValue){
                partialRedeem = rsbInvest.getSelectedMaxValue().floatValue() / 100;
                etAmountInvest.removeTextChangedListener(amountInvestWatcher);
                etAmountUnit.removeTextChangedListener(amountUnitWatcher);
                setvalueAmount(partialRedeem, false);
                etAmountUnit.addTextChangedListener(amountUnitWatcher);
                etAmountInvest.addTextChangedListener(amountInvestWatcher);
                rsbUnit.setSelectedMaxValue(maxValue);
            }

            @Override
            public void onTrackingTouch(Boolean isTouch) {
                manualInput = false;
                ((BaseActivity)getActivity()).hideKeyboard();
            }
        });
    }

    protected TextWatcher amountInvestWatcher = new TextWatcher() {

        @Override
        public void afterTextChanged(Editable s) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            etAmountInvest.removeTextChangedListener(this);
            if(rbInvest.isChecked()){
                try {
                    calculateInvest(s);
                }catch (Exception e){
                    Log.e(TAG, "afterTextChanged: exception catched...", e);
                }
            }
            etAmountInvest.addTextChangedListener(this);
        }
    };

    void initInvest(){
        InputFilter ip = (source, start, end, dest, dstart, dend) -> {
            int len = end - start;
            if (len == 0) {
                return source;
            }
            int dlen = dest.length();
            for (int i = 0; i < dstart; i++) {
                if (dest.charAt(i) == ',') {
                    return (dlen-(i+1) + len > 2) ?
                            "" :
                            new SpannableStringBuilder(source, start, end);
                }
            }
            return source;
        };

        InputFilter[] i = new InputFilter[] {ip};
        etAmountInvest.setFilters(i);
        etAmountInvest.addTextChangedListener(amountInvestWatcher);
    }

    protected TextWatcher amountUnitWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            etAmountUnit.removeTextChangedListener(this);
            if(rbUnit.isChecked()){
                try {
                    calculateUnit(s);
                }catch (Exception e){
                    Log.e(TAG, "afterTextChanged: exception catched...", e);
                }
            }
            etAmountUnit.addTextChangedListener(this);
        }
    };

    void initUnit(){
         InputFilter ip = (source, start, end, dest, dstart, dend) -> {
             int len = end - start;
             if (len == 0) {
                 return source;
             }
             int dlen = dest.length();
             for (int i = 0; i < dstart; i++) {
                 if (dest.charAt(i) == ',') {
                     return (dlen-(i+1) + len > 4) ?
                             "" :
                             new SpannableStringBuilder(source, start, end);
                 }
             }
             return source;
         };

         InputFilter[] i = new InputFilter[] {ip};
         etAmountUnit.setFilters(i);
         etAmountUnit.addTextChangedListener(amountUnitWatcher);
    }

    void calculateInvest(CharSequence s){
        if(s.toString().equals("")) {
            rsbInvest.setSelectedMaxValue(0);
            rsbUnit.setSelectedMaxValue(0);
            return;
        }

        try {
            Double amountRedeem = Double.parseDouble(s.toString().replace(".", "")
                    .replace(",","."));

            double percent = (amountRedeem / saldoInvest) * 100;

            if(percent <= min){
                rsbInvest.setSelectedMaxValue(min);
            }else if (percent >= max){
                rsbInvest.setSelectedMaxValue(max);
            }else{
                rsbInvest.setSelectedMaxValue(percent);
            }

            if(amountRedeem >= saldoInvest){
                percent = max;
                doEventChange = false;
                etAmountInvest.setText(AmountFormatter.formatWithoutIdr(saldoInvest));
                //rsbInvest.setSelectedMaxValue(max);
            } else {
                doEventChange = true;
            }

            partialRedeem = amountRedeem / saldoInvest; //rsbInvest.getSelectedMaxValue().floatValue() / 100;
            etAmountUnit.setText(AmountFormatter.formatUnitWithComma4(((partialRedeem*saldoUnit))));
            setvalueAmount(partialRedeem, true);
            rsbUnit.setSelectedMaxValue(percent);
            answerPartial = String.valueOf(amountRedeem / saldoInvest);
            //rsbInvest.setSelectedMaxValue(percent);
        } catch (Exception e) {
            doEventChange = true;
            Log.e(TAG, "calculateInvest: exception catched...", e);
        }
    }

    //dana reksa seruni pasar uang II
    void calculateUnit(CharSequence s){
        if(s.toString().equals("")) {
            rsbInvest.setSelectedMaxValue(0);
            rsbUnit.setSelectedMaxValue(0);
            return;
        }

        try {

            Double amountRedeem = Double.parseDouble(s.toString().replace(".", "")
                    .replace(",","."));

            double percent = (amountRedeem / saldoUnit) * 100;

            if(percent <= min){
                rsbUnit.setSelectedMaxValue(min);
            }else if (percent >= max){
                rsbUnit.setSelectedMaxValue(max);
            }else{
                rsbUnit.setSelectedMaxValue(percent);
            }

            if(amountRedeem >= saldoUnit){
                percent = max;
                doEventChange = false;
                etAmountUnit.setText(AmountFormatter.formatUnitWithComma4(saldoUnit));
                //rsbUnit.setSelectedMaxValue(max);
            } else {
                doEventChange = true;
            }

            partialRedeem = amountRedeem / saldoUnit; //rsbUnit.getSelectedMaxValue().floatValue() / 100;
            etAmountInvest.setText(AmountFormatter.formatWithoutIdr(partialRedeem*saldoInvest));
            setvalueAmount(partialRedeem, true);
            //rsbUnit.setSelectedMaxValue(percent);
            rsbInvest.setSelectedMaxValue(percent);
            answerPartial = String.valueOf(amountRedeem / saldoUnit);
            System.out.println("Percentage : " + answerPartial);
        }catch (Exception e) {
            doEventChange = true;
            Log.e(TAG, "calculateUnit: exception catched...", e);
        }
    }

    private void setvalueAmount(double partialRedeem, boolean changedBySeekbar){

        if (toggleButtonRedeemtion.isChecked() == false){
            //Partial
            Double partialAmount = redemFee * (partialRedeem*saldoInvest);
            etAmount.setText(AmountFormatter.formatFeePercent(redemFee) + " | " + AmountFormatter.format(partialAmount));
        } else {
            //Full
            etAmount.setText(AmountFormatter.formatFeePercent(redemFee) + " | " + AmountFormatter.format(redemFee * saldoInvest));
        }

        if (!changedBySeekbar) {
            etAmountUnit.setText(AmountFormatter.formatUnitWithComma4(((partialRedeem*saldoUnit))));
            etAmountInvest.setText(AmountFormatter.formatWithoutIdr(partialRedeem*saldoInvest));
        }

        answerPartial = String.valueOf(partialRedeem);
    }

    void showDialog(String info){
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.infortmation).toUpperCase())
                .titleColor(Color.BLACK)
                .content(info)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which){
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }

    private void setFisrtValueUnit(){
        float maxRsb = max;
        rsbUnit.setRangeValues(min, maxRsb);
        rsbUnit.setSelectedMinValue(min);
        //rsbUnit.setSelectedMaxValue(min);
    }

    private void setFisrtValueInvest(){
        float maxRsb = max;
        rsbInvest.setRangeValues(min, maxRsb);
        rsbInvest.setSelectedMinValue(min);
        //rsbInvest.setSelectedMaxValue(min);
    }
}
