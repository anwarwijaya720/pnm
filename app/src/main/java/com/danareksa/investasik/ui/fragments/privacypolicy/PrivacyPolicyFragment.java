package com.danareksa.investasik.ui.fragments.privacypolicy;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.PrivacyPolicy;
import com.danareksa.investasik.data.api.responses.PrivacyPolicyResponse;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;

import butterknife.Bind;
import butterknife.OnClick;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by pandu.abbiyuarsyah on 13/03/2017.
 */

public class PrivacyPolicyFragment extends BaseFragment {

    public static final String TAG = PrivacyPolicyFragment.class.getSimpleName();
    public static Activity activity;

    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;

    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            activity = sourceActivity;
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, R.anim.slide_out_left, android.R.anim.slide_in_left, R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.container, new PrivacyPolicyFragment(), TAG);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
         getPrivacyPolicy();
    }

    static void setContent(PrivacyPolicy privacyPolicy) {
        WebView wv1 = (WebView) activity.findViewById(R.id.webView);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.loadData(privacyPolicy.getPrivacyPolicy(),"text/html", null);
    }

    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }

    public void getPrivacyPolicy(){
        showProgressBar();
        this.getApi().getPrivacyPolicy()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PrivacyPolicyResponse>() {

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        connectionError();
                    }

                    @Override
                    public void onNext(PrivacyPolicyResponse privacyPolicyResponse) {
                        dismissProgressBar();
                        setContent(privacyPolicyResponse.getData());
                    }
                });
    }

    @Override
    protected int getLayout() {
        return R.layout.f_privacy_policy;
    }


    @Override
    public void onResume() {
        super.onResume();
        //getActivity().setTitle("Kebijakan Privasi");
    }

    @OnClick(R.id.tvTryAgain)
    void retryConnection(){
        getPrivacyPolicy();
    }

}
