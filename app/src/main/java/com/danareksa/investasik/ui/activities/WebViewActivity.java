package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.danareksa.investasik.R;

import butterknife.Bind;

public class WebViewActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.webView)
    WebView wv;

    public static void startActivity(BaseActivity sourceActivity, String url) {
        Intent intent = new Intent(sourceActivity, WebViewActivity.class);
        intent.putExtra("WEB_URL", url);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_web_view;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);

        String url  = (String) getIntent().getSerializableExtra("WEB_URL");

        wv.setWebViewClient(new WebViewClient());
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setDomStorageEnabled(true);
        wv.setHorizontalScrollBarEnabled(false);
        wv.setOverScrollMode(WebView.OVER_SCROLL_NEVER);
        wv.loadUrl(url);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        if (IS_FROM_LANDING){
            startActivity(new Intent(WebViewActivity.this, ArticleSlideshowActivity.class));
        } else {
            startActivity(new Intent(WebViewActivity.this, DashboardActivity.class));
        }

        finish();
    }


    @Override
    protected void onDestroy() {
        System.out.println("=============> call the garbage collector to free memory");
        super.onDestroy();
    }


}
