package com.danareksa.investasik.ui.adapters.rv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.TrxTransCode;
import com.danareksa.investasik.data.api.responses.CartListResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.AmountFormatter;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by asep.surahman on 25/06/2018.
 */

public class MandiriClickpaySumaaryAdapter extends RecyclerView.Adapter<MandiriClickpaySumaaryAdapter.MandiriClickpaySumaaryHolder>{

    private Context context;
    private List<CartListResponse> list;
    private TrxTransCode trxTransCode;
    private BaseFragment fragment;

    public MandiriClickpaySumaaryAdapter(Context context, List<CartListResponse> list, TrxTransCode trxTransCode, BaseFragment fragment) {
        this.context = context;
        this.list = list;
        this.trxTransCode = trxTransCode;
        this.fragment = fragment;
    }

    @Override
    public MandiriClickpaySumaaryAdapter.MandiriClickpaySumaaryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_mandiri_clikpay, parent, false);
        MandiriClickpaySumaaryAdapter.MandiriClickpaySumaaryHolder holder = new MandiriClickpaySumaaryAdapter.MandiriClickpaySumaaryHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MandiriClickpaySumaaryAdapter.MandiriClickpaySumaaryHolder holder, int position) {
        final CartListResponse item = list.get(position);

        holder.itemView.setTag(item);
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("");
        ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);

        Picasso.with(context).load(InviseeService.IMAGE_DOWNLOAD_URL + trxTransCode.getPackageImageKey() + "&token=" + PrefHelper.getString(PrefKey.TOKEN))
                .placeholder(R.drawable.ic_logo_launcher)
                .error(R.drawable.ic_logo_launcher)
                .into(holder.icon);

        holder.tvPackageName.setText(item.getFundPackages().getFundPackageName());
        holder.tvTransValue.setText(AmountFormatter.formatCurrencyWithoutComma(Double.parseDouble(item.getTransactionAmount())));
        holder.tvBiayaAdmin.setText(AmountFormatter.formatCurrencyWithoutComma(trxTransCode.getAdminFee()));
        holder.tvBiaya.setText(item.getFeePercentage() + " | " + AmountFormatter.formatUnitWithComma(item.getFeeAmount()));
        holder.tvTotal.setText(AmountFormatter.formatCurrencyWithoutComma(Double.parseDouble(item.getTotal())));
        double fee = trxTransCode.getAdminFee();
        double tot = Double.parseDouble(item.getTotal());
        holder.tvTotalBayar.setText(AmountFormatter.formatCurrencyWithoutComma(fee + tot));
    }


    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public static class MandiriClickpaySumaaryHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.icon)
        CircleImageView icon;

        @Bind(R.id.tvTransValue)
        TextView tvTransValue;

        @Bind(R.id.tvPackageName)
        TextView tvPackageName;

        @Bind(R.id.tvBiaya)
        TextView tvBiaya;

        @Bind(R.id.tvTotal)
        TextView tvTotal;

        @Bind(R.id.tvBiayaAdmin)
        TextView tvBiayaAdmin;


        @Bind(R.id.tvTotalBayar)
        TextView tvTotalBayar;



        public MandiriClickpaySumaaryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
