package com.danareksa.investasik.ui.fragments.kycnew;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.eventBus.RxBusObject;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by asep.surahman on 21/05/2018.
 */

public class Kyc10AdvancedSourceOfFundsFragment extends KycBaseNew{

    public static final String TAG = Kyc10AdvancedSourceOfFundsFragment.class.getSimpleName();

    public static final String KYC_DATA_REQUEST = "kycDataRequest";

    @Bind(R.id.sFundSourceRelationship)
    Spinner sFundSourceRelationship;

    @Bind(R.id.sFundSourceOccupation)
    Spinner sFundSourceOccupation;

    @Bind(R.id.sSundSourceOfFundSource)
    Spinner sSundSourceOfFundSource;

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etFundSourceName)
    EditText etFundSourceName;

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etFundSourceEmployer)
    EditText etFundSourceEmployer;

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etFundSourceIdNumber)
    EditText etFundSourceIdNumber;

    @Bind(R.id.tvSumberDana)
    TextView tvSumberDana;

    @Bind(R.id.bKtp)
    TextView bKtp;

    @Bind(R.id.bPaspor)
    TextView bPaspor;

    String fundSourceIdType = "";

    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new Kyc10AdvancedSourceOfFundsFragment(), TAG);
            fragmentTransaction.commit();
        }
    }


    public static Fragment getFragment(KycDataRequest kycDataRequest){
        Bundle bundle = new Bundle();
        bundle.putSerializable(KYC_DATA_REQUEST, kycDataRequest);
        Kyc10AdvancedSourceOfFundsFragment fragment = new Kyc10AdvancedSourceOfFundsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayout() {
        return R.layout.f_register_lanjut_sumber_dana_ktp_dan_passpor;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    public void init(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")) {
            disableField();
            tvSumberDana.setText("Sumber dana Anda berasal dari pasangan/orang tua Anda");
        }

        if(request.getFundSourceIdType() != null){
            if(request.getFundSourceIdType().equals(Constant.ID_TYPE_KTP)){
                fundSourceIdType = Constant.ID_TYPE_KTP;
                changeLabelKtp();
                changeButtonActive(true, false);
            }
            if(request.getFundSourceIdType().equals(Constant.ID_TYPE_PAS)){
                fundSourceIdType = Constant.ID_TYPE_PAS;
                changeLabelPaspor();
                changeButtonActive(false, true);
            }
        }

        etFundSourceIdNumber.setText(request.getFundSourceIdNumber());
        etFundSourceEmployer.setText(request.getFundSourceEmployer());
        etFundSourceName.setText(request.getFundSourceName());
        setupSpinnerWithSpecificLookupSelection(sFundSourceRelationship, getKycLookupFromRealm(Constant.KYC_CAT_RELATIONSHIP), request.getFundSourceRelationship());
        setupSpinnerWithSpecificLookupSelection(sFundSourceOccupation, getKycLookupFromRealm(Constant.KYC_CAT_OCCUPATION), request.getFundSourceOccupation());
        setupSpinnerWithSpecificLookupSelection(sSundSourceOfFundSource, getKycLookupFromRealm(Constant.KYC_CAT_SOURCE_OF_INCOME), request.getFundSourceOfFundSource());
    }


    public void setValueSourceOfFund(){
        request.setFundSourceIdType(fundSourceIdType);
        request.setFundSourceIdNumber(getAndTrimValueFromEditText(etFundSourceIdNumber));
        request.setFundSourceEmployer(getAndTrimValueFromEditText(etFundSourceEmployer));
        request.setFundSourceName(getAndTrimValueFromEditText(etFundSourceName));
        request.setFundSourceRelationship(getKycLookupCodeFromSelectedItemSpinner(sFundSourceRelationship));
        request.setFundSourceOccupation(getKycLookupCodeFromSelectedItemSpinner(sFundSourceOccupation));
        request.setFundSourceOfFundSource(getKycLookupCodeFromSelectedItemSpinner(sSundSourceOfFundSource));
    }

    //sumber dana selain orang tua anak langsung ke tab pemilik manfaar
    /*
        Kode Jenis ID Sumber Dana
        Front End : Jika sumber dana nasabah bukan dari penghasilan pasangan atau dari orang tua/anak,
        copy dari Jenis ID nasabah
    */


    @Override
    public void nextWithoutValidation(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")) {
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
        }else{
            if(!validate()){
                onValidFailed();
            }else{
                setValueSourceOfFund();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
            }
            return;
        }

    }


    @Override
    public void saveDataKycWithBackpress(){
        if(!validate()){
            onValidFailed();
            return;
        }else{
            if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
                setValueSourceOfFund();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.SAVE_KYC_DATA, request));
            }else{
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
            }
        }
    }


    @Override
    public void previewsWithoutValidation(){
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null));
    }


    @OnClick(R.id.llPaspor)
    void llPaspor(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
            fundSourceIdType = Constant.ID_TYPE_PAS;
            changeLabelPaspor();
            changeButtonActive(false, true);
        }
    }


    @OnClick(R.id.llKtp)
    void llKtp(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
            fundSourceIdType = Constant.ID_TYPE_KTP;
            changeLabelKtp();
            changeButtonActive(true, false);
        }

    }

    //disable
    @Override
    public void onValidationSucceeded(){
        super.onValidationSucceeded();
    }


    private void changeLabelPaspor(){
        bPaspor.setText(getCheckedIcon());
        bKtp.setText("A");
    }

    private void changeLabelKtp(){
        bKtp.setText(getCheckedIcon());
        bPaspor.setText("B");
    }

    private void disableField(){
        etFundSourceIdNumber.setEnabled(false);
        etFundSourceEmployer.setEnabled(false);
        etFundSourceName.setEnabled(false);
        sFundSourceRelationship.setEnabled(false);
        sFundSourceOccupation.setEnabled(false);
        sSundSourceOfFundSource.setEnabled(false);
    }

    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }

    public boolean validate(){
        boolean valid = true;
        String fundSourceIdNumber = etFundSourceIdNumber.getText().toString();
        String fundSourceEmployer = etFundSourceEmployer.getText().toString();
        String fundSourcename     = etFundSourceName.getText().toString();

        if(fundSourceIdNumber.isEmpty() && fundSourceIdNumber.length() <= 0){
            etFundSourceIdNumber.setError("Mohon isi kolom ini");
            valid = false;
        }


        if(fundSourceEmployer.isEmpty() && fundSourceEmployer.length() <= 0){
            etFundSourceEmployer.setError("Mohon isi kolom ini");
            valid = false;
        }

        if(!fundSourceEmployer.isEmpty() && fundSourceEmployer.length() < 3){
            etFundSourceEmployer.setError("Mohon isi nama perusahaan dengan benar");
            valid = false;
        }

        if(fundSourcename.isEmpty() && fundSourcename.length() <= 0){
            etFundSourceName.setError("Mohon isi kolom ini");
            valid = false;
        }

        if(!fundSourcename.isEmpty() && fundSourcename.length() < 3){
            etFundSourceName.setError("Mohon isi nama lengkap dengan benar");
            valid = false;
        }

        if(fundSourceIdType.equals("")){
            Toast.makeText(getContext(), "jenis ID belum dipilih", Toast.LENGTH_LONG).show();
            valid = false;
        }

        if(!fundSourceIdType.equals("")){
            if(fundSourceIdType == Constant.ID_TYPE_KTP){
                if(fundSourceIdNumber.length() < 16 || fundSourceIdNumber.length() > 16){
                    etFundSourceIdNumber.setError("Data tidak sesuai");
                    valid = false;
                }
            }else if(fundSourceIdType == Constant.ID_TYPE_PAS){
                if(fundSourceIdNumber.length() < 3){
                    etFundSourceIdNumber.setError("Data tidak sesuai");
                    valid = false;
                }
            }
        }

        if(sFundSourceRelationship.getSelectedItem().toString().equals("") ||
            sFundSourceRelationship.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
            valid = false;
            ((TextView) sFundSourceRelationship.getSelectedView()).setError("Mohon isi kolom ini");
        }

        if(sFundSourceOccupation.getSelectedItem().toString().equals("") ||
            sFundSourceOccupation.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
            valid = false;
            ((TextView) sFundSourceOccupation.getSelectedView()).setError("Mohon isi kolom ini");
        }

        if(sSundSourceOfFundSource.getSelectedItem().toString().equals("") ||
            sSundSourceOfFundSource.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
            valid = false;
            ((TextView) sSundSourceOfFundSource.getSelectedView()).setError("Mohon isi kolom ini");
        }
        return valid;
    }

      /*
           "fundSourceName" : "FAM",
           "fundSourceRelationship" : "HSB",
           "fundSourceIdType" : "IDC",
           "fundSourceIdNumber" : "2300000000001199",
           "fundSourceOccupation" : "PNS",
           "fundSourceEmployer" : "PT PLN",
           "fundSourceOfFundSource" : "EMP",
    */

    private void changeButtonActive(boolean ktp, boolean paspor){
        bKtp.setEnabled(ktp);
        bPaspor.setEnabled(paspor);
    }

    private Spannable getCheckedIcon(){
        Spannable buttonLabel = new SpannableString(" ");
        buttonLabel.setSpan(new ImageSpan(getActivity().getApplicationContext(), R.drawable.ic_checked,
                ImageSpan.ALIGN_BOTTOM), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return  buttonLabel;
    }



    @Override
    public void previewsWhileError(){

    }

}
