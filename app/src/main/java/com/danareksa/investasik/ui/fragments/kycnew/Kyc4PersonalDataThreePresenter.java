package com.danareksa.investasik.ui.fragments.kycnew;

import com.danareksa.investasik.data.api.beans.City;
import com.danareksa.investasik.data.api.beans.State;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by asep.surahman on 17/07/2018.
 */

public class Kyc4PersonalDataThreePresenter {

    private Kyc4PersonalDataThreeFragment fragment;

    public Kyc4PersonalDataThreePresenter (Kyc4PersonalDataThreeFragment fragment){
        this.fragment = fragment;
    }

    public void getState(String countryId, final String stateId){
        fragment.showProgressBar();
        fragment.getApi().getStateByCountry(countryId, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<State>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(List<State> states) {
                        if (states.size() > 0) {
                            fragment.states = states;
                            if(stateId != null && !stateId.equals("")){
                                fragment.setupSpinnerState(fragment.sState, states, stateId);
                            }else{
                                fragment.setupSpinnerState(fragment.sState, states, "0");
                            }

                            String stateId = String.valueOf(fragment.states.get(fragment.sState.getSelectedItemPosition()).getStateCode());
                            if (!stateId.equals("")){
                                getCity(stateId);
                            }
                            fragment.dismissProgressBar();
                        } else {
                            fragment.states = states;
                            fragment.setupSpinnerState(fragment.sState, states, "0");
                            fragment.setupSpinnerCity(fragment.sCity, null, "0");
                            fragment.dismissProgressBar();
                        }
                    }
                });
    }


    public void getCity(String stateId) {
        fragment.showProgressBar();
        fragment.getApi().getCityByState(stateId, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<City>>() {
                    @Override
                    public void onCompleted(){
                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(List<City> cities){
                        fragment.dismissProgressDialog();
                        if (cities.size() > 0){
                            fragment.cities = cities;
                            if (fragment.kycDataRequest.getIdCity() == null){
                                fragment.setupSpinnerCity(fragment.sCity, fragment.cities, "0");
                            } else {
                                fragment.setupSpinnerCity(fragment.sCity, fragment.cities, fragment.kycDataRequest.getIdCity());
                            }
                            fragment.dismissProgressBar();
                        } else {
                            fragment.cities = cities;
                            fragment.setupSpinnerCity(fragment.sCity, fragment.cities, "0");
                            fragment.dismissProgressBar();
                        }
                    }
                });
    }

}
