package com.danareksa.investasik.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.fragments.userProfile.UserInfoFragment;
import com.danareksa.investasik.util.eventBus.RxBusObject;

import butterknife.Bind;
import butterknife.BindDrawable;
import butterknife.BindInt;
import butterknife.BindString;

/**
 * Created by fajarfatur on 1/13/16.
 */
public class UserProfileActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;
    @BindString(R.string.user_profile)
    String userProfile;
    @BindString(R.string.user_profile_kyc_open)
    String kycOpen;
    @BindString(R.string.user_profile_fatca_open)
    String fatcaOpen;
    @BindString(R.string.user_profile_risk_profile_open)
    String rpOpen;
    @BindString(R.string.user_profile_fatca_no_open)
    String fatcaNoOpen;
    @BindString(R.string.user_profile_risk_profile_no_open)
    String rpNoOpen;
    @BindDrawable(R.drawable.button_oval_white_outline)
    Drawable buttonOvalWhiteOutline;
    @BindDrawable(R.drawable.rounded_button)
    Drawable roundedButton;
    @BindInt(R.color.white)
    int white;
    @BindInt(R.color.colorPrimary)
    int colorPrimary;

    private UserProfilePresenter presenter;
    private static final String TAG = "UserProfileActivity";

    public static Activity userProfileActivity;

    public static void startActivity(BaseActivity sourceActivity) {
        Intent intent = new Intent(sourceActivity, UserProfileActivity.class);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_user_profile;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userProfileActivity = this;
        setupToolbar();

        //disable
        //setSupportActionBar(toolbar);
        /*if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(null);
        }
        toolbar.setTitle("Profil");*/

        presenter = new UserProfilePresenter(this);
        UserInfoFragment.showFragment(UserProfileActivity.this);
    }

    public void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        title.setText("Profil");
    }


    /**
     * Event Bus
     */

    @Override
    public void busHandler(RxBusObject.RxBusKey busKey, Object busObject) {
        super.busHandler(busKey, busObject);
        switch (busKey) {
            case OPEN_FRAGMENT:
                String TAG = (String) busObject;
                break;
        }
    }


    public void showFailureDialog(String message) {
        new MaterialDialog.Builder(this)
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.sorry).toUpperCase())
                .titleColor(Color.BLACK)
                .content(message)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Snackbar snackbar = Snackbar.make(UserProfileActivity.this.findViewById(android.R.id.content), R.string.user_profile_exit, Snackbar.LENGTH_LONG).setAction(R.string.yes, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(UserProfileActivity.this, MainActivity.class));
//                finish();
//            }
//        });
//        View view = snackbar.getView();
//        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_action);
//        tv.setTypeface(null, Typeface.BOLD);
//        snackbar.show();
        startActivity(new Intent(UserProfileActivity.this, MainActivity.class));
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
