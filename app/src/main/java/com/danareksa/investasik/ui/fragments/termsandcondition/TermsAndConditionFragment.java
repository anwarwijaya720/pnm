package com.danareksa.investasik.ui.fragments.termsandcondition;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.TermAndCondition;
import com.danareksa.investasik.data.api.responses.TermAndConditionResponse;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;

import butterknife.Bind;
import butterknife.OnClick;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by pandu.abbiyuarsyah on 10/03/2017.
 */

public class TermsAndConditionFragment extends BaseFragment {

    public static final String TAG = TermsAndConditionFragment.class.getSimpleName();
    public static Activity activity;

    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;

    public static void showFragment(BaseActivity sourceActivity) {

        if (!sourceActivity.isFragmentNotNull(TAG)) {
            activity = sourceActivity;

            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, R.anim.slide_out_left, android.R.anim.slide_in_left, R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.container, new TermsAndConditionFragment(), TAG);
            fragmentTransaction.commit();
        }
    }

    public static void showFragment(BaseActivity sourceActivity, int code) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            activity = sourceActivity;

            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, R.anim.slide_out_left, android.R.anim.slide_in_left, R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.container, new TermsAndConditionFragment(), TAG);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getTermAndCondition();
    }

    static void setContent(TermAndCondition termAndCondition) {
        WebView wv1 = (WebView) activity.findViewById(R.id.webView);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.loadData(termAndCondition.getTerms(),"text/html", null);
    }

    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }

    public void getTermAndCondition(){
        showProgressBar();
        this.getApi().getTermAndCondition()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<TermAndConditionResponse>() {

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        connectionError();
                    }

                    @Override
                    public void onNext(TermAndConditionResponse termAndConditionResponse) {
                        dismissProgressBar();
                        setContent(termAndConditionResponse.getData());
                    }
                });
    }

    @Override
    protected int getLayout() {
        return R.layout.f_termsandcondition;
    }


    @Override
    public void onResume() {
        super.onResume();
        //getActivity().setTitle("Syarat dan Ketentuan");
    }

    @OnClick(R.id.tvTryAgain)
    void retryConnection(){
        getTermAndCondition();
    }
}
