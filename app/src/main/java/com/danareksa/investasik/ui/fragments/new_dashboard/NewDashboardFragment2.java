package com.danareksa.investasik.ui.fragments.new_dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.PromoResponse;
import com.danareksa.investasik.ui.activities.ArticleActivity;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.CartActivity;
import com.danareksa.investasik.ui.activities.InfoSijago;
import com.danareksa.investasik.ui.activities.KenaliResiko;
import com.danareksa.investasik.ui.activities.ListCategoryPortfolioActivity;
import com.danareksa.investasik.ui.activities.ListOfCatalogueActivity;
import com.danareksa.investasik.ui.activities.PromoActivity;
import com.danareksa.investasik.ui.activities.RegisterNasabahActivity;
import com.danareksa.investasik.ui.activities.ReminderListActivity;
import com.danareksa.investasik.ui.activities.RiwayatTransaksiActivity;
import com.danareksa.investasik.ui.activities.TambahRekeningActivity;
import com.danareksa.investasik.ui.activities.UserProfileActivity;
import com.danareksa.investasik.ui.adapters.gridview.DashboardGridViewAdapter;
import com.danareksa.investasik.ui.adapters.pager.AdvBannerMainPageAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc13AdvancedRiskProfileFragment;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by asep.surahman on 03/04/2018.
 */

public class NewDashboardFragment2 extends BaseFragment {

    public static final String TAG = NewDashboardFragment2.class.getSimpleName();

    @Bind(R.id.homeViewpager)
    ViewPager homeViewpager;

    @Bind(R.id.circleMenuPageIndicator)
    CirclePageIndicator circlePageIndicator;

    @Bind(R.id.dashboard_grid)
    GridView gridView;

    private Handler advSliderHandler;
    private final int SLIDER_DELAY_ADVERTISING_IN_SECOND = 4;
    private static int currentPage = 0;
    public List<PromoResponse> datas = new ArrayList<>();
    AdvBannerMainPageAdapter adapter;

    public void setData(List<PromoResponse> datas) {
        this.datas.clear();
        this.datas.addAll(datas);
        adapter.notifyDataSetChanged();
    }

    private DashboardGridViewAdapter mAdapter;
    private ArrayList<String> listImage;
    private ArrayList<Integer> listtext;
    //private GridView gridView;

    public List<PromoResponse> listPromo;
    private NewDashboardPresenter2 presenter;

    @Override
    protected int getLayout(){
        /*return R.layout.f_new_dashboard2;*/
        return R.layout.f_new_dashboard4;
    }

    public static void showFragment(BaseActivity sourceActivity){
        if (!sourceActivity.isFragmentNotNull(TAG)){
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            fragmentTransaction.replace(R.id.container, new NewDashboardFragment2(), TAG);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        presenter = new NewDashboardPresenter2(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        gridView.setVerticalScrollBarEnabled(false);
        gridView.setOnTouchListener(new View.OnTouchListener(){

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_MOVE){
                    return true;
                }
                return false;
            }

        });

        if (BuildConfig.FLAVOR.contentEquals("btn")) {
            loadMenuDashboardBTN();
        } else {
            loadMenuDashboard();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        //getActivity().setTitle("Dashboard");
        initAdv();
    }

    public void initAdv(){
        presenter.getPromoList();
        adapter = new AdvBannerMainPageAdapter(datas, getActivity());
        homeViewpager.setAdapter(adapter);
        circlePageIndicator.setViewPager(homeViewpager);
        advSliderHandler = new Handler();
        startPageAdvSwitchTimer();
    }

    private void startPageAdvSwitchTimer(){
        advSliderHandler.postDelayed(advSliderRunnable, SLIDER_DELAY_ADVERTISING_IN_SECOND * 1000);
    }

    private void stopPageAdvSwitchTimer(){
        advSliderHandler.removeCallbacks(advSliderRunnable);
    }

    private Runnable advSliderRunnable = new Runnable() {
        @Override
        public void run() {

            if (currentPage == datas.size()) {
                currentPage = 0;
            }

            homeViewpager.setCurrentItem(currentPage++,true);
            Log.d("TAG", "run: runBabe! current = " + currentPage);
            startPageAdvSwitchTimer();
        }
    };

    @Override
    public void onPause(){
        super.onPause();
        stopPageAdvSwitchTimer();
    }

    @OnClick(R.id.portofolio)
    void portofolio(){
        ListCategoryPortfolioActivity.startActivity((BaseActivity) getActivity());
    }

    @OnClick(R.id.tambahRekening)
    void tambahRekening(){
        TambahRekeningActivity.startActivity((BaseActivity) getActivity());
    }

    @OnClick(R.id.pengingat)
    void pengingat(){
        ReminderListActivity.startActivity((BaseActivity) getActivity());
    }

    @OnClick(R.id.SijagoInfo)
    void SijagoInfo(){
        InfoSijago.startActivity((BaseActivity) getActivity());
    }

    @OnClick(R.id.profile)
    void profile(){
        Intent intent = new Intent(getActivity(), UserProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.pesanan)
    void pesanan(){
        CartActivity.startActivity((BaseActivity) getActivity());
    }

    @OnClick(R.id.artikel)
    void artikel(){
        ArticleActivity.startActivity((BaseActivity) getActivity());
    }

    @OnClick(R.id.riwayatTransaksi)
    void riwayatTransaksi(){
        RiwayatTransaksiActivity.startActivity((BaseActivity) getActivity());
    }

    @OnClick(R.id.allprodak)
    void allprodak(){
        ListOfCatalogueActivity.startActivity((BaseActivity) getActivity());
    }

    @OnClick(R.id.kenaliResiko)
    void kenaliResiko(){
        KenaliResiko.startActivity((BaseActivity) getActivity());
//        Kyc13AdvancedRiskProfileFragment.showFragment((BaseActivity) getActivity());
    }

    private void loadMenuDashboard(){
            listImage = new ArrayList<String>();
            listImage.add(getString(R.string.menu_profile));
            listImage.add(getString(R.string.menu_riwayattransaksi));
            listImage.add(getString(R.string.menu_catalogue));
            listImage.add(getString(R.string.menu_portfolio));
            listImage.add(getString(R.string.menu_artikel));
            listImage.add(getString(R.string.menu_reminder));
            listImage.add(getString(R.string.menu_your_order));
            listImage.add(getString(R.string.menu_penambahanrekening));

            listtext = new ArrayList<Integer>();
            listtext.add(R.drawable.ic_profile);
            listtext.add(R.drawable.ic_riwayat_transaksi);
            listtext.add(R.drawable.ic_produk);
            listtext.add(R.drawable.ic_portfolio);
            listtext.add(R.drawable.ic_artikel);
            listtext.add(R.drawable.ic_pengingat);
            listtext.add(R.drawable.ic_pesanan_anda);
            listtext.add(R.drawable.ic_tambah_rekening);

            mAdapter = new DashboardGridViewAdapter(getContext(), listImage, listtext);
            //gridView = (GridView) getActivity().findViewById(R.id.dashboard_grid);
            gridView.setAdapter(mAdapter);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    if (position == 0) {
                        Intent intent = new Intent(getActivity(), UserProfileActivity.class);
                        startActivity(intent);
                    } else if (position == 1) {
                        RiwayatTransaksiActivity.startActivity((BaseActivity) getActivity());
                    } else if (position == 2) {
                        ListOfCatalogueActivity.startActivity((BaseActivity) getActivity());
                    } else if (position == 3) {
                        ListCategoryPortfolioActivity.startActivity((BaseActivity) getActivity());
                    } else if (position == 4) {
                        ArticleActivity.startActivity((BaseActivity) getActivity());
                    } else if (position == 5) {
                        ReminderListActivity.startActivity((BaseActivity) getActivity());
                    } else if (position == 6) {
                        CartActivity.startActivity((BaseActivity) getActivity());
                    } else if (position == 7) {
                        TambahRekeningActivity.startActivity((BaseActivity) getActivity());
                    }
                }
            });
    }

    private void loadMenuDashboardBTN(){
        listImage = new ArrayList<String>();
        listImage.add(getString(R.string.menu_profile));
        listImage.add(getString(R.string.menu_portfolio));
        listImage.add(getString(R.string.menu_catalogue));
        listImage.add(getString(R.string.menu_your_order));
        listImage.add(getString(R.string.menu_artikel));
        listImage.add(getString(R.string.menu_riwayattransaksi));
        listImage.add(getString(R.string.daily_product_summary));
        listImage.add(getString(R.string.promo));

        listtext = new ArrayList<Integer>();
        listtext.add(R.drawable.ic_profile);
        listtext.add(R.drawable.ic_portfolio);
        listtext.add(R.drawable.ic_produk);
        listtext.add(R.drawable.ic_pesanan_anda);
        listtext.add(R.drawable.ic_artikel);
        listtext.add(R.drawable.ic_riwayat_transaksi);
        listtext.add(R.drawable.ic_promo);
        listtext.add(R.drawable.ic_promo);

            mAdapter = new DashboardGridViewAdapter(getContext(), listImage, listtext);
            //gridView = (GridView) getActivity().findViewById(R.id.dashboard_grid);
            gridView.setAdapter(mAdapter);
            gridView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    if (position == 0) {
                        UserProfileActivity.startActivity((BaseActivity) getActivity());
                    } else if (position == 1) {
                        ListCategoryPortfolioActivity.startActivity((BaseActivity) getActivity());
                    } else if (position == 2) {
                        ListOfCatalogueActivity.startActivity((BaseActivity) getActivity());
                    } else if (position == 3) {
                        CartActivity.startActivity((BaseActivity) getActivity());
                    } else if (position == 4) {
                        ArticleActivity.startActivity((BaseActivity) getActivity());
                    } else if (position == 5) {
                        RiwayatTransaksiActivity.startActivity((BaseActivity) getActivity());
                    } else if (position == 6) {
//                        ReminderListActivity.startActivity((BaseActivity) getActivity());
                    } else if (position == 7) {
//                        TambahRekeningActivity.startActivity((BaseActivity) getActivity());
                        PromoActivity.startActivity((BaseActivity) getActivity());
                    }
                }
            });
    }

}
