package com.danareksa.investasik.ui.fragments.checkout;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.CartList;
import com.danareksa.investasik.data.api.beans.TrxTransCode;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.CartActivity;
import com.danareksa.investasik.ui.activities.CheckoutActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.AmountFormatter;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;

import butterknife.Bind;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import de.hdodenhof.circleimageview.CircleImageView;
import icepick.State;

/**
 * Created by asep.surahman on 21/06/2018.
 */

public class MandiriClickpayFragment extends BaseFragment {


    public static final String TAG = MandiriClickpayFragment.class.getSimpleName();
    private final static String CART_LIST = "cartList";

    @Bind(R.id.etChallengeCode1)
    EditText etChallengeCode1;
    @Bind(R.id.etChallengeCode2)
    EditText etChallengeCode2;
    @Bind(R.id.etChallengeCode3)
    EditText etChallengeCode3;
    @Bind(R.id.ivPackage)
    CircleImageView ivPackage;
    @Bind(R.id.tvPackageName)
    TextView tvPackageName;
    @Bind(R.id.tvTotal)
    TextView tvTotal;
    @Bind(R.id.bBayar)
    Button bBayar;
    @Bind(R.id.bCancel)
    Button bCancel;
    @Bind(R.id.etMandiriClickpayToken)
    EditText etMandiriClickpayToken;
    @Bind(R.id.etCardNumber)
    EditText etCardNumber;
    @State
    public CartList cartList;

    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;

    MandiriClickpayPresenter presenter;
    String type;
    String trxNumber;
    double adminFee = 0.0;

    @State
    TrxTransCode trxTransCode;

    String mandiriToken = "";
    String cardNumber = "";
    int idCart = 0;
    String packageName = "";

    public static void showFragment(BaseActivity sourceActivity, CartList cartList, String type, String trxNumber, double adminFee){

        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, R.anim.slide_out_left, android.R.anim.slide_in_left, R.anim.slide_out_left);

            MandiriClickpayFragment fragment = new MandiriClickpayFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(CART_LIST, cartList);
            bundle.putString("type", type);
            bundle.putString("trxNumber", trxNumber);
            bundle.putDouble("adminFee", adminFee);
            fragment.setArguments(bundle);

            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }


    @Override
    protected int getLayout(){
        return R.layout.f_mandiri_clickpay;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getString("type");
        trxNumber = getArguments().getString("trxNumber");
        adminFee = getArguments().getDouble("adminFee");
        presenter = new MandiriClickpayPresenter(this);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        disableChallengeCode();
        cartList = (CartList) getArguments().getSerializable(CART_LIST);
        idCart = cartList.getCartList().get(0).getTransaction().getId();
        packageName = cartList.getCartList().get(0).getFundPackages().getFundPackageName();
        tvPackageName.setText(packageName);
        etChallengeCode2.setText(getTotalWitoutSymbol());
        etChallengeCode3.setText(trxNumber);
        tvTotal.setText(getTotal());

        Glide.with(getActivity()).load(InviseeService.IMAGE_DOWNLOAD_URL + cartList.getCartList().get(0).getPackage().getPackageImage() + "&token=" + PrefHelper.getString(PrefKey.TOKEN))
                .override(200, 200)
                .error(R.drawable.border_grey)
                .fitCenter()
                .into(ivPackage);

    }




    public String getTotal(){
        Double total = 0d;
        for (int i = 0; i < cartList.getCartList().size(); i++) {
            Double t = Double.parseDouble(cartList.getCartList().get(i).getTotal());
            total += t;
        }
        total = total + adminFee;
        System.out.println("total : " + total + " - admin : " + adminFee);
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        nf.setMinimumFractionDigits(0);
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("");
        ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);
        //return "IDR " + nf.format(total);
        return AmountFormatter.formatCurrencyWithoutComma((total));
    }


    public String getTotalWitoutSymbol(){
        double total = 0d;
        int tot = 0;
        for (int i = 0; i < cartList.getCartList().size(); i++) {
            Double t = Double.parseDouble(cartList.getCartList().get(i).getTotal());
            total += t;
        }
        total = total + adminFee;
        tot = (int) total;
        return ""+tot;
    }


    @OnClick(R.id.bBayar)
    void bBayar(){
        if(!validate()){
            onValidFailed();
            return;
        }else{
            showDialogConfirmation();
        }
    }

    @OnClick(R.id.bCancel)
    void bCancel(){
        getActivity().finish();
    }


    public void fetchResultToFragment(){
        ((CheckoutActivity) getActivity()).setStep(3);
        MandiriClickpaySummaryFragment.showFragment((BaseActivity) getActivity(), cartList, trxTransCode);
        CartActivity.fa.finish();
    }


    public boolean validate(){
        boolean valid = true;
        mandiriToken  = etMandiriClickpayToken.getText().toString();
        cardNumber = etCardNumber.getText().toString();

        if(mandiriToken.isEmpty() && mandiriToken.length() <= 0){
            etMandiriClickpayToken.setError("Token mandiri tidak boleh kosong!");
            valid = false;
        }

        if(cardNumber.isEmpty() && cardNumber.length() <= 0){
            etCardNumber.setError("Card number tidak boleh kosong");
            valid = false;
        }

        return valid;
    }


    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }


    void showDialogAfterSubmit(String info){
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.transaksi_failed).toUpperCase())
                .titleColor(Color.BLACK)
                .content(info)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }


    public int getNetAmount(){
        int total = 0;
        double tot = 0;
        for (int i = 0; i < cartList.getCartList().size(); i++) {
            Double t = Double.parseDouble(cartList.getCartList().get(i).getTransactionAmount());
            tot += t;
        }
        total = (int) tot;
        return total;
    }

    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError(){
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }


    @OnClick(R.id.tvTryAgain)
    void retryConnection(){

    }


    @OnTextChanged(R.id.etCardNumber)
    void etCardNumberTextChanged(CharSequence s, int start, int count, int after){
        if(!s.equals("")){
            if(s.length() == 16){
                String last10Digits = getLastnCharacters(s.toString(),10);
                etChallengeCode1.setText(last10Digits);
            }else{
                etChallengeCode1.setText("");
            }
        }else {
            etChallengeCode1.setText("");
        }
    }


    public String getLastnCharacters(String inputString, int subStringLength){
        int length = inputString.length();
        if(length <= subStringLength){
            return inputString;
        }
        int startIndex = length-subStringLength;
        return inputString.substring(startIndex);
    }



    private void disableChallengeCode(){
        etChallengeCode1.setEnabled(false);
        etChallengeCode2.setEnabled(false);
        etChallengeCode3.setEnabled(false);
    }


    private void showDialogConfirmation(){
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.infortmation))
                .titleColor(Color.BLACK)
                .content(R.string.transaksi_konfirmasi)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        presenter.getPaymentMandiriClickpay(type, presenter.getMandiriClickpayModel(cardNumber, idCart, mandiriToken, getNetAmount()));
                        dialog.dismiss();
                    }
                })
                .cancelable(false)
                .show();
    }


}
