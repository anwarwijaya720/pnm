package com.danareksa.investasik.ui.fragments.purchase;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.FundAllocation;
import com.danareksa.investasik.data.api.beans.KycLookup;
import com.danareksa.investasik.data.api.beans.PackageList;
import com.danareksa.investasik.data.api.beans.PackageListProduct;
import com.danareksa.investasik.data.api.beans.PackageListReguler;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.ProductRegulerIfua;
import com.danareksa.investasik.data.api.requests.AddProductRegulerRequest;
import com.danareksa.investasik.data.api.responses.CartListResponse;
import com.danareksa.investasik.data.api.responses.CustomerDataByIfuaResponse;
import com.danareksa.investasik.data.api.responses.FundAllocationResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.data.realm.RealmHelper;
import com.danareksa.investasik.data.realm.model.KycLookupModel;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.CatalogueActivity;
import com.danareksa.investasik.ui.activities.ChooseIfuaActivity;
import com.danareksa.investasik.ui.activities.ListOfCatalogueActivity;
import com.danareksa.investasik.ui.adapters.rv.RegulerPurchaseAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.AmountFormatter;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.DateUtil;

import org.joda.time.DateTime;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import icepick.State;
import io.realm.Realm;
import io.realm.RealmResults;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;


/**
 * Created by asep.surahman on 25/06/2018.
 */
@RuntimePermissions
public class RegulerPurchaseFragment extends BaseFragment implements RegulerPurchaseAdapter.CallbackActionPackage {

    public static final String TAG = RegulerPurchaseFragment.class.getSimpleName();
    private final static String SUBSCRIBE = "SUBCR";
    private final static String TOPUP = "TOPUP";
    private final static String VERIFIED = "VER";
    private static final String PACKAGES = "packages";
    private static final String PRODUCT_LIST = "product_list";

    @Bind(R.id.rv)
    RecyclerView rv;
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.bTambah)
    Button bTambah;
    @Bind(R.id.bProcces)
    Button bProcces;
    @Bind(R.id.txvRekeningInvestasi)
    TextView txvRekeningInvestasi;
    @Bind(R.id.txvKodeInvestor)
    TextView txvKodeInvestor;
    @Bind(R.id.txvNamaBank)
    TextView txvNamaBank;
    @Bind(R.id.txvNomorRekening)
    TextView txvNomorRekening;
    @Bind(R.id.txvNamaPemegangRekening)
    TextView txvNamaPemegangRekening;
    @Bind(R.id.txvTanggalDebet)
    TextView txvTanggalDebet;
    @Bind(R.id.cbTerms)
    CheckBox cbTerms;
    String ifuaId;
    @State
    public FundAllocationResponse response;
    @State
    Packages packages;

    public Realm realmReguler;
    public List<CartListResponse> cartList;
    private RegulerPurchaseAdapter adapter;
    private RegulerPurchasePresenter presenter;
    public List<PackageListReguler> packageListRegulersAll;
    public List<PackageListReguler> packageListRegulers;

    ProductRegulerIfua productRegulerIfua;
    List<PackageList> packageListReguler;
    List<KycLookup> kycLookupTimePeriod;

    boolean isLoadingGetKyc = false;
    boolean isLoadingGetCustByIfua = false;
    boolean isLoadingGetPackage = false;
    boolean isLoadingAddReguler = false;
    boolean isFirst = false;

    public static void showFragment(BaseActivity sourceActivity, String ifuaId, Packages packages, boolean isFirst) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            Bundle bundle = new Bundle();
            bundle.putString("ifuaId", ifuaId);
            bundle.putBoolean("isFirst", isFirst);
            bundle.putSerializable(PACKAGES, packages);
            RegulerPurchaseFragment fragment = new RegulerPurchaseFragment();
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }


    @Override
    protected int getLayout() {
        return R.layout.f_list_reguler_purchase;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        realmReguler = Realm.getDefaultInstance();
        ifuaId = getArguments().getString("ifuaId");
        packages = (Packages) getArguments().get(PACKAGES);
        isFirst = getArguments().getBoolean("isFirst");
        packageListRegulers = new ArrayList<>();
        packageListRegulersAll = new ArrayList<>();
        presenter = new RegulerPurchasePresenter(this);

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.getKycLookupData();
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        //bProcces.setEnabled(false);
        cbTerms.setText(
                Html.fromHtml("Saya menyetujui <a href=\"http://www.google.com\">Syarat dan ketentuan</a> yang berlaku"));
    }

    @OnClick(R.id.cbTerms)
    void cbTerms() {
        if (cbTerms.isChecked()) {
            showTerms();
        }
        //bProcces.setEnabled(cbTerms.isChecked());
    }

    @OnClick(R.id.bTambah)
    void bTambah(){
        loadRole();
    }


    @OnClick(R.id.bProcces)
    void bProcces(){
        if(!validate()){
            onValidFailed();
            return;
        }else{
            if(cbTerms.isChecked() == true){
                packageListReguler = getDataPurchaseReguler().getPackageLists();
                AddProductRegulerRequest addProductRegulerRequest = new AddProductRegulerRequest();
                addProductRegulerRequest.setPackages(packageListReguler);
                addProductRegulerRequest.setIfuaNumber(ifuaId);
                addProductRegulerRequest.setApssType("ANDROID");
                presenter.addProductToRegulerIfua(addProductRegulerRequest);
            }else{
                Toast.makeText(getContext(), "Anda belum menyetujui syarat dan ketentuan yang berlaku!", Toast.LENGTH_LONG).show();
            }
        }
    }


    public void showSummaryPurchase(){
        //close before activity
        ListOfCatalogueActivity.faCatalogue.finish(); //finish list catalogue
        CatalogueActivity.catalogueActivity.finish(); //finish catalogue
        ChooseIfuaActivity.choIfuaActivity.finish(); //finish chose ifua
        SummaryPurchaseRegulerFragment.showFragment((BaseActivity) getActivity(), productRegulerIfua, Constant.INVEST_TYPE_REGULER, ifuaId);
    }

    public void loadRole(){
        showProgressBar();
        if(packageListRegulers != null && packageListRegulers.size() > 0){
            updateListPackage();
            addListPackage();
        }else{
            addPackageFirst();
        }
        loadList();
        if(isFirst == true){
            isFirst = false;
        }
        dismissProgressBar();
    }


    private void addPackageFirst(){
        if(isFirst == true){
            int id = (int) packages.getId().doubleValue();
            System.out.println("ID PACKAGE : " + packages.getId().doubleValue());
            PackageListReguler packageListReguler = new PackageListReguler();
            packageListReguler.setId(id);
            packageListReguler.setFeePercentage("");
            packageListReguler.setFeePrice(0.0);
            packageListReguler.setTransactionAmount("");
            packageListReguler.setTotal("");
            packageListReguler.setCode("");
            packageListReguler.setName("");
            packageListReguler.setTimePeriod("");
            packageListReguler.setMinSubscription(0.0);
            packageListReguler.setAccept(false);
            packageListReguler.setFirst(true);
            packageListRegulers.add(packageListReguler);
        }else{
            addListPackage();
        }
    }


    private void addListPackage(){
        PackageListReguler packageListReguler = new PackageListReguler();
        packageListReguler.setId(0);
        packageListReguler.setFeePercentage("");
        packageListReguler.setFeePrice(0.0);
        packageListReguler.setTransactionAmount("");
        packageListReguler.setTotal("");
        packageListReguler.setCode("");
        packageListReguler.setName("");
        packageListReguler.setTimePeriod("");
        packageListReguler.setMinSubscription(0.0);
        packageListReguler.setAccept(false);
        packageListReguler.setFirst(false);
        packageListRegulers.add(packageListReguler);
    }


    private void updateListPackage(){
        if(adapter.getList(true) != null && adapter.getList(true).size() > 0){
            packageListRegulers = new ArrayList<>();
            packageListRegulers = presenter.gePackageListRegulers(adapter.getList(true));
        }
    }


    @Override
    public void removePackage(int index){
        if(adapter.getList(false) != null && adapter.getList(false).size() != 0){
            showProgressBar();
            packageListRegulers = new ArrayList<>();
            packageListRegulers = presenter.gePackageListRegulers(adapter.getList(false));
            packageListRegulers.remove(index);
            loadList();
            dismissProgressBar();
        }
    }

    @Override
    public  void  downloadProspektus(int index){
        if(adapter.getList(false) != null && adapter.getList(false).size() != 0){
            packageListRegulers = new ArrayList<>();
            packageListRegulers = presenter.gePackageListRegulers(adapter.getList(false));
            PackageListReguler data = packageListRegulers.get(index);
            FundAllocation allocation = new FundAllocation();
            allocation.setProductName(data.getName());
            allocation.setProspectusKey(data.getProspectusKey());
            downloadProspektusDisp(allocation);
        }
    }

    private void loadList(){
        adapter = new RegulerPurchaseAdapter(presenter, packageListRegulers, packageListRegulersAll, getActivity(), this, kycLookupTimePeriod);
        rv.setAdapter(adapter);
    }


    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }


    @OnClick(R.id.tvTryAgain)
    void retryConnection(){
        if(isLoadingAddReguler == true){
            packageListReguler = getDataPurchaseReguler().getPackageLists();
            AddProductRegulerRequest addProductRegulerRequest = new AddProductRegulerRequest();
            addProductRegulerRequest.setPackages(packageListReguler);
            addProductRegulerRequest.setIfuaNumber(ifuaId);
            addProductRegulerRequest.setApssType("ANDROID");
            presenter.addProductToRegulerIfua(addProductRegulerRequest);
        }else{
            presenter.getKycLookupData();
        }
    }


    private PackageListProduct getDataPurchaseReguler(){
        PackageListProduct packageListProduct = new PackageListProduct();
        List<PackageList> packageLists = new ArrayList<>();
        if(adapter.getList(true) != null && adapter.getList(true).size() != 0){
            for(int i = 0; i < adapter.getList(true).size(); i++){
                PackageList packageList = new PackageList();
                packageList.setMonthlyInvestmentAmount(adapter.getList(true).get(i).getTransactionAmount());
                packageList.setPackageId(String.valueOf(adapter.getList(true).get(i).getId()));
                packageList.setRegulerPeriod(adapter.getList(true).get(i).getTimePeriod());
                packageLists.add(packageList);
            }
        }
        packageListProduct.setPackageLists(packageLists);
        return packageListProduct;
    }

    boolean isDuplicates(final List<PackageListReguler> packageListRegulers)
    {
        List<Integer> packages = new ArrayList<>();
        for (PackageListReguler i : packageListRegulers)
        {
            if (packages.contains(i.getId())) return true;
            packages.add(i.getId());
        }
        return false;
    }

    public boolean validate(){
        boolean valid = true;
        if(adapter.getList(true) != null && adapter.getList(true).size() != 0){
            for(int i = 0; i < adapter.getList(true).size(); i++){

                if(!adapter.getList(true).get(i).getTransactionAmount().equals("")){
                    double amount = Double.parseDouble(adapter.getList(true).get(i).getTransactionAmount());
                    if(amount < adapter.getList(true).get(i).getMinSubscription()){ //if(amount < 200000){ //minimal subcribe reguler == 200 ribu
                        valid = false;
                        showFailedDialog("Minimal pembelian untuk " + adapter.getList(true).get(i).getName() +
                                " adalah " +
                                AmountFormatter.formatIdrWithoutComma(adapter.getList(true).get(i).getMinSubscription()));
                        break;
                    }
                }else{
                    valid = false;
                    Toast.makeText(getContext(), "Nilai investasi harus diisi!", Toast.LENGTH_LONG).show();
                    break;
                }

                if(adapter.getList(true).get(i).getTimePeriod().equals("")){
                    valid = false;

                    if(adapter.getList(true).get(i).getId() == 0){
                        Toast.makeText(getContext(), "Jangka waktu tidak boleh kosong!", Toast.LENGTH_LONG).show();
                        break;
                    }
                    valid = false;
                    Toast.makeText(getContext(), "Silahkah pilih package!", Toast.LENGTH_LONG).show();
                    break;
                }

                if(adapter.getList(true).get(i).isAccept() == false){
                    valid = false;
                    Toast.makeText(getContext(), "Anda belum menyetujui produk!", Toast.LENGTH_LONG).show();
                    break;
                }

            }

            if (isDuplicates(adapter.getList(true))){
                showFailedDialog("Anda tidak dapat memilih produk yang sama. Silakan pilih produk lainnya.");
                valid = false;
            }

        }else{
            valid = false;
            Toast.makeText(getContext(), "Data tidak boleh kosong!", Toast.LENGTH_LONG).show();
        }
        return  valid;
    }


    public void fetchDataResultToLayout(CustomerDataByIfuaResponse customerDataRekeningResponse){
        txvRekeningInvestasi.setText(customerDataRekeningResponse.getData().getInvAccGroupType());
        txvKodeInvestor.setText(customerDataRekeningResponse.getData().getIfuaNumber());
        txvNamaBank.setText(customerDataRekeningResponse.getData().getSettlementBankName());
        txvNomorRekening.setText(customerDataRekeningResponse.getData().getSettlementBankAccNumber());
        txvNamaPemegangRekening.setText(customerDataRekeningResponse.getData().getSettlementBankAccName());

        if(!customerDataRekeningResponse.getData().getNextAutoDebit().equals("")){
            Date dateAutoDebet; String years = ""; String month = ""; String date = "";
            dateAutoDebet = DateUtil.format(customerDataRekeningResponse.getData().getNextAutoDebit(), "yyyy-MM-dd");
            DateTime dateTime = new DateTime(dateAutoDebet);
            date  = String.valueOf(dateTime.getDayOfMonth());
            month = String.valueOf(dateTime.getMonthOfYear()-1);
            years = String.valueOf(dateTime.getYear());
            txvTanggalDebet.setText(date + " " + DateUtil.getMonthString(month) + " " + years);
        }

        kycLookupTimePeriod = getKycLookupFromRealm(Constant.KYC_CAT_REGULER_PERIOD);
    }



    protected List<KycLookup>
    getKycLookupFromRealm(String category){
        realm = Realm.getDefaultInstance();
        List<KycLookup> list = new ArrayList<>();
        ModelMapper modelMapperDefaultValue = new ModelMapper();
        KycLookupModel kycLookupModelDefaultValue = new KycLookupModel();
        kycLookupModelDefaultValue.setCategory(category);
        kycLookupModelDefaultValue.setCode("");
        kycLookupModelDefaultValue.setValue("");
        kycLookupModelDefaultValue.setSeq(0); //new
        KycLookup dataFromRealm2 = modelMapperDefaultValue.map(kycLookupModelDefaultValue, KycLookup.class);
        list.add(dataFromRealm2);
        RealmResults<KycLookupModel> kycLookupModels = RealmHelper.readKycLookupListByCategory(realm, category);
        if(kycLookupModels != null){
            for (KycLookupModel model : kycLookupModels){
                ModelMapper modelMapper = new ModelMapper();
                KycLookup dataFromRealm = modelMapper.map(model, KycLookup.class); // map to non realm object
                list.add(dataFromRealm);
            }
        }
        return list;
    }


    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }


    public Realm getRealmReguler() {
        return realmReguler;
    }

    void showDialogAfterSubmit(String info){
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.infortmation).toUpperCase())
                .titleColor(Color.BLACK)
                .content(info)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }


    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void downloadProscpectus(FundAllocation allocation) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(InviseeService.IMAGE_DOWNLOAD_URL + allocation.getProspectusKey() + "&token=" + PrefHelper.getString(PrefKey.TOKEN)));
        request.setDescription("A download package with some files");
        request.setTitle("Prospektus " + allocation.getProductName());
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.allowScanningByMediaScanner();
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Prospektus " + allocation.getProductName()+".pdf");

        // get download service and enqueue file
        DownloadManager manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    public void showTerms() {
        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .customView(R.layout.dialog_terms, true)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).build();

        View view = dialog.getCustomView();
        ((TextView) ButterKnife.findById(view, R.id.tvTerms)).getText();
        (ButterKnife.findById(view, R.id.bOk)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }



    public void downloadProspektusDisp(FundAllocation fundAllocData){
        RegulerPurchaseFragmentPermissionsDispatcher.downloadProscpectusWithPermissionCheck(RegulerPurchaseFragment.this, fundAllocData);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        RegulerPurchaseFragmentPermissionsDispatcher.onRequestPermissionsResult(this,requestCode,grantResults);
    }


}
