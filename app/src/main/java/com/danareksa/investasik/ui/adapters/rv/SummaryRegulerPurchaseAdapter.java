package com.danareksa.investasik.ui.adapters.rv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.PackageReguler;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.util.AmountFormatter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by asep.surahman on 14/08/2018.
 */

public class SummaryRegulerPurchaseAdapter extends RecyclerView.Adapter<SummaryRegulerPurchaseAdapter.SummaryRegulerPurchaseHolder>{


    List<PackageReguler> packageList;
    PackageReguler packageReguler;
    private Context context;
    private List<SummaryRegulerPurchaseAdapter.SummaryRegulerPurchaseHolder> holderList;
    private  HashMap<String, String> data;


    public SummaryRegulerPurchaseAdapter(Context context, List<PackageReguler> packageList, HashMap<String, String> data) {
        this.context = context;
        this.packageList = packageList;
        this.holderList = new ArrayList<>();
        this.data = data;
    }


    @Override
    public SummaryRegulerPurchaseAdapter.SummaryRegulerPurchaseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_summary_reguler_purchase, parent, false);
        return new SummaryRegulerPurchaseAdapter.SummaryRegulerPurchaseHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final SummaryRegulerPurchaseAdapter.SummaryRegulerPurchaseHolder holder, final int position) {
        packageReguler = packageList.get(position);

        holder.tvOrderNumber.setText(packageReguler.getOrderNo());
        holder.tvAccountName.setText(data.get("account_name"));
        holder.tvBankName.setText(data.get("bank_name"));
        holder.tvAccountNumber.setText(data.get("account_number"));
        holder.tvIfuaId.setText(data.get("ifua_id"));
        holder.tvIfuaType.setText(data.get("ifua_type"));

        holder.itemView.setTag(packageReguler);
        holder.tvTimePeriod.setText(packageReguler.getReguler_period() + " bulan");
        holder.tvPackageName.setText(packageReguler.getPackageName());

        Picasso.with(context).load(InviseeService.IMAGE_DOWNLOAD_URL + packageReguler.getPackageImageKey() + "&token=" + PrefHelper.getString(PrefKey.TOKEN))
                .placeholder(R.drawable.ic_logo_launcher)
                .error(R.drawable.ic_logo_launcher)
                .into(holder.icon);

        if(packageReguler.getMonthlyInvestmentAmount() != 0.0){
            holder.tvAmount.setText(String.valueOf(AmountFormatter.formatCurrencyWithoutComma(packageReguler.getMonthlyInvestmentAmount())));
        }else{
            holder.tvAmount.setText("0");
        }


        if(packageReguler.getMonthlyInvestmentFee() != 0.0){
            double feeAmount = packageReguler.getMonthlyInvestmentFee();
            //long fAmount = (long) feeAmount;
            holder.tvfee.setText(AmountFormatter.formatFeePercent(packageReguler.getMonthly_investment_fee_percentage()/100) +" | "+ AmountFormatter.formatNonCurrency(feeAmount));
        }else{
            holder.tvfee.setText("0% | 0");
        }

        if(packageReguler.getTotalMonthlyAutodebet() != 0.0){
            holder.tvTotal.setText(String.valueOf(AmountFormatter.formatCurrencyWithoutComma(packageReguler.getTotalMonthlyAutodebet() )));
        }else{
            holder.tvTotal.setText("0");
        }

        holderList.add(holder);
    }


    @Override
    public int getItemCount() {
        return packageList != null ? packageList.size() : 0;
    }


    public static class SummaryRegulerPurchaseHolder extends RecyclerView.ViewHolder {


        @Bind(R.id.tvOrderNumber)
        TextView tvOrderNumber;
        @Bind(R.id.tvIfuaType)
        TextView tvIfuaType;
        @Bind(R.id.tvIfuaId)
        TextView tvIfuaId;
        @Bind(R.id.tvBankName)
        TextView tvBankName;
        @Bind(R.id.tvAccountNumber)
        TextView tvAccountNumber;
        @Bind(R.id.tvAccountName)
        TextView tvAccountName;

        @Bind(R.id.tvTimePeriod)
        TextView tvTimePeriod;
        @Bind(R.id.tvAmount)
        TextView tvAmount;
        @Bind(R.id.tvfee)
        TextView tvfee;
        @Bind(R.id.tvTotal)
        TextView tvTotal;
        @Bind(R.id.tvPackageName)
        TextView tvPackageName;
        @Bind(R.id.icon)
        CircleImageView icon;


        public SummaryRegulerPurchaseHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
