package com.danareksa.investasik.ui.fragments.changepassword;

import android.graphics.Color;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.requests.ChangePasswordRequest;
import com.danareksa.investasik.data.api.responses.GenericResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.util.Crypto;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by pandu.abbiyuarsyah on 14/03/2017.
 */

public class ChangePasswordPresenter {

    private ChangePasswordFragment fragment;

    public ChangePasswordPresenter(ChangePasswordFragment fragment) {
        this.fragment = fragment;
    }

    ChangePasswordRequest constructChangePasswordRequest() {
        ChangePasswordRequest request = new ChangePasswordRequest();
        request.setEmail(PrefHelper.getString(PrefKey.EMAIL));
        request.setToken(PrefHelper.getString(PrefKey.TOKEN));
        request.setConfPassword(Crypto.Encrypt(fragment.etConfirmPass.getText().toString()));
        request.setNewPassword(Crypto.Encrypt(fragment.etNewPass.getText().toString()));
        request.setOldPassword(Crypto.Encrypt(fragment.etOldPass.getText().toString()));
        return request;
    }

    void requestChangePassword(final ChangePasswordRequest changePasswordRequest) {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().changePassword(changePasswordRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(GenericResponse genericResponse) {
                        fragment.dismissProgressDialog();
                        if (genericResponse.getCode() == 0) {
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.success).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content(genericResponse.getInfo())
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.GRAY)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            fragment.getActivity().finish();
                                            //UserProfileActivity.startActivity((BaseActivity) fragment.getActivity());
                                        }
                                    })
                                    .cancelable(false)
                                    .show();

                        } else {
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.error).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content(genericResponse.getInfo())
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.GRAY)
                                    .cancelable(false)
                                    .show();
                        }

                    }
                });

    }




}
