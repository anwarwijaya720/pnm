package com.danareksa.investasik.ui.fragments.kycnew;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.RiskProfileNew;
import com.danareksa.investasik.data.api.beans.RiskProfileQuestion;
import com.danareksa.investasik.data.api.beans.RiskQuestionnaire;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.api.requests.RiskProfileRequest;
import com.danareksa.investasik.data.api.responses.RiskProfileNewResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.adapters.pager.RiskProfileAdapterNew;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.eventBus.RxBusObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import icepick.State;


/**
 * Created by asep.surahman on 21/05/2018.
 */

//
public class Kyc13AdvancedRiskProfileFragment extends KycBaseNew implements RiskProfileAdapterNew.CallbackRiskProfileQuestion{

    public static final String TAG = Kyc13AdvancedRiskProfileFragment.class.getSimpleName();

    public static final String KYC_DATA_REQUEST = "kycDataRequest";
    @Bind(R.id.rv)
    RecyclerView rv;

    @Bind(R.id.llResultScore)
    LinearLayout llResultScore;
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.tvValue)
    TextView tvValue;
    @Bind(R.id.tvScore)
    TextView tvScore;
    @Bind(R.id.tvDescription)
    TextView tvDescription;

    @Bind(R.id.tvTryAgain)
    TextView tvTryAgain;

    //new
    List<RiskQuestionnaire> listAnswerRisk;

    @State
    ArrayList<RiskProfileQuestion> riskProfileQuestionList;

    RiskProfileNewResponse riskProfileNewResponse;

    private Kyc13AdvancedRiskProfilePresenter presenter;
    private LinearLayoutManager llManager;
    boolean status = false;

    String sourceRoute;

    public int flakButton = 0; //0 = getScoreRiskProfile //1 = save data

    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new Kyc13AdvancedRiskProfileFragment(), TAG);
            fragmentTransaction.commit();
        }
    }


    public static Fragment getFragment(KycDataRequest kycDataRequest, String sourceRoute){
        Bundle bundle = new Bundle();
        bundle.putSerializable(KYC_DATA_REQUEST, kycDataRequest);
        bundle.putSerializable(Constant.SOURCE_ROUTE, sourceRoute);
        Kyc13AdvancedRiskProfileFragment fragment = new Kyc13AdvancedRiskProfileFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayout() {
        return R.layout.f_register_lanjut_profil_resiko;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        riskProfileQuestionList = new ArrayList<RiskProfileQuestion>();
        listAnswerRisk = new ArrayList<>();
        sourceRoute = getActivity().getIntent().getStringExtra(Constant.SOURCE_ROUTE);
        presenter = new Kyc13AdvancedRiskProfilePresenter(this);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        initRV();
        presenter.loadRiskProfileQuestionAndAnswer();
    }


    private void initRV(){
        llManager = new LinearLayoutManager(getActivity());
        llManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(llManager);
    }


    @Override
    public void onResume(){
        super.onResume();
    }


    public void initDataRiskProfile(){
        if(request.getRiskQuestionnaireList() != null){

            if(request.getRiskQuestionnaireList().size() != 0){
                setAnswerValueFromRiskProfile();
                System.out.println("=========> not null risk profile");
            }else{
                //riskQuestionnaireList = getAnswerDefaultFromRiskProfile();
                System.out.println("=========> null risk profile 00,,,,");
            }

        }else{
            //riskQuestionnaireList = getAnswerDefaultFromRiskProfile();
            System.out.println("=========> null risk profile...");
        }

        fetchQuestionToLayout();
    }


    public void fetchQuestionToLayout(){
        rv.setAdapter(new RiskProfileAdapterNew(getActivity(), riskProfileQuestionList, status, this));
    }


    @Override
    public void nextWithoutValidation(){
        if(flakButton == 0){

            if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")){
                /*new */
                if(request.getRiskQuestionnaireList() != null){
                    if(request.getRiskQuestionnaireList().size() != 0){
                        getScoreRiskProfile();
                    }else{
                        getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
                    }
                }else{
                    getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
                }
                /*end */

            }else{
                //cek spinner answer empty or no
                if(listAnswerRisk != null && listAnswerRisk.size() > 0){
                    if((listAnswerRisk.size() == riskProfileQuestionList.size())){
                        if(cekValidDataAnswer()){
                            getScoreRiskProfile();
                        }else{
                            Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
                }
            }

        }else if(flakButton == 1){

            if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")){
                request.setRiskQuestionnaire(listAnswerRisk);
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.SUBMIT_KYC_DATA, request));
            }else{
                if(sourceRoute.equals(Constant.SOURCE_ROUTE_REG)){
                    getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
                }else{
                    getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
                }
            }
        }
    }


    @Override
    public void onValidationSucceeded(){
        super.onValidationSucceeded();
    }

    @Override
    public void saveDataKycWithBackpress(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
            request.setRiskQuestionnaire(listAnswerRisk);
            getScoreRiskProfile();
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.SAVE_KYC_DATA_WITH_CHECKLIST, request));
        }else{
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
        }
    }


    @Override
    public void previewsWithoutValidation(){

        if(flakButton == 1){
            lnConnectionError.setVisibility(View.GONE);
            llResultScore.setVisibility(View.GONE);
            lnDismissBar.setVisibility(View.VISIBLE);
            flakButton = 0;
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.CHANGE_BUTTON_CEK_HASIL, null));
        }else if(flakButton == 0){
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.CHANGE_BUTTON, null));
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null));
            System.out.println("back.." );
        }
    }


    @Override
    public void getRiskProfileQuestion(RiskProfileQuestion question) {
        changeAnswerQuestion(question);
    }



    private boolean cekValidDataAnswer(){
        boolean result = false; int flak = 0;
        if(listAnswerRisk.size() > 0){
            for(int i = 0; i < listAnswerRisk.size(); i++) {
                if(listAnswerRisk.get(i).getRiskAnswerId().equals("0") ||
                        listAnswerRisk.get(i).getRiskAnswerId().equals("")) {
                    flak = 1;
                }
            }

            if(flak == 0){
                result = true;
            }
        }
        return result;
    }


    private void changeAnswerQuestion(RiskProfileQuestion question){

        if(riskProfileQuestionList != null && riskProfileQuestionList.size() > 0){
            for (int i = 0; i < riskProfileQuestionList.size(); i++){

                if(riskProfileQuestionList.get(i).getQuestionId() == question.getQuestionId()) {
                    riskProfileQuestionList.get(i).setAnswerId(question.getAnswerId()); //valuenya  array int [0]

                    RiskQuestionnaire riskQuestionnaire = new RiskQuestionnaire();
                    riskQuestionnaire.setRiskQuestionId(String.valueOf(question.getQuestionId()));
                    riskQuestionnaire.setRiskAnswerId(String.valueOf(question.getAnswerId().get(0).intValue())); //valuenya String "0"

                    if (listAnswerRisk != null && listAnswerRisk.size() > 0) {
                        int cek = 0;
                        for (int j = 0; j < listAnswerRisk.size(); j++) {
                            int questId = Integer.parseInt(listAnswerRisk.get(j).getRiskQuestionId());
                            if(riskProfileQuestionList.get(i).getQuestionId() == questId){
                                listAnswerRisk.set(j, riskQuestionnaire);
                                cek = 1;
                                System.out.println("replace answer ==> " + j);
                            }
                        }
                        if (cek == 0) {
                            listAnswerRisk.add(riskQuestionnaire);
                            System.out.println("insert answer ==> " + i);
                        }
                    } else {
                        listAnswerRisk.add(riskQuestionnaire);
                        System.out.println("answer still empty ==> " + i);
                    }

                    break;
                }

            }
        }
    }


    public void fetchResultToLayout(){
        lnConnectionError.setVisibility(View.GONE);
        llResultScore.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        tvValue.setText(riskProfileNewResponse.getData().getValue());
        tvDescription.setText(riskProfileNewResponse.getData().getDescription());
        tvScore.setText(String.valueOf(riskProfileNewResponse.getData().getScore()));
        flakButton = 1;
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.CHANGE_BUTTON, null));
    }


    public void setAnswerValueFromRiskProfile(){
        if(request.getRiskQuestionnaireList().size() != 0){
            for(int i = 0; i < request.getRiskQuestionnaireList().size(); i++){
                for(int j = 0; j < riskProfileQuestionList.size(); j++){
                    List<Integer> answerId = new ArrayList<>();
                    if(request.getRiskQuestionnaireList().get(i).getRiskQuestionId() == riskProfileQuestionList.get(j).getQuestionId()){
                        answerId.add(request.getRiskQuestionnaireList().get(i).getRiskAnswerId());
                        riskProfileQuestionList.get(j).setAnswerId(answerId);
                        break;
                    }
                }
            }
        }
    }


    //get answer name
    public String getAnswerName(int answerId){
        String result = "";
        for (int k = 0; k < riskProfileQuestionList.size(); k++) {
            if (riskProfileQuestionList.get(k).getAnswerOption() != null) {
                for (int l = 0; l < riskProfileQuestionList.get(k).getAnswerOption().size(); l++) {
                    if (answerId == riskProfileQuestionList.get(k).getAnswerOption().get(l).getId()) {
                        result = riskProfileQuestionList.get(k).getAnswerOption().get(l).getAnswerName();
                    }
                }
            }
        }
        return result;
    }



    //get score
    public void getScoreRiskProfile(){
        String[] answer = null;
        if(riskProfileQuestionList != null){
            List<RiskProfileNew> riskProfileNews = new ArrayList<>();
            if(riskProfileQuestionList.size() != 0){
                for (int i = 0; i < riskProfileQuestionList.size(); i++) {
                    RiskProfileNew riskProfileNew = new RiskProfileNew();
                    riskProfileNew.setQuestion(String.valueOf(riskProfileQuestionList.get(i).getQuestionName()));

                    if(riskProfileQuestionList.get(i).getAnswerId().size() != 0){
                        for(int j = 0; j < riskProfileQuestionList.get(i).getAnswerId().size(); j++){
                            answer = new String[1];
                            answer[0] = getAnswerName(riskProfileQuestionList.get(i).getAnswerId().get(j));
                            riskProfileNew.setAnswers(answer);
                            break;
                        }
                        riskProfileNews.add(riskProfileNew);
                    }
                }
            }

            RiskProfileRequest riskProfileRequest = new RiskProfileRequest();
            riskProfileRequest.setRisk_profile(riskProfileNews);
            presenter.getScoreRiskProfileLevel(riskProfileRequest);
        }
    }



    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError(){
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }

    public void connectionErrorGone(){
        lnConnectionError.setVisibility(View.GONE);
    }


    @OnClick(R.id.tvTryAgain)
    void tvTryAgain(){
        presenter.loadRiskProfileQuestionAndAnswer();
    }


    @Override
    public void previewsWhileError(){

    }


}
