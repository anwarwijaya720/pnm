package com.danareksa.investasik.ui.adapters.rv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.PortfolioInvestment;
import com.danareksa.investasik.util.AmountFormatter;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by fajarfatur on 3/1/16.
 */
public class PortfolioAdapter extends RecyclerView.Adapter<PortfolioAdapter.PortofolioHolder> {

    private Context context;
    private List<PortfolioInvestment> list;

    public PortfolioAdapter(Context context, List<PortfolioInvestment> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public PortofolioHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_portfolio, parent, false);
        return new PortofolioHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PortofolioHolder holder, int position) {
        final PortfolioInvestment item = list.get(position);
        double totalInv = item.getInvestmentAmount();
        double totalMarketValue = item.getTotalInvestmentMarketValue();
        double totalGainLoss = totalMarketValue - totalInv;
        float percentage = (float) (totalGainLoss / totalInv) * 100;
        double marketValue = 0.0;

        if(item.getInvestmentComposition() != null && item.getInvestmentComposition().size() > 0){
            marketValue = item.getInvestmentComposition().get(0).getMarketValue();
        }else{
            marketValue = totalMarketValue;
        }

        holder.itemView.setTag(item);

        Picasso.with(context).load(InviseeService.IMAGE_DOWNLOAD_URL + item.getPackageImageKey())
                .placeholder(R.drawable.ic_logo_launcher)
                .error(R.drawable.ic_logo_launcher)
                .into(holder.ivPackage);

        holder.tvPackageName.setText(item.getPackageName());
        //holder.tvUrealizeGainLoss.setText(AmountFormatter.format(totalGainLoss));

        if(item.getCurrency().equals("IDR")){
            holder.tvMarketValue.setText(AmountFormatter.format(marketValue));
        }else if(item.getCurrency().equals("USD")){
            holder.tvMarketValue.setText(AmountFormatter.formatUsd(marketValue));
        }

        if(item.getInvestmentComposition().size() > 0 && item.getInvestmentComposition() != null){
            double nab = item.getInvestmentComposition().get(0).getLastNav();
            String dateNab = item.getInvestmentComposition().get(0).getLastNavDate();
            holder.tvUrealizeGainLossPercent.setText(String.format("%.2f", percentage) + "%" + " | " + "NAB " + AmountFormatter.formatWithoutIdr(nab));
            holder.tvLastNavDate.setText("(" + "per " + dateNab + ")");
        }else{
            holder.tvUrealizeGainLossPercent.setText("YoY " + String.format("%.2f", percentage) + "%");
        }


    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public static class PortofolioHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.ivPackage)
        CircleImageView ivPackage;
        @Bind(R.id.tvPackageName)
        TextView tvPackageName;
        @Bind(R.id.tvLastNavDate)
        TextView tvLastNavDate;
        @Bind(R.id.tvMarketValue)
        TextView tvMarketValue;
        //@Bind(R.id.tvUrealizeGainLoss)
        //TextView tvUrealizeGainLoss;
        @Bind(R.id.tvUrealizeGainLossPercent)
        TextView tvUrealizeGainLossPercent;

        public PortofolioHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}