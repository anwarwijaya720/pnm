package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.ui.fragments.purchase.RegulerPurchaseFragment;

import butterknife.Bind;
import icepick.State;


/**
 * Created by asep.surahman on 25/06/2018.
 */

public class ListOfRegulerPurchaseActivity extends BaseActivity{


    private static final String PACKAGES = "packages";
    private static final String PRODUCT_LIST = "product_list";

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;
    String ifuaId;
    @State
    Packages packages;



    public static void startActivity(BaseActivity sourceActivity, String ifuaId, Packages packages) {
        Intent intent = new Intent(sourceActivity, ListOfRegulerPurchaseActivity.class);
        intent.putExtra("ifuaId", ifuaId);
        intent.putExtra(PACKAGES, packages);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_cart;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        setTitle("");
        ifuaId = getIntent().getStringExtra("ifuaId");
        packages = (Packages) getIntent().getSerializableExtra(PACKAGES);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        title.setText("Pembelian Reguler");
        RegulerPurchaseFragment.showFragment(this, ifuaId, packages, true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //ListOfCatalogueActivity.startActivity(ListOfRegulerPurchaseActivity.this);
        finish();
    }

}
