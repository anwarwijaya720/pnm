package com.danareksa.investasik.ui.fragments.checkout;

import com.danareksa.investasik.data.api.beans.DetailBcaKlikpay;
import com.danareksa.investasik.data.api.beans.PaymentDetail;
import com.danareksa.investasik.data.api.requests.BcaKlikpayTransRequest;
import com.danareksa.investasik.data.api.requests.GetOrderNumberMandiriClickpayRequest;
import com.danareksa.investasik.data.api.requests.PaymentTransferRequest;
import com.danareksa.investasik.data.api.responses.BcaKlikPayDataResponse;
import com.danareksa.investasik.data.api.responses.MandiriClickpayOrderNumberResponse;
import com.danareksa.investasik.data.api.responses.PaymentMethodResponse;
import com.danareksa.investasik.data.api.responses.TrxTransCodeResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by asep.surahman on 21/06/2018.
 */

public class PaymentListNewPresenter {

    private PaymentListNewFragment fragment;
    private String index12;
    private boolean retry;

    public PaymentListNewPresenter(PaymentListNewFragment fragment) {
        this.fragment = fragment;
    }



    void getPaymentList() {
        fragment.showProgressBar();
        fragment.getApi().getPaymentMethod(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PaymentMethodResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(PaymentMethodResponse response){
                        fragment.dismissProgressBar();//test
                        fragment.paymentMethodResponse = response;
                        fragment.payment();
                        //getWalletBalance();
                    }

                });
    }



    public PaymentTransferRequest getPaymentTransRequest(String paymentMethod, List<PaymentDetail> paymentDetails){
        PaymentTransferRequest paymentTransferRequest = new PaymentTransferRequest();
        paymentTransferRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        paymentTransferRequest.setAppsType("ANDROID");
        paymentTransferRequest.setPaymentMethod(paymentMethod);
        paymentTransferRequest.setDetail(paymentDetails);
        return paymentTransferRequest;
    }


    public GetOrderNumberMandiriClickpayRequest getOrderNumberMandiriClickpay(){
        GetOrderNumberMandiriClickpayRequest request = new GetOrderNumberMandiriClickpayRequest();
        request.setAppsType("ANDROID");
        request.setToken(PrefHelper.getString(PrefKey.TOKEN));
        return request;
    }

    void paymentTransferUniqeCode(PaymentTransferRequest paymentTransferRequest, final String paymentType) {
        fragment.showProgressBar();
        fragment.getApi().transferWithUniqeCode(paymentTransferRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<TrxTransCodeResponse>() {
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                        fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(TrxTransCodeResponse response){
                        fragment.dismissProgressBar();
                        if(response.getCode() == 0){
                            fragment.trxTransCodeResponse = response;
                            fragment.fetchResultToFragmentTransferKodeUnik(paymentType);
                        }else{
                            fragment.showDialogFailed(response.getInfo());
                        }
                    }
                });
    }


    void paymentVaPermata(PaymentTransferRequest paymentTransferRequest, final String paymentType) {
        fragment.showProgressBar();
        fragment.getApi().paymentVaPermata(paymentTransferRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<TrxTransCodeResponse>() {
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                        fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(TrxTransCodeResponse response){
                        fragment.dismissProgressBar();
                        if(response.getCode() == 0){
                            fragment.trxTransCodeResponse = response;
                            fragment.fetchResultToFragmentVaPermata(paymentType);
                        }else{
                            fragment.showDialogFailed(response.getInfo());
                        }
                    }

                });
    }




    void getOrderMandiriClickpay(final String paymentType){
        fragment.showProgressBar();
        fragment.getApi().getOrderNumberMandiriClickpay(getOrderNumberMandiriClickpay())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<MandiriClickpayOrderNumberResponse>() {
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.connectionError();
                        fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(MandiriClickpayOrderNumberResponse response){
                        fragment.dismissProgressBar();
                        if(response.getCode() == 0){
                            fragment.orderNumberResponse = response;
                            fragment.trxNumber = response.getData().getTrxNo();
                            fragment.adminFee  = response.getData().getAdminFee();
                            fragment.fetchResultMandiriClickpay(paymentType);
                        }else{
                            fragment.showDialogFailed(response.getInfo());
                        }
                    }
                });
    }





    Double getTotal() {
        Double total = 0d;
        for (int i = 0; i < fragment.cartList.getCartList().size(); i++) {
            Double t = Double.parseDouble(fragment.cartList.getCartList().get(i).getTotal());
            total += t;
        }
        return total;
    }


    //bca transaction
    void getBcaKlikpay(final String paymentMethod, final DetailBcaKlikpay detailBcaKlikpay){
        fragment.showProgressBar();
        fragment.getApi().getTransactionBcaKlikpay(getRequestBcaKlikpay(paymentMethod, detailBcaKlikpay))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<BcaKlikPayDataResponse>(){
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.connectionError();
                        fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(BcaKlikPayDataResponse response){
                        fragment.dismissProgressBar();
                        if(response.getCode() == 0){
                            fragment.transactionNo = response.getData().getData().getTransactionNo();
                            fragment.fetchResultBcaKlikpay(response.getData().getRedirectURL(), response.getData().getReferrer(), response.getData(), paymentMethod, fragment.transactionNo);
                        }else{
                            fragment.showDialogFailed(response.getInfo());
                        }
                    }
                });
    }




    public BcaKlikpayTransRequest getRequestBcaKlikpay(String paymentMethod, DetailBcaKlikpay detailBcaKlikpay){
        BcaKlikpayTransRequest request = new BcaKlikpayTransRequest();
        request.setAppsType("ANDROID");
        request.setToken(PrefHelper.getString(PrefKey.TOKEN));
        request.setDetail(detailBcaKlikpay);
        request.setPaymentMethod(paymentMethod);
        return request;
    }


    public DetailBcaKlikpay getDetailBcaKlikpay(String id, double netAmount){
        DetailBcaKlikpay detailBcaKlikpay = new DetailBcaKlikpay();
        detailBcaKlikpay.setId(id);
        detailBcaKlikpay.setNetAmount(netAmount);
        return detailBcaKlikpay;
    }






}
