package com.danareksa.investasik.ui.adapters.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc0CitizenshipFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc10AdvancedSourceOfFundsFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc11AdvancedOwnersOfBenefitFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc12AdvancedHeirFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc13AdvancedRiskProfileFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc1DocumentDataFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc2PersonalDataOneFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc3PersonalDataTwoFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc4PersonalDataThreeFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc5EmploymentDataOneFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc6EmploymentDataTwoFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc7FinancialDataFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc8FinancialDataDocumentFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc9AdvancedCorrespondenceFragment;


/**
 * Created by asep.surahman on 15/05/2018.
 */

public class KenaliResikoUserPageAdapter extends FragmentPagerAdapter {


    final int PAGE_COUNT = 1;
    private BaseFragment bFragment;
    private KycDataRequest request;

    private String jsonCountry;
    private String jsonState;
    private String jsonCity;
    private String jsonBank;
    private String sourceRoute;

    private String[] title = new String[] {"Profil risiko"};
    //,  "Selesai"

    public KenaliResikoUserPageAdapter(BaseFragment bFragment, FragmentManager fm, KycDataRequest request, String jsonCountry, String jsonState, String jsonCity, String jsonBank, String sourceRoute) {
        super(fm);
        this.bFragment = bFragment;
        this.request = request;
        this.jsonCountry = jsonCountry;
        this.jsonState = jsonState;
        this.jsonCity = jsonCity;
        this.jsonBank = jsonBank;
        this.sourceRoute = sourceRoute;
    }

    @Override
    public int getCount(){
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = Kyc13AdvancedRiskProfileFragment.getFragment(request, sourceRoute);
                break;
        }
        return fragment;
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

}
