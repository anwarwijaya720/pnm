package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.KenaliResikoUser;
import com.danareksa.investasik.ui.fragments.kycnew.KycKenaliResikoDataMain;
import com.danareksa.investasik.util.Constant;

import butterknife.Bind;

public class KenaliResiko extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;

    //public static KenaliResikoUser kenaliResikoUser = new KenaliResikoUser();

    String sourceRoute;

    public static void startActivity(BaseActivity sourceActivity) {
        Intent intent = new Intent(sourceActivity, KenaliResiko.class);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_register_data_pribadi;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar();
        //kenaliResikoUser.setDesResiko("KR");
        sourceRoute = getIntent().getStringExtra(Constant.SOURCE_ROUTE);
        KycKenaliResikoDataMain.showFragment(this, sourceRoute);
    }

    public void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        title.setText("Kenali Resiko");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        startActivity(new Intent(KenaliResiko.this, MainActivity.class));
        finish();
    }

}
