package com.danareksa.investasik.ui.fragments.promo;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.util.Linkify;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.PromoDetail;
import com.danareksa.investasik.data.api.beans.PromoResponse;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.DetailPromoActivity;
import com.danareksa.investasik.ui.activities.PromoActivity;
import com.danareksa.investasik.ui.activities.SignInActivity;
import com.danareksa.investasik.ui.activities.SyaratKetentuanPromoActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;

import butterknife.Bind;
import butterknife.OnClick;
import icepick.State;

/**
 * Created by pandu.abbiyuarsyah on 18/05/2017.
 */

public class DetailPromoFragment extends BaseFragment {

    public static final String TAG = DetailPromoFragment.class.getSimpleName();
    private static final String PROMO = "promo";

    @Bind(R.id.tvTitle)
    TextView tvTitle;
//    @Bind(R.id.tvDesc)
//    TextView tvDesc;
//    @Bind(R.id.img1)
//    ImageView img1;
//    @Bind(R.id.img2)
//    LinearLayout img2;
//    @Bind(R.id.img3)
//    LinearLayout img3;
    @Bind(R.id.txt_promo_link)
    TextView txtPromoLink;
    @Bind(R.id.wv)
    WebView wv;
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;

    @State
    PromoResponse response;

    public String promoDescription;
    public PromoDetail promoDetail;
    public DetailPromoPresenter presenter;

    public static void showFragment(BaseActivity sourceActivity, PromoResponse response) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, getFragment(response), TAG);
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, R.anim.slide_out_left, android.R.anim.slide_in_left, R.anim.slide_out_left);
            fragmentTransaction.commit();
        }
    }

    public static Fragment getFragment(PromoResponse response) {
        Fragment f = new DetailPromoFragment();

        Bundle extras = new Bundle();
        extras.putSerializable(PROMO, response);

        f.setArguments(extras);
        return f;
    }

    @Override
    protected int getLayout() {
        return R.layout.f_detail_promo;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new DetailPromoPresenter(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Linkify.addLinks(txtPromoLink, Linkify.ALL);


        if (response == null) {
            Bundle extras = getArguments();
            if (extras != null && extras.containsKey(PROMO)) {
                response = (PromoResponse) extras.getSerializable(PROMO);
            }
        }

        if (response != null && !response.getCode().equals("")) {
            presenter.getDetailDesc();
        }
    }

    public void loadDetail(){

        /*wv.setInitialScale(1);
        wv.getSettings().setLoadWithOverviewMode(true);
        wv.getSettings().setUseWideViewPort(true);
        wv.getSettings().setJavaScriptEnabled(true);
        System.out.println("conten : " + promoDescription);
        wv.loadDataWithBaseURL("", promoDescription, "text/html", "UTF-8", "");*/

        tvTitle.setText(response.getTitle());
        wv.getSettings().setLoadWithOverviewMode(true);
        wv.getSettings().setUseWideViewPort(true);
        wv.getSettings().setJavaScriptEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            wv.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING);
        } else {
            wv.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        }

        wv.loadDataWithBaseURL("file:///android_asset/", getHtmlData(promoDescription), "text/html", "utf-8", null);

    }

    private String getHtmlData(String bodyHTML) {
        String head = "<head><style>img{max-width: 100%;width:100%; display:inline; height: auto;}</style></head>";
        return "<html>" + head + "<body>" + bodyHTML + "</body></html>";
    }

    @OnClick(R.id.btnSubmit)
    void submitJoinPromo() {
        if (response.getCode().contains("PNBP")) {
            new MaterialDialog.Builder(getActivity())
                    .iconRes(R.mipmap.ic_launcher)
                    .backgroundColor(Color.WHITE)
                    .title(getString(R.string.infortmation).toUpperCase())
                    .titleColor(Color.BLACK)
                    .content(R.string.desc_bangkok_pattaya)
                    .contentColor(Color.GRAY)
                    .positiveText(R.string.ok)
                    .positiveColor(Color.GRAY)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            PromoActivity.startActivity((BaseActivity) getActivity());
                        }
                    })
                    .cancelable(false)
                    .show();

        } else {
            if (BaseActivity.IS_FROM_LANDING) {
                ((DetailPromoActivity)this.getContext()).finish();
                SignInActivity.startActivity((BaseActivity) getActivity());
            } else {
                presenter.joinPromo();
            }
        }

    }

    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.tvTryAgain)
    void retryConnection() {
        presenter.getDetailDesc();
    }

    @OnClick(R.id.txt_promo_link)
    public void goToSyaratKetentuan(){
        SyaratKetentuanPromoActivity.startActivity((BaseActivity) getActivity(), response);
    }

}
