package com.danareksa.investasik.ui.fragments.product;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.InvestmentAccountGroup;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.ProductList;
import com.danareksa.investasik.data.api.requests.InvestmentAcountGroupRequest;
import com.danareksa.investasik.data.api.responses.InvestmentAccountInfoResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.adapters.pager.ProductChooseIfuaPageAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.ui.NonSwipeableViewPager;
import com.danareksa.investasik.util.ui.WrapContentHeightViewPager;

import butterknife.Bind;
import butterknife.OnClick;
import icepick.State;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by asep.surahman on 11/06/2018.
 */

public class ChooseIfuaFragment extends BaseFragment {

    public static final String TAG = ChooseIfuaFragment.class.getSimpleName();

    @Bind(R.id.pager)
    NonSwipeableViewPager pager;

    @Bind(R.id.bLumpsum)
    Button bLumpsum;

    @Bind(R.id.bReguler)
    Button bReguler;

    @State
    int pageNumber = 0;
    int status = 0;
    int flak = 0;

    private ProductChooseIfuaPageAdapter pagerAdapter;

    @State
    ProductList product;

    @State
    Packages packages;

    @State
    InvestmentAccountGroup accountGroup;

    private final static String PRODUCT = "product";
    private final static String PACKAGES = "packages";


    public static void showFragment(BaseActivity sourceActivity, ProductList product, Packages packages) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, R.anim.slide_out_left, android.R.anim.slide_in_left, R.anim.slide_out_left);
            ChooseIfuaFragment fragment = new ChooseIfuaFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(PRODUCT, product);
            bundle.putSerializable(PACKAGES, packages);
            fragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.f_choose_ifua;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        product  = (ProductList) getArguments().getSerializable(PRODUCT);
        packages = (Packages) getArguments().getSerializable(PACKAGES);
        getInvestmentAccountGroup();
        status = 0;
    }


    @Override
    public void onResume(){
        super.onResume();
    }

    @OnClick(R.id.bLumpsum)
    void bLumpsum() {
        if (pageNumber > 0) {
            pageNumber--;
            pager.setCurrentItem(pageNumber);

            if (status == 0){
                bLumpsum.setBackgroundResource(R.drawable.left_bg_round_white);
                bReguler.setBackgroundResource(R.drawable.right_bg_round_blue);
                bLumpsum.setTextColor(getResources().getColor(R.color.colorPrimary));
                bReguler.setTextColor(getResources().getColor(R.color.white));
                status = 1;
            } else if (status == 1) {
                bLumpsum.setBackgroundResource(R.drawable.left_bg_round_blue);
                bReguler.setBackgroundResource(R.drawable.right_bg_round_white);
                bLumpsum.setTextColor(getResources().getColor(R.color.white));
                bReguler.setTextColor(getResources().getColor(R.color.colorPrimary));
                status = 0;
            }
        }
    }


    @OnClick(R.id.bReguler)
    void bReguler(){
        if(flak == 1){
            if (pageNumber < pagerAdapter.getCount() - 1) {
                pageNumber++;
                pager.setCurrentItem(pageNumber);

                if (status == 0) {
                    bLumpsum.setBackgroundResource(R.drawable.left_bg_round_white);
                    bReguler.setBackgroundResource(R.drawable.right_bg_round_blue);
                    bLumpsum.setTextColor(getResources().getColor(R.color.colorPrimary));
                    bReguler.setTextColor(getResources().getColor(R.color.white));
                    status = 1;
                } else if (status == 1) {
                    bLumpsum.setBackgroundResource(R.drawable.left_bg_round_blue);
                    bReguler.setBackgroundResource(R.drawable.right_bg_round_white);
                    bLumpsum.setTextColor(getResources().getColor(R.color.white));
                    bReguler.setTextColor(getResources().getColor(R.color.colorPrimary));
                    status = 0;
                }
            }
        }

    }

    public void setupViewPager(){
        pagerAdapter = new ProductChooseIfuaPageAdapter(this, getChildFragmentManager(), product, packages, accountGroup);
        pager.setAdapter(pagerAdapter);
        pager.addOnPageChangeListener(new WrapContentHeightViewPager.OnPageChangeListener(){

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels){
            }

            @Override
            public void onPageSelected(int position){
            }

            @Override
            public void onPageScrollStateChanged(int state){

            }
        });
    }


    private void getInvestmentAccountGroup() {
        InvestmentAcountGroupRequest request = new InvestmentAcountGroupRequest();
        request.setToken(PrefHelper.getString(PrefKey.TOKEN));

        this.getApi().getInvestmentAccountGroup(request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<InvestmentAccountInfoResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(InvestmentAccountInfoResponse investmentAccountInfoResponse) {

                        flak = 1;
                        accountGroup = investmentAccountInfoResponse.getData();
                        if (investmentAccountInfoResponse.getCode() == 0){
                            setupViewPager();
                        }else{
                            setupViewPager();
                        }

                    }
                });
    }



}
