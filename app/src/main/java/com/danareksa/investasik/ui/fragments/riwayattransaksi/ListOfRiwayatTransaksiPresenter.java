package com.danareksa.investasik.ui.fragments.riwayattransaksi;

import android.view.View;

import com.danareksa.investasik.data.api.beans.TransactionHistory;
import com.danareksa.investasik.data.api.responses.TransactionHistoryResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by asep.surahman on 29/06/2018.
 */

public class ListOfRiwayatTransaksiPresenter {

    private static final String MAX = "500";

    private ListOfRiwayatTransaksiFragment fragment;

    public ListOfRiwayatTransaksiPresenter(ListOfRiwayatTransaksiFragment fragment) {
        this.fragment = fragment;
    }

    void getTransactionHistory(Map<String, String> params) {
        fragment.showProgressBar();

        Map<String, String> data = new HashMap<>();
        data.put("token", PrefHelper.getString(PrefKey.TOKEN));
        data.put("max", MAX);
        data.putAll(params);

        fragment.txvJenisTransaksi.setText(data.get("p_trx_type").toString());
        fragment.txvPeriode.setText(data.get("p_period").toString());
        fragment.txvIfua.setText(data.get("ifua").toString());

        fragment.getApi().getTransactionHistory(data)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<TransactionHistoryResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();

                    }

                    @Override
                    public void onNext(TransactionHistoryResponse transactionHistories) {
                        if (transactionHistories.getData() != null) {
                            fragment.txvNamaNasabah.setText(transactionHistories.getData().getName());

                            fragment.transactionHistories = (ArrayList<TransactionHistory>) transactionHistories.getData().getTransactions();
                            if (fragment.transactionHistories.size() > 0) {
                                fragment.loadTrxHistoryList();
                                fragment.dismissProgressBar();
                                return;
                            }
                        }

                            fragment.tvMessage.setVisibility(View.VISIBLE);
                            fragment.rv.setVisibility(View.GONE);
                            fragment.dismissProgressBar();

                    }
                });
    }

}
