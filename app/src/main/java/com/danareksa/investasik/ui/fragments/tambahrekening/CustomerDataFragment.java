package com.danareksa.investasik.ui.fragments.tambahrekening;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.requests.AddAccountRequest;
import com.danareksa.investasik.data.api.responses.CustomerDataRekeningResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.eventBus.RxBusObject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by asep.surahman on 25/07/2018.
 */


public class CustomerDataFragment extends BaseInvest {

    public static final String TAG = CustomerDataFragment.class.getSimpleName();

    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.tvNumberCif)
    TextView tvNumberCif;
    @Bind(R.id.tvFullName)
    TextView tvFullName;
    @Bind(R.id.tvNumberCard)
    TextView tvNumberCard;
    @Bind(R.id.tvTryAgain)
    TextView tvTryAgain;
    @Bind(R.id.sInvestmentGoal)
    Spinner sInvestmentGoal;
    @Bind(R.id.tvDate)
    TextView tvDate;
    @Bind(R.id.tvMonth)
    TextView tvMonth;
    @Bind(R.id.tvYear)
    TextView tvYear;
    @Bind(R.id.tvLabelTglDebet)
    TextView tvLabelTglDebet;
    @Bind(R.id.llTglDebet)
    LinearLayout llTglDebet;
    @Bind(R.id.bNext)
    Button bNext;
    @Bind(R.id.etOtherGoal)
    EditText etOtherGoal;
    String jsonCountry;
    String jsonBank;
    String typeAccount;
    public static final String ADD_ACCOUNT_REQUEST = "accountRequest";
    CustomerDataPresenter presenter;


    public static Fragment getFragment(String jsonCountry, String jsonBank, String typeAccount, AddAccountRequest acountRequest){
        Bundle bundle = new Bundle();
        bundle.putString("jsonCountry", jsonCountry);
        bundle.putString("jsonBank", jsonBank);
        bundle.putString("type", typeAccount);
        bundle.putSerializable(ADD_ACCOUNT_REQUEST, acountRequest);
        CustomerDataFragment fragment = new CustomerDataFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected int getLayout(){
        return R.layout.f_tambah_rekening_isi_data_nasabah;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new CustomerDataPresenter(this);
        typeAccount = getArguments().getString("type");
        jsonCountry = getArguments().getString("jsonCountry");
        jsonBank = getArguments().getString("jsonBank");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        etOtherGoal.setVisibility(View.GONE);
        init();
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")){
           presenter.getCustomerData(typeAccount);
        }
    }


    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar() {
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }


    @Override
    public void onResume(){
        super.onResume();
    }


    @OnClick(R.id.bNext)
    void bNext(){
        if(!validate()){
            onValidFailed();
            return;
        }else{
            if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")){
                setValueCustomerData();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
            }else{
                Toast.makeText(getContext(), "Account Anda belum terverifikasi!", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void setValueCustomerData(){
        acountRequest.setGoalCode(getKycLookupCodeFromSelectedItemSpinner(sInvestmentGoal));
        if(getKycLookupCodeFromSelectedItemSpinner(sInvestmentGoal).equalsIgnoreCase("oth")){
            acountRequest.setGoalDetail(etOtherGoal.getText().toString());
        }
    }


    @OnClick(R.id.tvTryAgain)
    void tvTryAgain(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")) {
            presenter.getCustomerData(typeAccount);
        }
    }


    private void init(){
        setupSpinnerWithSpecificLookupSelection(sInvestmentGoal, getKycLookupFromRealm(Constant.KYC_CAT_INVESTMENT_GOAL), "");
        if(getKycLookupCodeFromSelectedItemSpinner(sInvestmentGoal).equalsIgnoreCase("oth")){
            etOtherGoal.setVisibility(View.VISIBLE);
            etOtherGoal.setText(acountRequest.getGoalDetail());
        }else{
            etOtherGoal.setVisibility(View.GONE);
        }
        onItemSelectedItem();
    }


    public void onItemSelectedItem(){

        sInvestmentGoal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(getKycLookupCodeFromSelectedItemSpinner(sInvestmentGoal).equalsIgnoreCase("oth")){
                    etOtherGoal.setVisibility(View.VISIBLE);
                    etOtherGoal.requestFocus();
                }else{
                    etOtherGoal.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }



    public void fetchResultToLayout(CustomerDataRekeningResponse customerDataRekeningResponse){

        tvNumberCif.setText(customerDataRekeningResponse.getData().getCif());
        tvFullName.setText(customerDataRekeningResponse.getData().getFullName());
        tvNumberCard.setText(customerDataRekeningResponse.getData().getIdNumber());

        if(typeAccount.equals(Constant.INVEST_TYPE_REGULER)){
            tvLabelTglDebet.setVisibility(View.VISIBLE);
            llTglDebet.setVisibility(View.VISIBLE);

            if(!customerDataRekeningResponse.getData().getNextAutoDebit().equalsIgnoreCase("") && customerDataRekeningResponse.getData().getNextAutoDebit() != null){
                String date = customerDataRekeningResponse.getData().getNextAutoDebit();
                String[] dateParts = date.split("-");
                String year = dateParts[0];
                String month = dateParts[1];
                String day = dateParts[2];
                tvDate.setText(day);
                tvMonth.setText(month);
                tvYear.setText(year);
            }

        }else if(typeAccount.equals(Constant.INVEST_TYPE_LUMPSUM)){
            tvLabelTglDebet.setVisibility(View.GONE);
            llTglDebet.setVisibility(View.GONE);
        }


    }


    public void fetchResultFailed(){

        if(typeAccount.equals(Constant.INVEST_TYPE_REGULER)){
            tvLabelTglDebet.setVisibility(View.VISIBLE);
            llTglDebet.setVisibility(View.VISIBLE);
        }else if(typeAccount.equals(Constant.INVEST_TYPE_LUMPSUM)){
            tvLabelTglDebet.setVisibility(View.GONE);
            llTglDebet.setVisibility(View.GONE);
        }


    }





    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }

    public boolean validate(){
        boolean valid = true;
        if(sInvestmentGoal.getSelectedItem().toString().equals("")){
            valid = false;
            ((TextView) sInvestmentGoal.getSelectedView()).setError("Mohon pilih kebutuhan investasi");
        }

        if (getKycLookupCodeFromSelectedItemSpinner(sInvestmentGoal).equalsIgnoreCase("oth")
                && etOtherGoal.getText().toString().equals("") ) {
            valid = false;
            etOtherGoal.setError("Field investasi lainnya harus diisi");
        }

        if(getKycLookupCodeFromSelectedItemSpinner(sInvestmentGoal).equalsIgnoreCase("oth") && !etOtherGoal.getText().toString().equals("")){
            String otherGoalString = etOtherGoal.getText().toString();
            if(otherGoalString.length() < 3){
                valid = false;
                etOtherGoal.setError("Field investasi lainnya tidak sesuai");
            }
        }

        return  valid;
    }


    @Override
    public void nextWithoutValidation(){

    }

    @Override
    public void saveDataKycWithBackpress(){

    }


    void showDialog(String info){
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.infortmation).toUpperCase())
                .titleColor(Color.BLACK)
                .content(info)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                        getActivity().finish();
                    }
                })
                .show();
    }


    @Override
    public void addAccountBackpress() {
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
    }

}
