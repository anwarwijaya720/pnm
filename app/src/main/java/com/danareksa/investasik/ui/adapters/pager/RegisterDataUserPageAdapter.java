package com.danareksa.investasik.ui.adapters.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc0CitizenshipFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc10AdvancedSourceOfFundsFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc11AdvancedOwnersOfBenefitFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc12AdvancedHeirFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc13AdvancedRiskProfileFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc1DocumentDataFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc2PersonalDataOneFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc3PersonalDataTwoFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc4PersonalDataThreeFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc5EmploymentDataOneFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc6EmploymentDataTwoFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc7FinancialDataFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc8FinancialDataDocumentFragment;
import com.danareksa.investasik.ui.fragments.kycnew.Kyc9AdvancedCorrespondenceFragment;


/**
 * Created by asep.surahman on 15/05/2018.
 */

public class RegisterDataUserPageAdapter extends FragmentPagerAdapter {


    final int PAGE_COUNT = 14; //15
    private BaseFragment bFragment;
    private KycDataRequest request;

    private String jsonCountry;
    private String jsonState;
    private String jsonCity;
    private String jsonBank;
    private String sourceRoute;

    private String[] title = new String[] {"Kewarganegaraan","Dokumen","Data Pribadi 1","Data Pribadi 2","Data Pribadi 3","Data Pekerjaan 1", "Data Pekerjaan 2", "Data Keuangan", "Dokumen Keuangan"
                     , "Surat Menyurat", "Sumber Dana", "Pemilik Manfaat", "Ahli waris", "Profil risiko"
    };

    //,  "Selesai"

    public RegisterDataUserPageAdapter(BaseFragment bFragment, FragmentManager fm, KycDataRequest request,  String jsonCountry, String jsonState, String jsonCity, String jsonBank, String sourceRoute) {
        super(fm);
        this.bFragment = bFragment;
        this.request = request;
        this.jsonCountry = jsonCountry;
        this.jsonState = jsonState;
        this.jsonCity = jsonCity;
        this.jsonBank = jsonBank;
        this.sourceRoute = sourceRoute;
    }

    @Override
    public int getCount(){
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = Kyc0CitizenshipFragment.getFragment(request, jsonCountry);
                break;
            case 1:
                fragment = Kyc1DocumentDataFragment.getFragment(request);
                break;
            case 2:
                fragment = Kyc2PersonalDataOneFragment.getFragment(request);
                break;
            case 3:
                fragment = Kyc3PersonalDataTwoFragment.getFragment(request);
                break;
            case 4:
                fragment = Kyc4PersonalDataThreeFragment.getFragment(request, jsonCountry, jsonState, jsonCity);
                break;
            case 5:
                fragment = Kyc5EmploymentDataOneFragment.getFragment(request);
                break;
            case 6:
                fragment = Kyc6EmploymentDataTwoFragment.getFragment(request,  jsonCountry, jsonState, jsonCity);
                break;
            case 7:
                fragment = Kyc7FinancialDataFragment.getFragment(request, jsonBank);
                break;
            case 8:
                fragment = Kyc8FinancialDataDocumentFragment.getFragment(request);
                break;
            case 9:
                fragment = Kyc9AdvancedCorrespondenceFragment.getFragment(request,  jsonCountry, jsonState, jsonCity);
                break;
            case 10:
                fragment = Kyc10AdvancedSourceOfFundsFragment.getFragment(request);
                break;
            case 11:
                fragment = Kyc11AdvancedOwnersOfBenefitFragment.getFragment(request);
                break;
            case 12:
                fragment = Kyc12AdvancedHeirFragment.getFragment(request);
                break;
            case 13:
                fragment = Kyc13AdvancedRiskProfileFragment.getFragment(request, sourceRoute);
                break;
           /* case 14:
                fragment = Kyc15AdvancedFinishFragment.getFragment(request);
                break;*/
        }
        return fragment;
    }


    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

}
