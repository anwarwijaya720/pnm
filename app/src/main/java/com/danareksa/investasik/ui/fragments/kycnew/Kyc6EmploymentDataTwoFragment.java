package com.danareksa.investasik.ui.fragments.kycnew;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.City;
import com.danareksa.investasik.data.api.beans.Country;
import com.danareksa.investasik.data.api.beans.State;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.eventBus.RxBusObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by asep.surahman on 15/05/2018.
 */

public class Kyc6EmploymentDataTwoFragment extends KycBaseNew {

    public static final String TAG = Kyc6EmploymentDataTwoFragment.class.getSimpleName();

    protected Validator validator;
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.sCountry)
    Spinner sCountry;
    @Bind(R.id.sState)
    Spinner sState;
    @Bind(R.id.sCity)
    Spinner sCity;

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etOfficeAddress)
    EditText etOfficeAddress;

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etOfficePostalCode)
    EditText etOfficePostalCode;

    public List<Country> countries;
    public List<State> states;
    public List<City> cities;
    public KycDataRequest kycDataRequest;
    private String jsonCountry;
    private String jsonState;
    private String jsonCity;

    String countryId = "";
    String stateId = "";

    Kyc6EmploymentDataTwoPresenter presenter;

    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new Kyc6EmploymentDataTwoFragment(), TAG);
            fragmentTransaction.commit();
        }
    }



    public static Fragment getFragment(KycDataRequest kycDataRequest, String jsonCountry, String jsonState, String jsonCity) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(KYC_DATA_REQUEST, kycDataRequest);
        bundle.putString("jsonCountry", jsonCountry);
        bundle.putString("jsonState", jsonState);
        bundle.putString("jsonCity", jsonCity);
        Kyc6EmploymentDataTwoFragment kyc6EmploymentDataTwoFragment = new Kyc6EmploymentDataTwoFragment();
        kyc6EmploymentDataTwoFragment.setArguments(bundle);
        return kyc6EmploymentDataTwoFragment;
    }


    @Override
    protected int getLayout() {
        return R.layout.f_register_data_pekerjaan_2;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        kycDataRequest = (KycDataRequest) getArguments().getSerializable(KYC_DATA_REQUEST);
        jsonCountry = getArguments().getString("jsonCountry");
        jsonState   = getArguments().getString("jsonState");
        jsonCity    = getArguments().getString("jsonCity");
        presenter   = new Kyc6EmploymentDataTwoPresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();

    }

    public void init(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")) {
            disableField();
        }

        etOfficeAddress.setText(request.getOfficeAddress());
        etOfficePostalCode.setText(request.getOfficePostalCode());

        try{
            Type listType = new TypeToken<List<Country>>(){}.getType();
            Gson gson = new Gson();
            countries = gson.fromJson(jsonCountry, listType);

            Type listTypeState = new TypeToken<List<State>>() {}.getType();
            Gson gsonState = new Gson();
            states = gsonState.fromJson(jsonState, listTypeState);

            Type listTypeCity = new TypeToken<List<City>>() {}.getType();
            Gson gsonCity = new Gson();
            cities = gsonCity.fromJson(jsonCity, listTypeCity);

            //new
            if(request.getOfficeCountry() != null){
                setupSpinnerCountry(sCountry, countries, request.getOfficeCountry());
            }else{
                setupSpinnerCountry(sCountry, countries, Constant.ID_COUNTRY_INDO);
            }
            setupSpinnerState(sState, states, request.getOfficeProvince());
            setupSpinnerCity(sCity, cities, request.getOfficeCity());

            if(request.getOfficeCountry() != null){
                if(request.getOfficeCountry().equals(Constant.ID_COUNTRY_INDO)){

                    stateId = String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode());
                    if(!stateId.equals("")){
                        presenter.getCity(String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode()));
                    }else{
                        presenter.getCity(String.valueOf(3));
                    }

                }else{
                    if(!request.getOfficeCountry().equals("")){
                        presenter.getState(request.getOfficeCountry(), request.getOfficeProvince());
                    }
                }
            }else{

                stateId = String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode());
                if(!stateId.equals("")){
                    presenter.getCity(String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode()));
                }else{
                    presenter.getCity(String.valueOf(3));
                }
            }
            //end


        }catch (Exception e){
            e.printStackTrace();
        }

        onItemSelectedItem();
    }



    public void onItemSelectedItem(){
        sCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                countryId = String.valueOf(countries.get(sCountry.getSelectedItemPosition()).getId());

                if(sCountry.getSelectedItem().toString().equals("")){
                    countryId = "";
                    states = new ArrayList<>();
                    setupSpinnerState(sState, states, null);

                    cities = new ArrayList<>();
                    setupSpinnerCity(sCity, cities, null);
                }else{
                    if(!countryId.equals("")){
                        presenter.getState(countryId, "");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView){
            }
        });

        sState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                stateId = String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode());

                if (!stateId.equals("")) {
                    presenter.getCity(stateId);
                }else{
                    stateId = "";
                    cities = new ArrayList<>();
                    setupSpinnerCity(sCity, cities, null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    private void setValueEmployment(){
        request.setOfficeAddress(getAndTrimValueFromEditText(etOfficeAddress));
        request.setOfficePostalCode(getAndTrimValueFromEditText(etOfficePostalCode));

        if(String.valueOf(countries.get(sCountry.getSelectedItemPosition()).getId()).equals("0")){
            request.setOfficeCountry("");
        }else{
            request.setOfficeCountry(String.valueOf(countries.get(sCountry.getSelectedItemPosition()).getId()));
        }

        if(String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode()).equals("0")){
            request.setOfficeProvince("");
        }else{
            request.setOfficeProvince(String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode()));
        }

        if(String.valueOf(cities.get(sCity.getSelectedItemPosition()).getId()).equals("0")){
            request.setOfficeCity("");
        }else{
            request.setOfficeCity(String.valueOf(cities.get(sCity.getSelectedItemPosition()).getId()));
        }
    }


    @Override
    public void nextWithoutValidation(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")) {
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
        }else{
            if(!validate()){
                onValidFailed();
            }else{
                setValueEmployment();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
            }
            return;
        }
    }

    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }


    @Override
    public void saveDataKycWithBackpress(){
        if(!validate()){
            onValidFailed();
            return;
        }else{
            if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
                setValueEmployment();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.SAVE_KYC_DATA, request));
            }else{
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
            }
        }
    }

    @Override
    public void previewsWithoutValidation() {
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null));
    }




    //disable
    @Override
    public void onValidationSucceeded(){
        super.onValidationSucceeded();
    }


    public boolean validate(){

        boolean valid = true;
        String officeAddress  = etOfficeAddress.getText().toString();
        String postalCode     = etOfficePostalCode.getText().toString();

        if(request.getOccupation() != null){

            if (!request.getOccupation().equals("IRT")){

                if(officeAddress.isEmpty() && officeAddress.length() <= 0){
                    etOfficeAddress.setError("Mohon isi kolom ini");
                    valid = false;
                }

                if(!officeAddress.isEmpty() && officeAddress.length() < 3){
                    etOfficeAddress.setError("Mohon isi alamat dengan benar");
                    valid = false;
                }

               /* if(postalCode.isEmpty() && postalCode.length() <= 0){
                    etOfficePostalCode.setError("Mohon isi kolom ini");
                    valid = false;
                }*/


                if(sCountry.getSelectedItem().toString().equals("") ||
                    sCountry.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
                    valid = false;
                    ((TextView) sCountry.getSelectedView()).setError("Mohon isi kolom ini");
                }

                if(sState.getSelectedItem().toString().equals("") ||
                    sState.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
                    valid = false;
                    ((TextView) sState.getSelectedView()).setError("Mohon isi kolom ini");
                }

                if(sCity.getSelectedItem().toString().equals("") ||
                    sCity.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
                    valid = false;
                    ((TextView) sCity.getSelectedView()).setError("Mohon isi kolom ini");
                }

            }
        }

        /*if (officeAddress.isEmpty() && officeAddress.length() <= 0) {
            etOfficeAddress.setError("Mohon isi kolom ini");
            valid = false;
        }

        if (!officeAddress.isEmpty() && officeAddress.length() < 3) {
            etOfficeAddress.setError("Mohon isi alamat dengan benar");
            valid = false;
        }*/




        return valid;
    }


    public void showProgressBar() {
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar() {
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }


    public void disableField(){
        sCountry.setEnabled(false);
        sState.setEnabled(false);
        sCity.setEnabled(false);
        etOfficeAddress.setEnabled(false);
        etOfficePostalCode.setEnabled(false);
    }

    @Override
    public void previewsWhileError(){

    }

}
