package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.requests.ForgotPasswordUserRequest;
import butterknife.Bind;

public class RePasswordActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;

    ForgotPasswordUserRequest request = new ForgotPasswordUserRequest();

    public static void startActivity(BaseActivity sourceActivity) {
        Intent intent = new Intent(sourceActivity, RePasswordActivity.class);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_repassword;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar();

        //RePasswordFragment.showFragment((BaseActivity) fragment.getActivity(), fragment.etEmail.getText().toString());

        String email = request.getEmail();

        //RePasswordFragment.showFragment((BaseActivity) RePasswordActivity.getActivity(), email.toString());

    }

    public void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        title.setText("Reset Kata Sandi");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //ForgotPasswordActivity.startActivity((BaseActivity) getActivity());

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(RePasswordActivity.this, SignInActivity.class));
        finish();
    }
}