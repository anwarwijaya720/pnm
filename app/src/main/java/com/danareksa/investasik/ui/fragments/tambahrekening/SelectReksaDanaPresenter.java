package com.danareksa.investasik.ui.fragments.tambahrekening;

import com.danareksa.investasik.data.api.beans.PackageListReguler;
import com.danareksa.investasik.data.api.requests.PackageListRequest;
import com.danareksa.investasik.data.api.responses.PackageListRegulerResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by asep.surahman on 02/08/2018.
 */

public class SelectReksaDanaPresenter {

    SelectReksaDanaFragment fragment;

    public SelectReksaDanaPresenter(SelectReksaDanaFragment fragment){
        this.fragment = fragment;
    }


    public PackageListRequest getPackageListRequest(){
        PackageListRequest packageListRequest = new PackageListRequest();
        packageListRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        return packageListRequest;
    }


    public void getPackageList(){
        fragment.showProgressBar();
        fragment.getApi().getPackageListReguler(getPackageListRequest())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PackageListRegulerResponse>(){
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.dismissProgressBar();
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(PackageListRegulerResponse packageListRegulerResponse){
                        fragment.dismissProgressBar();
                        fragment.packageListRegulersAll = packageListRegulerResponse.getPackageListRegulers();
                        //fragment.loadRole(); old
                        fragment.loadFirstRole();
                    }
                });
    }



    public List<PackageListReguler> gePackageListRegulers(List<PackageListReguler> listRegulers){
        List<PackageListReguler> packageListRegulers = new ArrayList<>();
        for(int i = 0; i < listRegulers.size(); i++){
            PackageListReguler packageListReguler = new PackageListReguler();
            packageListReguler.setId(listRegulers.get(i).getId());
            packageListReguler.setFeePercentage(listRegulers.get(i).getFeePercentage());
            packageListReguler.setFeePrice(listRegulers.get(i).getFeePrice());
            packageListReguler.setTransactionAmount(listRegulers.get(i).getTransactionAmount());
            packageListReguler.setTotal(listRegulers.get(i).getTotal());
            packageListReguler.setCode(listRegulers.get(i).getCode());
            packageListReguler.setName(listRegulers.get(i).getName());
            packageListReguler.setTimePeriod(listRegulers.get(i).getTimePeriod());
            packageListReguler.setMinSubscription(listRegulers.get(i).getMinSubscription());
            packageListReguler.setAccept(listRegulers.get(i).isAccept());
            packageListRegulers.add(packageListReguler);
        }
        return packageListRegulers;
    }





}
