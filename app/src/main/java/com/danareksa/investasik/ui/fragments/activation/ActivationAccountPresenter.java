package com.danareksa.investasik.ui.fragments.activation;

import android.widget.Toast;

import com.danareksa.investasik.data.api.requests.ActivationCodeRequest;
import com.danareksa.investasik.data.api.requests.LoginRequest;
import com.danareksa.investasik.data.api.responses.ActivationCodeResponse;
import com.danareksa.investasik.data.api.responses.GenericResponse;
import com.danareksa.investasik.data.api.responses.LoginResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.Crypto;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by fajarfatur on 2/3/16.
 */
public class ActivationAccountPresenter {

    private ActivationAccountFragment fragment;

    public ActivationAccountPresenter(ActivationAccountFragment fragment){
        this.fragment = fragment;
    }

    ActivationCodeRequest cunstructActivateUserRequest() {
        ActivationCodeRequest request = new ActivationCodeRequest();
        request.setUsername(fragment.username);
        request.setActivationCode(fragment.etActivationCode.getText().toString());
        return request;
    }


    void activate(final ActivationCodeRequest request){
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().activateAccountWithActivationCode(request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ActivationCodeResponse>(){
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(ActivationCodeResponse activationCodeResponse) {
                        fragment.dismissProgressDialog();

                        if(activationCodeResponse.getCode() == fragment.activicationSuccessCode){

                            Toast.makeText(fragment.getContext(), activationCodeResponse.getInfo(), Toast.LENGTH_SHORT).show();
                            if(activationCodeResponse.getData().getUserStatus().equalsIgnoreCase(Constant.USER_STATUS_ACTIVE)){
                                saveCredential(activationCodeResponse);
                                fragment.dialogConfirmation();
                            }else{
                                saveCredential(activationCodeResponse);
                                fragment.gotoMainActivity();
                            }

                        }else if(activationCodeResponse.getCode() == 0){

                            if(activationCodeResponse.getData().getUserStatus().equalsIgnoreCase(Constant.USER_STATUS_ACTIVE)){
                                saveCredential(activationCodeResponse);
                                fragment.dialogConfirmation();
                            }else{
                                saveCredential(activationCodeResponse);
                                fragment.gotoMainActivity();
                            }

                        }else if(activationCodeResponse.getCode() == 50){
                            fragment.showDialog("Kode Aktivasi tidak valid, Lihat kembali kode Aktivasi di email Anda atau klik Kirim kode ulang untuk mendapatkan kode Aktivasi baru");
                        }else{
                            if(activationCodeResponse.getInfo().contains("Ambiguous method overloading for method java.lang.String")) {
                                login(constructLoginRequest());
                            }else{
                                fragment.showDialog(activationCodeResponse.getInfo());
                            }
                        }
                    }
                });
    }




    private void saveCredential(ActivationCodeResponse activationCodeResponse) {
        /*PrefHelper.setInt(PrefKey.ID, loginResponse.getUser().getId());*/
        /*PrefHelper.setInt(PrefKey.KYC_ID, loginResponse.getKyc().getId());*/
        /* PrefHelper.setInt(PrefKey.KYC_ID, loginResponse.getData().getKyc());*/
        /*PrefHelper.setInt(PrefKey.KYC_ID, loginResponse.getKyc().getId());*/
        String name = activationCodeResponse.getData().getFirstName() + " " + activationCodeResponse.getData().getMiddleName() + " " + activationCodeResponse.getData().getLastName();
        name = name.replace("  ", " ");
        PrefHelper.setString(PrefKey.FIRST_NAME, name);
        PrefHelper.setString(PrefKey.EMAIL, activationCodeResponse.getData().getEmail());
        PrefHelper.setString(PrefKey.CUSTOMER_STATUS, activationCodeResponse.getData().getUserStatus());
        PrefHelper.setString(PrefKey.KYC_ID, activationCodeResponse.getData().getFirstName());
        PrefHelper.setString(PrefKey.TOKEN, activationCodeResponse.getData().getToken());
    }


    void resend(String email) {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().resendActivationCode(email)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(GenericResponse genericResponse) {
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), genericResponse.getInfo(), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    LoginRequest constructLoginRequest() {
        LoginRequest request = new LoginRequest();
        request.setPassword(Crypto.Encrypt(fragment.password));
        request.setUsername(fragment.username);
        return request;
    }


    void login(final LoginRequest loginRequest) {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().loginUser(loginRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<LoginResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();
                        fragment.dismissProgressDialog();
                    }

                    @Override
                    public void onNext(LoginResponse loginResponse) {
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), loginResponse.getInfo(), Toast.LENGTH_SHORT).show();
                        if (loginResponse.getCode() == fragment.successCode) {
                            if (loginResponse.getUser().getUserStatus().equalsIgnoreCase(Constant.USER_STATUS_REGISTER)) {
                                //fragment.gotoActivationCodeActivity(loginRequest.getEmail());
                            } else if (loginResponse.getUser().getUserStatus().equalsIgnoreCase(Constant.USER_STATUS_ACTIVE)) {
                                saveCredential(loginResponse);
                                fragment.showUserProfileSuggestionDialog();
                            } else {
                                saveCredential(loginResponse);
                                fragment.gotoMainActivity();
                            }
                        }
                    }
                });
    }



    private void saveCredential(LoginResponse loginResponse){
        /*PrefHelper.setInt(PrefKey.ID, loginResponse.getUser().getId());*/
        /*PrefHelper.setInt(PrefKey.KYC_ID, loginResponse.getKyc().getId());*/
        /*PrefHelper.setInt(PrefKey.KYC_ID, loginResponse.getData().getKyc());*/
        PrefHelper.setInt(PrefKey.KYC_ID, loginResponse.getKyc().getId());
        PrefHelper.setString(PrefKey.EMAIL, loginResponse.getData().getEmail());
        PrefHelper.setString(PrefKey.CUSTOMER_STATUS, loginResponse.getData().getUserStatus());
        PrefHelper.setString(PrefKey.TOKEN, loginResponse.getData().getToken());
    }



}
