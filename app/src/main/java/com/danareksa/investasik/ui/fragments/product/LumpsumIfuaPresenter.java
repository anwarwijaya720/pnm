package com.danareksa.investasik.ui.fragments.product;

import android.graphics.Color;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.InvestmentAccountInfo;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.ProductList;
import com.danareksa.investasik.data.api.responses.GenericResponse;
import com.danareksa.investasik.data.api.responses.MaxScoreResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.CartActivity;
import com.danareksa.investasik.ui.activities.CatalogueActivity;
import com.danareksa.investasik.ui.activities.ChooseIfuaActivity;
import com.danareksa.investasik.ui.activities.ListOfCatalogueActivity;
import com.danareksa.investasik.ui.activities.UserProfileActivity;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by asep.surahman on 23/06/2018.
 */

public class LumpsumIfuaPresenter {

    private LumspumIfuaFragment fragment;
    private InvestmentAccountInfo accountInfo;

    public LumpsumIfuaPresenter(LumspumIfuaFragment fragment) {
        this.fragment = fragment;
    }

    void getMaxScore(final ProductList packages, InvestmentAccountInfo info) {
        this.accountInfo = info;
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().getMaxScore(packages.getId(), PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<MaxScoreResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                    }

                    @Override
                    public void onNext(MaxScoreResponse response) {
                        fragment.dismissProgressDialog();
                        if (response.getCode() == 1) {
                            if(response.getData().getMaxScoreKyc() < response.getData().getMaxScoreFundPackage()) {
                                new MaterialDialog.Builder(fragment.getActivity())
                                        .iconRes(R.mipmap.ic_launcher)
                                        .backgroundColor(Color.WHITE)
                                        .title("KONFIRMASI")
                                        .titleColor(Color.BLACK)
                                        .content(R.string.catalogue_dialog_risk_profile)
                                        .contentColor(Color.GRAY)
                                        .positiveText(R.string.yes)
                                        .positiveColor(Color.GRAY)
                                        .negativeText(R.string.no)
                                        .negativeColor(fragment.getResources().getColor(R.color.colorPrimary))
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                                if (PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equalsIgnoreCase("ACT")) {
                                                    new MaterialDialog.Builder(fragment.getActivity())
                                                            .iconRes(R.mipmap.ic_launcher)
                                                            .backgroundColor(Color.WHITE)
                                                            .title(fragment.getString(R.string.success).toUpperCase())
                                                            .titleColor(Color.BLACK)
                                                            .content(R.string.catalogue_act_dialog)
                                                            .contentColor(Color.GRAY)
                                                            .positiveText(R.string.yes)
                                                            .positiveColor(Color.GRAY)
                                                            .negativeText(R.string.no)
                                                            .negativeColor(fragment.getResources().getColor(R.color.colorPrimary))
                                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                                @Override
                                                                public void onClick(MaterialDialog dialog, DialogAction which) {
                                                                    UserProfileActivity.startActivity((BaseActivity) fragment.getActivity());
                                                                }
                                                            })
                                                            .cancelable(false)
                                                            .show();

                                                } else {
                                                    subscribeToCart(fragment.packages);
                                                }

                                            }
                                        })
                                        .cancelable(false)
                                        .show();
                            } else {
                                if (PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equalsIgnoreCase("ACT")) {
                                    new MaterialDialog.Builder(fragment.getActivity())
                                            .iconRes(R.mipmap.ic_launcher)
                                            .backgroundColor(Color.WHITE)
                                            .title(fragment.getString(R.string.success).toUpperCase())
                                            .titleColor(Color.BLACK)
                                            .content(R.string.catalogue_act_dialog)
                                            .contentColor(Color.GRAY)
                                            .positiveText(R.string.yes)
                                            .positiveColor(Color.GRAY)
                                            .negativeText(R.string.no)
                                            .negativeColor(fragment.getResources().getColor(R.color.colorPrimary))
                                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                                @Override
                                                public void onClick(MaterialDialog dialog, DialogAction which) {
                                                    UserProfileActivity.startActivity((BaseActivity) fragment.getActivity());

                                                }
                                            })
                                            .cancelable(false)
                                            .show();

                                } else {
                                    subscribeToCart(fragment.packages);
                                }

                            }

                        }

                    }
                });
    }


    void subscribeToCart(final Packages packages) {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().createTrxCart(this.accountInfo.getIfua(), packages.getPackageCode(), PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();

                        new MaterialDialog.Builder(fragment.getActivity())
                                .iconRes(R.mipmap.ic_launcher)
                                .backgroundColor(Color.WHITE)
                                .title(fragment.getString(R.string.failed).toUpperCase())
                                .titleColor(Color.BLACK)
                                .content(fragment.connectionError)
                                .contentColor(Color.GRAY)
                                .positiveText(R.string.ok)
                                .positiveColor(Color.GRAY)
                                .cancelable(false)
                                .show();
                    }

                    @Override
                    public void onNext(GenericResponse response) {
                        fragment.dismissProgressDialog();

                        if (response.getCode() == 1) {
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.failed).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content(response.getInfo())
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.GRAY)
                                    .cancelable(false)
                                    .show();
                        } if (response.getCode() == 13) {
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.failed).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    //.content(response.getInfo())
                                    .content("Gagal menambahkan ke dalam keranjang, produk ini sudah ada di dalam keranjang Anda")
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.GRAY)
                                    .cancelable(false)
                                    .show();
                        } else if(response.getCode() == 50){
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.infortmation).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content(response.getInfo())
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.GRAY)
                                    .cancelable(false)
                                    .show();

                        } else if(response.getCode() == 0){

                            ((BaseActivity) fragment.getActivity()).addNotifCount();
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.success).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content(R.string.catalogue_confirm_subscription)
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.GRAY)
                                    .negativeText(R.string.catalogue_view_packages)
                                    .negativeColor(fragment.getResources().getColor(R.color.colorPrimary))
                                    .onPositive(new MaterialDialog.SingleButtonCallback(){
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            CartActivity.startActivity((BaseActivity) fragment.getActivity());
                                            ChooseIfuaActivity.choIfuaActivity.finish(); //finish choose ifua
                                            ListOfCatalogueActivity.faCatalogue.finish(); //finish list catalogue
                                            CatalogueActivity.catalogueActivity.finish(); //finish catalog
                                        }
                                    })
                                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            ListOfCatalogueActivity.startActivity((BaseActivity) fragment.getActivity());
                                            ChooseIfuaActivity.choIfuaActivity.finish(); //finish choose ifua
                                            CatalogueActivity.catalogueActivity.finish(); //finish catalog
                                        }
                                    })
                                    .cancelable(false)
                                    .show();
                        }

                    }
                });

    }


}
