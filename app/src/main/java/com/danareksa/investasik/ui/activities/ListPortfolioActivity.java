package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.fragments.portfolio.ListPortfolioFragment;

import butterknife.Bind;
import butterknife.BindString;

/**
 * Created by fajarfatur on 1/12/16.
 */
public class ListPortfolioActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;
    @BindString(R.string.portofolioTitle)
    String portofolioTitle;


    public static void startActivity(BaseActivity sourceActivity, Integer categoryId) {
        Intent intent = new Intent(sourceActivity, ListPortfolioActivity.class);
        intent.putExtra("CAT_ID", categoryId);
        sourceActivity.startActivity(intent);
    }




    @Override
    protected int getLayout() {
        return R.layout.a_signup;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar();
        Integer catId = getIntent().getIntExtra("CAT_ID",0);
        ListPortfolioFragment.showFragment(this, catId);
    }

    public void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        title.setText(portofolioTitle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }
}
