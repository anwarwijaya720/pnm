package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.SecurityQuestion;
import com.danareksa.investasik.data.api.requests.AndroidVersionRequest;
import com.danareksa.investasik.data.api.responses.AndroidVersionResponse;
import com.danareksa.investasik.data.realm.RealmHelper;
import com.danareksa.investasik.data.realm.model.SecurityQuestionModel;

import java.util.List;

import io.realm.RealmResults;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by fajarfatur on 2/1/16.
 */
public class SplashScreenPresenter {

    private SplashScreenActivity activity;

    public SplashScreenPresenter(SplashScreenActivity activity) {
        this.activity = activity;
    }

    void checkSecurityQuestionOnRealm() {
        RealmHelper.readSecurityQuestions(activity.getRealm())
                .subscribe(new Action1<RealmResults<SecurityQuestionModel>>() {
                    @Override
                    public void call(RealmResults<SecurityQuestionModel> securityQuestionModels) {
                        int size = securityQuestionModels.size();
                        Timber.i("Security Question Size %d", size);
                        if (size > 0) {
                            activity.startApplication();
                        } else {
                            getSecurityQuestion();
                        }
                    }
                });
    }

    public void getSecurityQuestion() {
        activity.showProgressBar();
        activity.getInviseeService().getApi().loadSecurityQuestion()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<SecurityQuestion>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        activity.llRetry.setVisibility(View.VISIBLE);
                        //activity.stopLoadingAnimation();
                        activity.hideProgressBar();
                    }

                    @Override
                    public void onNext(List<SecurityQuestion> securityQuestions) {
                        activity.hideProgressBar();
                        RealmHelper.createSecurityQuestionList(activity.getRealm(), securityQuestions);
                        activity.startApplication();
                    }
                });

    }

    AndroidVersionRequest constructandroidversionrequest() {
        AndroidVersionRequest androidVersionRequest = new AndroidVersionRequest();
        androidVersionRequest.setIdDevice(1);
        return androidVersionRequest;
    }

    public void checkVersion() {
        activity.showProgressBar();
        activity.getInviseeService().getApi().checkVersion(constructandroidversionrequest())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<AndroidVersionResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        activity.llRetry.setVisibility(View.VISIBLE);
                        //activity.stopLoadingAnimation();
                        activity.hideProgressBar();
                    }

                    @Override
                    public void onNext(AndroidVersionResponse response) {
                        activity.hideProgressBar();
                        if (response.getCode() == 0) {
                            Integer coreCode = response.getData().getCore().intValue();
                            if(BuildConfig.VERSION_CODE < coreCode) {
                                new MaterialDialog.Builder(activity)
                                        .iconRes(R.mipmap.ic_launcher)
                                        .backgroundColor(Color.WHITE)
                                        .title(activity.getString(R.string.update).toUpperCase())
                                        .titleColor(Color.BLACK)
                                        .content(activity.getString(R.string.android_version))
                                        .contentColor(Color.GRAY)
                                        .positiveText(R.string.yes)
                                        .positiveColor(Color.GRAY)
                                        .negativeText(R.string.no)
                                        .negativeColor(activity.getResources().getColor(R.color.colorPrimary))
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                                Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.danareksa.investasik"));
                                                activity.startActivity(webIntent);
                                            }
                                        })
                                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                                Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                                                homeIntent.addCategory( Intent.CATEGORY_HOME );
                                                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                activity.startActivity(homeIntent);
                                            }
                                        })
                                        .show();
                            } else {
                                checkSecurityQuestionOnRealm();
                            }
                        } else {
                                checkSecurityQuestionOnRealm();
                        }

                    }
                });

    }


}
