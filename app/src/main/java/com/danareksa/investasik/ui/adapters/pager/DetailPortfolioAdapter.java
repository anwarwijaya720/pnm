package com.danareksa.investasik.ui.adapters.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.PortfolioInvestment;
import com.danareksa.investasik.ui.fragments.portfolio.PortfolioDetailFragment;
import com.danareksa.investasik.ui.fragments.portfolio.PortfolioInvesmentPerformanceFragment;

/**
 * Created by fajarfatur on 3/2/16.
 */
public class DetailPortfolioAdapter extends FragmentPagerAdapter {

    private final int PAGE_COUNT = 2;
    private static final String[] SUBMENU_TITLE = {"DETAIL",  "PERFORMA INVESTASI"};
    private PortfolioInvestment investment;
    private Packages packages;
    private String ifuaId;

    public DetailPortfolioAdapter(FragmentManager fm, PortfolioInvestment investment, Packages packages, String ifuaId) {
        super(fm);
        this.investment = investment;
        this.packages = packages;
        this.ifuaId = ifuaId;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position){
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = PortfolioDetailFragment.initiateFragment(investment, ifuaId);
                break;
            /*case 1:
                fragment = PortfolioFundAlocationFragment.initiateFragment(investment, packages);
                break;*/
            case 1:
                fragment = PortfolioInvesmentPerformanceFragment.initiateFragment(investment);
                break;
        }
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return SUBMENU_TITLE[position];
    }
}

