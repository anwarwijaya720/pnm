package com.danareksa.investasik.ui.fragments.signIn;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.widget.EditText;

import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.SignInActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by pandu.abbiyuarsyah on 13/07/2017.
 */

public class ForgotPasswordTempFragment extends BaseFragment {

    public static final String TAG = ForgotPasswordTempFragment.class.getSimpleName();

    @Email
    @NotEmpty(messageResId = R.string.rules_no_empty_email)
    @Bind(R.id.etEmail)
    EditText etEmail;

    ForgotPasswordTempPresenter presenter;

    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            fragmentTransaction.add(R.id.container, new ForgotPasswordTempFragment(), TAG);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.f_forgot_password_temp;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ForgotPasswordTempPresenter(this);
    }

    @OnClick(R.id.bSubmit)
    void submitForgotPassword(){
        getValidator().validate();
    }

    @Override
    public void onValidationSucceeded() {
        super.onValidationSucceeded();
        presenter.urgentForgotPass(presenter.constructForgotPasswordRequest());
    }

}
