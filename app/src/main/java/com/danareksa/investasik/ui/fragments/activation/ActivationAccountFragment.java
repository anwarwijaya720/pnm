package com.danareksa.investasik.ui.fragments.activation;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.MainActivity;
import com.danareksa.investasik.ui.activities.RegisterDataPribadiActivity;
import com.danareksa.investasik.ui.activities.UserProfileActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.Constant;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.OnClick;
import icepick.State;


/**
 * Created by fajarfatur on 2/3/16.
 */


public class ActivationAccountFragment extends BaseFragment implements Validator.ValidationListener{

    public static final String TAG = ActivationAccountFragment.class.getSimpleName();
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Length(max = 6, message = "Maximal 6 Karakter")
    @Bind(R.id.etActivationCode)
    EditText etActivationCode;

    @Bind(R.id.tvClue)
    TextView tvClue;

    @BindString(R.string.activation_clue)
    String activationClue;

    @State
    String username;

    @State
    String password;

    @State
    String email;

    Validator validator;
    Dialog dialogKonfimasi;
    LayoutInflater inflater;

    Button btnLanjut; Button btnNanti;

    private ActivationAccountPresenter presenter;

    public static void showFragment(BaseActivity sourceActivity, String username, String password, String email) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            Bundle bundle = new Bundle();
            bundle.putString(USERNAME, username);
            bundle.putString(PASSWORD, password);
            bundle.putString(EMAIL, email);
            ActivationAccountFragment fragment = new ActivationAccountFragment();
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }


    @Override
    protected int getLayout() {
        return R.layout.f_register_nasabah_aktifasi_akun;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        validator = new Validator(this);
        validator.setValidationListener(this);
        presenter = new ActivationAccountPresenter(this);
        username = getArguments().getString(USERNAME);
        password = getArguments().getString(PASSWORD);
        email = getArguments().getString(EMAIL);
        if (username == null) getActivity().onBackPressed();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String clue = String.format(activationClue, email);
        tvClue.setText(clue);
        if(!BuildConfig.FLAVOR.contentEquals("pnm")) {
            tvClue.setTextColor(Color.WHITE);
        }
        etActivationCode.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(6)});
    }

    @OnClick(R.id.bAktivasi)
    void bAktivasi() {
        validator.validate();
    }

    @OnClick(R.id.bResend)
    void resend() {
        presenter.resend(email);
    }

    public void showUserProfileSuggestionDialog() {
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.welcome).toUpperCase())
                .titleColor(Color.BLACK)
                .content(R.string.user_profile_suggestion)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .negativeText(R.string.later)
                .negativeColor(getResources().getColor(R.color.colorPrimary))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        UserProfileActivity.startActivity((BaseActivity) getActivity());
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        gotoMainActivity();
                    }
                })
                .cancelable(false)
                .show();
    }


    public void gotoMainActivity() {
        MainActivity.startActivity((BaseActivity) getActivity());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this.getContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this.getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public void onValidationSucceeded(){
        presenter.activate(presenter.cunstructActivateUserRequest());
    }


    public void dialogConfirmation(){
        dialogKonfimasi = new Dialog(getActivity());
        View view  = inflater.inflate(R.layout.popup_konfirmasi_registrasi, null);
        btnLanjut  = (Button) view.findViewById(R.id.btnLanjut);
        btnNanti   = (Button) view.findViewById(R.id.btnNanti);
        dialogKonfimasi.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogKonfimasi.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#50000000")));
        dialogKonfimasi.setContentView(view);
        dialogKonfimasi.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialogKonfimasi.show();
        btnLanjut.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                dialogKonfimasi.dismiss();
                getActivity().finish();
                RegisterDataPribadiActivity.startActivity((BaseActivity) getActivity(), Constant.SOURCE_ROUTE_REG);
            }
        });
        btnNanti.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                dialogKonfimasi.dismiss();
                getActivity().finish();
                gotoMainActivity();
            }
        });
    }



    void showDialog(String info){
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.infortmation).toUpperCase())
                .titleColor(Color.BLACK)
                .content(info)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }



}
