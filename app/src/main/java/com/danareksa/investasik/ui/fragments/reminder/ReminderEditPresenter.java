package com.danareksa.investasik.ui.fragments.reminder;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.requests.InvesmentNumberRequest;
import com.danareksa.investasik.data.api.requests.ReminderEditRequest;
import com.danareksa.investasik.data.api.requests.SaveReminderRequest;
import com.danareksa.investasik.data.api.responses.GenericResponse;
import com.danareksa.investasik.data.api.responses.InvestmentAccountResponse;
import com.danareksa.investasik.data.api.responses.PackageByTokenResponse;
import com.danareksa.investasik.data.api.responses.ReminderDetailResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by pandu.abbiyuarsyah on 08/06/2017.
 */

public class ReminderEditPresenter {

    private ReminderEditFragment fragment;

    public ReminderEditPresenter (ReminderEditFragment fragment) {
        this.fragment = fragment;
    }

    ReminderEditRequest constructreminderedit() {
        ReminderEditRequest reminderEditRequest = new ReminderEditRequest();
        reminderEditRequest.setId(String.valueOf(fragment.reminder.getInvestmentAccountId()));
        reminderEditRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));

        return reminderEditRequest;
    }

    void getReminderDetail(final ReminderEditRequest request, String id) {
        //fragment.getApi().getReminderDetail(request.getId(), request.getToken())
        fragment.getApi().getReminderDetail(id, request.getToken())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ReminderDetailResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.dismissProgressDialog();
                    }

                    @Override
                    public void onNext(ReminderDetailResponse response) {
                        if (response.getCode() == 0) {
                            fragment.reminderDetail = response.getReminder();
                            fragment.loadDetail();
                        }
                    }
                });
    }

    void getPackageNameByToken(final String id) {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().viewPackageName(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PackageByTokenResponse>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getMessage());
                        fragment.dismissProgressDialog();
                        fragment.showFailedDialog(fragment.connectionError);
                    }

                    @Override
                    public void onNext(PackageByTokenResponse response) {
                        fragment.dismissProgressDialog();
                        if (response.getCode() == 1) {
                            fragment.packageList = response.getData();
                            getReminderDetail(constructreminderedit(), id);
                        } else {
                            fragment.showFailedDialog(response.getInfo());
                        }
                    }
                });
    }

    InvesmentNumberRequest constructInvesmentNumbber() {
        InvesmentNumberRequest invesmentNumberRequest = new InvesmentNumberRequest();
        invesmentNumberRequest.setFundPackage(String.valueOf(fragment.reminderDetail.getFundPackageRefId()));
        invesmentNumberRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));

        return invesmentNumberRequest;
    }

    void getInvestmentNumberByPackage(final InvesmentNumberRequest request) {
        fragment.getApi().viewInvestementAccount(request.getFundPackage(), request.getToken())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<InvestmentAccountResponse>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getMessage());
                        fragment.dismissProgressDialog();
                        fragment.showFailedDialog(fragment.connectionError);
                    }

                    @Override
                    public void onNext(InvestmentAccountResponse response) {
                        if (response.getCode() == 1) {
                            fragment.investmentAccountList = response.getData();
                        /*    for (int i = 0; i < response.getData().size(); i++) {*/
                                /*fragment.setSpinnerInvestmentNo(response.getData(), fragment.reminderDetail.getFundPackageRefId().intValue());*/
                                /*fragment.setSpinnerInvestmentNo();*/



                         /*   getReminderDetail(constructreminderedit());*/
                        } else {
                            fragment.showFailedDialog(response.getInfo());
                        }
                        fragment.dismissProgressDialog();
                    }
                });
    }

    void getInvestmentNumberByPackage(String fundPackageId) {
        fragment.getApi().viewInvestementAccountrEeminder(PrefHelper.getString(PrefKey.TOKEN), fundPackageId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<InvestmentAccountResponse>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getMessage());
                        fragment.dismissProgressDialog();
                        fragment.showFailedDialog(fragment.connectionError);
                    }

                    @Override
                    public void onNext(InvestmentAccountResponse response) {

                        if (response.getCode() == 1) {
                            fragment.investmentAccountList = response.getData();
                            //fragment.setSpinnerInvestmentNo();
                        } else {
                            fragment.showFailedDialog(response.getInfo());
                        }
                    }
                });
    }

    void saveOrUpdate(SaveReminderRequest request) {
        fragment.showProgressDialog(fragment.getString(R.string.please_wait));
        fragment.getApi().saveOrUpdateReminder(PrefHelper.getString(PrefKey.TOKEN), request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getMessage());
                        fragment.dismissProgressDialog();
                        fragment.showFailedDialog(fragment.connectionError);
                    }

                    @Override
                    public void onNext(GenericResponse response) {
                        fragment.dismissProgressDialog();

                        if (response.getCode() == 1) {
                            fragment.showSuccessDialog(response.getInfo(), true);
                        } else {
                            fragment.showFailedDialog(response.getInfo());
                        }
                    }
                });
    }






}
