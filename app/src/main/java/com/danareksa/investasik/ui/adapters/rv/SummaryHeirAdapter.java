package com.danareksa.investasik.ui.adapters.rv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.HeirList;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by asep.surahman on 08/08/2018.
 */

public class SummaryHeirAdapter extends RecyclerView.Adapter<SummaryHeirAdapter.SummaryHeirHolder>{


    List<HeirList> heirList;
    private HeirList itemHeir;
    private Context context;
    private List<SummaryHeirAdapter.SummaryHeirHolder> holderList;



    public SummaryHeirAdapter(Context context, List<HeirList> heirList) {
        this.context = context;
        this.heirList = heirList;
        this.holderList = new ArrayList<>();
    }


    @Override
    public SummaryHeirAdapter.SummaryHeirHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_summary_heir, parent, false);
        return new SummaryHeirAdapter.SummaryHeirHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final SummaryHeirAdapter.SummaryHeirHolder holder, final int position){
        itemHeir = heirList.get(position);
        holder.itemView.setTag(itemHeir);
        holder.tvHeirName.setText(itemHeir.getHeirName());
        if(itemHeir.getHeirRelationship().equalsIgnoreCase("others")){
            holder.tvRelationship.setText(itemHeir.getHeirRelationshipDetail());
        }else{
            holder.tvRelationship.setText(itemHeir.getHeirRelationship());
        }
        holder.tvNumberPhone.setText(itemHeir.getHeirMobileNumber());
        holderList.add(holder);
    }


    @Override
    public int getItemCount() {
        return heirList != null ? heirList.size() : 0;
    }


    public static class SummaryHeirHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tvHeirName)
        TextView tvHeirName;
        @Bind(R.id.tvRelationship)
        TextView tvRelationship;
        @Bind(R.id.tvNumberPhone)
        TextView tvNumberPhone;

        public SummaryHeirHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
