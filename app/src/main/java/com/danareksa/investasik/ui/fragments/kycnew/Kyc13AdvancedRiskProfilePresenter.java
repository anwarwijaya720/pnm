package com.danareksa.investasik.ui.fragments.kycnew;

import android.widget.Toast;

import com.danareksa.investasik.data.api.beans.RiskProfileQuestion;
import com.danareksa.investasik.data.api.requests.RiskProfileRequest;
import com.danareksa.investasik.data.api.responses.RiskProfileNewResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import java.util.ArrayList;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by asep.surahman on 12/07/2018.
 */

public class Kyc13AdvancedRiskProfilePresenter {


    private Kyc13AdvancedRiskProfileFragment fragment;

    public Kyc13AdvancedRiskProfilePresenter(Kyc13AdvancedRiskProfileFragment fragment){
        this.fragment = fragment;
    }

    void loadRiskProfileQuestionAndAnswer(){
        fragment.showProgressBar();
        fragment.getApi().loadRiskProfileQuestionAndAnswer(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ArrayList<RiskProfileQuestion>>() {
                    @Override
                    public void onCompleted(){
                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.dismissProgressBar();
                        fragment.connectionError();
                        Timber.e(e.getMessage());
                    }

                    @Override
                    public void onNext(ArrayList<RiskProfileQuestion> riskProfileQuestions) {
                        fragment.dismissProgressBar();
                        fragment.riskProfileQuestionList = riskProfileQuestions;
                        fragment.status = true;
                        fragment.initDataRiskProfile();
                    }
                });
    }


    void getScoreRiskProfileLevel(RiskProfileRequest riskProfileRequest){
        fragment.showProgressBar();
        fragment.getApi().riskProfileSimulation(riskProfileRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<RiskProfileNewResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.dismissProgressBar();
                        fragment.connectionError();
                        Timber.e(e.getMessage());
                    }

                    @Override
                    public void onNext(RiskProfileNewResponse riskProfileNewResponse) {
                        fragment.dismissProgressBar();
                        fragment.connectionErrorGone();
                        fragment.riskProfileNewResponse = riskProfileNewResponse;
                        if(riskProfileNewResponse.getCode() == 0){
                            fragment.fetchResultToLayout();
                        }else if(riskProfileNewResponse.getCode() == 1){
                            Toast.makeText(fragment.getContext(), riskProfileNewResponse.getInfo(), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }






}
