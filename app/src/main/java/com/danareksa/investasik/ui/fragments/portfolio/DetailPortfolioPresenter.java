package com.danareksa.investasik.ui.fragments.portfolio;

import android.graphics.Color;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.PortfolioInvestment;
import com.danareksa.investasik.data.api.responses.CartListResponse;
import com.danareksa.investasik.data.api.responses.CheckRedemptionResponse;
import com.danareksa.investasik.data.api.responses.FundAllocationResponse;
import com.danareksa.investasik.data.api.responses.GenericResponse;
import com.danareksa.investasik.data.api.responses.PackageResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by fajarfatur on 3/1/16.
 */
public class DetailPortfolioPresenter {

    private DetailPortfolioFragment fragment;

    public DetailPortfolioPresenter(DetailPortfolioFragment fragment) {
        this.fragment = fragment;
    }


    void packageDetail(final PortfolioInvestment investment) {
        fragment.showProgressBar();
        fragment.getApi().packageDetail(investment.getPackageId(), PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PackageResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                        fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(PackageResponse response) {
                        fragment.dismissProgressBar();
                        if (response.getCode() == 1) {
                            fragment.packages = response.getData();
                            fragment.packages.setId(investment.getPackageId());
                            fragment.setupViewPager();
                            //fragment.setupTabLayout();
                            fundAllocation(fragment.packages);
                        } else {
                            fragment.dismissProgressBar();
                        }

                    }
                });

    }


    void fundAllocation(Packages packages) {
        fragment.getApi().fundAllocationInfo(packages.getId(), PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<FundAllocationResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e){
                      fragment.connectionError();
                    }

                    @Override
                    public void onNext(FundAllocationResponse response) {

                        if (response.getCode() == 1) {
                            fragment.fundAlloc = response;
                            for (int i= 0; i < response.getData().size(); i++) {
                                System.out.println("product type : " + response.getData().get(i).getProductType() + " pros : " + response.getData().get(i).getProspectusKey() + " ffs : " + response.getData().get(i).getFundFactSheetKey());
                                if (response.getData().get(i).getProductType().equals("Reksa Dana Terproteksi")) {
                                    fragment.lnTopUp.setVisibility(View.GONE);
                                } else {
                                    fragment.lnTopUp.setVisibility(View.VISIBLE);
                                }
                            }

                            fragment.validationProductReguler();
                            fragment.dismissProgressBar();
                        } else {
                            fragment.dismissProgressBar();
                        }


                    }
                });

    }

    void checkRedemptionTransaction(String investmentAccountNo){
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().checkRedemptionTransaction(investmentAccountNo, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<CheckRedemptionResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(CheckRedemptionResponse checkRedemptionResponse) {
                        fragment.dismissProgressDialog();
                        if (checkRedemptionResponse.getCode() == 1) {
                            fragment.statusSpecialFee = checkRedemptionResponse.isStatusSpecialFee();
                            fragment.specialFee = checkRedemptionResponse.getSpecialFee();
                            fragment.totalDays = checkRedemptionResponse.getTotalDays();
                            fragment.redemptionGranted();
                        } else if (checkRedemptionResponse.getCode() == 0 && checkRedemptionResponse.isStatus()){
                            fragment.showConfirmation(checkRedemptionResponse.getInfo());
                        } else {
                            fragment.showConfirmation(checkRedemptionResponse.getInfo());
                        }
                    }
                });
    }


    void topUpValidation(String investmentAccountNo) {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().topUpValidation(investmentAccountNo, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>(){
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(GenericResponse response){
                        fragment.dismissProgressDialog();
                        if(response.getCode() == 0){
                            checkTrxCartByInvestmentNumber(fragment.investment.getInvestmentAccountNumber());
                        }else if(response.getCode() == 50){

                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(R.string.redeemtion_failed_title)
                                    .titleColor(Color.BLACK)
                                    .content(response.getInfo())
                                    .contentGravity(GravityEnum.CENTER)
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.redeemtion_failed_tutup)
                                    .positiveColor(Color.GRAY)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .show();

                        }else{

                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(R.string.redeemtion_failed_title)
                                    .titleColor(Color.BLACK)
                                    .content(response.getInfo())
                                    .contentGravity(GravityEnum.CENTER)
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.redeemtion_failed_tutup)
                                    .positiveColor(Color.GRAY)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .show();
                        }
                    }
                });
    }

    void checkTrxCartByInvestmentNumber(String investmentAccountNo) {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().checkTrxCartByInvestmentNumber(investmentAccountNo, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(GenericResponse response) {
                        fragment.dismissProgressDialog();
                        if (response.getCode() == 0) {
                            fragment.portfolioConfirmation();
                        } else if (response.getCode() == 1) {

                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.failed).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content("Investasi ini sudah dimasukkan ke keranjang belanja")
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.redeemtion_failed_tutup)
                                    .positiveColor(Color.GRAY)
                                    .cancelable(false)
                                    .show();
                        }
                    }
                });
    }

    public void cartList() {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().getCartList(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<CartListResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                    }

                    @Override
                    public void onNext(List<CartListResponse> response) {
                        fragment.dismissProgressDialog();
                        ((BaseActivity) fragment.getActivity()).setNotifCount(0);

                        if (response != null && response.size() > 0) {
                            ((BaseActivity) fragment.getActivity()).setNotifCount(response.size());

                            for (int i = 0; i < response.size(); i++) {

                            }

                        } else {

                        }
                    }
                });

    }


}
