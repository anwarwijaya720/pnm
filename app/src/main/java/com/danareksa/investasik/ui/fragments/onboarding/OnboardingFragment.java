package com.danareksa.investasik.ui.fragments.onboarding;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.util.StringUtility;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;

/**
 * Created by asep.surahman on 27/03/2018.
 */

public class OnboardingFragment extends Fragment {

    private static final String KEY_TITLE = "GuideFragment:Title";
    private static final String KEY_DESC = "GuideFragment:Description";
    private static final String KEY_IMAGE = "GuideFragment:Image";
    private static final String KEY_COLOR = "GuideFragment:Color";
    private String mTitle, mDescription;
    private int mImage;
    private int mColor;
    static Context context;


    public static OnboardingFragment newInstance(String title, String description, int image, int color, Context context_) {
        OnboardingFragment fragment = new OnboardingFragment();
        fragment.mTitle = title;
        fragment.mImage = image;
        fragment.mColor = color;
        fragment.mDescription = description;
        context = context_;
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(context);
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_TITLE)) {
            mTitle = savedInstanceState.getString(KEY_TITLE);
            mImage = savedInstanceState.getInt(KEY_IMAGE);
            mColor = savedInstanceState.getInt(KEY_COLOR);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        if(container == null){
            return null;
        }

        View view = inflater.inflate(R.layout.f_onboarding, container, false);
        LinearLayout lnrContainer = (LinearLayout) view.findViewById(R.id.container);
        final SimpleDraweeView imvGuide = (SimpleDraweeView) view.findViewById(R.id.sdv_onboarding);
        TextView title       = (TextView) view.findViewById(R.id.tv_title);
        TextView description = (TextView) view.findViewById(R.id.tv_description);
        title.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeueLts.otf"), Typeface.BOLD);
        description.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/HelveticaNeueLts.otf"));

        if(StringUtility.isNotNull(mTitle)){
            title.setText(mTitle);
            title.setVisibility(View.VISIBLE);
        }else{
            title.setVisibility(View.GONE);
        }

        if (StringUtility.isNotNull(mDescription)){
            description.setText(mDescription);
            description.setVisibility(View.VISIBLE);
        } else {
            description.setVisibility(View.GONE);
        }

        ControllerListener listener = new BaseControllerListener<ImageInfo>() {
            @Override
            public void onIntermediateImageSet(String id, ImageInfo imageInfo) {
                if (imageInfo != null) {
                    imvGuide.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
                    imvGuide.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    imvGuide.setAspectRatio((float) imageInfo.getWidth() / imageInfo.getHeight());
                }
            }

            @Override
            public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {
                if (imageInfo != null) {
                    imvGuide.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
                    imvGuide.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    imvGuide.setAspectRatio((float) imageInfo.getWidth() / imageInfo.getHeight());
                }
            }

            @Override
            public void onFailure(String id, Throwable throwable) {
                super.onFailure(id, throwable);
            }

        };

        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse("res:/" + mImage))
                .setControllerListener(listener)
                .build();
        imvGuide.setController(controller);
        return view;

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_TITLE, mTitle);
        outState.putString(KEY_DESC, mDescription);
        outState.putInt(KEY_IMAGE, mImage);
        outState.putInt(KEY_COLOR, mColor);
    }

}
