package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;

import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.fragments.landingpage.LandingPageFragment;
import com.danareksa.investasik.util.FontsOverride;

/**
 * Created by asep.surahman on 28/03/2018.
 */

public class LandingPageActivity extends BaseActivity{

    public static final String LANDINGPAGE = "landingpage";

    @Override
    protected int getLayout() {
        return R.layout.a_landingpage;
    }

    public static void startActivity(BaseActivity sourceActivity){
        Intent intent = new Intent(sourceActivity, LandingPageActivity.class);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (BuildConfig.FLAVOR.contentEquals("dim")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
        }

        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/HelveticaNeueLts.otf");
        LandingPageFragment.showFragment(this);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        this.finish();
    }




}
