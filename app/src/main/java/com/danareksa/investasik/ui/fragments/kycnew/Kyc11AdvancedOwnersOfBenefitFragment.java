package com.danareksa.investasik.ui.fragments.kycnew;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.eventBus.RxBusObject;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by asep.surahman on 21/05/2018.
 */

public class Kyc11AdvancedOwnersOfBenefitFragment extends KycBaseNew{

    public static final String TAG = Kyc11AdvancedOwnersOfBenefitFragment.class.getSimpleName();

    public static final String KYC_DATA_REQUEST = "kycDataRequest";

    @Bind(R.id.llOwnersBenefit)
    LinearLayout llOwnersBenefit;

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etBeneficiaryName)
    EditText etBeneficiaryName;

    @Bind(R.id.sBeneficiaryRelationship)
    Spinner sBeneficiaryRelationship;

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etBeneficiaryIdNumber)
    EditText etBeneficiaryIdNumber;

    @Bind(R.id.sBeneficiaryOccupation)
    Spinner sBeneficiaryOccupation;

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etBeneficiaryEmployer)
    EditText etBeneficiaryEmployer;

    @Bind(R.id.sBeneficiaryFundSource)
    Spinner sBeneficiaryFundSource;

    @Bind(R.id.sBeneficiaryAnnualIncome)
    Spinner sBeneficiaryAnnualIncome;

    @Bind(R.id.bMySelf)
    TextView bMySelf;

    @Bind(R.id.bOtherPeople)
    TextView bOtherPeople;

    @Bind(R.id.etBeneficiaryRelationshipOther)
    EditText etBeneficiaryRelationshipOther;

    @Bind(R.id.bKtp)
    TextView bKtp;

    @Bind(R.id.bPaspor)
    TextView bPaspor;

    String beneficiaryType = "";
    String beneficiaryIdType = "";
    String relationshipOther = "";

    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new Kyc11AdvancedOwnersOfBenefitFragment(), TAG);
            fragmentTransaction.commit();
        }
    }


    public static Fragment getFragment(KycDataRequest kycDataRequest){
        Bundle bundle = new Bundle();
        bundle.putSerializable(KYC_DATA_REQUEST, kycDataRequest);
        Kyc11AdvancedOwnersOfBenefitFragment fragment = new Kyc11AdvancedOwnersOfBenefitFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayout() {
        return R.layout.f_register_lanjut_pemilik_manfaat;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @Override
    public void onResume(){
        super.onResume();
    }


    public void init(){

        hideForm();
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")) {
            disableField();
        }


        if(request.getBeneficiaryType() != null){

            beneficiaryType = request.getBeneficiaryType();
            if(request.getBeneficiaryType().equals(Constant.BENEFICIARY_TYPE_OTHER)){
                changeLabelOtherPeople();
                beneficiaryType = Constant.BENEFICIARY_TYPE_OTHER;
                showForm();
                changeButtonMain(false, true);
            }

            if(request.getBeneficiaryType().equals(Constant.BENEFICIARY_TYPE_SELF)){
                changeLabelMySelf();
                beneficiaryType = Constant.BENEFICIARY_TYPE_SELF;
                hideForm();
                changeButtonMain(true, false);
            }
        }

        //default
        if(request.getBeneficiaryType() == null){
            changeLabelMySelf();
            hideForm();
            beneficiaryType = Constant.BENEFICIARY_TYPE_SELF;
            changeButtonMain(true, false);
        }


        if(request.getBeneficiaryIdType() != null){

            if(request.getBeneficiaryIdType().equals(Constant.ID_TYPE_KTP)){
                changeLabelKtp();
                changeButtonId(true, false);
                beneficiaryIdType = Constant.ID_TYPE_KTP;
            }
            if(request.getBeneficiaryIdType().equals(Constant.ID_TYPE_PAS)){
                changeLabelPaspor();
                changeButtonId(false, true);
                beneficiaryIdType = Constant.ID_TYPE_PAS;
            }
        }


        if(request.getBeneficiaryType() != null){
            if(request.getBeneficiaryType().equals(Constant.BENEFICIARY_TYPE_OTHER)){
                etBeneficiaryName.setText(request.getBeneficiaryName());
                etBeneficiaryIdNumber.setText(request.getBeneficiaryIdNumber());
                etBeneficiaryEmployer.setText(request.getBeneficiaryEmployer());
                setupSpinnerWithSpecificLookupSelection(sBeneficiaryRelationship, getKycLookupFromRealm(Constant.KYC_CAT_RELATIONSHIP), request.getBeneficiaryRelationship());
                setupSpinnerWithSpecificLookupSelection(sBeneficiaryOccupation, getKycLookupFromRealm(Constant.KYC_CAT_OCCUPATION), request.getBeneficiaryOccupation());
                setupSpinnerWithSpecificLookupSelection(sBeneficiaryFundSource, getKycLookupFromRealm(Constant.KYC_CAT_SOURCE_OF_INCOME), request.getBeneficiaryFundSource());
                setupSpinnerWithSpecificLookupSelection(sBeneficiaryAnnualIncome, getKycLookupFromRealm(Constant.KYC_CAT_ANNUAL_INCOME), request.getBeneficiaryAnnualIncome());

                if(request.getBeneficiaryRelationship() != null){
                    if(request.getBeneficiaryRelationship().equals("OTH")){
                        etBeneficiaryRelationshipOther.setVisibility(View.VISIBLE);
                        etBeneficiaryRelationshipOther.setText(request.getBeneficiaryRelationshipOther());
                    }
                }
            }
        }

        onItemSelectedItem();

    }



    public void onItemSelectedItem(){
        sBeneficiaryRelationship.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String itemRelationship  = String.valueOf(adapterView.getItemAtPosition(i).toString()); //others
                String otherRelationship = itemRelationship.toLowerCase();
                if(otherRelationship.contains("lainnya") || otherRelationship.contains("others") || otherRelationship.contains("oth")){
                    showFieldOtherRelationship();
                    relationshipOther = getAndTrimValueFromEditText(etBeneficiaryRelationshipOther);
                }else{
                    hideFieldOtherRelationship();
                    relationshipOther = "";
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView){

            }

        });

    }




    private void showFieldOtherRelationship(){
        etBeneficiaryRelationshipOther.setVisibility(View.VISIBLE);
    }

    private void hideFieldOtherRelationship(){
        etBeneficiaryRelationshipOther.setVisibility(View.GONE);
    }

    public void setValueSourceOfFund(){
        if(beneficiaryType.equals(Constant.BENEFICIARY_TYPE_OTHER)){
            request.setBeneficiaryType(beneficiaryType);
            request.setBeneficiaryIdType(beneficiaryIdType);
            request.setBeneficiaryName(getAndTrimValueFromEditText(etBeneficiaryName));
            request.setBeneficiaryIdNumber(getAndTrimValueFromEditText(etBeneficiaryIdNumber));
            request.setBeneficiaryEmployer(getAndTrimValueFromEditText(etBeneficiaryEmployer));
            request.setBeneficiaryRelationship(getKycLookupCodeFromSelectedItemSpinner(sBeneficiaryRelationship));

            if(getKycLookupCodeFromSelectedItemSpinner(sBeneficiaryRelationship) != null){
                if(getKycLookupCodeFromSelectedItemSpinner(sBeneficiaryRelationship).equals("OTH")){
                    request.setBeneficiaryRelationshipOther(getAndTrimValueFromEditText(etBeneficiaryRelationshipOther));
                }
            }

            request.setBeneficiaryOccupation(getKycLookupCodeFromSelectedItemSpinner(sBeneficiaryOccupation));
            request.setBeneficiaryFundSource(getKycLookupCodeFromSelectedItemSpinner(sBeneficiaryFundSource));
            request.setBeneficiaryAnnualIncome(getKycLookupCodeFromSelectedItemSpinner(sBeneficiaryAnnualIncome));
        }else if(beneficiaryType.equals(Constant.BENEFICIARY_TYPE_SELF)){
            request.setBeneficiaryType(beneficiaryType);
            request.setBeneficiaryIdType(request.getIdType());
            request.setBeneficiaryName(request.getName());
            request.setBeneficiaryIdNumber(request.getIdNumber());
            request.setBeneficiaryEmployer(request.getEmployer());
            request.setBeneficiaryRelationship("OTH");
            request.setBeneficiaryRelationshipOther("Diri Sendiri");
            request.setBeneficiaryOccupation(request.getOccupation());
            request.setBeneficiaryFundSource(request.getFundSource());
            request.setBeneficiaryAnnualIncome(request.getAnnualIncome());
        }
    }



    //disable
    @Override
    public void onValidationSucceeded(){
        super.onValidationSucceeded();
    }

    @Override
    public void saveDataKycWithBackpress(){

        if(!validate()){
            onValidFailed();
            return;
        }else{
            if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {

                 if(request.getFundSource().equals("FAM") || request.getFundSource().equals("SPS") || request.getFundSource().equals("OTH")){
                }else{
                    checkFundSource();
                }


                setValueSourceOfFund();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.SAVE_KYC_DATA, request));
            }else{
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
            }
        }
    }

    @Override
    public void previewsWithoutValidation(){
        if(request.getFundSource() != null){
            if(request.getFundSource().equals("FAM") || request.getFundSource().equals("SPS") || request.getFundSource().equals("OTH")){
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null)); //back to fund source
            }else{
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.TO_PAGE_CORRESPONDEN, null));
            }
        }else{

            getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null)); //back to fund source
        }

    }

    @Override
    public void nextWithoutValidation(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")) {
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
        }else{
            if(!validate()){
                onValidFailed();
            }else{
                /*setValueSourceOfFund();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));*/

                if(request.getFundSource().equals("FAM") || request.getFundSource().equals("SPS") || request.getFundSource().equals("OTH")){
                    setValueSourceOfFund();
                    getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
                }else{
                    checkFundSource();
                    setValueSourceOfFund();
                    getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
                }
            }
            return;
        }
    }


    @OnClick(R.id.llMySelf)
    void llMySelf(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
            hideForm();
            changeLabelMySelf();
            changeButtonMain(true, false);
            beneficiaryType = Constant.BENEFICIARY_TYPE_SELF;
        }
    }


    @OnClick(R.id.llOtherPeople)
    void llOtherPeople(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
            showForm();
            changeLabelOtherPeople();
            changeButtonMain(false, true);
            beneficiaryType = Constant.BENEFICIARY_TYPE_OTHER;

            if(request.getBeneficiaryType() != null){
                if(request.getBeneficiaryType().equals(Constant.BENEFICIARY_TYPE_OTHER)){
                    setupSpinnerWithSpecificLookupSelection(sBeneficiaryRelationship, getKycLookupFromRealm(Constant.KYC_CAT_RELATIONSHIP), request.getBeneficiaryRelationship());
                    setupSpinnerWithSpecificLookupSelection(sBeneficiaryOccupation, getKycLookupFromRealm(Constant.KYC_CAT_OCCUPATION), request.getBeneficiaryOccupation());
                    setupSpinnerWithSpecificLookupSelection(sBeneficiaryFundSource, getKycLookupFromRealm(Constant.KYC_CAT_SOURCE_OF_INCOME), request.getBeneficiaryFundSource());
                    setupSpinnerWithSpecificLookupSelection(sBeneficiaryAnnualIncome, getKycLookupFromRealm(Constant.KYC_CAT_ANNUAL_INCOME), request.getBeneficiaryAnnualIncome());
                }else{
                    setupSpinnerWithSpecificLookupSelection(sBeneficiaryRelationship, getKycLookupFromRealm(Constant.KYC_CAT_RELATIONSHIP), "");
                    setupSpinnerWithSpecificLookupSelection(sBeneficiaryOccupation, getKycLookupFromRealm(Constant.KYC_CAT_OCCUPATION), "");
                    setupSpinnerWithSpecificLookupSelection(sBeneficiaryFundSource, getKycLookupFromRealm(Constant.KYC_CAT_SOURCE_OF_INCOME), "");
                    setupSpinnerWithSpecificLookupSelection(sBeneficiaryAnnualIncome, getKycLookupFromRealm(Constant.KYC_CAT_ANNUAL_INCOME), "");
                }
            }else{
                setupSpinnerWithSpecificLookupSelection(sBeneficiaryRelationship, getKycLookupFromRealm(Constant.KYC_CAT_RELATIONSHIP), "");
                setupSpinnerWithSpecificLookupSelection(sBeneficiaryOccupation, getKycLookupFromRealm(Constant.KYC_CAT_OCCUPATION), "");
                setupSpinnerWithSpecificLookupSelection(sBeneficiaryFundSource, getKycLookupFromRealm(Constant.KYC_CAT_SOURCE_OF_INCOME), "");
                setupSpinnerWithSpecificLookupSelection(sBeneficiaryAnnualIncome, getKycLookupFromRealm(Constant.KYC_CAT_ANNUAL_INCOME), "");
            }

        }
    }


    @OnClick(R.id.llKtp)
    void llKtp(){
        changeLabelKtp();
        changeButtonId(true, false);
        beneficiaryIdType = Constant.ID_TYPE_KTP;
    }


    @OnClick(R.id.llPaspor)
    void llPaspor(){
        changeLabelPaspor();
        changeButtonId(false, true);
        beneficiaryIdType = Constant.ID_TYPE_PAS;
    }

    private void hideForm(){
        llOwnersBenefit.setVisibility(View.GONE);
    }

    private void showForm(){
        llOwnersBenefit.setVisibility(View.VISIBLE);
    }


    private void changeLabelMySelf(){
        bMySelf.setText(getCheckedIcon());
        bOtherPeople.setText("B");
    }


    private void changeLabelOtherPeople(){
        bMySelf.setText("A");
        bOtherPeople.setText(getCheckedIcon());
    }

    private void changeLabelKtp(){
        bKtp.setText(getCheckedIcon());
        bPaspor.setText("B");
    }

    private void changeLabelPaspor(){
        bKtp.setText("A");
        bPaspor.setText(getCheckedIcon());
    }

    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }



    private void disableField(){
        etBeneficiaryName.setEnabled(false);
        etBeneficiaryIdNumber.setEnabled(false);
        etBeneficiaryEmployer.setEnabled(false);
        sBeneficiaryRelationship.setEnabled(false);
        sBeneficiaryOccupation.setEnabled(false);
        sBeneficiaryFundSource.setEnabled(false);
        sBeneficiaryAnnualIncome.setEnabled(false);
    }




    public boolean validate(){
        boolean valid = true;
        String beneficiaryName     = etBeneficiaryName.getText().toString();
        String beneficiaryIdNumber = etBeneficiaryIdNumber.getText().toString();
        String beneficiaryEmployer = etBeneficiaryEmployer.getText().toString();

        if(beneficiaryType.equals("")){
            Toast.makeText(getContext(), "Pilih tipe pemilik manfaat", Toast.LENGTH_LONG).show();
            valid = false;
        }

        if(beneficiaryType.equals(Constant.BENEFICIARY_TYPE_OTHER)){

            if(beneficiaryName.isEmpty() && beneficiaryName.length() <= 0){
                etBeneficiaryName.setError("Mohon isi kolom ini");
                valid = false;
            }

            if(beneficiaryIdType.equals("")){
                Toast.makeText(getContext(), "Silahkan Pilih Jenis ID", Toast.LENGTH_LONG).show();
                valid = false;
            }

            if(!beneficiaryIdType.equals("")){
                if(beneficiaryIdType == Constant.ID_TYPE_KTP){
                    if(beneficiaryIdNumber.length() < 16 || beneficiaryIdNumber.length() > 16){
                        etBeneficiaryIdNumber.setError("Data tidak sesuai");
                        valid = false;
                    }
                }else if(beneficiaryIdType == Constant.ID_TYPE_PAS){
                    if(beneficiaryIdNumber.length() < 3){
                        etBeneficiaryIdNumber.setError("Data tidak sesuai");
                        valid = false;
                    }
                }
            }

            if(beneficiaryIdNumber.isEmpty() && beneficiaryIdNumber.length() <= 0){
                etBeneficiaryIdNumber.setError("Mohon isi kolom ini");
                valid = false;
            }

            if(beneficiaryEmployer.isEmpty() && beneficiaryEmployer.length() <= 0){
                etBeneficiaryEmployer.setError("Mohon isi kolom ini");
                valid = false;
            }

            if(sBeneficiaryRelationship.getSelectedItem().toString().equals("") ||
                    sBeneficiaryRelationship.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
                valid = false;
                ((TextView) sBeneficiaryRelationship.getSelectedView()).setError("Mohon isi kolom ini");
            }

            if(!sBeneficiaryRelationship.getSelectedItem().toString().equals("")){
                if(getKycLookupCodeFromSelectedItemSpinner(sBeneficiaryRelationship).equals("OTH")){
                    if(etBeneficiaryRelationshipOther.getText().toString().equals("")){
                        valid = false;
                        etBeneficiaryRelationshipOther.setError("Mohon isi kolom ini");
                    }
                }
            }

            if(sBeneficiaryOccupation.getSelectedItem().toString().equals("") ||
                sBeneficiaryOccupation.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
                valid = false;
                ((TextView) sBeneficiaryOccupation.getSelectedView()).setError("Mohon isi kolom ini");
            }

            if(sBeneficiaryFundSource.getSelectedItem().toString().equals("") ||
                sBeneficiaryFundSource.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
                valid = false;
                ((TextView) sBeneficiaryFundSource.getSelectedView()).setError("Mohon isi kolom ini");
            }

            if(sBeneficiaryAnnualIncome.getSelectedItem().toString().equals("") ||
                sBeneficiaryAnnualIncome.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
                valid = false;
                ((TextView) sBeneficiaryAnnualIncome.getSelectedView()).setError("Mohon isi kolom ini");
            }
        }

        return valid;
    }


    /*
      "beneficiaryType" : "DEP",
      "beneficiaryName" : "Tian Hong",
      "beneficiaryRelationship" : "HSB",
      "beneficiaryIdType" : "IDC",
      "beneficiaryIdNumber" : "2300000000001199",
      "beneficiaryOccupation" : "PNS",
      "beneficiaryEmployer" : "PT PLN",
      "beneficiaryFundSource" : "EMP",
      "beneficiaryAnnualIncome" : "INC3",
    */



    private void changeButtonMain(boolean mySelf, boolean other){
        bMySelf.setEnabled(mySelf);
        bOtherPeople.setEnabled(other);
    }


    private void changeButtonId(boolean ktp, boolean paspor){
        bKtp.setEnabled(ktp);
        bPaspor.setEnabled(paspor);
    }


    private Spannable getCheckedIcon(){
        Spannable buttonLabel = new SpannableString(" ");
        buttonLabel.setSpan(new ImageSpan(getActivity().getApplicationContext(), R.drawable.ic_checked,
                ImageSpan.ALIGN_BOTTOM), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return  buttonLabel;
    }


    private void checkFundSource(){
        request.setFundSourceIdType("");
        request.setFundSourceIdNumber("");
        request.setFundSourceEmployer("");
        request.setFundSourceName("");
        request.setFundSourceRelationship("");
        request.setFundSourceOccupation("");
        request.setFundSourceOfFundSource("");
    }


    @Override
    public void previewsWhileError(){

    }

}
