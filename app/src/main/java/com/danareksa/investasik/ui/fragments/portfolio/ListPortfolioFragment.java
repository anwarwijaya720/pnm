package com.danareksa.investasik.ui.fragments.portfolio;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.InvestmentList;
import com.danareksa.investasik.data.api.beans.PortfolioInvestment;
import com.danareksa.investasik.data.api.responses.CartListResponse;
import com.danareksa.investasik.data.api.responses.InvestmentListByProductTypeResponse;
import com.danareksa.investasik.data.api.responses.PortfolioInvestmentListResponse;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.ListOfCatalogueActivity;
import com.danareksa.investasik.ui.activities.PortfolioActivity;
import com.danareksa.investasik.ui.adapters.rv.PortfolioAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.AmountFormatter;
import com.danareksa.investasik.util.ui.RecyclerItemClickListener;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import icepick.State;

/**
 * Created by fajarfatur on 3/1/16.
 */
public class ListPortfolioFragment extends BaseFragment {

    public static final String TAG = ListPortfolioFragment.class.getSimpleName();

    @Bind(R.id.llPortfolio)
    LinearLayout llPortfolio;
    @Bind(R.id.llNoPortfolio)
    LinearLayout llNoPortfolio;
    @Bind(R.id.rv)
    RecyclerView rv;
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.bTotInvest)
    Button btnTotalInvestment;
    @Bind(R.id.tvTitle)
    TextView tvTitle;

    String accountType = "";


    @State
    InvestmentListByProductTypeResponse investmentListByProductType;
    @State
    PortfolioInvestmentListResponse investmentList;
    public List<CartListResponse> cartList;

    private ListPortfolioPresenter presenter;


    public static void showFragment(BaseActivity sourceActivity, Integer catId) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {

            Bundle bundle = new Bundle();
            bundle.putSerializable("CAT_ID", catId);
            ListPortfolioFragment fragment = new ListPortfolioFragment();
            fragment.setArguments(bundle);

            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }





    @Override
    protected int getLayout() {
        return R.layout.f_list_portfolio;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ListPortfolioPresenter(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRV();
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 1 : 1;
            }
        });

        rv.setLayoutManager(layoutManager);

        Integer catId = getArguments().getInt("CAT_ID",0);

        if (investmentList != null) {
            loadInvestmentList();
        } else {
            presenter.getInvestmentList(catId);
        }
        presenter.cartList();
    }

    /*
        @Override
        public void onResume() {
            super.onResume();
            getActivity().setTitle("Portofolio");

        }
    */

    private void initRV() {
        LinearLayoutManager llManager = new LinearLayoutManager(getActivity());
        llManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(llManager);
        rv.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position) {
                super.onItemClick(childView, position);
                PortfolioInvestment investment = (PortfolioInvestment) childView.getTag();
                Integer catId = getArguments().getInt("CAT_ID",0);
                investment.setInvestmentGoal(investmentList.getData().getInvestmentGoal());

                //disable dulu
                //if(investment.getInvestmentComposition().size() > 0 && investment.getInvestmentComposition() != null){
                    PortfolioActivity.startActivity((BaseActivity) getActivity(), investmentList.getData().getInvestmentList().get(investmentList.getData().getInvestmentList().indexOf(investment)), investmentList.getData().getIfua(), catId, accountType);
                //}

            }
        }));
    }

    //new
    private double totalNilaiInvestasi(List<PortfolioInvestment> investmentList){
        double totalInvestasi = 0.0;
        if(investmentList != null && investmentList.size()>0){
            for(int i = 0; i < investmentList.size(); i++){
                if (investmentList.get(i).getCurrency().equals("USD")){
                    //14,268.25
                    totalInvestasi = totalInvestasi + investmentList.get(i).getTotalInvestmentMarketValue() * 14268.25;
                } else {
                    totalInvestasi = totalInvestasi + investmentList.get(i).getTotalInvestmentMarketValue();
                }

            }
        }
        return totalInvestasi;
    }


    public void loadInvestmentList() {
        InvestmentList investment = investmentList.getData();
        double totalInv = investment.getTotalInvestment();
        accountType = investment.getAccountType();
        btnTotalInvestment.setText("Total Nilai Investasi : " + AmountFormatter.format(totalInv));

        String title = accountType.toLowerCase().substring(0,1).toUpperCase() +
                accountType.substring(1).toLowerCase();

        tvTitle.setText(title + " - " + investment.getIfua() + " - " + investment.getInvestmentGoal());
        rv.setAdapter(new PortfolioAdapter(getActivity(), investmentList.getData().getInvestmentList()));
    }

    void noPortfolio(boolean b) {
        llNoPortfolio.setVisibility(b ? View.VISIBLE : View.GONE);
        llPortfolio.setVisibility(b ? View.GONE : View.VISIBLE);
    }


    @OnClick(R.id.bStartToInvest)
    void startToInvest() {
        ListOfCatalogueActivity.startActivity((BaseActivity) getActivity());
    }

    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.tvTryAgain)
    void retryConnection() {
        Integer catId = getArguments().getInt("CAT_ID",0);
        presenter.getInvestmentList(catId);
    }


    void showDialogAfterSubmit(String info){
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.infortmation).toUpperCase())
                .titleColor(Color.BLACK)
                .content(info)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        getActivity().finish();
                    }
                })
                .show();
    }


}
