package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.fragments.signIn.ForgotPasswordTempFragment;
import com.danareksa.investasik.ui.fragments.signIn.RePasswordFragment;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.OnClick;

/**
 * Created by pandu.abbiyuarsyah on 20/04/2017.
 */

public class ForgotPasswordActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;
    @BindString(R.string.forgot_pwd_title)
    String forgotPasswordTittle;

    public static void startActivity(BaseActivity sourceActivity) {
        Intent intent = new Intent(sourceActivity, ForgotPasswordActivity.class);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_forgot_password;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar();

        if (BuildConfig.FLAVOR.contentEquals("dim") || BuildConfig.FLAVOR.contentEquals("btn")|| BuildConfig.FLAVOR.contentEquals("pnm")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
        }

        ForgotPasswordTempFragment.showFragment(this);
    }

    public void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        if(BuildConfig.FLAVOR.contentEquals("pnm")){
            title.setText("Login");
//            title.setTypeface(null, Typeface.BOLD);
        }else {
            title.setText(forgotPasswordTittle);
        }
    }

    @OnClick(R.id.bSignIn)
    void bSignIn(){
        startActivity(new Intent(ForgotPasswordActivity.this, SignInActivity.class));
        finish();
    }

    @OnClick(R.id.bSignUp)
    void bSignUp(){
        startActivity(new Intent(ForgotPasswordActivity.this, RegisterNasabahActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        Fragment f = getSupportFragmentManager().findFragmentByTag(RePasswordFragment.TAG);
        if (f instanceof RePasswordFragment)
        {
            super.onBackPressed();
        } else {
            super.onBackPressed();
            startActivity(new Intent(ForgotPasswordActivity.this, SignInActivity.class));
            ForgotPasswordActivity.this.finish();
        }
    }
}
