package com.danareksa.investasik.ui.fragments.kycnew;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.BankAccountImage;
import com.danareksa.investasik.data.api.beans.BankAccountList;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.util.eventBus.RxBusObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

import static android.app.Activity.RESULT_OK;

/**
 * Created by asep.surahman on 16/05/2018.
 */
@RuntimePermissions
public class Kyc8FinancialDataDocumentFragment extends KycBaseNew {

    public static final String TAG = Kyc8FinancialDataDocumentFragment.class.getSimpleName();

    @Bind(R.id.ivBukuTabungan)
    CircleImageView ivBukuTabungan;

    @Bind(R.id.tvBukuTabungan)
    TextView tvBukuTabungan;

    private int choosenTask = 0;
    private final int SELECT_PHOTO_BUKU = 0;
    private final int REQUEST_CAMERA_BUKU = 2;

    private File tempFile;
    private Uri picUri;
    private String IdPhoto = "";
    public String fileName = "";

    boolean validImage = false;
    public KycDataRequest kycDataRequest;



    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new Kyc8FinancialDataDocumentFragment(), TAG);
            fragmentTransaction.commit();
        }
    }


    public static Fragment getFragment(KycDataRequest kycDataRequest){
        Bundle bundle = new Bundle();
        bundle.putSerializable(KYC_DATA_REQUEST, kycDataRequest);
        Kyc8FinancialDataDocumentFragment fragment = new Kyc8FinancialDataDocumentFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected int getLayout(){
        return R.layout.f_register_dokumen_keuangan;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        kycDataRequest = (KycDataRequest) getArguments().getSerializable(KYC_DATA_REQUEST);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        loadImage();
    }


    @Override
    public void onResume(){
        super.onResume();
    }


    private void init(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")) {
            tvBukuTabungan.setText("Foto buku Tabungan Anda");
        }
    }

   /* @Override
    public void onValidationSucceeded(){
        super.onValidationSucceeded();
    }
    */


    //image ID
    @OnClick(R.id.ivBukuTabungan)
    void ivBukuTabungan(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
            new MaterialDialog.Builder(getActivity())
                    .title("Change Picture")
                    .items(R.array.change_picture)
                    .itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                            if (which == 0) {
                                choosenTask = REQUEST_CAMERA_BUKU;
                                Kyc8FinancialDataDocumentFragmentPermissionsDispatcher.startCameraIdWithPermissionCheck(Kyc8FinancialDataDocumentFragment.this);
                            } else {
                                choosenTask = SELECT_PHOTO_BUKU;
                                Kyc8FinancialDataDocumentFragmentPermissionsDispatcher.startGalleryIdWithPermissionCheck(Kyc8FinancialDataDocumentFragment.this);
                            }
                        }
                    })
                    .show();
        }
    }


    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void startGalleryId() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_PHOTO_BUKU);
    }

    public static File createTemporaryFile(Context context, String folder_name, String ext) {
        try {
            File folder = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), folder_name);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            return File.createTempFile(""+System.currentTimeMillis(), ext, folder);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void startCameraId(){
        tempFile = createTemporaryFile(getContext(), "save_book_pic", ".jpg");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        picUri = FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".provider", tempFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
        startActivityForResult(intent, REQUEST_CAMERA_BUKU);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Kyc8FinancialDataDocumentFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent){
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode){
            case SELECT_PHOTO_BUKU:
                if (resultCode == RESULT_OK){

                    Uri selectedImage = imageReturnedIntent.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    File file = new File(picturePath);
                    tempFile = file;
                    onCaptureImageResultId();

                }
                break;
            case REQUEST_CAMERA_BUKU:
                if (resultCode == Activity.RESULT_OK) {
                    onCaptureImageResultId();
                }
                break;


        }
    }



    void onCaptureImageResultId(){
        // Setting option to resize image
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(tempFile.getPath(), options);
        final int REQUIRED_SIZE = 310;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;

        // Load file with option parameter, then compress it
        Bitmap b = BitmapFactory.decodeFile(tempFile.getPath(), options);

        Matrix matrix = new Matrix();

        try {
            ExifInterface exif = null;
            exif = new ExifInterface(tempFile.getPath());
            String orientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            if (orientation.equals(ExifInterface.ORIENTATION_NORMAL)) {
                // Do nothing. The original image is fine.
            } else if (orientation.equals(ExifInterface.ORIENTATION_ROTATE_90 + "")) {
                matrix.postRotate(90);
            } else if (orientation.equals(ExifInterface.ORIENTATION_ROTATE_180 + "")) {
                matrix.postRotate(180);
            } else if (orientation.equals(ExifInterface.ORIENTATION_ROTATE_270 + "")) {
                matrix.postRotate(270);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        Bitmap resizedBitmap = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
        resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, bytes);

        byte[] byteArray = bytes.toByteArray();
        // get the base 64 string

        fileName = tempFile.getName();
        IdPhoto = Base64.encodeToString(byteArray, Base64.DEFAULT);
        ivBukuTabungan.setImageBitmap(resizedBitmap);

        if(!fileName.equals("") && !IdPhoto.equals("")){
            PrefHelper.setString(PrefKey.NAME_PHOTO_SAVING_BOOK, fileName);
            PrefHelper.setString(PrefKey.ID_PHOTO_SAVING_BOOK, IdPhoto);
            validImage = true;
        }


    }


    //disable
    @Override
    public void onValidationSucceeded(){
        super.onValidationSucceeded();
    }


    @Override
    public void nextWithoutValidation(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")) {
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
        }else{
            if(!validate()){
                onValidFailed();
            }else{
                setValueImage();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
            }
            return;
        }
    }


    @Override
    public void saveDataKycWithBackpress() {
        if(!validate()){
            onValidFailed();
            return;
        }else{
            if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
                setValueImage();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.SAVE_KYC_DATA, request));
            }else{
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
            }
        }
    }

    @Override
    public void previewsWithoutValidation() {
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null));
    }




    private void setValueImage(){
        if(request.getBankAccountLists().get(0).getBankAccountImageKey() != null){
            if(!request.getBankAccountLists().get(0).getBankAccountImageKey().equals("")){
                if(!fileName.equals("") && !IdPhoto.equals("")){
                    setDataBank();
                }
            }else{
                setDataBank();
            }
        }else{
            setDataBank();
        }
    }


    private void setDataBank(){
        List<BankAccountList> bankAccountLists = new ArrayList<>();
        BankAccountList bankAccountList = new BankAccountList();
        BankAccountImage bankAccountImage = new BankAccountImage();

        if(!fileName.equals("") && !IdPhoto.equals("")){
            bankAccountImage.setContent(IdPhoto);
            bankAccountImage.setFile_name(fileName);
        }else{
            if(!PrefHelper.getString(PrefKey.NAME_PHOTO_SAVING_BOOK).equals("")){
                bankAccountImage.setFile_name(PrefHelper.getString(PrefKey.NAME_PHOTO_SAVING_BOOK));
            }
            if(!PrefHelper.getString(PrefKey.ID_PHOTO_SAVING_BOOK).equals("")){
                bankAccountImage.setContent(PrefHelper.getString(PrefKey.ID_PHOTO_SAVING_BOOK));
            }
        }


        if(request.getBankAccountLists() != null && request.getBankAccountLists().size() > 0){ //update
            bankAccountList.setBankId(request.getBankAccountLists().get(0).getBankId());
            bankAccountList.setBankAccountName(request.getBankAccountLists().get(0).getBankAccountName());
            bankAccountList.setBankAccountNumber(request.getBankAccountLists().get(0).getBankAccountNumber());
            bankAccountList.setBankBranch(request.getBankAccountLists().get(0).getBankBranch());
            bankAccountList.setBankBranchOther(request.getBankAccountLists().get(0).getBankBranchOther());
            bankAccountList.setBankAccountId(request.getBankAccountLists().get(0).getBankAccountId());
            bankAccountList.setBankAccountImage(bankAccountImage);
            bankAccountLists.add(bankAccountList);
            request.setBankAccountLists(bankAccountLists);
        }else{ //create
            bankAccountList.setBankId(request.getBankAccountLists().get(0).getBankId());
            bankAccountList.setBankAccountName(request.getBankAccountLists().get(0).getBankAccountName());
            bankAccountList.setBankAccountNumber(request.getBankAccountLists().get(0).getBankAccountNumber());
            bankAccountList.setBankBranch(request.getBankAccountLists().get(0).getBankBranch());
            bankAccountList.setBankBranchOther(request.getBankAccountLists().get(0).getBankBranchOther());
            bankAccountList.setBankAccountImage(bankAccountImage);
            bankAccountLists.add(bankAccountList);
            request.setBankAccountLists(bankAccountLists);
        }
    }


    public void loadImage(){
        try {
            if(request.getBankAccountLists() !=  null && request.getBankAccountLists().size() > 0){
                if(!request.getBankAccountLists().get(0).getBankAccountImageKey().equals("")){

                    validImage = true;
                    Glide.with(getContext()).load(InviseeService.IMAGE_CUSTOMER_DOWNLOAD_URL + request.getBankAccountLists().get(0).getBankAccountImageKey() + "&token=" + PrefHelper.getString(PrefKey.TOKEN))
                            .override(400, 400)
                            .error(R.color.grey_200)
                            .fitCenter()
                            .into(ivBukuTabungan);

                }
            }
        } catch (Exception e) {
            ivBukuTabungan.setImageResource(R.drawable.ic_tab_book);
        }
    }


    public boolean validate(){

        boolean valid = true;
        if(validImage == true){
            valid = true;
        }else{

            if(IdPhoto.equals("")){
                Toast.makeText(getActivity(), "Masukkan foto buku tabungan", Toast.LENGTH_SHORT).show();
                valid = false;
            }

        }

        return valid;

    }


    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }


    @Override
    public void previewsWhileError(){

    }


}
