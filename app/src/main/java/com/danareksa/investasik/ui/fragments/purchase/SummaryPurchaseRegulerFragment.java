package com.danareksa.investasik.ui.fragments.purchase;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.PackageReguler;
import com.danareksa.investasik.data.api.beans.ProductRegulerIfua;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.MainActivity;
import com.danareksa.investasik.ui.adapters.rv.SummaryRegulerPurchaseAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by asep.surahman on 14/08/2018.
 */

public class SummaryPurchaseRegulerFragment extends BaseFragment{

    public static final String TAG = SummaryPurchaseRegulerFragment.class.getSimpleName();
    public static final String PRODUCT_REGULER = "productReguler";

//    @Bind(R.id.tvOrderNumber)
//    TextView tvOrderNumber;
//    @Bind(R.id.tvIfuaType)
//    TextView tvIfuaType;
//    @Bind(R.id.tvIfuaId)
//    TextView tvIfuaId;
//    @Bind(R.id.tvBankName)
//    TextView tvBankName;
//    @Bind(R.id.tvAccountNumber)
//    TextView tvAccountNumber;
//    @Bind(R.id.tvAccountName)
//    TextView tvAccountName;
    @Bind(R.id.rv)
    RecyclerView rv;
    @Bind(R.id.bNext)
    Button bNext;

    ProductRegulerIfua productRegulerIfua;
    String accountType, ifuaId;
    List<PackageReguler> packageList;
    SummaryRegulerPurchaseAdapter adapter;


    public static void showFragment(BaseActivity sourceActivity, ProductRegulerIfua productReguler, String accountType, String ifuaId){
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(PRODUCT_REGULER, productReguler);
            bundle.putString("type", accountType);
            bundle.putString("ifuaId", ifuaId);
            SummaryPurchaseRegulerFragment fragment = new SummaryPurchaseRegulerFragment();
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }



    @Override
    protected int getLayout() {
        return R.layout.f_summary_purchase_reguler;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        packageList = new ArrayList<>();
        accountType = getArguments().getString("type");
        ifuaId = getArguments().getString("ifuaId");
        productRegulerIfua = (ProductRegulerIfua) getArguments().getSerializable(PRODUCT_REGULER);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        init();
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
    }


    public void init(){
        if(productRegulerIfua != null){

//            tvOrderNumber.setText(productRegulerIfua.getIdNumber());
//            tvIfuaType.setText(accountType);
//            tvIfuaId.setText(ifuaId);
//            tvBankName.setText(productRegulerIfua.getBankName());
//            tvAccountNumber.setText(productRegulerIfua.getBankAccountNumber());
//            tvAccountName.setText(productRegulerIfua.getBankAccountName());

            HashMap<String, String> data = new HashMap<>();
            data.put("ifua_type",accountType);
            data.put("ifua_id", ifuaId);
            data.put("bank_name",productRegulerIfua.getBankName());
            data.put("account_number", productRegulerIfua.getBankAccountNumber());
            data.put("account_name",productRegulerIfua.getBankAccountName());

            if(productRegulerIfua.getPackageList() != null && productRegulerIfua.getPackageList().size() != 0){
                packageList = productRegulerIfua.getPackageList();
            }

            loadList(data);
        }
    }


    private void loadList(HashMap<String, String> datas){
        adapter = new SummaryRegulerPurchaseAdapter(getActivity(), packageList, datas);
        rv.setAdapter(adapter);
    }


    @OnClick(R.id.bNext)
    void bNext(){
        getActivity().finish();
        startActivity(new Intent((BaseActivity) getActivity(), MainActivity.class));
    }



}
