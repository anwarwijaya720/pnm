package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.util.Constant;

import java.util.Locale;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by fajarfatur on 1/12/16.
 */
public class SplashScreenActivity extends BaseActivity {

    @Bind(R.id.llRetry)
    LinearLayout llRetry;
    @Bind(R.id.splash_background)
    ImageView splashBackground;
    @Bind(R.id.ivLogo)
    ImageView splashLogoImg;
    @Bind(R.id.progressBar)
    ProgressBar progressBar;

    private SplashScreenPresenter presenter;

    @Override
    protected int getLayout() {
        return R.layout.a_splashscreen;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        if ( BuildConfig.FLAVOR.contentEquals("dim") ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
            Glide.with(this).load(R.drawable.logo_ia)
                    .fitCenter()
                    .into(splashLogoImg);
        } else if ( BuildConfig.FLAVOR.contentEquals("btn")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
            Glide.with(this).load(R.drawable.btn_logo)
                    .fitCenter()
                    .into(splashLogoImg);
        } else if ( BuildConfig.FLAVOR.contentEquals("pnm")) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
//                //getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN|View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
//                // edited here
//                getWindow().setStatusBarColor(Color.WHITE);
//            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }

        } else {
            Glide.with(this).load(R.drawable.logo_ia)
                    .fitCenter()
                    .into(splashLogoImg);
        }

        presenter = new SplashScreenPresenter(this);
        presenter.checkVersion();
    }

    void startApplication() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (PrefHelper.getBoolean(PrefKey.IS_LOGIN)) {
                    String userStatus = PrefHelper.getString(PrefKey.CUSTOMER_STATUS);
                    if (userStatus.equalsIgnoreCase(Constant.USER_STATUS_ACTIVE)) {
                        showUserProfileSuggestionDialog();
                    } else {
                        gotoMainActivity();
                    }
                } else {

                    if(isFirstRun()){
                        OnBoardingActivity.startActivity(SplashScreenActivity.this);
                    }else{
                        if(BuildConfig.FLAVOR.contentEquals("pnm")){
                            Login();
                        }else{
                            LandingPageActivity.startActivity(SplashScreenActivity.this);
                        }
                    }

                    finish();

                }
            }
        }, 3000);

    }

    public void Login(){
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
    }

    public boolean isFirstRun() {
        return PrefHelper.getIsFirstRunFromPreference(PrefKey.IS_FIRST_RUN2);
    }

    @OnClick(R.id.bRetry)
    void retry(){
        presenter.checkVersion();
        llRetry.setVisibility(View.GONE);
    }


    public  void setLanguage() {

        String languageToLoad  = "in"; // your language
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    public void showUserProfileSuggestionDialog() {
        new MaterialDialog.Builder(this)
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.welcome).toUpperCase())
                .titleColor(Color.BLACK)
                .content(R.string.user_profile_suggestion)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .negativeText(R.string.later)
                .negativeColor(getResources().getColor(R.color.colorPrimary))
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        UserProfileActivity.startActivity(SplashScreenActivity.this);
                        finish();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        gotoMainActivity();
                    }
                })
                .cancelable(false)
                .show();
    }

    public void gotoMainActivity() {
        MainActivity.startActivity(this);
        finish();
    }

    public void showProgressBar(){
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar(){
        progressBar.setVisibility(View.GONE);
    }

}
