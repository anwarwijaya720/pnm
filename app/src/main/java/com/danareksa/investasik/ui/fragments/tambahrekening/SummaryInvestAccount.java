package com.danareksa.investasik.ui.fragments.tambahrekening;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.HeirList;
import com.danareksa.investasik.data.api.beans.InvestAccount;
import com.danareksa.investasik.data.api.beans.PackageDetail;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.MainActivity;
import com.danareksa.investasik.ui.activities.TambahRekeningActivity;
import com.danareksa.investasik.ui.adapters.rv.SummaryHeirAdapter;
import com.danareksa.investasik.ui.adapters.rv.SummaryPackageAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.Constant;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by asep.surahman on 07/08/2018.
 */

public class SummaryInvestAccount extends BaseFragment {


    public static final String TAG = SummaryInvestAccount.class.getSimpleName();

    @Bind(R.id.tvTitle)
    TextView tvTitle;

    @Bind(R.id.vLine1)
    View vLine1;
    @Bind(R.id.tvNumberCif)
    TextView tvNumberCif;
    @Bind(R.id.tvFullName)
    TextView tvFullName;
    @Bind(R.id.tvNumberCard)
    TextView tvNumberCard;
    @Bind(R.id.tvBankName)
    TextView tvBankName;
    @Bind(R.id.tvAccountNumber)
    TextView tvAccountNumber;
    @Bind(R.id.tvAccountName)
    TextView tvAccountName;
    @Bind(R.id.rvProduct)
    RecyclerView rvProduct;
    @Bind(R.id.rvHeir)
    RecyclerView rvHeir;
    @Bind(R.id.llPackage)
    RelativeLayout llPackage;
    @Bind(R.id.llHeir)
    RelativeLayout llHeir;
    @Bind(R.id.titlePackage)
    TextView titlePackage;
    @Bind(R.id.titleHeir)
    TextView titleHeir;

    SummaryHeirAdapter adapterHeir;
    SummaryPackageAdapter adapterPackage;
    List<HeirList> heirList;
    List<PackageDetail> packageList;

    public static final String INVEST_ACCOUNT = "accountRequest";
    public InvestAccount investAccount;
    String accountType = "";

    public static void showFragment(BaseActivity sourceActivity, InvestAccount investAccount, String accountType){
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(INVEST_ACCOUNT, investAccount);
            bundle.putString("type", accountType);
            SummaryInvestAccount fragment = new SummaryInvestAccount();
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }

    @Override
    protected int getLayout(){
        return R.layout.dialog_summary_invest;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        heirList = new ArrayList<>();
        packageList = new ArrayList<>();
        investAccount = (InvestAccount) getArguments().getSerializable(INVEST_ACCOUNT);
        accountType = getArguments().getString("type");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        rvHeir.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvProduct.setLayoutManager(new LinearLayoutManager(getActivity()));
        init();
    }

    public void init(){
        if(investAccount != null){

            tvAccountName.setText(investAccount.getBankAccountName());
            tvAccountNumber.setText(investAccount.getBankAccountNumber());
            tvNumberCif.setText(investAccount.getCif());
            tvFullName.setText(investAccount.getFullName());
            tvNumberCard.setText(investAccount.getIdNumber());
            tvBankName.setText(investAccount.getBankName());

            if(accountType.equals(Constant.INVEST_TYPE_REGULER)){
                tvTitle.setText("Permohonan Pembukaan Rekening Reguler Berhasil Dibuat");
                packageList = investAccount.getPackageList();
                loadListProduct();
            }else if(accountType.equals(Constant.INVEST_TYPE_LUMPSUM)){
                tvTitle.setText("Permohonan Pembukaan Rekening Lump Sum Berhasil Dibuat");
                loadListProduct();
                rvProduct.setVisibility(View.GONE);
                llPackage.setVisibility(View.GONE);
                vLine1.setVisibility(View.GONE);
            }

            if(investAccount.getHeirList() != null && investAccount.getHeirList().size() != 0){
                heirList = investAccount.getHeirList();
                loadListHeir();
            }else{
                loadListHeir();
                titleHeir.setVisibility(View.GONE);
                rvHeir.setVisibility(View.GONE);
                llHeir.setVisibility(View.GONE);
            }

        }
    }


    private void loadListHeir(){
        adapterHeir = new SummaryHeirAdapter(getActivity(), heirList);
        rvHeir.setAdapter(adapterHeir);
        rvHeir.setNestedScrollingEnabled(false);
    }

    private void loadListProduct(){
        adapterPackage = new SummaryPackageAdapter(getActivity(), packageList);
        rvProduct.setAdapter(adapterPackage);
        rvProduct.setNestedScrollingEnabled(false);
    }


    @Override
    public void onResume(){
        super.onResume();
    }


    @OnClick(R.id.bOk)
    void bOk(){
        TambahRekeningActivity.tambahRekActivity.finish();
        getActivity().finish();
        MainActivity.startActivity((BaseActivity) getActivity());
    }


}
