package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.os.Bundle;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.InvestAccount;
import com.danareksa.investasik.ui.fragments.tambahrekening.SummaryInvestAccount;

/**
 * Created by asep.surahman on 10/09/2018.
 */

public class SummaryInvestAccountActivity extends BaseActivity {


    private InvestAccount investAccount;
    private String accountType;


    public static final String INVEST_ACCOUNT = "accountRequest";

    public static void startActivity(BaseActivity sourceActivity, InvestAccount investAccount, String accountType){
        Intent intent = new Intent(sourceActivity, SummaryInvestAccountActivity.class);
        intent.putExtra(INVEST_ACCOUNT, investAccount);
        intent.putExtra("type", accountType);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_summary_invest_account;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        investAccount = (InvestAccount) getIntent().getSerializableExtra(INVEST_ACCOUNT);
        accountType = getIntent().getStringExtra("type");
        SummaryInvestAccount.showFragment(this, investAccount, accountType);
    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }

}
