package com.danareksa.investasik.ui.adapters.pager;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.PromoResponse;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.DetailPromoActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asep.surahman on 24/04/2018.
 */

public class AdvBannerMainPageAdapter extends PagerAdapter {

    private List<PromoResponse> advertises = new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;


    public AdvBannerMainPageAdapter(List<PromoResponse> advertises, Context context) {
        this.advertises = advertises;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount(){
        return advertises.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.row_slide_home, view, false);
        assert imageLayout != null;

        final ImageView imgIntro = (ImageView) imageLayout.findViewById(R.id.ivLanding);
        final ProgressBar progressBar = (ProgressBar) imageLayout.findViewById(R.id.progressBar);


        try{
            progressBar.setVisibility(View.VISIBLE);
            imgIntro.layout(0,0,0,0);
            Picasso.with(context).load(InviseeService.IMAGE_DOWNLOAD_URL + advertises.get(position).getImage_android())
                    //.error(R.drawable.upload)
                    .placeholder(R.color.grey_200)
                    //.resize(600, 200) // resizes the image to these dimensions (in pixel)
                    .into(imgIntro, new com.squareup.picasso.Callback(){
                        @Override
                        public void onSuccess(){
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(){
                            progressBar.setVisibility(View.GONE);
                        }
                    });

        }catch (Exception e){
            e.printStackTrace();
        }

        imgIntro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPromoDetail(advertises.get(position).getCode(), advertises.get(position).getTitle());
            }
        });


        view.addView(imageLayout, 0);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object){
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader){

    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    public void showPromoDetail(String code, String title) {
        PromoResponse param = new PromoResponse();
        param.setCode(code);
        param.setTitle(title);
        DetailPromoActivity.startActivity((BaseActivity) context, param);
    }


}
