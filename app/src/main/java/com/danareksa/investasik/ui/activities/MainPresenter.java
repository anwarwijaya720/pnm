package com.danareksa.investasik.ui.activities;

import android.graphics.Color;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.responses.CartListResponse;
import com.danareksa.investasik.data.api.responses.GenericResponse;
import com.danareksa.investasik.data.api.responses.PortfolioInvestmentSummaryResponse;
import com.danareksa.investasik.data.api.responses.StatusCustomerResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.util.Constant;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by fajarfatur on 2/18/16.
 */
public class MainPresenter {

    private MainActivity activity;

    public MainPresenter(MainActivity activity) {
        this.activity = activity;
    }

    void cartList() {
        activity.getInviseeService().getApi().getCartList(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<CartListResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getMessage());
                    }

                    @Override
                    public void onNext(List<CartListResponse> response) {
                        if (response != null)
                            activity.setNotifCount(response.size());

                    }
                });

    }



    void logout() {
        activity.showProgressDialog(activity.loading);
        activity.getInviseeService().getApi().logout(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getMessage());
                        activity.dismissProgressDialog();

                        new MaterialDialog.Builder(activity)
                                .iconRes(R.mipmap.ic_launcher)
                                .backgroundColor(Color.WHITE)
                                .title(activity.getString(R.string.logout).toUpperCase())
                                .titleColor((Color.BLACK))
                                .content(activity.connectionError)
                                .contentColor((Color.GRAY))
                                .neutralText(R.string.ok)
                                .show();
                    }

                    @Override
                    public void onNext(GenericResponse response) {
                        activity.dismissProgressDialog();
                        if (response.getCode() == 1) {
                            PrefHelper.clearAllPreferences();
                            //SignInActivity.startActivity(activity);
                            LandingPageActivity.startActivity(activity);
                            activity.finish();
                        } else {
                            new MaterialDialog.Builder(activity)
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(activity.getString(R.string.logout).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content(response.getInfo())
                                    .contentColor(Color.GRAY)
                                    .neutralText(R.string.ok)
                                    .show();
                        }

                    }
                });

    }


    //new
    void getInvestmentSummary(){
        activity.hideTextSaldo();
        activity.loadInvestmentAmmount(0);
        activity.getInviseeService().getApi().getInvestmentSummary(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PortfolioInvestmentSummaryResponse>() {
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        activity.progressBar.setVisibility(View.GONE);
                        activity.hideTextSaldo();
                    }

                    @Override
                    public void onNext(PortfolioInvestmentSummaryResponse portfolioInvestmentSummaryResponse){
                        activity.showTextSaldo();
                        if(portfolioInvestmentSummaryResponse.getCode() == 1){
                            activity.loadInvestmentAmmount(portfolioInvestmentSummaryResponse.getTotalMarketValue());
                        }
                    }
                });
    }

    void getCustomerStatus(){
        activity.getInviseeService().getApi().getStatusCustomer(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<StatusCustomerResponse>() {
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(StatusCustomerResponse statusCustomerResponse){
                        if (statusCustomerResponse.getCode() == 0){
                            if(statusCustomerResponse.getData().equalsIgnoreCase("activated")){
                                PrefHelper.setString(PrefKey.CUSTOMER_STATUS, Constant.USER_STATUS_ACTIVE);
                            }else if(statusCustomerResponse.getData().equalsIgnoreCase("pending")){
                                PrefHelper.setString(PrefKey.CUSTOMER_STATUS, Constant.USER_STATUS_PENDING);
                            }else if(statusCustomerResponse.getData().equalsIgnoreCase("verified")){
                                PrefHelper.setString(PrefKey.CUSTOMER_STATUS, Constant.USER_STATUS_VERIFIED);
                            }
                        }
                    }
                });
    }


}
