package com.danareksa.investasik.ui.fragments.promo;

import android.graphics.Color;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.requests.JoinPromoRequest;
import com.danareksa.investasik.data.api.requests.PromoDetailRequest;
import com.danareksa.investasik.data.api.responses.GenericResponse;
import com.danareksa.investasik.data.api.responses.PromoDetailResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.PromoActivity;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import okhttp3.ConnectionPool;
import okhttp3.Dispatcher;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.danareksa.investasik.data.api.InviseeService.BASE_URL;

/**
 * Created by pandu.abbiyuarsyah on 18/05/2017.
 */

public class DetailPromoPresenter {

    private DetailPromoFragment fragment;

    public DetailPromoPresenter(DetailPromoFragment fragment) {
        this.fragment = fragment;
    }

    PromoDetailRequest constructPromoDetailRequest() {
        PromoDetailRequest promoDetailRequest = new PromoDetailRequest();
        promoDetailRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        promoDetailRequest.setCode(fragment.response.getCode());
        return promoDetailRequest;
    }


    JoinPromoRequest construcJoinPromoRequest() {
        JoinPromoRequest joinPromoRequest = new JoinPromoRequest();
        joinPromoRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        joinPromoRequest.setPromo(fragment.response.getCode());
        return joinPromoRequest;
    }


    void getDetailPromo() {
        fragment.showProgressBar();
        fragment.getApi().getPromoDetail(constructPromoDetailRequest())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PromoDetailResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(PromoDetailResponse promoListResponse) {
                        fragment.promoDetail = promoListResponse.getData();
                        //fragment.loadDetail();
                        fragment.response.setTitle(promoListResponse.getData().getTitle());
                        fragment.dismissProgressBar();
                    }
                });
    }


    void getDetailDesc(){

        fragment.showProgressBar();
        Dispatcher dispatcher = new Dispatcher(Executors.newFixedThreadPool(20));
        dispatcher.setMaxRequests(20);
        dispatcher.setMaxRequestsPerHost(1);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .dispatcher(dispatcher)
                .connectionPool(new ConnectionPool(100, 30, TimeUnit.SECONDS))
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(PageAdapter.FACTORY)
                .build();

        PageService api = retrofit.create(PageService.class);
        Call<Page> responseBodyCall =  api.getPromoDesc(constructPromoDetailRequest());
        responseBodyCall.enqueue(new Callback<Page>() {
            @Override
            public void onResponse(Call<Page> call, Response<Page> response) {
                String desc = response.body().content;
                fragment.promoDescription = desc;
                fragment.loadDetail();
                fragment.dismissProgressBar();
            }

            @Override
            public void onFailure(Call<Page> call, Throwable t) {
                fragment.dismissProgressBar();
            }

        });
    }

    void joinPromo() {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().joinPromo(construcJoinPromoRequest())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.dismissProgressDialog();
                    }

                    @Override
                    public void onNext(GenericResponse response) {
                        fragment.dismissProgressDialog();
                        if (response.getCode() == 0 ) {
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.success).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content(response.getInfo())
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.GRAY)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            PromoActivity.startActivity((BaseActivity) fragment.getActivity());
                                        }
                                    })
                                    .cancelable(false)
                                    .show();

                        } else if (response.getCode() == 1) {
                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.infortmation).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content(response.getInfo())
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.ok)
                                    .positiveColor(Color.GRAY)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            PromoActivity.startActivity((BaseActivity) fragment.getActivity());
                                        }
                                    })
                                    .cancelable(false)
                                    .show();
                        } else {
                            fragment.showFailedDialog(response.getInfo());
                        }

                    }
                });
    }


    static class Page {
        String content;
        Page(String content) {
            this.content = content;
        }
    }

    static final class PageAdapter implements Converter<ResponseBody, Page> {

        static final Converter.Factory FACTORY = new Factory() {
            @Override
            public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
                if (type == Page.class) return new PageAdapter();
                return null;
            }

            @Override
            public Converter<?, RequestBody> requestBodyConverter(Type type, Annotation[] parameterAnnotations, Annotation[] methodAnnotations, Retrofit retrofit) {
                Gson gson = new Gson();
                TypeAdapter<?> adapter = gson.getAdapter(TypeToken.get(type));
                return new GsonRequestBodyConverter<>(gson, adapter);

            }
        };

        @Override
        public Page convert(okhttp3.ResponseBody value) throws IOException {
            return new Page(value.string());
        }
    }

    static final class GsonRequestBodyConverter<T> implements Converter<T, RequestBody> {
        private final MediaType MEDIA_TYPE = MediaType.parse("application/json; charset=UTF-8");
        private final Charset UTF_8 = Charset.forName("UTF-8");

        private final Gson gson;
        private final TypeAdapter<T> adapter;

        GsonRequestBodyConverter(Gson gson, TypeAdapter<T> adapter) {
            this.gson = gson;
            this.adapter = adapter;
        }

        @Override public RequestBody convert(T value) throws IOException {
            Buffer buffer = new Buffer();
            Writer writer = new OutputStreamWriter(buffer.outputStream(), UTF_8);
            JsonWriter jsonWriter = gson.newJsonWriter(writer);
            adapter.write(jsonWriter, value);
            jsonWriter.close();
            return RequestBody.create(MEDIA_TYPE, buffer.readByteString());
        }
    }

    interface PageService {
        @POST("promo/promo_detail?token=userNotLogin")
        Call<Page> getPromoDesc(@Body PromoDetailRequest request);
    }
}