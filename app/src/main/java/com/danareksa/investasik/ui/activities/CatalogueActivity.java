package com.danareksa.investasik.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.ProductList;
import com.danareksa.investasik.ui.fragments.catalogue.DetailOfCatalogueFragment;

import butterknife.Bind;
import icepick.State;

public class CatalogueActivity extends BaseActivity {

    private static final String PACKAGES = "packages";

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;

    @State
    Packages packages;

    @State
    ProductList products;

    public static Activity catalogueActivity;

    public static void startActivity(BaseActivity sourceActivity, ProductList packages) {
        Intent intent = new Intent(sourceActivity, CatalogueActivity.class);
        intent.putExtra(PACKAGES, packages);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_catalogue;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        catalogueActivity = this;
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(null);
        }

        products = (ProductList) getIntent().getSerializableExtra(PACKAGES);
        title.setText(products.getName());
        DetailOfCatalogueFragment.showFragment(this, products);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        if (IS_FROM_LANDING) {
            startActivity(new Intent(CatalogueActivity.this, LandingPageActivity.class));
        }else{
            startActivity(new Intent(CatalogueActivity.this, ListOfCatalogueActivity.class));
        }
        CatalogueActivity.this.finish();
    }

    public void setTitleName(String title){
        this.title.setText(title);
    }
}
