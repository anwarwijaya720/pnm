package com.danareksa.investasik.ui.fragments.cart;

import android.graphics.Color;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.Fee;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.responses.CartListResponse;
import com.danareksa.investasik.data.api.responses.FundAllocationResponse;
import com.danareksa.investasik.data.api.responses.GenericResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.MainActivity;

import java.util.List;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by fajarfatur on 2/2/16.
 */

public class CartPresenter {

    private CartFragment fragment;

    public CartPresenter(CartFragment fragment){
        this.fragment = fragment;
    }

    private Subscription cartSubscribetion;

    public void cartList(){
        fragment.showProgressBar();
        cartSubscribetion = fragment.getApi().getCartList(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<CartListResponse>>() {
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.connectionError();
                        fragment.getActivity().finish();
                        fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(List<CartListResponse> response){

                        if((BaseActivity) fragment.getActivity() != null){
                            ((BaseActivity) fragment.getActivity()).setNotifCount(0); //error
                        }

                        if (response != null && response.size() > 0){
                            ((BaseActivity) fragment.getActivity()).setNotifCount(response.size());
                            fragment.cartList = response;
                            fragment.loadRule();
                            fragment.dismissProgressBar();
                        } else {
                            fragment.dismissProgressBar();

                            //ViewDialog alert = new ViewDialog();
                            //alert.showDialog(fragment.getActivity(), "Keranjang Anda kosong", "INFORMASI");

                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.infortmation).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content("Keranjang Anda kosong")
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.back)
                                    .positiveColor(Color.GRAY)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            MainActivity.startActivity((BaseActivity) fragment.getActivity());
                                            fragment.getActivity().finish();
                                        }
                                    })
                                    .cancelable(false)
                                    .show();
                        }


                    }
                });
    }


    public void subscriptionFee(final Packages packages, final Boolean isLastItem, final int index) {
        cartSubscribetion = fragment.getApi().subscriptionFee(packages.getId(), 1l, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Fee>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                    }

                    @Override
                    public void onNext(List<Fee> list){
                        System.out.println("subs");
                        if (list.size() > 0){
                            fragment.setFee(index, list);
                            if (isLastItem) {
                                fragment.loadList();
                                fragment.dismissProgressDialog();
                            }
                        }
                    }
                });

    }



    public void deleteCart(int id, final CartListResponse item){
        fragment.showProgressDialog(fragment.loading);
        cartSubscribetion = fragment.getApi().deleteCart(Integer.toString(id), PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>() {
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                    }

                    @Override
                    public void onNext(GenericResponse response){
                        fragment.dismissProgressDialog();
                        if (response.getCode() == 1){
                            fragment.adapter.remove(item);

                            if (fragment.adapter.getList().size() <= 0)
                                cartList();

                        } else {
                            Toast.makeText(fragment.getActivity(), response.getInfo(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }



    public void fundAllocationFromAdapter(Long id, final String type) {
        fragment.showProgressDialog(fragment.loading);
        cartSubscribetion = fragment.getApi().fundAllocationInfo(id, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<FundAllocationResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                    }

                    @Override
                    public void onNext(FundAllocationResponse response) {
                        fragment.dismissProgressDialog();
                        if (response.getCode() == 1) {
                            if(type.equals("prospectus")){
                                fragment.download(response.getData().get(0), "prospectus");
                            }else if(type.equals("ffs")){
                                fragment.download(response.getData().get(0), "ffs");
                            }

                        }
                    }
                });

    }

    public void cartListToDownload(final int position, final String type){
        fragment.showProgressDialog(fragment.loading);
        cartSubscribetion = fragment.getApi().getCartList(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<CartListResponse>>() {
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                    }

                    @Override
                    public void onNext(List<CartListResponse> response) {

                        ((BaseActivity) fragment.getActivity()).setNotifCount(0);

                        if (response != null && response.size() > 0) {
                            fragment.dismissProgressDialog();
                            ((BaseActivity) fragment.getActivity()).setNotifCount(response.size());
                            fragment.cartList = response;
                            fundAllocationFromAdapter(response.get(position).getPackages().getId(), type);
                            fragment.loadRule();
                        } else {
                            fragment.dismissProgressDialog();

                            //ViewDialog alert = new ViewDialog();
                            //alert.showDialog(fragment.getActivity(), "Keranjang Anda kosong", "INFORMASI");

                            new MaterialDialog.Builder(fragment.getActivity())
                                    .iconRes(R.mipmap.ic_launcher)
                                    .backgroundColor(Color.WHITE)
                                    .title(fragment.getString(R.string.infortmation).toUpperCase())
                                    .titleColor(Color.BLACK)
                                    .content("Keranjang Anda kosong")
                                    .contentColor(Color.GRAY)
                                    .positiveText(R.string.back)
                                    .positiveColor(Color.GRAY)
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(MaterialDialog dialog, DialogAction which) {
                                            fragment.getActivity().finish();
                                        }
                                    })
                                    .cancelable(false)
                                    .show();
                        }
                    }
                });
    }



   public void cleanResource(){
        if(cartSubscribetion != null){
            cartSubscribetion.unsubscribe();
        }
    }




}
