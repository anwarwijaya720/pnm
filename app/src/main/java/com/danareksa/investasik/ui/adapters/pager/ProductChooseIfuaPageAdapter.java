package com.danareksa.investasik.ui.adapters.pager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.danareksa.investasik.data.api.beans.InvestmentAccountGroup;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.ProductList;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.ui.fragments.product.LumspumIfuaFragment;
import com.danareksa.investasik.ui.fragments.product.RegulerIfuaFragment;

/**
 * Created by asep.surahman on 11/06/2018.
 */

public class ProductChooseIfuaPageAdapter  extends FragmentPagerAdapter{

    final int PAGE_COUNT = 2;
    private BaseFragment bFragment;
    private ProductList product;
    private Packages packages;
    private InvestmentAccountGroup investmentAccountGroup;

    public ProductChooseIfuaPageAdapter(BaseFragment bFragment, FragmentManager fm, ProductList product, Packages packages) {
        super(fm);
        this.bFragment = bFragment;
        this.product = product;
        this.packages = packages;
    }

    public ProductChooseIfuaPageAdapter(BaseFragment bFragment, FragmentManager fm, ProductList product, Packages packages, InvestmentAccountGroup accountGroup) {
        super(fm);
        this.bFragment = bFragment;
        this.product = product;
        this.packages = packages;
        this.investmentAccountGroup = accountGroup;
    }

    @Override
    public int getCount(){
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = LumspumIfuaFragment.getFragment(product, packages);
                ((LumspumIfuaFragment) fragment).lumpsum = this.investmentAccountGroup.getLumpsum();
                break;
            case 1:
                fragment = RegulerIfuaFragment.getFragment(product, packages);
                ((RegulerIfuaFragment) fragment).reguler = this.investmentAccountGroup.getReguler();
                break;
        }
        return fragment;
    }



}
