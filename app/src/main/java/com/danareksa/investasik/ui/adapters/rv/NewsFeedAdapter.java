package com.danareksa.investasik.ui.adapters.rv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.News;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by fajarfatur on 3/6/16.
 */
public class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.NewsFeedHolder> {

    private Context context;
    private List<News> list;

    public NewsFeedAdapter(Context context, List<News> list){
        this.context = context;
        this.list = list;
    }

    @Override
    public NewsFeedHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_article, parent, false);
        NewsFeedHolder catalogueHolder = new NewsFeedHolder(itemView);
        return catalogueHolder;
    }

    @Override
    public void onBindViewHolder(NewsFeedHolder holder, int position){
        final News item = list.get(position);
        holder.itemView.setTag(item);
        holder.tvNewsTitle.setText(item.getNewsTitle());
        holder.tvNewsContent.setText(Html.fromHtml(item.getNewsContent()).toString());

        Picasso.with(context).load(InviseeService.IMAGE_DOWNLOAD_URL + item.getImageLocation())
                .placeholder(R.drawable.ic_logo_launcher)
                .error(R.drawable.ic_logo_launcher)
                .into(holder.ivPhoto);

    }


    @Override
    public int getItemCount(){
        return list != null ? list.size() : 0;
    }


    public static class NewsFeedHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.ivPhoto)
        CircleImageView ivPhoto;
        @Bind(R.id.tvtTime)
        TextView tvtTime;
        @Bind(R.id.tvNewsTitle)
        TextView tvNewsTitle;
        @Bind(R.id.tvNewsContent)
        TextView tvNewsContent;
        @Bind(R.id.tvtNext)
        TextView tvtNext;
        public NewsFeedHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }



}
