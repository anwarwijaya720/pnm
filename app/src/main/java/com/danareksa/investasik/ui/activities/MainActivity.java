package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.pojo.Menu;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.adapters.rv.DrawerAdapter;
import com.danareksa.investasik.ui.fragments.contact_us.ContactUsFragment;
import com.danareksa.investasik.ui.fragments.new_dashboard.NewDashboardFragment;
import com.danareksa.investasik.ui.fragments.new_dashboard.NewDashboardFragment2;
import com.danareksa.investasik.ui.fragments.privacypolicy.PrivacyPolicyFragment;
import com.danareksa.investasik.ui.fragments.support.SupportFragment;
import com.danareksa.investasik.ui.fragments.termsandcondition.TermsAndConditionFragment;
import com.danareksa.investasik.util.AmountFormatter;
import com.danareksa.investasik.util.ui.RecyclerItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by fajarfatur on 1/14/16.
 */
public class MainActivity extends BaseActivity {

    @Bind(R.id.ivPhotoProfile)
    CircleImageView ivPhotoProfile;
    @Bind(R.id.txvCustomerStatus)
    TextView txvCustomerStatus;
    @Bind(R.id.tvName)
    TextView tvName;
    @Bind(R.id.textUser)
    TextView textUser;
    @Bind(R.id.ivUser)
    CircleImageView ivUser;
    @Bind(R.id.textSaldo)
    TextView textSaldo;
    @Bind(R.id.tvSaldo)
    TextView tvSaldo;
    @Bind(R.id.progressBar)
    ProgressBar progressBar;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;
    @Bind(R.id.navigationView)
    NavigationView navigationView;
    @Bind(R.id.drawer)
    DrawerLayout drawer;
    @Bind(R.id.rv)
    RecyclerView rv;

    @Bind(R.id.rlProfile)
    RelativeLayout rlProfile;
    @Bind(R.id.imageView1)
    ImageView imgView1;
    @Bind(R.id.imgBackImage)
    ImageView imgView2;
    @Bind(R.id.appBar)
    AppBarLayout appBarLayout;
    @Bind(R.id.lvContainer)
    LinearLayout lvContainer;

    @BindString(R.string.menu_menu)
    String menu;
    @BindString(R.string.menu_dashboard)
    String menuDashboard;
    @BindString(R.string.menu_portfolio)
    String menuPortfolio;
    @BindString(R.string.menu_wallet)
    String menuWallet;
    @BindString(R.string.menu_reminder)
    String menuReminder;
    @BindString(R.string.menu_transaction)
    String menuTransaction;
    @BindString(R.string.menu_catalogue)
    String menuCatalogue;
    @BindString(R.string.menu_support)
    String menuSupport;
    @BindString(R.string.menu_setting)
    String menuSetting;
    @BindString(R.string.menu_FAQ)
    String menuFAQ;
    @BindString(R.string.menu_terms_and_condition)
    String menuTermsAndCondition;
    @BindString(R.string.menu_privacy_policy)
    String menuPrivacyPolicy;
    @BindString(R.string.menu_contactus)
    String menuContactUs;
    @BindString(R.string.menu_user_profile)
    String menuUserProfile;
    @BindString(R.string.menu_logout)
    String menuLogout;

    @BindDrawable(R.drawable.ic_dashboard_dr)
    Drawable icDashboard;
    @BindDrawable(R.drawable.ic_faq_dr)
    Drawable icFaq;
    @BindDrawable(R.drawable.ic_kebijakan_privasi_dr)
    Drawable icPrivacyPolicy;
    @BindDrawable(R.drawable.ic_syarat_ketentuan_dr)
    Drawable icTermsAndConditions;
    @BindDrawable(R.drawable.ic_hubungi_kami_dr)
    Drawable icContact;
    @BindDrawable(R.drawable.ic_keluar_dr)
    Drawable icLogout;

    @BindDrawable(R.drawable.ic_dashboard_dr)
    Drawable icdashboardpnm;
    @BindDrawable(R.drawable.icon_navigation_pnm)
    Drawable icNavigationpnm;
    @BindDrawable(R.drawable.ic_keluar_dr)
    Drawable icLogoutpnm;

    ViewGroup.LayoutParams params;
    private MainPresenter presenter;
    private Menu selectedMenu;

    public static void startActivity(BaseActivity sourceActivity) {
        Intent intent = new Intent(sourceActivity, MainActivity.class);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout(){
        return R.layout.a_main2;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupToolbar();

        showProgressDialog(loading);
        ButterKnife.bind(this);
        setNotifCount(0);
        presenter = new MainPresenter(this);
        logUser();
        initDrawer();
        initMenuRV();
        populateFirstMenu();
        customerStatus();
        dismissProgressDialog();
        params = lvContainer.getLayoutParams();
    }

    private void logUser() {
        if (BuildConfig.ENABLE_CRASHLYTICS){
            Crashlytics.setUserIdentifier(PrefHelper.getString(PrefKey.PREF_UNAME));
            Crashlytics.setUserEmail(PrefHelper.getString(PrefKey.EMAIL));
            Crashlytics.setUserName(PrefHelper.getString(PrefKey.FIRST_NAME));
        }
    }

    public void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        if(!BuildConfig.FLAVOR.contentEquals("pnm")){
            title.setText("Dashboard");
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        System.out.println("================>>>>resume ");
        presenter.cartList();
        textUser.setText(PrefHelper.getString(PrefKey.FIRST_NAME));
        try {
            Picasso.with(this).load(InviseeService.IMAGE_DOWNLOAD_URL + PrefHelper.getString(PrefKey.IMAGE) + "&token=" + PrefHelper.getString(PrefKey.TOKEN))
                    .error(R.drawable.ic_profile_avatar)
                    .into(ivUser);

        }catch (Exception e){
            ivUser.setImageResource(R.drawable.ic_profile_avatar);
        }

        tvName.setText(PrefHelper.getString(PrefKey.FIRST_NAME));
        setNotifCount(PrefHelper.getInt(PrefKey.CART));

        try{

            Picasso.with(this).load(InviseeService.IMAGE_DOWNLOAD_URL + PrefHelper.getString(PrefKey.IMAGE) + "&token=" + PrefHelper.getString(PrefKey.TOKEN))
                    .error(R.drawable.ic_profile_avatar)
                    .into(ivPhotoProfile);

        }catch (Exception e){
            ivPhotoProfile.setImageResource(R.drawable.ic_profile_avatar);
        }

        presenter.getCustomerStatus();
        presenter.getInvestmentSummary();
    }


    @OnClick(R.id.nav_header)
    void toUserProfile(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        UserProfileActivity.startActivity(this);
    }


    @OnClick(R.id.lProf)
    void toProfile(){
        UserProfileActivity.startActivity(this);
    }

    private void populateFirstMenu() {
        if(selectedMenu != null){
            rootingMenu(selectedMenu);
        }else{
            NewDashboardFragment2.showFragment(this);
        }
    }

    private void initDrawer() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.open, R.string.close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (selectedMenu != null) {
                    rootingMenu(selectedMenu);
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                hideKeyboard();
            }
        };
        drawer.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    public void customerStatus() {
        if (PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT")) {
            txvCustomerStatus.setText("Active");
        } else if (PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")) {
            txvCustomerStatus.setText("Pending");
        } else if (PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")) {
            txvCustomerStatus.setText("Verified");
        }
    }

    private void initMenuRV(){
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(new DrawerAdapter(this, initDrawerMenu()));
        rv.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position){
                super.onItemClick(childView, position);
                Menu menu = (Menu) childView.getTag();
                selectedMenu = menu;
                drawer.closeDrawers();
            }
        }));
    }

    private List<Menu> initDrawerMenu(){
        List<Menu> menuList = new ArrayList<>();

        if (BuildConfig.FLAVOR.contentEquals("btn")){
            menuList.add(new Menu(R.string.menu_dashboard, icDashboard, menuDashboard));
            menuList.add(new Menu(R.string.menu_terms_and_condition, icTermsAndConditions, menuTermsAndCondition));
            menuList.add(new Menu(R.string.menu_privacy_policy, icPrivacyPolicy, menuPrivacyPolicy));
            menuList.add(new Menu(R.string.menu_FAQ, icFaq, menuFAQ));
            menuList.add(new Menu(R.string.menu_contactus, icContact, menuContactUs));
            menuList.add(new Menu(R.string.menu_logout, icLogout, menuLogout));
        }else if (BuildConfig.FLAVOR.contentEquals("pnm")){
            menuList.add(new Menu(R.string.menu_dashboard, icNavigationpnm, menuDashboard));
            menuList.add(new Menu(R.string.menu_terms_and_condition, icNavigationpnm, menuTermsAndCondition));
            menuList.add(new Menu(R.string.menu_privacy_policy, icNavigationpnm, menuPrivacyPolicy));
            menuList.add(new Menu(R.string.menu_FAQ, icNavigationpnm, menuFAQ));
            menuList.add(new Menu(R.string.menu_contactus, icNavigationpnm, menuContactUs));
            menuList.add(new Menu(R.string.menu_logout, icNavigationpnm, menuLogout));
        }
        else {
            menuList.add(new Menu(R.string.menu_dashboard, icDashboard, menuDashboard));
            menuList.add(new Menu(R.string.menu_FAQ, icFaq, menuFAQ));
            menuList.add(new Menu(R.string.menu_privacy_policy, icPrivacyPolicy, menuPrivacyPolicy));
            menuList.add(new Menu(R.string.menu_terms_and_condition, icTermsAndConditions, menuTermsAndCondition));
            menuList.add(new Menu(R.string.menu_contactus, icContact, menuContactUs));
            menuList.add(new Menu(R.string.menu_logout, icLogout, menuLogout));
        }

        return menuList;
    }

    private void updateLayout(boolean isDefault){
        LinearLayout.LayoutParams par = (LinearLayout.LayoutParams) params;
        if (isDefault) {
            imgView1.setVisibility(View.VISIBLE);
            imgView2.setVisibility(View.VISIBLE);
            rlProfile.setVisibility(View.VISIBLE);
            appBarLayout.setBackgroundColor(Color.TRANSPARENT);
            par.weight = 1.5f;
            lvContainer.setLayoutParams(par);
        } else {
            imgView1.setVisibility(View.GONE);
            imgView2.setVisibility(View.GONE);
            rlProfile.setVisibility(View.GONE);
            appBarLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            par.weight = 9.5f;
            lvContainer.setLayoutParams(par);
        }
    }

    private void rootingMenu(Menu menu) {
        if (menu == null) return;
        selectedMenu = menu;
        switch (menu.getId()) {
            case R.string.menu_dashboard:
                updateLayout(true);
                title.setText("Dashboard");
                NewDashboardFragment2.showFragment(this);
                break;
            case R.string.menu_FAQ:
                updateLayout(false);
                title.setText("F.A.Q");
                SupportFragment.showFragment(this);
                break;
            case R.string.menu_terms_and_condition:
                updateLayout(false);
                title.setText("Syarat dan Ketentuan");
                TermsAndConditionFragment.showFragment(this);
                break;
            case R.string.menu_privacy_policy:
                updateLayout(false);
                title.setText("Kebijakan Privasi");
                PrivacyPolicyFragment.showFragment(this);
                break;
             case R.string.menu_contactus:
                 LinearLayout.LayoutParams par = (LinearLayout.LayoutParams) params;
                 imgView1.setVisibility(View.GONE);
                 imgView2.setVisibility(View.GONE);
                 rlProfile.setVisibility(View.GONE);
                 appBarLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                 par.weight = 11.5f;
                 lvContainer.setLayoutParams(par);
                 title.setText("Hubungi Kami");
                 ContactUsFragment.showFragment(this);
                break;
            case R.string.menu_logout:

                new MaterialDialog.Builder(this)
                        .iconRes(R.mipmap.ic_launcher)
                        .backgroundColor(Color.WHITE)
                        .title(getString(R.string.logout).toUpperCase())
                        .titleColor(Color.BLACK)
                        .content(R.string.are_u_sure)
                        .contentColor(Color.GRAY)
                        .positiveText(R.string.ya)
                        .positiveColor(Color.GRAY)
                        .negativeText(R.string.no)
                        .negativeColor(getResources().getColor(R.color.colorPrimary))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                                presenter.logout();
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();

                break;

        }
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if (currentFragment instanceof NewDashboardFragment || currentFragment instanceof NewDashboardFragment2) {
                    Snackbar snackbar = Snackbar.make(MainActivity.this.findViewById(android.R.id.content), R.string.exit_apps, Snackbar.LENGTH_LONG).setAction(R.string.yes, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
                View view = snackbar.getView();
                TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_action);
                tv.setTypeface(null, Typeface.BOLD);
                snackbar.show();
            } else {
                selectedMenu = null;
                updateLayout(true);
                title.setText("Dashboard");
                NewDashboardFragment2.showFragment(this);
            }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    void loadInvestmentAmmount(double Amount){
        textSaldo.setText(AmountFormatter.formatCurrencyWithoutComma(Amount));
        tvSaldo.setText(AmountFormatter.formatCurrencyWithoutComma(Amount));
    }

    @OnClick(R.id.textSaldo)
    void showInvestationDetail() {
        Intent intent = new Intent(this, DashboardActivity.class);
        startActivity(intent);
    }

    public void hideTextSaldo(){
        textSaldo.setVisibility(View.GONE);
        tvSaldo.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    public void showTextSaldo(){
        textSaldo.setVisibility(View.VISIBLE);
        tvSaldo.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }


}
