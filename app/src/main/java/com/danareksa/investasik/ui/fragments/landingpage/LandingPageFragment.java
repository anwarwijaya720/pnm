package com.danareksa.investasik.ui.fragments.landingpage;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.News;
import com.danareksa.investasik.data.api.beans.ProductList;
import com.danareksa.investasik.data.api.beans.PromoResponse;
import com.danareksa.investasik.data.api.beans.Slider;
import com.danareksa.investasik.data.api.beans.Slideshow;
import com.danareksa.investasik.ui.activities.ArticleSlideshowActivity;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.CatalogueActivity;
import com.danareksa.investasik.ui.activities.DetailPromoActivity;
import com.danareksa.investasik.ui.activities.FaqActivity;
import com.danareksa.investasik.ui.activities.LandingPageActivity;
import com.danareksa.investasik.ui.activities.MainActivity;
import com.danareksa.investasik.ui.activities.NewsActivity;
import com.danareksa.investasik.ui.activities.RegisterNasabahActivity;
import com.danareksa.investasik.ui.activities.SignInActivity;
import com.danareksa.investasik.ui.activities.TermsAndConditionActivity;
import com.danareksa.investasik.ui.activities.UserProfileActivity;
import com.danareksa.investasik.ui.activities.WebViewActivity;
import com.danareksa.investasik.ui.activities.YouTubePlayerActivity;
import com.danareksa.investasik.ui.adapters.rv.LandingPageAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.ui.fragments.promo.PromoFragment;
import com.danareksa.investasik.util.ui.RecyclerItemClickListener;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by asep.surahman on 28/03/2018.
 */

public class LandingPageFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = PromoFragment.class.getSimpleName();

    @Bind(R.id.rv)
    RecyclerView rv;
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.btnMasuk)
    Button btnMasuk;
    @Bind(R.id.btnDaftar)
    Button btnDaftar;

    @Bind(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;


    @Override
    protected int getLayout() {
        return R.layout.f_landingpage;
    }

    public List<Slider> listSlider;
    public List<Slideshow> listSlideshow;
    private LandingPagePresenter presenter;

    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, R.anim.slide_out_left, android.R.anim.slide_in_left, R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.container, new LandingPageFragment(), TAG);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        presenter = new LandingPagePresenter(this);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        presenter.getSlideshowPageList();
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 1 : 1;
            }
        });
        rv.setLayoutManager(layoutManager);
        rv.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.SimpleOnItemClickListener(){
            @Override
            public void onItemClick(View childView, int position){
                super.onItemClick(childView, position);

                Slideshow slideItem = (Slideshow) childView.getTag();
                handleClick(slideItem);
            }
        }));
    }

    @Override
    public void onResume(){
        super.onResume();

        swipe_container.setOnRefreshListener(this);
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);
                presenter.getSlideshowPageList();
            }
        });

    }

    private void handleClick(Slideshow slideshow) {
        if (slideshow != null) {

            String type = slideshow.getType();
            if(type.equals("ARTICLE")){
                presenter.getNewsDetail(slideshow.getUrl());
            }else if(type.equals("PRODUCT")){
                showProductDetail(slideshow);
            }else if(type.equals("PROMO")){
                presenter.getDetailPromo(slideshow.getUrl(), slideshow.getImage());
            }else if(type.equals("YOUTUBE")) {

                YouTubePlayerActivity.VIDEO_ID = slideshow.getUrl();
                YouTubePlayerActivity.IS_FROM_LANDING = true;
                Intent intent = new Intent(getActivity(), YouTubePlayerActivity.class);
                startActivity(intent);

            } else if (type.equals("OTHER")) {

                String url = slideshow.getUrl();

                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;

                this.getActivity().invalidateOptionsMenu();
                WebViewActivity.HIDE_CART = true;
                WebViewActivity.IS_FROM_LANDING = true;
                WebViewActivity.startActivity((LandingPageActivity) getActivity(), url);

            }

        }
    }

    public void showNewsDetail(News news){
        this.getActivity().invalidateOptionsMenu();
        NewsActivity.HIDE_CART = true;
        NewsActivity.IS_FROM_LANDING = true;
        NewsActivity.startActivity((LandingPageActivity) getActivity(), news);
    }

    public void showProductDetail(Slideshow p){
        this.getActivity().invalidateOptionsMenu();
        CatalogueActivity.HIDE_CART = true;
        CatalogueActivity.IS_FROM_LANDING = true;
        ProductList products = new ProductList();
        products.setId(Long.parseLong(p.getUrl()));
        products.setName(p.getTitle());
        CatalogueActivity.startActivity((LandingPageActivity) getActivity(), products);
    }

    public void showPromoDetail(String code, String key, String title) {
        this.getActivity().invalidateOptionsMenu();
        DetailPromoActivity.HIDE_CART = true;
        DetailPromoActivity.IS_FROM_LANDING = true;
        PromoResponse param = new PromoResponse();
        param.setCode(code);
        param.setImage_android(key);
        param.setTitle(title);
        DetailPromoActivity.startActivity((LandingPageActivity) getActivity(), param);
    }

//    public void showFAQDetail() {
//        this.getActivity().invalidateOptionsMenu();
//        FaqActivity.HIDE_CART = true;
//        FaqActivity.IS_FROM_LANDING = true;
//        FaqResponse param = new FaqResponse();
//        FaqActivity.startActivity((LandingPageActivity) getActivity(), param);
//    }

    public void getListSlider() {
        rv.setAdapter(new LandingPageAdapter(getActivity(), listSlideshow));
    }


    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.tvTryAgain)
    void retryConnection() {
        presenter.getSlideshowPageList();
    }

    @OnClick(R.id.card_faq)
    void cardFaq(){
        FaqActivity.startActivity((BaseActivity) getActivity());
    }

    @OnClick(R.id.card_terms_n_conditions)
    void cardTerms(){
        TermsAndConditionActivity.startActivity((BaseActivity) getActivity());
    }

    @OnClick(R.id.btnMasuk)
    void btnMasuk(){
        SignInActivity.startActivity((BaseActivity) getActivity());
    }

    @OnClick(R.id.btnDaftar)
    void btnDaftar(){
        RegisterNasabahActivity.startActivity((BaseActivity) getActivity());
    }

    @OnClick(R.id.card_article)
    void btnArtikel(){
        ArticleSlideshowActivity.startActivity((BaseActivity) getActivity());
    }

    void showDialog(String info){
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.infortmation).toUpperCase())
                .titleColor(Color.BLACK)
                .content(info)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        MainActivity.startActivity((BaseActivity) getActivity());
                        UserProfileActivity.userProfileActivity.finish();
                        getActivity().finish();
                    }
                })
                .show();
    }


    @Override
    public void onRefresh() {
        presenter.getSlideshowPageList();
    }
}
