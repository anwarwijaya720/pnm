package com.danareksa.investasik.ui.fragments.article;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.News;
import com.danareksa.investasik.data.api.responses.CartListResponse;
import com.danareksa.investasik.data.api.responses.NewsFeedResponse;
import com.danareksa.investasik.ui.activities.ArticleActivity;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.NewsActivity;
import com.danareksa.investasik.ui.adapters.rv.NewsFeedAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.ui.RecyclerItemClickListener;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import icepick.State;

/**
 * Created by asep.surahman on 02/05/2018.
 */

public class ArticleFragment  extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener{

    public static final String TAG = ArticleFragment.class.getSimpleName();


    @Bind(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;

    @Bind(R.id.rv)
    RecyclerView rv;
    @State
    NewsFeedResponse response;
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;

    ArticlePresenter presenter;
    public List<News> listArticle;
    public List<CartListResponse> cartList;

    @Override
    protected int getLayout() {
        return R.layout.f_list_of_article;
    }

    @Override
    public void onResume(){
        super.onResume();
        getActivity().setTitle("Article");
        //getCartList();

        swipe_container.setOnRefreshListener(this);
        swipe_container.post(new Runnable() {
            @Override
            public void run() {
                swipe_container.setRefreshing(true);
                presenter.articleList();
            }
        });

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ArticlePresenter(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 1 : 1;
            }
        });

        rv.setLayoutManager(layoutManager);
        rv.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.SimpleOnItemClickListener(){
            @Override
            public void onItemClick(View childView, int position){
                super.onItemClick(childView, position);
                News news = (News) childView.getTag();
                NewsActivity.startActivity((ArticleActivity) getActivity(), news);
            }
        }));

    }


    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            fragmentTransaction.replace(R.id.container, new ArticleFragment(), TAG);
            fragmentTransaction.commit();
        }
    }


    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }


    public void loadList(){
        rv.setAdapter(new NewsFeedAdapter(getActivity(), listArticle));
    }

    @OnClick(R.id.tvTryAgain)
    void retryConnection(){
        presenter.articleList();
    }


    public void getCartList() {
        presenter.cartList();
    }

    @Override
    public void onRefresh() {
        presenter.articleList();
    }
}
