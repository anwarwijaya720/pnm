package com.danareksa.investasik.ui.fragments.articleslideshow;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.ProductList;
import com.danareksa.investasik.data.api.beans.PromoResponse;
import com.danareksa.investasik.data.api.beans.Slideshow;
import com.danareksa.investasik.ui.activities.ArticleSlideshowActivity;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.CatalogueActivity;
import com.danareksa.investasik.ui.activities.DetailPromoActivity;
import com.danareksa.investasik.ui.activities.WebViewActivity;
import com.danareksa.investasik.ui.activities.YouTubePlayerActivity;
import com.danareksa.investasik.ui.adapters.rv.LandingPageAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.ui.RecyclerItemClickListener;

import java.util.List;

import butterknife.Bind;


public class ArticleSlideshowFragment extends BaseFragment {
    public static final String TAG = ArticleSlideshowFragment.class.getSimpleName();

    @Bind(R.id.rv)
    RecyclerView rv;
    @Bind(R.id.swipe_container)
    SwipeRefreshLayout swipe_container;
//    @Bind(R.id.lnProgressBar)
//    LinearLayout lnProgressBar;
//    @Bind(R.id.lnDismissBar)
//    RelativeLayout lnDismissBar;
//    @Bind(R.id.pbLoading)
//    ProgressBar pbLoading;
//    @Bind(R.id.lnConnectionError)
//    LinearLayout lnConnectionError;

    public List<Slideshow> listSlideshow;

    private ArticleSlideshowPresenter presenter;

    @Override
    protected int getLayout() {
        return R.layout.f_article_slideshow;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ArticleSlideshowPresenter(this);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.getSlideshowPageList();
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 1 : 1;
            }
        });
        rv.setLayoutManager(layoutManager);
        rv.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.SimpleOnItemClickListener(){
            @Override
            public void onItemClick(View childView, int position){
                super.onItemClick(childView, position);

                Slideshow slideItem = (Slideshow) childView.getTag();
                handleClick(slideItem);
            }
        }));
    }

    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            fragmentTransaction.replace(R.id.container, new ArticleSlideshowFragment(), TAG);
            fragmentTransaction.commit();
        }
    }

    public void getListSlider() {
        rv.setAdapter(new LandingPageAdapter(getActivity(), listSlideshow));
    }

//    public void showProgressBar(){
//        pbLoading.setVisibility(View.VISIBLE);
//        lnConnectionError.setVisibility(View.GONE);
//        lnProgressBar.setVisibility(View.VISIBLE);
//        lnDismissBar.setVisibility(View.GONE);
//    }
//
//    public void dismissProgressBar(){
//        lnProgressBar.setVisibility(View.GONE);
//        lnDismissBar.setVisibility(View.VISIBLE);
//    }
//
//    public void connectionError() {
//        lnProgressBar.setVisibility(View.VISIBLE);
//        lnDismissBar.setVisibility(View.GONE);
//        pbLoading.setVisibility(View.GONE);
//        lnConnectionError.setVisibility(View.VISIBLE);
//    }

    private void handleClick(Slideshow slideshow) {
        if (slideshow != null) {

            String type = slideshow.getType();
            if(type.equals("ARTICLE")){
                presenter.getNewsDetail(slideshow.getUrl());
            }else if(type.equals("PRODUCT")){
                showProductDetail(slideshow);
            }else if(type.equals("PROMO")){
                presenter.getDetailPromo(slideshow.getUrl(), slideshow.getImage());
            }else if(type.equals("YOUTUBE")) {

                YouTubePlayerActivity.VIDEO_ID = slideshow.getUrl();
                YouTubePlayerActivity.IS_FROM_LANDING = true;
                Intent intent = new Intent(getActivity(), YouTubePlayerActivity.class);
                startActivity(intent);

            } else if (type.equals("OTHER")) {

                String url = slideshow.getUrl();

                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;

                this.getActivity().invalidateOptionsMenu();
                WebViewActivity.HIDE_CART = true;
                WebViewActivity.IS_FROM_LANDING = true;
                WebViewActivity.startActivity((ArticleSlideshowActivity) getActivity(), url);

            }

        }
    }

    public void showProductDetail(Slideshow p){
        this.getActivity().invalidateOptionsMenu();
        CatalogueActivity.HIDE_CART = true;
        CatalogueActivity.IS_FROM_LANDING = true;
        ProductList products = new ProductList();
        products.setId(Long.parseLong(p.getUrl()));
        products.setName(p.getTitle());
        CatalogueActivity.startActivity((ArticleSlideshowActivity) getActivity(), products);
    }

    public void showPromoDetail(String code, String key, String title) {
        this.getActivity().invalidateOptionsMenu();
        DetailPromoActivity.HIDE_CART = true;
        DetailPromoActivity.IS_FROM_LANDING = true;
        PromoResponse param = new PromoResponse();
        param.setCode(code);
        param.setImage_android(key);
        param.setTitle(title);
        DetailPromoActivity.startActivity((ArticleSlideshowActivity) getActivity(), param);
    }

}
