package com.danareksa.investasik.ui.fragments.riwayattransaksi;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.TransactionHistory;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.adapters.rv.TransactionHistoryAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.AmountFormatter;
import com.danareksa.investasik.util.DateUtil;
import com.danareksa.investasik.util.ui.RecyclerItemClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import icepick.State;

/**
 * Created by asep.surahman on 29/06/2018.
 */

public class ListOfRiwayatTransaksiFragment extends BaseFragment {


    private static final String TAG = "ListOfRiwayatTransaksiFragment";
    private static final String PARAMS = "PARAMS";

    @Bind(R.id.tvMessage)
    TextView tvMessage;
    @Bind(R.id.rv)
    RecyclerView rv;
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.txvIfua)
    TextView txvIfua;
    @Bind(R.id.txvNamaNasabah)
    TextView txvNamaNasabah;
    @Bind(R.id.txvPeriode)
    TextView txvPeriode;
    @Bind(R.id.txvJenisTransaksi)
    TextView txvJenisTransaksi;

    @State
    ArrayList<TransactionHistory> transactionHistories;

    static  HashMap<String, String> filterParams = new HashMap<>();

    private ListOfRiwayatTransaksiPresenter presenter;

    public static void showFragment(BaseActivity sourceActivity, HashMap<String,String> params) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            Bundle extras = new Bundle();
            extras.putSerializable(PARAMS, params);
            ListOfRiwayatTransaksiFragment fragment = new ListOfRiwayatTransaksiFragment();
            fragment.setArguments(extras);
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.f_list_riwayat_transaksi;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ListOfRiwayatTransaksiPresenter(this);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getActivity().getIntent().getExtras();
        if(bundle != null) {
            filterParams = (HashMap<String, String>) bundle.getSerializable(PARAMS);
        }
        presenter.getTransactionHistory(filterParams);
        initRV();
    }

    private void initRV() {
        LinearLayoutManager llManager = new LinearLayoutManager(getActivity());
        llManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv.setLayoutManager(llManager);


        rv.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.SimpleOnItemClickListener() {
            @Override
            public void onItemClick(View childView, int position){
                super.onItemClick(childView, position);

                TransactionHistory transactionHistory = (TransactionHistory) childView.getTag();
                showDetailTrx(transactionHistory);

            }
        }));

    }

    public void showDetailTrx(TransactionHistory transactionHistory) {
        MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.detail_trans)
                .customView(R.layout.dialog_trx_history_detail, true)
                .positiveText(R.string.close)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).build();

        View view = dialog.getCustomView();

        SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.INVISEE_RETURN_FORMAT2, Locale.getDefault());
        Date dateParse = null;
        try {
            dateParse = sdf.parse(transactionHistory.getTransactionDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat(DateUtil.DD_MMM_YYYY, Locale.getDefault());
        String convertedDate = formatter.format(dateParse);

        ((TextView) ButterKnife.findById(view, R.id.tvTrxDate)).setText(convertedDate);
        ((TextView) ButterKnife.findById(view, R.id.tvOrderNumber)).setText(transactionHistory.getOrderNumber());
        ((TextView) ButterKnife.findById(view, R.id.tvTrxTime)).setText(transactionHistory.getTransactionTime());
        //((TextView) ButterKnife.findById(view, R.id.tvInvestmentNumber)).setText(transactionHistory.getInvestmentNumber());
        ((TextView) ButterKnife.findById(view, R.id.tvPackageName)).setText(transactionHistory.getPackageName());

        if(!transactionHistory.getTransactionType().equals("REDEMPTION")){
            if(transactionHistory.getCurrency().equals("IDR")){
                if(transactionHistory.getAmount() %2==0 || (transactionHistory.getAmount()+1)%2==0){
                    ((TextView) ButterKnife.findById(view, R.id.tvAmount)).setText(AmountFormatter.format(transactionHistory.getAmount()));
                }else{
                    ((TextView) ButterKnife.findById(view, R.id.tvAmount)).setText(AmountFormatter.format(Math.ceil(transactionHistory.getAmount())));
                }
            }else if(transactionHistory.getCurrency().equals("USD")){
                if(transactionHistory.getAmount() %2==0 || (transactionHistory.getAmount()+1)%2==0){
                    ((TextView) ButterKnife.findById(view, R.id.tvAmount)).setText(AmountFormatter.formatUsd(transactionHistory.getAmount()));
                }else{
                    ((TextView) ButterKnife.findById(view, R.id.tvAmount)).setText(AmountFormatter.formatUsd(Math.ceil(transactionHistory.getAmount())));
                }
            }
        } else {
            ((TextView) ButterKnife.findById(view, R.id.tvAmount)).setText(AmountFormatter.format(transactionHistory.getAmount()));
        }

        if(transactionHistory.getUnit() != 0 && !(String.valueOf(transactionHistory.getUnit()).equals("null"))){
            ((TextView) ButterKnife.findById(view, R.id.tvUnit)).setText(AmountFormatter.formatUnitComma4(transactionHistory.getUnit()));
        }else{
            ((TextView) ButterKnife.findById(view, R.id.tvUnit)).setText("-");
        }

        ((TextView) ButterKnife.findById(view, R.id.tvPaymentType)).setText(transactionHistory.getPaymentType());

        if (transactionHistory.getTransactionType().equals("SUBSCRIPTION")){
            ((TextView) ButterKnife.findById(view, R.id.tvTrxType)).setText("Pembelian");
        } else if (transactionHistory.getTransactionType().equals("TOPUP")){
            ((TextView) ButterKnife.findById(view, R.id.tvTrxType)).setText("Pembelian");
        } else if(transactionHistory.getTransactionType().equals("SWITCHING IN")){
            ((TextView) ButterKnife.findById(view, R.id.tvTrxType)).setText("Pengalihan Masuk");
        }else if(transactionHistory.getTransactionType().equals("SWITCHING OUT")){
            ((TextView) ButterKnife.findById(view, R.id.tvTrxType)).setText("Pengalihan Keluar");
        }else if(transactionHistory.getTransactionType().equals("REDEMPTION")){
            ((TextView) ButterKnife.findById(view, R.id.tvTrxType)).setText("Penjualan");
        }

        //((TextView) ButterKnife.findById(view, R.id.tvTrxStatus)).setText(transactionHistory.getTransactionStatus());
        ((TextView) ButterKnife.findById(view, R.id.tvIfuaNumber)).setText(txvIfua.getText());
        dialog.show();
    }

    public void loadTrxHistoryList() {
        rv.setAdapter(new TransactionHistoryAdapter(getActivity(), transactionHistories));
    }



    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError(){
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.tvTryAgain)
    void retryConnection() {
        presenter.getTransactionHistory(filterParams);
    }







}
