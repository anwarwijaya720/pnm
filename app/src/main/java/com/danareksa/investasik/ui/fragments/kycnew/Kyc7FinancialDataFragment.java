package com.danareksa.investasik.ui.fragments.kycnew;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.Bank;
import com.danareksa.investasik.data.api.beans.BankAccountList;
import com.danareksa.investasik.data.api.beans.Branch;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.eventBus.RxBusObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by asep.surahman on 16/05/2018.
 */


public class Kyc7FinancialDataFragment extends KycBaseNew {

    public static final String TAG = Kyc7FinancialDataFragment.class.getSimpleName();

    @Bind(R.id.sAdditionalFundSource)
    Spinner sAdditionalFundSource;
    @Bind(R.id.sBankAccountName)
    Spinner sBankAccountName;
    @Bind(R.id.sBranchName)
    Spinner sBranchName;
    @Bind(R.id.sAdditionalIncome)
    Spinner sAdditionalIncome;
    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etAccountNumber)
    EditText etAccountNumber;
    @Bind(R.id.etBranchOther)
    EditText etBranchOther;
    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etAccountHolderName)
    EditText etAccountHolderName;
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.tvTryAgain)
    TextView tvTryAgain;

    public List<Bank> banks;
    private String jsonbank;
    public List<Branch> branches;

    KycDataRequest kycDataRequest;

    String branchOther = "";
    Kyc7FinancialDataPresenter presenter;
    String bankId = "";
    String bBankId = "";
    boolean isLoadingBranch = false;

    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new Kyc7FinancialDataFragment(), TAG);
            fragmentTransaction.commit();
        }
    }


    public static Fragment getFragment(KycDataRequest kycDataRequest, String jsonBank){
        Bundle bundle = new Bundle();
        bundle.putSerializable(KYC_DATA_REQUEST, kycDataRequest);
        bundle.putString("jsonBank", jsonBank);
        Kyc7FinancialDataFragment fragment = new Kyc7FinancialDataFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected int getLayout() {
        return R.layout.f_register_data_keuangan;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        jsonbank = getArguments().getString("jsonBank");
        kycDataRequest = (KycDataRequest) getArguments().getSerializable(KYC_DATA_REQUEST);
        presenter = new Kyc7FinancialDataPresenter(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }


    @Override
    public void onResume(){
        super.onResume();
    }


    public void init(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")) {
            disableField();
        }
        Type listTypeBank = new TypeToken<List<Bank>>(){}.getType();
        Gson gsonBank = new Gson();
        banks = gsonBank.fromJson(jsonbank, listTypeBank);

        if(request.getBankAccountLists() != null){
            if(request.getBankAccountLists().size() != 0){

                etAccountNumber.setText(request.getBankAccountLists().get(0).getBankAccountNumber());
                etAccountHolderName.setText(request.getBankAccountLists().get(0).getBankAccountName());
                setupSpinnerBank(sBankAccountName, banks, request.getBankAccountLists().get(0).getBankId());

                if(request.getBankAccountLists().get(0).getBankId() != null){
                    if(!request.getBankAccountLists().get(0).getBankId().equals("")){
                        bankId = String.valueOf(banks.get(sBankAccountName.getSelectedItemPosition()).getId());

                        if(request.getBankAccountLists().get(0).getBankBranch() != null){
                            if(!request.getBankAccountLists().get(0).getBankBranch().equals("")){
                                if(!bankId.equals("")){
                                    presenter.getBranch(bankId, true);
                                }
                            }
                        }else{
                            presenter.getBranch(bankId, false);
                        }
                    }
                }

                if(request.getBankAccountLists().get(0).getBankBranchOther() != null){
                    if(!request.getBankAccountLists().get(0).getBankBranchOther().equals("")){
                        etBranchOther.setVisibility(View.VISIBLE);
                        etBranchOther.setText(request.getBankAccountLists().get(0).getBankBranchOther());
                    }
                }

            }else{
                setupSpinnerBank(sBankAccountName, banks, "");
            }
        }else{
            setupSpinnerBank(sBankAccountName, banks, "");
        }

        setupSpinnerWithSpecificLookupSelection(sAdditionalIncome, getKycLookupFromRealm(Constant.KYC_CAT_ANNUAL_INCOME), request.getAdditionalIncome());
        setupSpinnerWithSpecificLookupSelection(sAdditionalFundSource, getKycLookupFromRealm(Constant.KYC_CAT_SOURCE_OF_INCOME), request.getAdditionalFundSource());
        onItemSelectedItem();
    }


    public void onItemSelectedItem(){
        sBankAccountName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l){
                bBankId = String.valueOf(banks.get(sBankAccountName.getSelectedItemPosition()).getId());

                if(!sBankAccountName.getSelectedItem().toString().equals("")){
                    if(!bBankId.equals("")){
                        branchOther = "";
                        etBranchOther.setText("");
                        presenter.getBranch(bBankId, false);
                    }
                }else{
                    bBankId = "";
                    branches = new ArrayList<>();
                    setupSpinnerBranch(sBranchName, branches, "null");
                    hideBrachOtherLayout();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView){
            }
        });

        sBranchName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l){
                String itemBranchName = String.valueOf(adapterView.getItemAtPosition(i).toString());
                if(itemBranchName.equalsIgnoreCase("other") || itemBranchName.equalsIgnoreCase("lainnya") || itemBranchName.equalsIgnoreCase("others")){
                    branchOther = getAndTrimValueFromEditText(etBranchOther);
                    showBranchOtherLayout();
                }else{
                    hideBrachOtherLayout();
                    branchOther = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView){
            }
        });
    }


    @Override
    public void nextWithoutValidation(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")) {
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
        }else{
            if(!validate()){
                onValidFailed();
            }else{
                setValueDataFinancial();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
            }
            return;
        }

    }


    @Override
    public void saveDataKycWithBackpress(){
        if(!validate()){
            onValidFailed();
            return;
        }else{
            if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
                setValueDataFinancial();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.SAVE_KYC_DATA, request));
            }else{
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
            }
        }
    }


    @Override
    public void previewsWithoutValidation(){
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null));
    }



    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }


    @Override
    public void onValidationSucceeded(){

    }


    public void setValueDataFinancial(){
        List<BankAccountList> bankAccountListArrayList_ = new ArrayList<>();
        BankAccountList bankAccountLists_ = new BankAccountList();

        if(sBranchName.getSelectedItem().toString().equalsIgnoreCase("other") || sBranchName.getSelectedItem().toString().equalsIgnoreCase("lainnya") || sBranchName.getSelectedItem().toString().equalsIgnoreCase("others")){
            branchOther = etBranchOther.getText().toString();
        }

        if(request.getBankAccountLists() != null && request.getBankAccountLists().size() > 0){ //if update send "BankAccountId"
            bankAccountLists_.setBankAccountId(request.getBankAccountLists().get(0).getBankAccountId());
            bankAccountLists_.setBankId(String.valueOf(banks.get(sBankAccountName.getSelectedItemPosition()).getId()));
            bankAccountLists_.setBankAccountName(getAndTrimValueFromEditText(etAccountHolderName));
            bankAccountLists_.setBankAccountNumber(getAndTrimValueFromEditText(etAccountNumber));
            bankAccountLists_.setBankBranch(String.valueOf(branches.get(sBranchName.getSelectedItemPosition()).getId()));
            bankAccountLists_.setBankBranchOther(branchOther);
            bankAccountListArrayList_.add(bankAccountLists_);
            request.setBankAccountLists(bankAccountListArrayList_);
        }else{ //create if create not send "BankAccountId"
            bankAccountLists_.setBankId(String.valueOf(banks.get(sBankAccountName.getSelectedItemPosition()).getId()));
            bankAccountLists_.setBankAccountName(getAndTrimValueFromEditText(etAccountHolderName));
            bankAccountLists_.setBankAccountNumber(getAndTrimValueFromEditText(etAccountNumber));
            bankAccountLists_.setBankBranch(String.valueOf(branches.get(sBranchName.getSelectedItemPosition()).getId()));
            bankAccountLists_.setBankBranchOther(branchOther);
            bankAccountListArrayList_.add(bankAccountLists_);
            request.setBankAccountLists(bankAccountListArrayList_);
        }

        request.setAdditionalIncome(getKycLookupCodeFromSelectedItemSpinner(sAdditionalIncome));
        request.setAdditionalFundSource(getKycLookupCodeFromSelectedItemSpinner(sAdditionalFundSource));
    }


    public boolean validate(){
        boolean valid = true;
        String accountNumber     = etAccountNumber.getText().toString();
        String accountHolderName = etAccountHolderName.getText().toString();

        if(accountNumber.isEmpty() && accountNumber.length() <= 0){
            etAccountNumber.setError("Mohon isi kolom ini");
            valid = false;
        }
        if(accountHolderName.isEmpty() && accountHolderName.length() <= 0){
            etAccountHolderName.setError("Mohon isi kolom ini");
            valid = false;
        }

        if(!accountHolderName.isEmpty() && accountHolderName.length() < 3){
            etAccountHolderName.setError("Mohon isi nama pemegang rekening dengan benar");
            valid = false;
        }

       /* if(sAdditionalFundSource.getSelectedItem().toString().equals("")){
            valid = false;
            ((TextView) sAdditionalFundSource.getSelectedView()).setError("Mohon isi kolom ini");
        }
        if(sAdditionalIncome.getSelectedItem().toString().equals("")){
            valid = false;
            ((TextView) sAdditionalIncome.getSelectedView()).setError("Mohon isi kolom ini");
        }*/
        if(sBankAccountName.getSelectedItem().toString().equals("") ||
            sBankAccountName.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
            valid = false;
            ((TextView) sBankAccountName.getSelectedView()).setError("Mohon isi kolom ini");
        }
        if(sBranchName.getSelectedItem().toString().equals("") ||
                sBranchName.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
            valid = false;
            ((TextView) sBranchName.getSelectedView()).setError("Mohon isi kolom ini");
        }

        if(!sBranchName.getSelectedItem().toString().equals("") ||
                sBankAccountName.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
            if(sBranchName.getSelectedItem().toString().equalsIgnoreCase("other") || sBranchName.getSelectedItem().toString().equalsIgnoreCase("lainnya") || sBranchName.getSelectedItem().toString().equalsIgnoreCase("others")){
                if(etBranchOther.getText().toString().length() < 3){
                    valid = false;
                    etBranchOther.setError("Nama cabang tidak sesuai");
                }
            }
        }

        return valid;
    }


    public void showProgressBar() {
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar() {
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }


    private void hideBrachOtherLayout(){
        etBranchOther.setVisibility(View.GONE);
    }

    private void showBranchOtherLayout(){
        etBranchOther.setVisibility(View.VISIBLE);
    }


    public void disableField(){
        sAdditionalFundSource.setEnabled(false);
        sBankAccountName.setEnabled(false);
        sBranchName.setEnabled(false);
        sAdditionalIncome.setEnabled(false);
        etAccountNumber.setEnabled(false);
        etBranchOther.setEnabled(false);
        etAccountHolderName.setEnabled(false);
    }


    public void fetchResultBranchToLayout(){
        setupSpinnerBranch(sBranchName, branches, request.getBankAccountLists().get(0).getBankBranch());
    }




    @OnClick(R.id.tvTryAgain)
    void btnTryAgain(){
        if(isLoadingBranch == false){

            System.out.println("bank Id ====> " + bankId);
            if(!bankId.equals("")){
                presenter.getBranch(bankId, false);
            }else{
                if(banks.size() > 0){
                    setupSpinnerBank(sBankAccountName, banks, "");
                    dismissProgressBar();
                }
            }
        }
    }

    @Override
    public void previewsWhileError(){

    }


}
