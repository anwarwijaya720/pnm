package com.danareksa.investasik.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.PortfolioInvestment;
import com.danareksa.investasik.ui.fragments.portfolio.DetailPortfolioFragment;

import butterknife.Bind;

/**
 * Created by fajarfatur on 3/2/16.
 */
public class PortfolioActivity extends BaseActivity {

    private static final String PORTFOLIO = "portfolio";
    private static final String IFUA_ID = "ifuaId";
    private static final String CAT_ID = "CAT_ID";
    private static final String ACCOUNT_TYPE = "accountType";

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;

    private PortfolioInvestment investment;
    String ifuaId = "";
    Integer catid = 0;
    String accountType = "";

    public static Activity portoActivity;


    public static void startActivity(BaseActivity sourceActivity, PortfolioInvestment investment, String ifua, Integer catId, String accountType) {
        Intent intent = new Intent(sourceActivity, PortfolioActivity.class);
        intent.putExtra(PORTFOLIO, investment);
        intent.putExtra(IFUA_ID, ifua);
        intent.putExtra(CAT_ID, catId);
        intent.putExtra(ACCOUNT_TYPE, accountType);
        sourceActivity.startActivity(intent);
    }


    @Override
    protected int getLayout() {
        return R.layout.a_portfolio;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        portoActivity = this;
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(null);
        }
        investment = (PortfolioInvestment) getIntent().getSerializableExtra(PORTFOLIO);
        ifuaId = getIntent().getStringExtra(IFUA_ID);
        accountType = getIntent().getStringExtra(ACCOUNT_TYPE);
        title.setText(investment.getPackageName());
        DetailPortfolioFragment.showFragment(this, investment, ifuaId, accountType);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        this.finish();
    }
}
