package com.danareksa.investasik.ui.fragments.portfolio;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.PortfolioInvestment;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.AmountFormatter;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by fajarfatur on 3/2/16.
 */
public class PortfolioDetailFragment extends BaseFragment {

    private static final String PORTFOLIO = "portfolio";

    @Bind(R.id.tvTipe)
    TextView tvTipe;
    @Bind(R.id.ivPackage)
    CircleImageView ivPackage;
    @Bind(R.id.tvPackageName)
    TextView tvPackageName;
    @Bind(R.id.tvInvestmentNumber)
    TextView tvInvestmentNumber;
    @Bind(R.id.tvInvesmentAmount)
    TextView tvInvesmentAmount;
    @Bind(R.id.tvGoal)
    TextView tvGoal;
    @Bind(R.id.tvMarketValue)
    TextView tvMarketValue;
    @Bind(R.id.tvUrealizeGainLossPercent)
    TextView tvUrealizeGainLossPercent;
    @Bind(R.id.tvUnit)
    TextView tvUnit;
    @Bind(R.id.tvNabAkhir)
    TextView tvNabAkhir;
    @Bind(R.id.tvUrealizeGainLossValue)
    TextView tvUrealizeGainLossValue;
    String ifuaId = "";

    double unit        = 0.0;
    double lastNab     = 0.0;
    String lastNabDate = "";
    String individualFundType = "";
    double gainLoss    = 0.0;

    private PortfolioInvestment investment;

    public static PortfolioDetailFragment initiateFragment(PortfolioInvestment investment, String ifuaId) {
        PortfolioDetailFragment fragment = new PortfolioDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(PORTFOLIO, investment);
        bundle.putString("ifuaId", ifuaId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayout() {
        return R.layout.f_portfolio_detail;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        investment = (PortfolioInvestment) getArguments().get(PORTFOLIO);
        ifuaId = getArguments().getString("ifuaId");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        if(investment.getIdrUnReliezeGainLoss() != null){
            if(!investment.getIdrUnReliezeGainLoss().equals("")){
                gainLoss = Double.parseDouble(investment.getIdrUnReliezeGainLoss());
            }
        }


        double totalInv = investment.getInvestmentAmount();
        double totalMarketValue = investment.getTotalInvestmentMarketValue();
        double totalGainLoss = totalMarketValue - totalInv;
        float percentage = (float) (totalGainLoss / totalInv) * 100;
        String currency = investment.getCurrency();

        if(investment.getInvestmentComposition() != null && investment.getInvestmentComposition().size() > 0){
            unit               = investment.getInvestmentComposition().get(0).getCurrentUnit();
            lastNab            = investment.getInvestmentComposition().get(0).getLastNav();
            lastNabDate        = investment.getInvestmentComposition().get(0).getLastNavDate();
            individualFundType = investment.getInvestmentComposition().get(0).getIndividualFundType();
        }else{
            lastNabDate        = "-";
            individualFundType = "-";
        }

        Picasso.with(getActivity()).load(InviseeService.IMAGE_DOWNLOAD_URL + investment.getPackageImageKey())
                .placeholder(R.drawable.ic_logo_launcher)
                .error(R.drawable.ic_logo_launcher)
                .into(ivPackage);


        tvTipe.setText(individualFundType);
        tvPackageName.setText(investment.getPackageName());
        tvInvestmentNumber.setText(ifuaId);
        tvUnit.setText(AmountFormatter.formatUnitComma4(unit));
        //tvUnit.setText(AmountFormatter.formatNonCurrency(unit));

        if(investment.getInvestmentGoal() == null || (investment.getInvestmentGoal().equals("null") || investment.getInvestmentGoal().equals(""))){
            tvGoal.setText("-");
        }else{
            tvGoal.setText(investment.getInvestmentGoal());
        }

        tvUrealizeGainLossPercent.setText(String.format("%.2f", percentage) + " %");

        if(currency.equals("IDR")){
            //tvUrealizeGainLossValue.setText(AmountFormatter.format(totalGainLoss));
            System.out.println("gain loss : " + gainLoss);
            tvUrealizeGainLossValue.setText(AmountFormatter.format(gainLoss));
            tvMarketValue.setText(AmountFormatter.format(totalMarketValue));
            tvInvesmentAmount.setText(AmountFormatter.format(investment.getInvestmentAmount()));
            tvNabAkhir.setText(AmountFormatter.format(lastNab) + " | " + lastNabDate);
        }else if(currency.equals("USD")){
            //tvUrealizeGainLossValue.setText(AmountFormatter.formatUsd(totalGainLoss));
            tvUrealizeGainLossValue.setText(AmountFormatter.formatUsd(gainLoss));
            tvMarketValue.setText(AmountFormatter.formatUsd(totalMarketValue));
            tvInvesmentAmount.setText(AmountFormatter.formatUsd(investment.getInvestmentAmount()));
            tvNabAkhir.setText(AmountFormatter.formatUsd(lastNab) + " | " + lastNabDate);
        }else{
            //tvUrealizeGainLossValue.setText(AmountFormatter.format(totalGainLoss));
            tvUrealizeGainLossValue.setText(AmountFormatter.format(gainLoss));
            tvMarketValue.setText(AmountFormatter.format(totalMarketValue));
            tvInvesmentAmount.setText(AmountFormatter.format(investment.getInvestmentAmount()));
            tvNabAkhir.setText(AmountFormatter.format(lastNab) + " | " + lastNabDate);
        }
    }

}
