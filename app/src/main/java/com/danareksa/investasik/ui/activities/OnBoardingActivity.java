package com.danareksa.investasik.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.fragments.onboarding.OnboardingFragment;
import com.danareksa.investasik.util.Animator;
import com.viewpagerindicator.CirclePageIndicator;
import com.viewpagerindicator.IconPagerAdapter;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by asep.surahman on 27/03/2018.
 */

public class OnBoardingActivity extends BaseActivity implements ViewPager.OnPageChangeListener {

    //@Bind(R.id.btnNext)
    //Button btnNext;

    @Bind(R.id.btnGotIt)
    Button btnGotIt;

    @Bind(R.id.indicator)
    CirclePageIndicator mIndicator;

    @Bind(R.id.pager)
    ViewPager mPager;

    private OnboardingFragmentAdapter mAdapter;
    private Animator animator = new Animator();


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        btnGotIt.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLts.otf"));

        if (getSupportActionBar() != null){
            getSupportActionBar().hide();
        }

        if (BuildConfig.FLAVOR.contentEquals("dim") || BuildConfig.FLAVOR.contentEquals("btn")){
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            changeStatusBarColor();
        }

        initGuides();
    }

    @Override
    protected int getLayout() {
        return R.layout.a_onboarding;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position){

        if (position == (mAdapter.getCount() - 1)) {
            //animator.disappearView(btnNext);
            //animator.appearView(btnGotIt);
        }else{
            //animator.disappearView(btnGotIt);
            //animator.appearView(btnNext);
        }

    }

    @Override
    public void onPageScrollStateChanged(int state){

    }

    /*
    @OnClick(R.id.btnNext)
    public void onClickButtonNext() {
        mPager.setCurrentItem(mPager.getCurrentItem() + 1, true);
    }
    */

    public static void startActivity(BaseActivity sourceActivity) {
        Intent i = new Intent(sourceActivity, OnBoardingActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        sourceActivity.startActivity(i);
    }

    public static void startActivity(Activity sourceActivity, Bundle bundle) {
        Intent i = new Intent(sourceActivity, OnBoardingActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.putExtras(bundle);
        sourceActivity.startActivity(i);
    }

    @OnClick(R.id.btnGotIt)
    public void onClickButtonGotIt(){
        LandingPageActivity.startActivity(this);
        PrefHelper.setIsFirstRunToPreference(PrefKey.IS_FIRST_RUN2, false);
        finish();
    }

    /*
    @OnClick(R.id.btnSkip)
    public void onClickButtonSkip(View view) {
        onClickButtonGotIt();
    }
    */

    public void initGuides() {
        int background_color = ContextCompat.getColor(this, R.color.colorPrimary);
        String[] tTITLE = {"Selamat datang di InvestASIK", "Persiapan kebutuhan masa depan ?", "Aplikasi investasi cocok untuk beragam profesi"};
        String[] tDESCRIPTIONS = {"Atur, Sisihkan, Invest, Komit. Cara ASIK untuk berinvestasi.",
                                  "InvestASIK punya banyak pilihan produk untuk berbagai tujuan finansial Anda.",
                                  "Ibu rumah tangga, karyawan, pengusaha, anak kuliahan. Semua pakai InvestASIK."};
        int[] tIMAGES = {R.drawable.img_intro_1, R.drawable.img_intro_2, R.drawable.img_intro_3};
        int[] tCOLORS = {background_color, background_color, background_color};
        mAdapter = new OnboardingFragmentAdapter(getSupportFragmentManager(), tTITLE, tDESCRIPTIONS, tIMAGES, tCOLORS, this);
        mPager.setOffscreenPageLimit(tTITLE.length);
        mPager.setAdapter(mAdapter);
        mPager.addOnPageChangeListener(this);
        mIndicator.setViewPager(mPager);
        mIndicator.setOnPageChangeListener(this);
    }

    private void changeStatusBarColor(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

}

class OnboardingFragmentAdapter extends FragmentPagerAdapter implements IconPagerAdapter {

    private String[] TITLE;
    private String[] DESCRIPTIONS;
    private int[] IMAGES;
    private int[] COLORS;
    private int mCount = 0;
    Context context;

    public OnboardingFragmentAdapter(FragmentManager fm, String[] tTITLE, String[] tDESCRIPTIONS, int[] tIMAGES, int[] tCOLORS, Context context_) {
        super(fm);
        TITLE = tTITLE;
        IMAGES = tIMAGES;
        COLORS = tCOLORS;
        DESCRIPTIONS = tDESCRIPTIONS;
        mCount = TITLE.length;
        context = context_;
    }

    @Override
    public Fragment getItem(int position) {
        return OnboardingFragment.newInstance(TITLE[position % TITLE.length],
                DESCRIPTIONS[position % DESCRIPTIONS.length],
                IMAGES[position % IMAGES.length], COLORS[position % COLORS.length]
                , context);
    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLE[position % TITLE.length];
    }

    @Override
    public int getIconResId(int index) {
        return R.mipmap.ic_launcher;
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }

}
