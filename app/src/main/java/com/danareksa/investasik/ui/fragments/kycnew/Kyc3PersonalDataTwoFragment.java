package com.danareksa.investasik.ui.fragments.kycnew;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.eventBus.RxBusObject;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.Bind;

/**
 * Created by asep.surahman on 15/05/2018.
 */

public class Kyc3PersonalDataTwoFragment extends KycBaseNew{

    public static final String TAG = Kyc3PersonalDataTwoFragment.class.getSimpleName();

    @Bind(R.id.sEducationBackground)
    Spinner sEducationBackground;
    @Bind(R.id.sAnnualIncome)
    Spinner sAnnualIncome;
    @Bind(R.id.sSourceOfIncome)
    Spinner sSourceOfIncome;
    @Bind(R.id.sInvestmentPurpose)
    Spinner sInvestmentPurpose;

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etMotherMaidenName)
    EditText etMotherMaidenName;
    public KycDataRequest kycDataRequest;

    public static void showFragment(BaseActivity sourceActivity){
        if (!sourceActivity.isFragmentNotNull(TAG)){
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new Kyc3PersonalDataTwoFragment(), TAG);
            fragmentTransaction.commit();
        }
    }

    public static Fragment getFragment(KycDataRequest kycDataRequest){
        Bundle bundle = new Bundle();
        bundle.putSerializable(KYC_DATA_REQUEST, kycDataRequest);
        Kyc3PersonalDataTwoFragment fragment = new Kyc3PersonalDataTwoFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayout(){
        return R.layout.f_register_data_pribadi_2;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    public void init(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")) {
            disableField();
        }
        etMotherMaidenName.setText(request.getMotherName());
        setupSpinnerWithSpecificLookupSelection(sEducationBackground, getKycLookupFromRealm(Constant.KYC_CAT_EDUCATION_BACKGROUND), request.getEducation());
        setupSpinnerWithSpecificLookupSelection(sAnnualIncome, getKycLookupFromRealm(Constant.KYC_CAT_ANNUAL_INCOME), request.getAnnualIncome());
        setupSpinnerWithSpecificLookupSelection(sSourceOfIncome, getKycLookupFromRealm(Constant.KYC_CAT_SOURCE_OF_INCOME), request.getFundSource());
        setupSpinnerWithSpecificLookupSelection(sInvestmentPurpose, getKycLookupFromRealm(Constant.KYC_CAT_INVESTMENT_PURPOSE), request.getInvestmentPurpose());
    }

    public void setValuePersonalData(){
        request.setMotherName(getAndTrimValueFromEditText(etMotherMaidenName));
        request.setAnnualIncome(getKycLookupCodeFromSelectedItemSpinner(sAnnualIncome));
        request.setEducation(getKycLookupCodeFromSelectedItemSpinner(sEducationBackground));
        request.setFundSource(getKycLookupCodeFromSelectedItemSpinner(sSourceOfIncome));
        request.setInvestmentPurpose(getKycLookupCodeFromSelectedItemSpinner(sInvestmentPurpose));
    }

    @Override
    public void nextWithoutValidation(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")) {
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
        }else{
            if(!validate()){
                onValidFailed();
            }else{
                setValuePersonalData();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
            }
            return;
        }
    }

    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }


    @Override
    public void saveDataKycWithBackpress(){

        if(!validate()){
            onValidFailed();
            return;
        }else{
            if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
                setValuePersonalData();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.SAVE_KYC_DATA, request));
            }else{
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
            }
        }
    }


    @Override
    public void previewsWithoutValidation() {
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null));
    }

    //disable
    @Override
    public void onValidationSucceeded(){

    }

    public boolean validate(){
        boolean valid = true;
        String motherName  = etMotherMaidenName.getText().toString();

        if(motherName.isEmpty() && motherName.length() <= 0){
            etMotherMaidenName.setError("Mohon isi kolom ini");
            valid = false;
        }

        if(!motherName.isEmpty() && motherName.length() < 3){
            etMotherMaidenName.setError("Mohon isi nama gadis ibu kandung dengan benar");
            valid = false;
        }

        if(sInvestmentPurpose.getSelectedItem().toString().equals("") ||
            sInvestmentPurpose.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
            valid = false;
            ((TextView) sInvestmentPurpose.getSelectedView()).setError("Mohon isi kolom ini");
        }

        if(sEducationBackground.getSelectedItem().toString().equals("") ||
            sEducationBackground.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
            valid = false;
            ((TextView) sEducationBackground.getSelectedView()).setError("Mohon isi kolom ini");
        }

        if(sAnnualIncome.getSelectedItem().toString().equals("") ||
            sAnnualIncome.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
            valid = false;
            ((TextView) sAnnualIncome.getSelectedView()).setError("Mohon isi kolom ini");
        }

        if(sSourceOfIncome.getSelectedItem().toString().equals("") ||
            sSourceOfIncome.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
            valid = false;
            ((TextView) sSourceOfIncome.getSelectedView()).setError("Mohon isi kolom ini");
        }

        return valid;
    }


    public void disableField(){
        etMotherMaidenName.setEnabled(false);
        sInvestmentPurpose.setEnabled(false);
        sEducationBackground.setEnabled(false);
        sAnnualIncome.setEnabled(false);
        sSourceOfIncome.setEnabled(false);
    }


    @Override
    public void previewsWhileError(){

    }

}
