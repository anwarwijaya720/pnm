package com.danareksa.investasik.ui.fragments.article;

import com.danareksa.investasik.data.api.responses.CartListResponse;
import com.danareksa.investasik.data.api.responses.NewsFeedResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by asep.surahman on 02/05/2018.
 */

public class ArticlePresenter {

    private ArticleFragment fragment;

    public ArticlePresenter(ArticleFragment fragment) {
        this.fragment = fragment;
    }

    void articleList(){
        fragment.showProgressBar();
        fragment.getApi().getAllNews(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<NewsFeedResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(NewsFeedResponse response) {
                        if (response != null) {
                            fragment.response = response;
                            fragment.listArticle = response.getData();
                            fragment.loadList();
                            fragment.dismissProgressBar();
                            fragment.swipe_container.setRefreshing(false);
                        } else {
                            fragment.dismissProgressBar();
                            fragment.swipe_container.setRefreshing(false);
                        }
                        fragment.swipe_container.setRefreshing(false);
                        cartList();
                    }
                });

    }


    public void cartList(){
        fragment.getApi().getCartList(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<CartListResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(List<CartListResponse> response) {
                        ((BaseActivity) fragment.getActivity()).setNotifCount(0);

                        if(response != null && response.size() > 0){
                            ((BaseActivity) fragment.getActivity()).setNotifCount(response.size());
                            fragment.cartList = response;
                        }
                    }
                });

    }



}
