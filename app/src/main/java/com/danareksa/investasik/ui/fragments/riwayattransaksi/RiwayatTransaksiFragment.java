package com.danareksa.investasik.ui.fragments.riwayattransaksi;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.InvestmentAccountInfo;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.ListOfRiwayatTransaksiActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.DateUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by asep.surahman on 29/06/2018.
 */

public class RiwayatTransaksiFragment extends BaseFragment {

    public static final String TAG = RiwayatTransaksiFragment.class.getSimpleName();

    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.sTimePeriod)
    Spinner sTimePeriod;
    @Bind(R.id.sJenisTran)
    Spinner sJenisTran;
    @Bind(R.id.sIfua)
    Spinner sIfua;
    @Bind(R.id.bShowRiwayat)
    Button bShowRiwayat;

    public int status = 0;

    public List<InvestmentAccountInfo> listIFUA = new ArrayList<>();

    private RiwayatTransaksiPresenter presenter;
    int listPeriod[] = {0,-1,-2,-3,-4,-5,-6};
    String listPeriodNames[] = {"Silahkan Pilih","1 Bulan","2 Bulan","3 Bulan","4 Bulan","5 Bulan","6 Bulan"};
    String listType[] = {"","","SUBSCRIPTION","REDEMPTION","SWITCHING"};
    String listTypes[] = {"Silahkan Pilih","Semua","Pembelian","Penjualan","Pengalihan"};
    String listIfuaNull[] = {"Belum memiliki IFUA"};

    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, R.anim.slide_out_left, android.R.anim.slide_in_left, R.anim.slide_out_left);
            fragmentTransaction.replace(R.id.container, new RiwayatTransaksiFragment(), TAG);
            fragmentTransaction.commit();
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.f_riwayat_transaksi;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new RiwayatTransaksiPresenter(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.getInvestmentAccountGroup();
    }


    @OnClick(R.id.bShowRiwayat)
    void bShowRiwayat(){

        if(status == 1){

            HashMap<String, String> data = new HashMap<>();
            data.put("p_trx_type", sJenisTran.getSelectedItem().toString());
            data.put("p_period", sTimePeriod.getSelectedItem().toString());

            InvestmentAccountInfo selectedIfua = (InvestmentAccountInfo) sIfua.getSelectedItem();
            if (selectedIfua != null) {
                data.put("ifua", selectedIfua.getIfua() );
            }

            if(!listType[sJenisTran.getSelectedItemPosition()].equals("")){
                data.put("trxType", listType[sJenisTran.getSelectedItemPosition()]);
            }

            Date currentDate = Calendar.getInstance().getTime();
            data.put("endDate", DateUtil.format(currentDate,DateUtil.YYYY_MM_DD));
            data.put("startDate", DateUtil.getDateFromMonthAgo(listPeriod[sTimePeriod.getSelectedItemPosition()]));

            if(!validate()){

            }else{
                ListOfRiwayatTransaksiActivity.startActivity((BaseActivity) getActivity(), data);
            }

        }else{

            new MaterialDialog.Builder(getActivity())
                    .iconRes(R.mipmap.ic_launcher)
                    .backgroundColor(Color.WHITE)
                    .title("INFO")
                    .titleColor(Color.BLACK)
                    .content("Anda belum memiliki IFUA")
                    .contentGravity(GravityEnum.CENTER)
                    .contentColor(Color.GRAY)
                    .positiveText(R.string.ok)
                    .positiveColor(Color.GRAY)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            dialog.dismiss();
                        }
                    })
                    .cancelable(true)
                    .show();
        }

    }


    public boolean validate(){
        boolean valid = true;

        String jenisTrans = sJenisTran.getSelectedItem().toString();
        String period     = sTimePeriod.getSelectedItem().toString();
        String ifua       = sIfua.getSelectedItem().toString();

        if(jenisTrans.equalsIgnoreCase("silahkan pilih")){
            Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
            valid = false;
        }

        if(period.equalsIgnoreCase("silahkan pilih")){
            Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
            valid = false;
        }

        if(ifua.equalsIgnoreCase("silahkan pilih")){
            Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
            valid = false;
        }

        return  valid;
    }



    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError(){
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }

    public void setupSpinner(){
        if (this.listIFUA == null) this.listIFUA = new ArrayList<>();

        if(listIFUA != null && listIFUA.size() > 0){
            ArrayAdapter<InvestmentAccountInfo> spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner, listIFUA);
            sIfua.setAdapter(spinnerArrayAdapter);
        }else{
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner, listIfuaNull);
            sIfua.setAdapter(spinnerArrayAdapter);
        }

        ArrayAdapter<String> timePeriodeAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner, listPeriodNames);
        sTimePeriod.setAdapter(timePeriodeAdapter);

        ArrayAdapter<String> trxTypeAdapter = new ArrayAdapter<>(getContext(), R.layout.spinner, listTypes);
        sJenisTran.setAdapter(trxTypeAdapter);
    }


}
