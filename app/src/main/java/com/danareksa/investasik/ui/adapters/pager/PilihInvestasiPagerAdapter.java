package com.danareksa.investasik.ui.adapters.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.ui.fragments.tambahrekening.LumpsumInvestFragment;
import com.danareksa.investasik.ui.fragments.tambahrekening.RegulerInvestFragment;

/**
 * Created by asep.surahman on 30/05/2018.
 */

public class PilihInvestasiPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 2;
    private BaseFragment bFragment;
    private Packages packages;

    public PilihInvestasiPagerAdapter(BaseFragment bFragment, FragmentManager fm) {
        super(fm);
        this.bFragment = bFragment;
    }

    @Override
    public int getCount(){
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = LumpsumInvestFragment.getFragment();
                break;
            case 1:
                fragment = RegulerInvestFragment.getFragment();
                break;
        }
        return fragment;
    }



}
