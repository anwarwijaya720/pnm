package com.danareksa.investasik.ui.fragments.portfolio;

import com.danareksa.investasik.data.api.requests.InvestmentAcountGroupRequest;
import com.danareksa.investasik.data.api.responses.InvestmentAccountInfoResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by asep.surahman on 10/08/2018.
 */

public class ListCategoryPortfolioPresenter {

    ListCategoryPortfolioFragment fragment;

    public ListCategoryPortfolioPresenter(ListCategoryPortfolioFragment fragment){
        this.fragment = fragment;
    }

    public void getInvestmentAccountGroup(){
        fragment.showProgressBar();
        InvestmentAcountGroupRequest request = new InvestmentAcountGroupRequest();
        request.setToken(PrefHelper.getString(PrefKey.TOKEN));
        fragment.getApi().getInvestmentAccountGroup(request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<InvestmentAccountInfoResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.dismissProgressBar();
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(InvestmentAccountInfoResponse investmentAccountInfoResponse) {
                        fragment.dismissProgressBar();
                        if(investmentAccountInfoResponse.getCode() == 0) {
                            fragment.accountGroup = investmentAccountInfoResponse.getData();
                            fragment.setupViewPager();
                        }else{
                            fragment.setupViewPager();
                        }

                    }
                });
    }

}
