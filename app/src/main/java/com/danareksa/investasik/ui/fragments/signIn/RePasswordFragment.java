package com.danareksa.investasik.ui.fragments.signIn;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.text.InputFilter;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.mobsandgeeks.saripaar.annotation.Pattern;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by pandu.abbiyuarsyah on 13/07/2017.
 */

public class RePasswordFragment extends BaseFragment {

    public static final String TAG = RePasswordFragment.class.getSimpleName();
    private static final String EMAIL = "email";

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etResetCode)
    EditText etResetCode;
    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Pattern(regex = "^(?=.*[0-9])(?=.*?[.:;#?!@$%^&*-])(?=\\S+$).{8,}$", messageResId = R.string.rules_password)
    @Password
    @Bind(R.id.etPassword)
    EditText etPassword;
    @NotEmpty(messageResId = R.string.rules_no_empty)
    @ConfirmPassword
    @Bind(R.id.etConfirmPassword)
    EditText etConfirmPassword;

    @Bind(R.id.tvPwdHint)
    TextView tvPwdHint;

    private RePasswordPresenter presenter;
    private String email;
    private String question;
    private String answer;
    private int maxLength = 6;

    public static void showFragment(BaseActivity sourceActivity, String email) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, android.R.anim.slide_in_left, android.R.anim.slide_out_right);

            Fragment fragment = new RePasswordFragment();
            Bundle bundle = new Bundle();
            bundle.putString(EMAIL, email);
            fragment.setArguments(bundle);

            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.f_re_password;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new RePasswordPresenter(this);
        email = getArguments().getString(EMAIL);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvPwdHint.setHint(Html.fromHtml(getString(R.string.signup_password_hint)));
        etResetCode.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(6)});
    }

    @OnClick(R.id.bSubmit)
    void submitResetPassword(){
        getValidator().validate();
    }


    @Override
    public void onValidationSucceeded() {
        super.onValidationSucceeded();

        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title("Konfirmasi")
                .titleColor(Color.BLACK)
                .content("Simpan password baru Anda?")
                .contentColor(Color.GRAY)
                .positiveText("Simpan")
                .positiveColor(Color.BLUE)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        presenter.requestResetPassword(presenter.constructResetPasswordRequest(email));
                    }
                })
                .negativeText("Batal")
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .cancelable(false)
                .show();
    }

    @OnClick(R.id.bResendCode)
    void resendCode() {
        presenter.urgentForgotPass(presenter.constructForgotPasswordRequest(email));
    }

    void showDialogAfterSubmit(String info){
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.infortmation).toUpperCase())
                .titleColor(Color.BLACK)
                .content(info)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }
}
