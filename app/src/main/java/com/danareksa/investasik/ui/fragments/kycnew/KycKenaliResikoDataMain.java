package com.danareksa.investasik.ui.fragments.kycnew;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.Bank;
import com.danareksa.investasik.data.api.beans.City;
import com.danareksa.investasik.data.api.beans.Country;
import com.danareksa.investasik.data.api.requests.KewarganegaraanRequest;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.api.requests.RiskProfileRequest;
import com.danareksa.investasik.data.api.responses.LoadKycDataResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.KycFinishActivity;
import com.danareksa.investasik.ui.activities.MainActivity;
import com.danareksa.investasik.ui.activities.UserProfileActivity;
import com.danareksa.investasik.ui.adapters.pager.KenaliResikoUserPageAdapter;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.eventBus.RxBusObject;
import com.danareksa.investasik.util.ui.NonSwipeableViewPager;
import com.google.gson.Gson;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import icepick.State;

/**
 * Created by asep.surahman on 05/07/2018.
 */

public class KycKenaliResikoDataMain extends KycBaseNew  {

    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.bNext)
    Button bNext;

    @Bind(R.id.tvTryAgain)
    TextView tvTryAgain;

    public LoadKycDataResponse loadKycDataResponse;

    @Bind(R.id.pager)
    NonSwipeableViewPager pager;

    @State
    KycDataRequest request;

    @State
    RiskProfileRequest riskProfileRequest;

    @State
    KewarganegaraanRequest kewarganegaraanRequest;
    private KenaliResikoUserPageAdapter pagerAdapter;
    Dialog dialogKonfimasi;
    LayoutInflater inflater;
    Button btnLanjut;
    Button btnSimpan;
    TextView textInfo;
    private long mLastClickTime = 0;

    boolean isErrorLoadKyc = false;

    public List<Country> countries;
    public List<com.danareksa.investasik.data.api.beans.State> states;
    public List<City> cities;
    public List<Bank> banks;

    String sourceRoute;

    @State
    int pageNumber = 0;
    KycKenaliResikoDataMainPresenter presenter;
    public static final String TAG = KycKenaliResikoDataMain.class.getSimpleName();

    public static void showFragment(BaseActivity sourceActivity, String sourceRoute){
        if (!sourceActivity.isFragmentNotNull(TAG)){
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constant.SOURCE_ROUTE, sourceRoute);
            KycKenaliResikoDataMain fragment = new KycKenaliResikoDataMain();
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new KycKenaliResikoDataMain(), TAG);
            fragmentTransaction.commit();
        }
    }

    @Override
    protected int getLayout(){
        return R.layout.f_register_documen_kenali_resiko;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        sourceRoute = getActivity().getIntent().getStringExtra(Constant.SOURCE_ROUTE);
        presenter = new KycKenaliResikoDataMainPresenter(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        presenter.getKycLookupData();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void busHandler(RxBusObject.RxBusKey busKey, Object busObject){
        super.busHandler(busKey, busObject);
        switch (busKey){
            case SUBMIT_KYC_DATA:
                //request = (KycDataRequest) busObject;
                changeNameButtonNext();
                and();
                //presenter.saveAndSubmitKyc(request, sourceRoute);
                break;
            case NEXT_FORM:
                toNextForm();
                break;
//            case SAVE_KYC_DATA:
//                request = (KycDataRequest) busObject;
//                presenter.saveDataKyc(request, false);
//                break;
//            case SAVE_KYC_DATA_WITH_CHECKLIST:
//                request = (KycDataRequest) busObject;
//                presenter.saveDataKyc(request, true);
//                break;
            case CHANGE_BUTTON:
                changeNameButtonNext();
                break;
//            case CHANGE_BUTTON_CEK_HASIL:
//                changeNameButtonNextCekHasil();
//                break;
//            case FINISH_STEP:
//                finishStep();
//                break;
//            case TO_PAGE_BENEFECERY:
//                toPageOwnersOfBenefit();
//                break;
            case BACK_TO_PAGE:
                toPreviewsForm();
                break;
//            case TO_PAGE_CORRESPONDEN:
//                toPageCorresponden();
//                break;
        }
    }

    public void toPreviewsForm(){
        if(pageNumber < pagerAdapter.getCount()){
            pageNumber--;
            pager.setCurrentItem(pageNumber);
            setTextNavigation(pageNumber);
        }
    }

    public void toNextForm(){
        if (pageNumber < pagerAdapter.getCount()){
            pageNumber++;
            pager.setCurrentItem(pageNumber);
            setTextNavigation(pageNumber);
        }
    }

    private void finishStep(){
        getActivity().finish();
    }

    @OnClick(R.id.bNext)
    void bNext(){
        if(SystemClock.elapsedRealtime() - mLastClickTime < 1000){return;}
        mLastClickTime = SystemClock.elapsedRealtime();

        if(pageNumber == 0 ){
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.GET_NEW_INPUTTED_KYC_DATA_WITHOUT_VALIDATION, null));
        }else{
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.GET_NEW_INPUTTED_KYC_DATA, null)); //default
        }
    }

    private void setTextNavigation(int current){
        TextView title = (TextView) getActivity().findViewById(R.id.title);
        switch (current) {
            case 0:
                title.setText("Data Profil Risiko");
                bNext.setText("Cek hasil");
                break;
        }
    }

//    public void showDialogBackForm(){
//        getBus().send(new RxBusObject(RxBusObject.RxBusKey.TO_PAGE_BEFORE_KYC, null));
//    }
//
//    public void showDialogKonfirmasi(){
//        dialogKonfimasi = new Dialog(getActivity());
//        View view  = inflater.inflate(R.layout.popup_konfirmasi_registrasi, null);
//        btnLanjut  = (Button) view.findViewById(R.id.btnLanjut);
//        btnSimpan  = (Button) view.findViewById(R.id.btnNanti);
//        textInfo   = (TextView) view.findViewById(R.id.textInfo);
//        textInfo.setText("Simpan sementara?");
//        btnLanjut.setText("LANJUT");
//        btnSimpan.setText("SIMPAN");
//        dialogKonfimasi.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialogKonfimasi.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#50000000")));
//        dialogKonfimasi.setContentView(view);
//        dialogKonfimasi.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        dialogKonfimasi.show();
//        btnLanjut.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v){
//                dialogKonfimasi.dismiss();
//            }
//        });
//        btnSimpan.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v){
//                dialogKonfimasi.dismiss();
//                getBus().send(new RxBusObject(RxBusObject.RxBusKey.SAVE_KYC_DATA_WHILE_BACK, null));
//            }
//        });
//    }

    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError(){
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }

    public void setUpAdapter(String jsonCountry, String jsonState, String jsonCity){
        String jsonBank = new Gson().toJson(banks);
        pagerAdapter = new KenaliResikoUserPageAdapter(this, getChildFragmentManager(), request, jsonCountry, jsonState, jsonCity, jsonBank, sourceRoute);
        pager.setAdapter(pagerAdapter);

        if(pageNumber == 0){
            TextView title = (TextView) getActivity().findViewById(R.id.title);
            title.setText("Data Profil Risiko");
        }
    }

    @Override
    public void nextWithoutValidation(){

    }

    @Override
    public void saveDataKycWithBackpress(){
        System.out.println("Personal data main...");
    }

    @Override
    public void previewsWithoutValidation(){
        //getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null));
    }

    public void and(){
        MainActivity.startActivity((BaseActivity) getActivity());
    }

    void showDialogAfterSubmit(String info, Boolean showInfo){

        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.infortmation).toUpperCase())
                .titleColor(Color.BLACK)
                .content(info)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
//                        if (showInfo) {
//                            KycFinishActivity.startActivity((BaseActivity) getActivity());
//                        } else {
                            MainActivity.startActivity((BaseActivity) getActivity());
//                        }
//                        if(isActivityRunning("com.danareksa.investasik.ui.activities.UserProfileActivity")){
//                            UserProfileActivity.userProfileActivity.finish();
//                        }
                        getActivity().finish();
                    }
                })
                .show();
    }


    void showDialogSubmit(String info){
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.infortmation).toUpperCase())
                .titleColor(Color.BLACK)
                .content(info)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which){

                        //new
                        getActivity().finish();
                        KycFinishActivity.startActivity((BaseActivity) getActivity());
                        //end

                        //MainActivity.startActivity((BaseActivity) getActivity());
                        if(isActivityRunning("com.danareksa.investasik.ui.activities.UserProfileActivity")){
                            UserProfileActivity.userProfileActivity.finish();
                        }
                    }
                })
                .show();
    }

    protected Boolean isActivityRunning(String canonicalNameClass)
    {
        ActivityManager activityManager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);
        for (ActivityManager.RunningTaskInfo task : tasks){
            System.out.println("activity running :: " + task.baseActivity.getClassName());
            if(canonicalNameClass.equalsIgnoreCase(task.baseActivity.getClassName())){
                return true;
            }
        }
        return false;
    }

    @OnClick(R.id.tvTryAgain)
    void tvTryAgain(){
        presenter.getKycLookupData();
    }

    public void changeNameButtonNext(){
//        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")) {
//            bNext.setText("Cek Hasil");
//        }else{
            bNext.setText("And");
//        }
    }

//    public void changeNameButtonNextCekHasil(){
//        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")) {
//            bNext.setText("Cek Hasil");
//        }else{
//            bNext.setText("Cek Hasil");
//        }
//    }

    public void toPageOwnersOfBenefit(){
        if (pageNumber < pagerAdapter.getCount()-1){
            pageNumber = 11; //11 == page pemilik manfaat
            pager.setCurrentItem(pageNumber);
            setTextNavigation(pageNumber);
        }
    }

    public void toPageCorresponden(){
        if (pageNumber < pagerAdapter.getCount()-1){
            pageNumber = 9; //9 == page correspondens
            pager.setCurrentItem(pageNumber);
            setTextNavigation(pageNumber);
        }
    }

    @Override
    public void previewsWhileError(){
        if(isErrorLoadKyc == true){
            System.out.println("error kyc===>");
            UserProfileActivity.startActivity((BaseActivity) getActivity());
            getActivity().finish();
        }
    }

}
