package com.danareksa.investasik.ui.adapters.rv;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.Fee;
import com.danareksa.investasik.data.api.responses.CartListResponse;
import com.danareksa.investasik.data.api.responses.FundAllocationResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.fragments.cart.CartFragment;
import com.danareksa.investasik.ui.fragments.cart.CartPresenter;
import com.danareksa.investasik.util.AmountFormatter;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.CartHolder> {

    private List<CartListResponse> list;
    private CartListResponse item;
    private CartPresenter presenter;
    private Context context;
    private CartFragment cartFragment;


    public CartListAdapter(CartPresenter presenter, List<CartListResponse> list, FundAllocationResponse packages, Context context, CartFragment fragment) {
        this.list = list;
        this.context = context;
        this.presenter = presenter;
        this.cartFragment = fragment;
    }


    @Override
    public CartHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_cart_new, parent, false);
        return new CartHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final CartHolder holder, final int position){
        item = list.get(holder.getAdapterPosition());
        holder.item = item;

        holder.tvProductName.setText(item.getFundPackages().getFundPackageName());
        holder.tvIfua.setText(item.getTrxPkg().getIfua() + " - " + item.getTrxPkg().getInvestmentGoal());
        holder.tvAccountType.setText(item.getTrxPkg().getAccountType());
        holder.edtAmount.setText(item.getTransactionAmount());
        holder.edtTotal.setText(AmountFormatter.formatCurrencyWithoutComma(Double.parseDouble(item.getTotal())));
        holder.edtFee.setText(item.getFeePercentage() + " | " + AmountFormatter.formatNonCurrency(item.getFeeAmount()));

        if (item.getTransactionType().getTrxCode().equalsIgnoreCase("SUBCR")){
            holder.tvProductType.setText("Pembelian");
        } else if (item.getTransactionType().getTrxCode().equalsIgnoreCase("TOPUP")){
            holder.tvProductType.setText("Pembelian");
        }

        Picasso.with(context).load(InviseeService.IMAGE_DOWNLOAD_URL + item.getPackage().getPackageImage() + "&token=" + PrefHelper.getString(PrefKey.TOKEN))
                .placeholder(R.drawable.ic_logo_launcher)
                .error(R.drawable.ic_logo_launcher)
                .into(holder.ivProduct);


        holder.edtAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equalsIgnoreCase("")){

                    CartListResponse objItem = holder.item; //(CartListResponse) holder.itemView.getTag();

                    for (int i = 0; i < objItem.getFeeList().size(); i++) {

                        Long amount = Long.parseLong(s.toString());

                        Fee fee = objItem.getFeeList().get(i);
                        Long amountMin = fee.getAmountMin().longValue();
                        Long amountMax = fee.getAmountMax().longValue();
                        if (amountMax == 0)
                            amountMax = amount + 1;

                        if (amount >= amountMin && amount <= amountMax) {

                            double feeAmount  = amount * fee.getFeeAmount();
                            //double feePercent  = fee.getFeeAmount() * 100;
                            //long fAmount = (long) feeAmount;
                            holder.edtFee.setText(AmountFormatter.formatFeePercent(fee.getFeeAmount()) + " | " + AmountFormatter.formatNonCurrency(feeAmount));
                            Double total = amount + (amount * fee.getFeeAmount());
                            /*holder.edtTotal.setText(BigDecimal.valueOf(total).toPlainString());*/
                            holder.edtTotal.setText(AmountFormatter.formatCurrencyWithoutComma(total));
                            holder.item.setFeePercentage(AmountFormatter.formatFeePercent(fee.getFeeAmount()));
                            holder.item.setFeeAmount(feeAmount);
                            holder.item.setTransactionAmount(amount.toString());
                            holder.item.setTotal(total.toString());
                            holder.item.setFeePrice(total - amount);

                        }
                    }

                } else {
                    /*holder.edtAmount.setText("0");*/
                    holder.edtFee.setText("0%");
                    holder.edtTotal.setText("0");
                    holder.item.setTotal("0");
                    holder.item.setFeePercentage("0%");
                }

            }

            @Override
            public void afterTextChanged(Editable editable){

            }
        });


        holder.bDelete.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                presenter.deleteCart(holder.item.getTrx().getId(), holder.item);
            }
        });


        holder.bProspectus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //presenter.cartListToDownload(position, "prospectus");
                if(list.size() > 0){
                    presenter.fundAllocationFromAdapter(holder.item.getPackages().getId(), "prospectus");
                }
            }
        });

        holder.bFfs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //presenter.cartListToDownload(position, "ffs");
                if(list.size() > 0){
                    presenter.fundAllocationFromAdapter(holder.item.getPackages().getId(), "ffs");
                }
            }
        });
    }

    public void remove(CartListResponse item) {
        int position = list.indexOf(item);
        list.remove(position);
        cartFragment.removeCart(item);
        notifyItemRemoved(position);
    }

    public List<CartListResponse> getList() {

//        System.out.println("size : " + list.size());
//        for (int i = 0; i < list.size(); i++) {
//
//            System.out.println("i ke + " + i);
//            CartListResponse it = list.get(i);
//            if ( i > holderList.size() -1 ) break;
//            CartHolder ho = holderList.get(i);
//
//            it.setTransactionAmount(ho.edtAmount.getText().toString());
//            it.setFeePercentage(ho.edtFee.getText().toString());
//            /*it.setFeePrice(Double.parseDouble(ho.edtTotal.getText().toString()) - Double.parseDouble(ho.edtAmount.getText().toString()));*/
//
//            if(!ho.edtAmount.getText().toString().equals("0") && !ho.edtAmount.getText().toString().equals("") && ho.total != null){
//                it.setFeePrice(ho.total - Double.parseDouble(ho.edtAmount.getText().toString()));
//            }else{
//                it.setFeePrice(0.0);
//            }
//            /*it.setTotal(ho.edtTotal.getText().toString());*/
//
//            if(ho.total != null){
//                BigDecimal totalConvert = BigDecimal.valueOf(ho.total);
//                it.setTotal(totalConvert.toString());
//            }else{
//                it.setTotal("0");
//            }
//
//            Log.d("id", "getList: "+it.getPackage().getId());
//
//            list.set(i, it);
//        }
//
//
        return list;
    }


    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class CartHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.ivProduct)
        CircleImageView ivProduct;
        @Bind(R.id.bProspectus)
        ImageView bProspectus;
        @Bind(R.id.bFfs)
        ImageView bFfs;
        @Bind(R.id.bDelete)
        ImageView bDelete;
        @Bind(R.id.tvProductName)
        TextView tvProductName;
        /*   @Bind(R.id.tvProductType)
           TextView tvProductType;*/ //disable
        @Bind(R.id.edtAmount)
        EditText edtAmount;
        @Bind(R.id.edtFee)
        EditText edtFee;
        @Bind(R.id.edtTotal)
        EditText edtTotal;
        @Bind(R.id.edtAmountWrapper)
        TextInputLayout edtAmountWrapper;
        @Bind(R.id.edtFeeWrapper)
        TextInputLayout edtFeeWrapper;
        @Bind(R.id.tvIfua)
        TextView tvIfua;
        @Bind(R.id.tvAccountType)
        TextView tvAccountType;
        @Bind(R.id.tvProductType)
        TextView tvProductType;
        private CartListResponse item;

/*
        @Bind(R.id.edtTotalWrapper)
        TextInputLayout edtTotalWrapper;
*/

        public CartHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }





}
