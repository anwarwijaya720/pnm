package com.danareksa.investasik.ui.fragments.checkout;
import com.danareksa.investasik.data.api.requests.BcaKlikpayConfirmationRequest;
import com.danareksa.investasik.data.api.responses.GenericResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.CartActivity;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by asep.surahman on 17/10/2018.
 */

public class BcaKlikpayPresenter {

    private BcaKlikpayFragment fragment;


    public BcaKlikpayPresenter(BcaKlikpayFragment fragment) {
        this.fragment = fragment;
    }


    void getInfoPayment(String transNo) {
        fragment.showProgressBar();
        fragment.getApi().paymentConfirmationBcaKlikpay(getBcaKlikpayRequest(transNo))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                        fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(GenericResponse response){
                        fragment.dismissProgressBar();
                        if(response.getCode() == 0){
                            fragment.fetchResult();
                            CartActivity.fa.finish();
                        }else if(response.getCode() == 1){//{"code":1,"info":"Payment successfully cancelled."}
                            fragment.showDialog("Transaksi dibatalkan");
                        }else{
                            fragment.showDialog(response.getInfo());
                        }
                    }
                });
    }



    public BcaKlikpayConfirmationRequest getBcaKlikpayRequest(String transNo){
        BcaKlikpayConfirmationRequest bcaKlikpayConfirmationRequest = new BcaKlikpayConfirmationRequest();
        bcaKlikpayConfirmationRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        bcaKlikpayConfirmationRequest.setTransactionNo(transNo);
        return  bcaKlikpayConfirmationRequest;
    }



}
