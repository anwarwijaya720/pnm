package com.danareksa.investasik.ui.fragments.tambahrekening;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.Bank;
import com.danareksa.investasik.data.api.beans.BankAccountImage;
import com.danareksa.investasik.data.api.beans.Branch;
import com.danareksa.investasik.data.api.beans.Country;
import com.danareksa.investasik.data.api.requests.AddAccountRequest;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.eventBus.RxBusObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

import static android.app.Activity.RESULT_OK;

/**
 * Created by asep.surahman on 25/07/2018.
 */

@RuntimePermissions
public class AccountDataFragment extends BaseInvest{

    public static final String TAG = AccountDataFragment.class.getSimpleName();

    @Bind(R.id.bNext)
    Button bNext;
    @Bind(R.id.sCountry)
    Spinner sCountry;
    @Bind(R.id.sBankName)
    Spinner sBankName;
    @Bind(R.id.sBranchName)
    Spinner sBranchName;
    @Bind(R.id.etAccountNumber)
    EditText etAccountNumber;
    @Bind(R.id.etAccountName)
    EditText etAccountName;
    @Bind(R.id.imgAccount)
    ImageView imgAccount;
    @Bind(R.id.etBranchOther)
    EditText etBranchOther;
    public List<Country> countries;
    public List<Bank> bank;
    public List<Branch> branches;
    private String jsonCountry;
    private String jsonBank;
    private String type;
    AccountDataPresenter presenter;
    String bankAccountName, bankAccountNumber;
    Integer bankBranchId, bankId, countryId;
    String bankIdString = "";

    int choosenTask = 0;
    private final int SELECT_PHOTO_BUKU = 0;
    private final int REQUEST_CAMERA_BUKU = 2;
    private File tempFile;
    private Uri picUri;
    private String IdPhoto = "";
    public String fileName = "";
    BankAccountImage bankAccountImage;
    boolean validImage = false;

    List<Bank> bankList;



    public static Fragment getFragment(String jsonCountry, String jsonBank, String typeAccount, AddAccountRequest acountRequest) {
        Bundle bundle = new Bundle();
        bundle.putString("jsonCountry", jsonCountry);
        bundle.putString("jsonBank", jsonBank);
        bundle.putString("type", typeAccount);
        bundle.putSerializable(ADD_ACCOUNT_REQUEST, acountRequest);
        AccountDataFragment accountDataFragment = new AccountDataFragment();
        accountDataFragment.setArguments(bundle);
        return accountDataFragment;
    }


    @Override
    protected int getLayout(){
        return R.layout.f_tambah_rekening_data_rekening;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        countries = new ArrayList<>();
        bank = new ArrayList<>();
        branches = new ArrayList<>();
        jsonCountry = getArguments().getString("jsonCountry");
        jsonBank = getArguments().getString("jsonBank");
        type = getArguments().getString("type");
        presenter = new AccountDataPresenter(this);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        if(type.equals(Constant.INVEST_TYPE_LUMPSUM)){
            presenter.getAllBank();
        }else{
            presenter.getBankList();
        }

    }


    @Override
    public void onResume(){
        super.onResume();
    }


    @OnClick(R.id.bNext)
    void bNext(){
        if(!validate()){
            onValidFailed();
            return;
        }else{
            setDataAccount();
            System.out.println("country id : " + countryId);
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
        }
    }


    private void setDataAccount(){
        acountRequest.setBankId(bankId);
        acountRequest.setBankBranchId(bankBranchId);

        if(!String.valueOf(countries.get(sCountry.getSelectedItemPosition()).getId()).equals("")){
            countryId = Integer.valueOf(countries.get(sCountry.getSelectedItemPosition()).getId());
        }

        System.out.println("country id = > " + countryId + " - bank branch id = > " + bankBranchId);
        acountRequest.setCountryId(countryId);
        acountRequest.setBankAccountName(getAndTrimValueFromEditText(etAccountName));
        acountRequest.setBankAccountNumber(getAndTrimValueFromEditText(etAccountNumber));
        acountRequest.setBankAccountImage(bankAccountImage);
    }

    public void init(){

        Type listType = new TypeToken<List<Country>>(){}.getType();
        Gson gson = new Gson();
        countries = gson.fromJson(jsonCountry, listType);
        setupSpinnerCountry(sCountry, countries, Constant.ID_COUNTRY_INDO);

        /*Type listTypeBank= new TypeToken<List<Bank>>() {}.getType();
        Gson gsonBank = new Gson();
        bank = gsonBank.fromJson(jsonBank, listTypeBank);*/
        setupSpinnerBank(sBankName, bankList, "null");
        onItemSelectedItem();

        bankAccountName   = getAndTrimValueFromEditText(etAccountName);
        bankAccountNumber = getAndTrimValueFromEditText(etAccountNumber);
    }


    public void onItemSelectedItem(){
        sBankName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                bankIdString = bankList.get(sBankName.getSelectedItemPosition()).getId();
                if(!bankIdString.equals("")){
                    bankId = Integer.valueOf(bankIdString);
                    presenter.getBranch(String.valueOf(bankId));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView){
            }
        });


        sBranchName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id){
                etBranchOther.setVisibility(View.GONE);

                if(branches != null && branches.size() != 0){
                    bankBranchId = branches.get(sBranchName.getSelectedItemPosition()).getId();
                    if (bankBranchId == -1)
                        etBranchOther.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){

            }
        });

        sCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countryId = Integer.valueOf(countries.get(sCountry.getSelectedItemPosition()).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void fetchResultBranchToLayout(){
        setupSpinnerBranch(sBranchName, branches, "");
    }


    public void fetchResultBankToLayout(){
        init();
    }




    @Override
    public void nextWithoutValidation(){

    }

    @Override
    public void saveDataKycWithBackpress(){

    }



    @OnClick(R.id.imgAccount)
    void imgAccount(){

        new MaterialDialog.Builder(getActivity())
                .title("Change Picture")
                .items(R.array.change_picture)
                .itemsCallback(new MaterialDialog.ListCallback(){
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if(which == 0){
                            choosenTask = REQUEST_CAMERA_BUKU;
                            AccountDataFragmentPermissionsDispatcher.startCameraBukuWithPermissionCheck(AccountDataFragment.this);
                        } else {
                            choosenTask = SELECT_PHOTO_BUKU;
                            AccountDataFragmentPermissionsDispatcher.startGalleryBukuWithPermissionCheck(AccountDataFragment.this);
                        }
                    }
                })
                .show();
    }


    public static File createTemporaryFile(Context context, String folder_name, String ext) {
        try {
            File folder = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), folder_name);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            return File.createTempFile(""+System.currentTimeMillis(), ext, folder);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void startGalleryBuku(){
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_PHOTO_BUKU);
    }


    @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void startCameraBuku(){
        tempFile = createTemporaryFile(getContext(), "id_pic", ".jpg");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        picUri = FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".provider", tempFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);
        startActivityForResult(intent, REQUEST_CAMERA_BUKU);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        AccountDataFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent){
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode){
            case SELECT_PHOTO_BUKU:
                if (resultCode == RESULT_OK){

                    Uri selectedImage = imageReturnedIntent.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    File file = new File(picturePath);
                    long length = file.length() / 1024; // Size in KB

                  /*  if(length > 2000){
                        showFailedDialogUploadImage(getResources().getString(R.string.uploadlimit_image));
                    }*/

                    tempFile = file;
                    onCaptureImageResultId();

                }
                break;
            case REQUEST_CAMERA_BUKU:
                if (resultCode == Activity.RESULT_OK) {
                    onCaptureImageResultId();
                }
                break;

        }
    }



    void onCaptureImageResultId(){
        // Setting option to resize image
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(tempFile.getPath(), options);
        final int REQUIRED_SIZE = 310;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;

        // Load file with option parameter, then compress it
        Bitmap b = BitmapFactory.decodeFile(tempFile.getPath(), options);

        Matrix matrix = new Matrix();

        try {
            ExifInterface exif = null;
            exif = new ExifInterface(tempFile.getPath());
            String orientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            if (orientation.equals(ExifInterface.ORIENTATION_NORMAL)) {
                // Do nothing. The original image is fine.
            } else if (orientation.equals(ExifInterface.ORIENTATION_ROTATE_90 + "")) {
                matrix.postRotate(90);
            } else if (orientation.equals(ExifInterface.ORIENTATION_ROTATE_180 + "")) {
                matrix.postRotate(180);
            } else if (orientation.equals(ExifInterface.ORIENTATION_ROTATE_270 + "")) {
                matrix.postRotate(270);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        Bitmap resizedBitmap = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
        resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, bytes);

        byte[] byteArray = bytes.toByteArray();
        // get the base 64 string

        fileName = tempFile.getName();
        IdPhoto = Base64.encodeToString(byteArray, Base64.DEFAULT);
        imgAccount.setImageBitmap(resizedBitmap);

        if(!fileName.equals("") && !IdPhoto.equals("")){
            validImage = true;
            bankAccountImage = new BankAccountImage();
            bankAccountImage.setFile_name(fileName);
            bankAccountImage.setContent(IdPhoto);
        }
    }



    public boolean validate(){
        boolean valid = true;

        String accountNmae = etAccountName.getText().toString();
        String accountNumber = etAccountNumber.getText().toString();

        if(accountNmae.isEmpty() && accountNmae.length() <= 0){
            etAccountName.setError("Mohon isi kolom ini");
            valid = false;
        }

        if(accountNumber.isEmpty() && accountNumber.length() <= 0){
            etAccountNumber.setError("Mohon isi kolom ini");
            valid = false;
        }

        if(accountNumber.length() < 3){
            etAccountNumber.setError("Minimum 3 digit");
            valid = false;
        }

        if(accountNmae.length() < 3){
            etAccountName.setError("Minimum 3 karakter");
            valid = false;
        }

        if(sCountry.getSelectedItem().toString().equals("")){
            valid = false;
            ((TextView) sCountry.getSelectedView()).setError("Mohon isi kolom ini");
        }

        if(sBankName.getSelectedItem().toString().equals("")){
            valid = false;
            ((TextView) sBankName.getSelectedView()).setError("Mohon isi kolom ini");
        }

        if(sBranchName.getSelectedItem().toString().equals("")){
            valid = false;
            ((TextView) sBranchName.getSelectedView()).setError("Mohon isi kolom ini");
        }

        if(etBranchOther.getText().toString().isEmpty() && etBranchOther.getVisibility() == View.VISIBLE){
            valid = false;
            etBranchOther.setError("Mohon isi kolom ini");
        }

        if(validImage == false){
            valid = false;
        }

        return  valid;
    }


    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }


    public void showFailedDialogUploadImage(String message) {
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.infortmation).toUpperCase())
                .titleColor(Color.BLACK)
                .content(message)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .cancelable(false)
                .show();
    }


    @Override
    public void addAccountBackpress() {
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null));
    }



}





