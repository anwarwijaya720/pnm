package com.danareksa.investasik.ui.fragments.tambahrekening;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.SelectInvestAccountActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.Constant;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by asep.surahman on 30/05/2018.
 */

public class RegulerInvestFragment extends BaseFragment{


    public static final String TAG = RegulerInvestFragment.class.getSimpleName();
    private static final String PACKAGES = "packages";
    @Bind(R.id.bNext)
    Button bNext;

    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new RegulerInvestFragment(), TAG);
            fragmentTransaction.commit();
        }
    }

    public static Fragment getFragment() {
        Fragment f = new RegulerInvestFragment();
        return f;
    }


    @Override
    protected int getLayout() {
        return R.layout.f_pilih_investasi_desc_reguler;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @OnClick(R.id.bNext)
    void bNext(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")){
            SelectInvestAccountActivity.startActivity((BaseActivity) getActivity(), Constant.INVEST_TYPE_REGULER);
        }else{
            showDialogAfterSubmit("Akun Anda belum terverifikasi!");
        }
    }


    void showDialogAfterSubmit(String info){
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.infortmation).toUpperCase())
                .titleColor(Color.BLACK)
                .content(info)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        getActivity().finish();
                    }
                })
                .show();
    }





}
