package com.danareksa.investasik.ui.fragments.checkout;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.BcaKlikPayData;
import com.danareksa.investasik.data.api.beans.CartList;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.MainActivity;
import com.danareksa.investasik.ui.adapters.rv.SummaryBcaKlikpayAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.DateUtil;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.OnClick;
import icepick.State;

/**
 * Created by asep.surahman on 04/10/2018.
 */

public class SummaryBcaKlikpayFragment extends BaseFragment {


    public static final String TAG = SummaryBcaKlikpayFragment.class.getSimpleName();
    private final static String CART_LIST = "cartList";
    private final static String BCA_KLIK_LIST = "bcaKlikList";
    @State
    public CartList cartList;
    @State
    public BcaKlikPayData bcaKlikpayTrans;

    @Bind(R.id.bOk)
    Button bOk;
    @Bind(R.id.rv)
    RecyclerView rv;

    @Bind(R.id.txvOrderNumber)
    TextView txvOrderNumber;
    @Bind(R.id.txvRekeningInvestasi)
    TextView txvRekeningInvestasi;
    @Bind(R.id.txvKodeInvestor)
    TextView txvKodeInvestor;
    @Bind(R.id.txvMetodePembayaran)
    TextView txvMetodePembayaran;
    @Bind(R.id.tvNABDesc)
    TextView tvNABDesc;
    String paymentType;
    String transType = "";

    public static void showFragment(BaseActivity sourceActivity, CartList cartList, BcaKlikPayData bcaKlikpayTrans, String paymentType){

        if (!sourceActivity.isFragmentNotNull(TAG)){
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, R.anim.slide_out_left, android.R.anim.slide_in_left, R.anim.slide_out_left);
            SummaryBcaKlikpayFragment fragment = new SummaryBcaKlikpayFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(CART_LIST, cartList);
            bundle.putSerializable(BCA_KLIK_LIST, bcaKlikpayTrans);
            bundle.putString("paymentType", paymentType);
            fragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }


    @Override
    protected int getLayout() {
        return R.layout.f_summary_bcaklikpay;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        paymentType = getArguments().getString("paymentType");
        cartList = (CartList) getArguments().getSerializable(CART_LIST);
        bcaKlikpayTrans = (BcaKlikPayData) getArguments().getSerializable(BCA_KLIK_LIST);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        init();
        loadList();
    }




    public void init(){
        txvOrderNumber.setText(bcaKlikpayTrans.getData().getTransactionNo());
        if(cartList.getCartList().get(0).getTransactionType().getTrxCode().equalsIgnoreCase("SUBCR")){
            transType = "Pembelian";
        }else if(cartList.getCartList().get(0).getTransactionType().getTrxCode().equalsIgnoreCase("TOPUP")){
            transType = "Pembelian";
        }
        txvRekeningInvestasi.setText(transType +" - "+ cartList.getCartList().get(0).getTrxPkg().getAccountType());
        txvKodeInvestor.setText(cartList.getCartList().get(0).getTrxPkg().getIfua()+" - "+cartList.getCartList().get(0).getTrxPkg().getInvestmentGoal());
        if(paymentType.equals("BCAK")){
            txvMetodePembayaran.setText("BCA KlikPay");
        }

        if(!bcaKlikpayTrans.getPriceDate().equals("")){
            String priceDate = bcaKlikpayTrans.getPriceDate();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date pDate = null;
            try {
                pDate = formatter.parse(priceDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            tvNABDesc.setText("• Transaksi Anda akan diproses menggunakan NAB tanggal " + DateUtil.format(pDate,"dd-MM-yyyy"));
        }else{
            tvNABDesc.setVisibility(View.GONE);
        }
    }

    public void loadList() {
        rv.setAdapter(new SummaryBcaKlikpayAdapter(getActivity(), cartList.getCartList(), bcaKlikpayTrans.getData(), this));
    }

    public String getTotal(){
        Double total = 0d;
        for (int i = 0; i < cartList.getCartList().size(); i++) {
            Double t = Double.parseDouble(cartList.getCartList().get(i).getTotal());
            total += t;
        }
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        nf.setMinimumFractionDigits(0);
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("");
        ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);
        return "IDR " + nf.format(total);
    }


    @OnClick(R.id.bOk)
    void bOk(){
        getActivity().finish();
        MainActivity.startActivity((BaseActivity) getActivity());
    }


}
