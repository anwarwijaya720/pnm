package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.PromoResponse;

import butterknife.Bind;
import icepick.State;

public class SyaratKetentuanPromoActivity extends BaseActivity {

    private static final String NEWS = "news";
    private static final String PROMO = "promo";
    @State
    PromoResponse response;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;

    public static void startActivity(BaseActivity sourceActivity) {
        Intent intent = new Intent(sourceActivity, SyaratKetentuanPromoActivity.class);
        sourceActivity.startActivity(intent);
    }

    //untuk membawa parameter(optional)

    public static void startActivity(BaseActivity sourceActivity, PromoResponse news) {
        Intent intent = new Intent(sourceActivity, NewsActivity.class);
        intent.putExtra(PROMO, news);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_syarat_ketentuan_promo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        response = (PromoResponse) getIntent().getSerializableExtra(PROMO);
        super.onCreate(savedInstanceState);
        setupToolbar();
    }

    public void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        title.setText("Promo");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        System.out.println("exit===>>");
        if(response.getCode()==null){
            PromoActivity.startActivity(this);
            finish();
        }else{
            DetailPromoActivity.startActivity(this, response);
            finish();
        }

    }
}
