package com.danareksa.investasik.ui.fragments.catalogue;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.ProductList;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.CatalogueActivity;
import com.danareksa.investasik.ui.activities.ChooseIfuaActivity;
import com.danareksa.investasik.ui.activities.SignInActivity;
import com.danareksa.investasik.ui.adapters.pager.DetailOfCataloguePagerAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.DateUtil;
import com.danareksa.investasik.util.eventBus.RxBusObject;
import com.danareksa.investasik.util.ui.WrapContentHeightViewPager;
import com.squareup.picasso.Picasso;

import org.sufficientlysecure.htmltextview.HtmlResImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.text.DecimalFormat;
import java.util.Calendar;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.OnClick;
import icepick.State;

/**
 * Created by fajarfatur on 1/22/16.
 */
public class DetailOfCatalogueFragment extends BaseFragment {

    public static final String TAG = DetailOfCatalogueFragment.class.getSimpleName();
    private static final String PACKAGES = "packages";

    @Bind(R.id.tabs)
    TabLayout tabs;

    @Bind(R.id.ivProduct)
    ImageView ivProduct;
    @Bind(R.id.tvProductName)
    TextView tvProductName;
    @Bind(R.id.tvProductDesc)
    TextView tvProductDesc;
    @Bind(R.id.txvCurrency)
    TextView txvCurrency;
    @Bind(R.id.txvRiskCategory)
    TextView txvRiskCategory;
    @Bind(R.id.pager)
    WrapContentHeightViewPager pager;

    /*
    @Bind(R.id.txvTitle)
    TextView txvTitle;
    */

    @Bind(R.id.scrollView)
    ScrollView scrollView;
    @Bind(R.id.tvDisclaimerDesc)
    TextView tvDisclaimer;
    @Bind(R.id.tvProvision)
    TextView tvProvision;

    @Bind(R.id.typeProdak)
    TextView typeProdak;

    @Bind(R.id.cbAgree)
    CheckBox cbAgree;
    @Bind(R.id.bSubscribe)
    Button btnSubscribe;
    @Bind(R.id.llDesc)
    LinearLayout llDesc;
    @Bind(R.id.ivArrow)
    ImageView ivArrow;
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;

    @BindString(R.string.catalogue_diclaimer)
    String catalogueDisclaimer;
    @BindString(R.string.catalogue_provision)
    String provison;

    @BindString(R.string.fund_alloc)
    String fundAlloc;
    @BindString(R.string.package_performance)
    String packagePerformance;
    @BindString(R.string.business_rule)
    String businessRule;

    private DetailOfCataloguePresenter presenter;
    private DetailOfCataloguePagerAdapter pagerAdapter;

    @State
    Packages packages;

    @State
    ProductList product;

    @State
    int pageNumber = 0;

    public static void showFragment(BaseActivity sourceActivity, ProductList packages) {

        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            fragmentTransaction.replace(R.id.container, getFragment(packages), TAG);
            fragmentTransaction.commit();
        }
    }

    public static Fragment getFragment(ProductList packages) {
        Fragment f = new DetailOfCatalogueFragment();
        Bundle extras = new Bundle();
        extras.putSerializable(PACKAGES, packages);
        f.setArguments(extras);
        return f;
    }

    @Override
    protected int getLayout() {
        return R.layout.f_detail_of_catalogue;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        presenter = new DetailOfCataloguePresenter(this);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        if (product == null) {
            Bundle extras = getArguments();
            if (extras != null && extras.containsKey(PACKAGES)) {
                product = (ProductList) extras.getSerializable(PACKAGES);
            }
        }


        presenter.packageDetail(product);
        cbAgree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                                @Override
                                               public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                   if (isChecked) {
                                                       btnSubscribe.setEnabled(true);
                                                   } else {
                                                       btnSubscribe.setEnabled(false);
                                                   }
                                               }
                                           }
        );

    }

    @Override
    public void busHandler(RxBusObject.RxBusKey busKey, Object busObject) {
        super.busHandler(busKey, busObject);
        switch (busKey) {
            case SCROLL_TO_BOTTOM:
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        scrollView.fullScroll(View.FOCUS_DOWN);
//                    }
//                }, 200);
//                break;
        }

    }

    public void setupViewPager() {
        pagerAdapter = new DetailOfCataloguePagerAdapter(this, getChildFragmentManager(), packages);
        pagerAdapter.addFragmentTitle("Kinerja");
        pagerAdapter.addFragmentTitle("Info Produk");
        pager.setAdapter(pagerAdapter);
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
                pager.requestLayout();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        //setTabTitle();
        tabs.setupWithViewPager(pager);
    }

    public void setPackagesDataToView() {
        DecimalFormat df = new DecimalFormat("0.000");

        Calendar cal = Calendar.getInstance();
        cal.setTime(DateUtil.format(packages.getEffectiveDate(), DateUtil.INVISEE_RETURN_FORMAT2));

        ivProduct.setScaleType(ImageView.ScaleType.FIT_XY);
        ivProduct.setAdjustViewBounds(true);

        ivProduct.layout(0,0,0,0);
        Picasso.with(getActivity()).load(InviseeService.IMAGE_DOWNLOAD_URL + packages.getPackageImage())
                .into(ivProduct);

        ((CatalogueActivity)(getActivity())).setTitleName(packages.getFundPackageName());

        tvProductName.setText(packages.getFundPackageName());

        typeProdak.setText(product.getTypeproduct());

        HtmlTextView htmlTextView = (HtmlTextView) getActivity().findViewById(R.id.tvProductDesc);
        htmlTextView.setHtml(packages.getPackageDesc(),
                new HtmlResImageGetter(htmlTextView));

        /*tvProductDesc.setText(packages.getPackageDesc());*/
        txvCurrency.setText(packages.getCurrency());
        txvRiskCategory.setText(packages.getRiskProfile());
        tvDisclaimer.setText(catalogueDisclaimer);
        tvProvision.setText(provison);
    }


    @OnClick(R.id.bSubscribe)
    void onClickSubscribe() {
        //presenter.getMaxScore(product);
        if (BaseActivity.IS_FROM_LANDING) {
            ((CatalogueActivity)this.getContext()).finish();
            SignInActivity.startActivity((BaseActivity) getActivity());
        } else {
            ChooseIfuaActivity.startActivity((BaseActivity) getActivity(), product, packages);
        }

        /*
        if (PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equalsIgnoreCase("ACT")) {
            new MaterialDialog.Builder(getActivity())
                    .iconRes(R.mipmap.ic_launcher)
                    .backgroundColor(cAppsPrimary)
                    .title(getString(R.string.success).toUpperCase())
                    .titleColor(Color.WHITE)
                    .content(R.string.catalogue_act_dialog)
                    .contentColor(Color.WHITE)
                    .positiveText(R.string.yes)
                    .positiveColor(Color.WHITE)
                    .negativeText(R.string.no)
                    .negativeColor(cDanger)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog dialog, DialogAction which) {
                            UserProfileActivity.startActivity((BaseActivity) getActivity());
                        }
                    })
                    .cancelable(false)
                    .show();

        } else {
            presenter.subscribeToCart(packages);
        }*/

    }


    @OnClick(R.id.btnFull)
    void showFullDesc() {
        if (llDesc.getVisibility() == View.GONE) {
            tvProductDesc.setMaxLines(Integer.MAX_VALUE);
            tvProductDesc.setEllipsize(null);
            llDesc.setVisibility(View.VISIBLE);
            ivArrow.setImageResource(R.drawable.ic_keyboard_arrow_up_white_24dp);
        } else {
            tvProductDesc.setMaxLines(5);
            tvProductDesc.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            llDesc.setVisibility(View.GONE);
            ivArrow.setImageResource(R.drawable.ic_keyboard_arrow_down_white_24dp);
        }
    }

    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.tvTryAgain)
    void retryConnection() {
        presenter.packageDetail(product);
    }
}
