package com.danareksa.investasik.ui.fragments.signIn;

import android.support.annotation.NonNull;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.data.api.requests.ForgotPasswordUserRequest;
import com.danareksa.investasik.data.api.requests.ResendCodePasswordRequest;
import com.danareksa.investasik.data.api.requests.ResetPasswordRequest;
import com.danareksa.investasik.data.api.responses.GenericResponse;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.SignInActivity;
import com.danareksa.investasik.util.Crypto;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by pandu.abbiyuarsyah on 13/07/2017.
 */

public class RePasswordPresenter {

    private RePasswordFragment fragment;

    public RePasswordPresenter(RePasswordFragment fragment) {
        this.fragment = fragment;
    }

    ResetPasswordRequest constructResetPasswordRequest(String email) {
        ResetPasswordRequest request = new ResetPasswordRequest();
        request.setEmail(email);
        request.setResetCode(fragment.etResetCode.getText().toString());
        request.setConfirmPassword(Crypto.Encrypt(fragment.etConfirmPassword.getText().toString()));
        return request;
    }


    void requestResetPassword(final ResetPasswordRequest request) {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().resetPassword(request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                        fragment.showFailedDialog(fragment.connectionError);
                    }

                    @Override
                    public void onNext(GenericResponse genericResponse) {
                        Timber.i("Code %s", genericResponse.getCode());
                        Timber.i("Info %s", genericResponse.getInfo());
                        fragment.dismissProgressDialog();
                        if (genericResponse.getCode() == 0 || genericResponse.getInfo().contains("Password berhasil diganti.")) {
                            fragment.showSuccessDialogCallback(genericResponse.getInfo(), new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                    SignInActivity.startActivity((BaseActivity) fragment.getActivity());
                                    fragment.getActivity().finish();
                                }
                            });
                        }else{
                            fragment.showFailedDialog(genericResponse.getInfo());
                        }
                    }
                });

    }

    ResendCodePasswordRequest constructForgotResendCode(String email, String answer, String question) {
        ResendCodePasswordRequest request = new ResendCodePasswordRequest();
        request.setEmail(email);
        request.setAnswer(answer);
        request.setQuestion(question);
        return request;
    }

    void requestResendCode(final ResendCodePasswordRequest resendCodePasswordRequest) {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().resendCode(
                resendCodePasswordRequest.getEmail(),
                resendCodePasswordRequest.getAnswer(),
                resendCodePasswordRequest.getQuestion())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(GenericResponse genericResponse) {
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), genericResponse.getInfo(), Toast.LENGTH_SHORT).show();

                    }
                });

    }

    void urgentForgotPass(ForgotPasswordUserRequest forgotPasswordUserRequest) {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().urgentForgotPassword(forgotPasswordUserRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onNext(GenericResponse response) {
                        if (response.getCode() == 1) {
                            fragment.dismissProgressDialog();
                            Toast.makeText(fragment.getContext(), response.getInfo(), Toast.LENGTH_SHORT).show();
                        } else {
                            fragment.dismissProgressDialog();
                            Toast.makeText(fragment.getContext(), response.getInfo(), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }




    ForgotPasswordUserRequest constructForgotPasswordRequest(String email) {
        ForgotPasswordUserRequest request = new ForgotPasswordUserRequest();
        request.setEmail(email);
        return request;
    }



}
