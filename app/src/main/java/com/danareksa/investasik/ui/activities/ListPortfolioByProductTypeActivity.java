package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.fragments.portfolio.ListPortfolioByProductTypeFragment;

import butterknife.Bind;
import butterknife.BindString;

/**
 * Created by asep.surahman on 08/10/2018.
 */

public class ListPortfolioByProductTypeActivity extends BaseActivity {


    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;
    @BindString(R.string.portofolioTitle)
    String portofolioTitle;
    Integer productTypeId;


    public static void startActivity(BaseActivity sourceActivity, Integer productTypeId) {
        Intent intent = new Intent(sourceActivity, ListPortfolioByProductTypeActivity.class);
        intent.putExtra("product_type", productTypeId);
        sourceActivity.startActivity(intent);
    }


    @Override
    protected int getLayout() {
        return R.layout.a_signup;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar();
        productTypeId = getIntent().getIntExtra("product_type", 0);
        ListPortfolioByProductTypeFragment.showFragment(this, productTypeId);
    }

    public void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        title.setText(portofolioTitle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }

}
