package com.danareksa.investasik.ui.fragments.signIn;

import android.widget.Toast;
import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.ui.activities.RePasswordActivity;
import com.danareksa.investasik.ui.activities.SignInActivity;
import com.danareksa.investasik.ui.fragments.registernasabah.RegisterNasabahbaruFragment;
import com.danareksa.investasik.data.api.requests.ForgotPasswordUserRequest;
import com.danareksa.investasik.data.api.responses.GenericResponse;
import com.danareksa.investasik.ui.activities.BaseActivity;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by pandu.abbiyuarsyah on 13/07/2017.
 */

public class ForgotPasswordTempPresenter {

    ForgotPasswordTempFragment fragment;

    public ForgotPasswordTempPresenter(ForgotPasswordTempFragment fragment) {
        this.fragment = fragment;
    }

    ForgotPasswordUserRequest constructForgotPasswordRequest() {
        ForgotPasswordUserRequest request = new ForgotPasswordUserRequest();
        String email = fragment.etEmail.getText().toString();
        request.setEmail(email);
        return request;
    }

    void urgentForgotPass(ForgotPasswordUserRequest forgotPasswordUserRequest) {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().urgentForgotPassword(forgotPasswordUserRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e.getLocalizedMessage());
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(GenericResponse response) {
                        if (response.getCode() == 0 || response.getInfo().contains("Code for reset your passsword has been sent")) {
                            fragment.dismissProgressDialog();
                            Toast.makeText(fragment.getContext(), response.getInfo(), Toast.LENGTH_SHORT).show();

                            if(BuildConfig.FLAVOR.contentEquals("pnm")){
                                RePasswordActivity.startActivity((BaseActivity) fragment.getActivity());
                            }else{
                                RePasswordFragment.showFragment((BaseActivity) fragment.getActivity(), fragment.etEmail.getText().toString());
                            }
                        } else {
                            fragment.dismissProgressDialog();
                            Toast.makeText(fragment.getContext(), response.getInfo(), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }

}