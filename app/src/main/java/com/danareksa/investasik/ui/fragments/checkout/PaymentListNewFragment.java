package com.danareksa.investasik.ui.fragments.checkout;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.BcaKlikPayData;
import com.danareksa.investasik.data.api.beans.CartList;
import com.danareksa.investasik.data.api.beans.DetailBcaKlikpay;
import com.danareksa.investasik.data.api.beans.PaymentDetail;
import com.danareksa.investasik.data.api.responses.MandiriClickpayOrderNumberResponse;
import com.danareksa.investasik.data.api.responses.PaymentMethodResponse;
import com.danareksa.investasik.data.api.responses.TrxTransCodeResponse;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.CartActivity;
import com.danareksa.investasik.ui.activities.CheckoutActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.AmountFormatter;
import com.danareksa.investasik.util.Constant;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.BindString;
import butterknife.OnClick;
import icepick.State;


/**
 * Created by asep.surahman on 21/06/2018.
 */

public class PaymentListNewFragment extends BaseFragment{

    public static final String TAG = PaymentListNewFragment.class.getSimpleName();
    private final static String CART_LIST = "cartList";

    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.llReminder)
    ScrollView llReminder;
    @Bind(R.id.llNoReminder)
    LinearLayout llNoReminder;
    @Bind(R.id.rbMandiriClickpay)
    RadioButton rbMandiriClickpay;
    @Bind(R.id.rbBcaKlikpay)
    RadioButton rbBcaKlikpay;
    @Bind(R.id.rbVaPermata)
    RadioButton rbVaPermata;
    @Bind(R.id.rbTransferBca)
    RadioButton rbTransferBca;
    @Bind(R.id.rbTransferMandiri)
    RadioButton rbTransferMandiri;
    @Bind(R.id.txvTotal)
    TextView txvTotal;
    @Bind(R.id.llMandiriClickpay)
    LinearLayout llMandiriClickpay;
    @Bind(R.id.llBcaKlikpay)
    LinearLayout llBcaKlikpay;
    @Bind(R.id.llTransferVaPermata)
    LinearLayout llTransferVaPermata;
    @Bind(R.id.llTransferBca)
    LinearLayout llTransferBca;
    @Bind(R.id.llTransferMandiri)
    LinearLayout llTransferMandiri;
    @Bind(R.id.rlWording)
    RelativeLayout rlWording;
    @Bind(R.id.tvWording1)
    TextView tvWording1;
    @Bind(R.id.tvWording2)
    TextView tvWording2;

    @BindString(R.string.payment_bca_klikpay_wording1)
    String bcaWording1;

    @BindString(R.string.payment_bca_klikpay_wording2)
    String bcaWording2;

    @BindString(R.string.payment_mandiri_clickpay_wording1)
    String mandiriWording1;

    @BindString(R.string.payment_mandiri_clickpay_wording2)
    String mandiriWording2;

    public String type = "";
    public String trxNumber = "";
    public double adminFee = 0.0;

    @State
    public CartList cartList;

    private PaymentListNewPresenter presenter;

    public PaymentMethodResponse paymentMethodResponse;
    public TrxTransCodeResponse trxTransCodeResponse;
    public MandiriClickpayOrderNumberResponse orderNumberResponse;

    boolean mandiriClickPayStatus = false;
    boolean bcaKlikpayStatus = false;
    boolean vaPermataStatus = false;
    boolean transferBcaStatus = false;
    boolean transferMandiriStatus = false;
    String transactionNo = "";

    public static void showFragment(BaseActivity sourceActivity, CartList cartList) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, R.anim.slide_out_left, android.R.anim.slide_in_left, R.anim.slide_out_left);

            PaymentListNewFragment fragment = new PaymentListNewFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(CART_LIST, cartList);
            fragment.setArguments(bundle);

            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }


    @Override
    protected int getLayout(){
        return R.layout.f_payment_method_danareksa;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        presenter = new PaymentListNewPresenter(this);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.getPaymentList();
        cartList = (CartList) getArguments().getSerializable(CART_LIST);
        txvTotal.setText(getTotal());
        hideWording();

        rbMandiriClickpay.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                setActiveRadioMethod(true, false, false, false, false);
                showWording();
                wordingMandiri();
            }
        });

        rbBcaKlikpay.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                setActiveRadioMethod(false, true, false, false, false);
                showWording();
                wordingBca();
            }
        });

        rbVaPermata.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                setActiveRadioMethod(false, false, true, false, false);
                hideWording();
            }
        });

        rbTransferBca.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                setActiveRadioMethod(false, false, false, true, false);
                hideWording();
            }
        });

        rbTransferMandiri.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setActiveRadioMethod(false, false, false, false, true);
                hideWording();
            }
        });
    }


    public String getTotal(){
        Double total = 0d;
        for (int i = 0; i < cartList.getCartList().size(); i++) {
            Double t = Double.parseDouble(cartList.getCartList().get(i).getTotal());
            total += t;
        }
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("");
        ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);
        //return "IDR " + nf.format(total);
        return AmountFormatter.formatCurrencyWithoutComma((total));
    }



    void payment(){
        int searchListLength = paymentMethodResponse.getData().size();
        for (int i = 0; i < searchListLength; i++){
            if(paymentMethodResponse.getData().get(i).getCode().contains(Constant.CODE_PAYMENT_TYPE_MANDIRI_CLICKPAY)){
                if(paymentMethodResponse.getData().get(i).getAvailable() == true){
                    mandiriClickPayStatus = true;
                }else{
                    mandiriClickPayStatus = false;
                }
            }else if(paymentMethodResponse.getData().get(i).getCode().contains(Constant.CODE_PAYMENT_TYPE_BCA_KLIKPAY)){
                if(paymentMethodResponse.getData().get(i).getAvailable() == true){
                    bcaKlikpayStatus = true;
                }else{
                    bcaKlikpayStatus = false;
                }
            }else if (paymentMethodResponse.getData().get(i).getCode().contains(Constant.CODE_PAYMENT_TYPE_VA_PERMATA)){
                if(paymentMethodResponse.getData().get(i).getAvailable() == true){
                    vaPermataStatus = true;
                }else{
                    vaPermataStatus = false;
                }
            }else if (paymentMethodResponse.getData().get(i).getCode().contains(Constant.CODE_PAYMENT_TYPE_TRANS_MANDIRI)){
                if(paymentMethodResponse.getData().get(i).getAvailable() == true){
                    transferMandiriStatus = true;
                }else{
                    transferMandiriStatus = false;
                }
            }else if (paymentMethodResponse.getData().get(i).getCode().contains(Constant.CODE_PAYMENT_TYPE_TRANS_BCA)){
                if(paymentMethodResponse.getData().get(i).getAvailable() == true){
                    transferBcaStatus = true;
                }else{
                    transferBcaStatus = false;
                }
            }

        }
    }


    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError(){
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.tvTryAgain)
    void retryConnection() {
        presenter.getPaymentList();
    }



    @OnClick(R.id.bProceed)
    void onClickProceed(){

//        List<String> invalidProducts = new ArrayList<>();
//
//        for (CartListResponse res: this.cartList.getCartList()) {
//            if (res.getPackages().getPackageCode().equals("DRPTIS")){
//                invalidProducts.add(res.getPackages().getPackageName());
//            }else if (res.getPackages().getPackageCode().equals("RDGI2")){
//                invalidProducts.add(res.getPackages().getPackageName());
//            }
//        }

        if(rbMandiriClickpay.isChecked()){

            if(mandiriClickPayStatus == true){
//                if (invalidProducts.size() > 0) {
//                    showDialog("Danareksa Pendapatan Tetap Indonesia Sehat dan Danareksa Gebyar Indonesia II tidak dapat ditransaksikan dengan Mandiri Klikpay");
//                } else {
                    type = Constant.CODE_PAYMENT_TYPE_MANDIRI_CLICKPAY;
                    presenter.getOrderMandiriClickpay(type);
                //}
            }else if(mandiriClickPayStatus == false){
                showDialog("Mandiri Clickpay tidak bisa untuk transaksi lebih dari 1 produk");
            }

        }else if(rbBcaKlikpay.isChecked()){

            if(bcaKlikpayStatus == true){
                type = Constant.CODE_PAYMENT_TYPE_BCA_KLIKPAY;
                presenter.getBcaKlikpay(type, getTrxAmountObj());
            }else if(bcaKlikpayStatus == false){
                showDialog("BCA KlikPay tidak bisa untuk transaksi lebih dari 1 produk");
            }

        }else if(rbVaPermata.isChecked() && vaPermataStatus == true){
//            if (invalidProducts.size() > 0) {
//                showDialog("Danareksa Pendapatan Tetap Indonesia Sehat dan Danareksa Gebyar Indonesia II tidak dapat ditransaksikan dengan VA Permata");
//            } else {
                type = Constant.CODE_PAYMENT_TYPE_VA_PERMATA;
                presenter.paymentVaPermata(presenter.getPaymentTransRequest(type, getTrxAmountList()), Constant.PAYMENT_TYPE_VA_PERMATA);
            //}
        }else if(rbTransferBca.isChecked() == transferBcaStatus == true){
            type = Constant.CODE_PAYMENT_TYPE_TRANS_BCA;
            presenter.paymentTransferUniqeCode(presenter.getPaymentTransRequest(type, getTrxAmountList()), Constant.PAYMENT_TYPE_TRANS_BCA);
        }else if(rbTransferMandiri.isChecked() && transferMandiriStatus == true){
            type = Constant.CODE_PAYMENT_TYPE_TRANS_MANDIRI;
            presenter.paymentTransferUniqeCode(presenter.getPaymentTransRequest(type, getTrxAmountList()), Constant.PAYMENT_TYPE_TRANS_MANDIRI);
        }
    }


    public List<PaymentDetail> getTrxAmountList(){
        List<PaymentDetail> trxAmountList = new ArrayList<>();
        if(cartList.getCartList() != null && cartList.getCartList().size() != 0){
            for (int i = 0; i < cartList.getCartList().size(); i++) {
                PaymentDetail paymentDetail = new PaymentDetail();
                paymentDetail.setId(cartList.getCartList().get(i).getTransaction().getId());
                paymentDetail.setNetAmount(Math.ceil(Double.parseDouble(cartList.getCartList().get(i).getTransactionAmount())));
                System.out.println("net amount : " + String.valueOf(cartList.getCartList().get(i).getTransaction().getNetAmount()));
                System.out.println("net amount : " + String.valueOf(cartList.getCartList().get(i).getTransactionAmount()));
                trxAmountList.add(paymentDetail);
            }
        }
        return trxAmountList;
    }


    public DetailBcaKlikpay getTrxAmountObj(){
        DetailBcaKlikpay trxAmountList = new DetailBcaKlikpay();
        if(cartList.getCartList() != null && cartList.getCartList().size() != 0){
            for (int i = 0; i < cartList.getCartList().size(); i++) {
                trxAmountList = new DetailBcaKlikpay();
                trxAmountList.setId(String.valueOf(cartList.getCartList().get(i).getTransaction().getId()));
                trxAmountList.setNetAmount(Double.parseDouble(cartList.getCartList().get(i).getTransactionAmount()));
            }
        }
        return trxAmountList;
    }




    public void fetchResultToFragmentTransferKodeUnik(String paymentType){
        ((CheckoutActivity) getActivity()).setStep(3);
        TransferSummaryFragmentNew.showFragment((BaseActivity) getActivity(), trxTransCodeResponse, cartList, paymentType);
        CartActivity.fa.finish();
    }



    public void fetchResultToFragmentVaPermata(String paymentType){
        ((CheckoutActivity) getActivity()).setStep(3);
        VAPermataSummaryFragement.showFragment((BaseActivity) getActivity(), trxTransCodeResponse, cartList, paymentType);
        CartActivity.fa.finish();
    }


    public void fetchResultMandiriClickpay(String paymentType){
        MandiriClickpayFragment.showFragment((BaseActivity) getActivity(), cartList, paymentType, trxNumber, adminFee);
    }


    public void fetchResultBcaKlikpay(String urlRequest, String referrer,  BcaKlikPayData bcaKlikPayData, String paymentMethod, String transactionNo){
        BcaKlikpayFragment.showFragment((BaseActivity) getActivity(), cartList, urlRequest, referrer, bcaKlikPayData, paymentMethod, transactionNo);
    }


    private void setActiveRadioMethod(boolean mandiriClick, boolean bcaklik, boolean vapermata, boolean transbca, boolean transman){
        rbMandiriClickpay.setChecked(mandiriClick);
        rbBcaKlikpay.setChecked(bcaklik);
        rbVaPermata.setChecked(vapermata);
        rbTransferBca.setChecked(transbca);
        rbTransferMandiri.setChecked(transman);
    }



    void showDialogFailed(String info){
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.failed).toUpperCase())
                .titleColor(Color.BLACK)
                .content(info)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        getActivity().finish();
                    }
                })
                .show();
    }


    private void hideWording(){
        rlWording.setVisibility(View.GONE);
    }

    private void showWording(){
        rlWording.setVisibility(View.VISIBLE);
    }

    private void wordingMandiri(){
        tvWording1.setText(mandiriWording1);
        tvWording2.setText(mandiriWording2);
    }

    private void wordingBca(){
        tvWording1.setText(bcaWording1);
        tvWording2.setText(bcaWording2);
    }


    void showDialog(String info){
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.infortmation).toUpperCase())
                .titleColor(Color.BLACK)
                .content(info)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }




}
