package com.danareksa.investasik.ui.fragments.articleslideshow;

import com.danareksa.investasik.data.api.beans.News;
import com.danareksa.investasik.data.api.requests.DetailNewsRequest;
import com.danareksa.investasik.data.api.requests.PromoDetailRequest;
import com.danareksa.investasik.data.api.responses.NewsFeedResponse;
import com.danareksa.investasik.data.api.responses.PromoDetailResponse;
import com.danareksa.investasik.data.api.responses.SlideshowResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ArticleSlideshowPresenter {

    private ArticleSlideshowFragment fragment;

    public ArticleSlideshowPresenter(ArticleSlideshowFragment fragment){
        this.fragment = fragment;
    }

    void getSlideshowPageList(){
//        fragment.showProgressBar();
        fragment.getApi().getSlideshow()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<SlideshowResponse>(){
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
//                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(SlideshowResponse sliderResponse){

                        if (sliderResponse != null) {
                            fragment.listSlideshow = sliderResponse.getData();
                            fragment.getListSlider();
//                            fragment.dismissProgressBar();
                            fragment.swipe_container.setRefreshing(false);
                        } else {
//                            fragment.dismissProgressBar();
                            fragment.swipe_container.setRefreshing(false);
                        }

                    }

                });
    }

    void getNewsDetail(String id){
//        fragment.showProgressBar();

        DetailNewsRequest request = new DetailNewsRequest();
        request.setId(id);
        request.setToken("userNotLogin");

        fragment.getApi().getDetailNews("userNotLogin", request )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<NewsFeedResponse>(){
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
//                        fragment.connectionError();
//                        fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(NewsFeedResponse response){
//                        fragment.dismissProgressBar();
                        if (response.getData().get(0) != null) {
                            News news = response.getData().get(0);
//                            fragment.showNewsDetail(news);
                        }else{
//                            fragment.showDialog("Halaman tidak tersedia");
                        }

                    }

                });
    }

    PromoDetailRequest constructPromoDetailRequest(String code) {
        PromoDetailRequest promoDetailRequest = new PromoDetailRequest();
        promoDetailRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        promoDetailRequest.setCode(code);
        return promoDetailRequest;
    }

    void getDetailPromo(String code, String imageKey) {
//        fragment.showProgressBar();
        fragment.getApi().getPromoDetail(constructPromoDetailRequest(code))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PromoDetailResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
//                        fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(PromoDetailResponse promoListResponse) {

                        fragment.showPromoDetail(
                                promoListResponse.getData().getCode(),
                                imageKey,
                                promoListResponse.getData().getTitle()
                        );

//                        fragment.dismissProgressBar();
                    }
                });
    }


}
