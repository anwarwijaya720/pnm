package com.danareksa.investasik.ui.fragments.checkout;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.CartList;
import com.danareksa.investasik.data.api.beans.TrxTransCode;
import com.danareksa.investasik.data.api.responses.TrxTransCodeResponse;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.MainActivity;
import com.danareksa.investasik.ui.adapters.rv.SummaryPaymentTransferAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.DateUtil;

import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import icepick.State;

/**
 * Created by asep.surahman on 22/06/2018.
 */

public class VAPermataSummaryFragement extends BaseFragment {

    public static final String TAG = VAPermataSummaryFragement.class.getSimpleName();
    private final static String TRANSACTION_TRANSFER = "transactionTransfer";
    private final static String CART_LIST = "cartList";
    private final static String PAYMENT_TYPE = "paymentType";

    @Bind(R.id.rv)
    RecyclerView rv;
    @Bind(R.id.bOk)
    Button bOk;
    @Bind(R.id.tvSettlementCutOff)
    TextView tvSettlementCutOff;

    @Bind(R.id.tvNABProcessDate)
    TextView tvNABProcessDate;

    @State
    public TrxTransCodeResponse response;
    @State
    public CartList cartList;
    List<TrxTransCode> trxTransCodeList;
    SummaryPaymentTransferAdapter adapter;
    String settlementCutOff = "";
    String priceDate = "";
    String paymentType = "";
    String years = ""; String month = ""; String date = ""; String time = "";

    public static void showFragment(BaseActivity sourceActivity, TrxTransCodeResponse response, CartList cartList, String paymentType) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, R.anim.slide_out_left, android.R.anim.slide_in_left, R.anim.slide_out_left);
            VAPermataSummaryFragement fragment = new VAPermataSummaryFragement();
            Bundle bundle = new Bundle();
            bundle.putSerializable(CART_LIST, cartList);
            bundle.putSerializable(TRANSACTION_TRANSFER, response);
            bundle.putString(PAYMENT_TYPE, paymentType);
            fragment.setArguments(bundle);
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }


    @Override
    protected int getLayout() {
        return R.layout.f_summary_va_permata;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        paymentType = getArguments().getString(PAYMENT_TYPE);
        cartList = (CartList) getArguments().getSerializable(CART_LIST);
        response = (TrxTransCodeResponse) getArguments().getSerializable(TRANSACTION_TRANSFER);
        trxTransCodeList = response.getData();
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        loadList();
    }



    private void loadList(){
        if(trxTransCodeList != null && trxTransCodeList.size() != 0){
            if(!trxTransCodeList.get(0).getSettlementCutOff().equals("")){
                settlementCutOff = trxTransCodeList.get(0).getSettlementCutOff();

                Date settleCutOf ;
                settleCutOf = DateUtil.format(settlementCutOff, "yyyy-MM-dd hh:mm:ss");
                DateTime dateTime = new DateTime(settleCutOf);
                date  = String.valueOf(dateTime.getDayOfMonth());
                month = String.valueOf(dateTime.getMonthOfYear()-1);
                years = String.valueOf(dateTime.getYear());

                String[] parts = settlementCutOff.split(" ");
                if(parts.length > 0){
                    time = parts[1];
                }

                tvSettlementCutOff.setText("• Batas waktu pembayaran adalah sebelum tanggal " + date + "-" +DateUtil.getMonthString(month)+"-"+years+ " " + time);

            }else{
                tvSettlementCutOff.setVisibility(View.GONE);
            }

            if(!trxTransCodeList.get(0).getPriceDate().equals("")){
                priceDate = trxTransCodeList.get(0).getPriceDate();
                Date pDate = DateUtil.format(priceDate, "yyyy-MM-dd hh:mm:ss");
                tvNABProcessDate.setText("• Transaksi Anda akan diproses menggunakan NAB tanggal " + DateUtil.format(pDate,"dd-MM-yyyy"));
            }else{
                tvNABProcessDate.setVisibility(View.GONE);
            }
        }

        adapter = new SummaryPaymentTransferAdapter(getActivity(), trxTransCodeList, paymentType);
        rv.setAdapter(adapter);
    }



    @OnClick(R.id.bOk)
    void bOk() {
        getActivity().finish();
        MainActivity.startActivity((BaseActivity) getActivity());
    }


}
